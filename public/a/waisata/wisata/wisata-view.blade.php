@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia" />
<meta name="keywords" content="Pandu Asia" />

<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


{{-- Slick --}}
<link rel="stylesheet" type="text/css" href="{{URL::asset('slick/slick/slick.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('slick/slick/slick-theme.css')}}">

<style type="text/css">
  .slider {
    width: 95%;
    margin: 30px auto;
    min-width: 220px;
    min-height: auto;
  }
  
  .slick-slide {
    margin: 0px 20px;
  }
  
  .slick-slide img {
    width: 100%;
    height: 320px;
  }
  
  .slick-prev:before,
  .slick-next:before {
    color: black;
  }
  
  .slick-slide {
    transition: all ease-in-out .3s;
  }
  
  
</style>


@endsection
@section('main-content')

<div class="container">
  <div class="row">
    <div class="cardPostView" style="padding:15px;">
      
      <div class="row">
        
        <div class="col-lg-3" style="float:right;">
          <img src="{{URL::asset('blog/images/foto2.jpg')}}" alt="" class="img-responsive" style="margin-left:35px;">
          <br>
          <small>Pemandu : </small><label for="">Ilham Nur Hakim</label>
          <br>
          <small>Bahasa : </small><label for="">Indonesia, English</label>
          <div class="text-center">
            <small>Rating</small>
            <div class="stars">
              <span class="glyphicon glyphicon-star"></span>
              <span class="glyphicon glyphicon-star"></span>
              <span class="glyphicon glyphicon-star"></span>
              <span class="glyphicon glyphicon-star"></span>
              <span class="glyphicon glyphicon-star-empty"></span>
            </div>
            <p class="rating-wisata"> Average 4.5 <small> / </small> 5 </p>
            <input type="number" placeholder="jumlah orang">
            <h2> Rp. 150.000 <small> / orang </small></h2>
            <button class="btn btn-book" style="margin-bottom:30px;"> Book Now! </button> 
          </div>
        </div>
        
        <div class="col-lg-8" >
          <h2 class="card-title-wisata">Trip Dewata</h2>
          <p><i class="glyphicon glyphicon-map-marker"></i>Sunset Road 88 A, Kuta (Next to Krisna Oleh Oleh)
            , Kuta,
            Badung, Bali, Indonesia, 80361</p>  
            <div class="main-image-wisata">
              <img src="{{URL::asset('blog/images/beach.jpg')}}" alt="" style="
              max-width: 100%;
              height: auto; 
              ">
            </div>
            <hr>
            <section class="regular slider">
              <div>
                <img src="{{URL::asset('blog/images/balisnork.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/bandung.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/sungaimusi.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/bandung.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/sungaimusi.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/balisnork.jpg')}}" alt="" style="
                height : 120px;
                ">
              </div>
              <div>
                <img src="{{URL::asset('blog/images/sungaimusi.jpg')}}" alt="" style="
                height : 120px;
                ">
              </section>
            </div>
            <div class="col-lg-12">
              <h3 class="text-center">Fasilitas</h3>
              <hr>
              <div class="col-md-4">
                <img src="{{URL::asset('blog/images/meal.png')}}" alt=""> Snack
              </div>
              <div class="col-md-4">
                <img src="{{URL::asset('blog/images/tickets.png')}}" alt=""> Tiket
              </div>
              <div class="col-md-4">
                <img src="{{URL::asset('blog/images/placeholder.png')}}" alt=""> Homestay
              </div>
            </div>
            
            <div class="col-lg-12">
              <br>
              <div class="card-hover-deksripsi text-center">
                <h3>Penjelasan singkat</h3>
                <p>Bali can fit into any kind of trip - long or short, beach or adventure. Sip on cocktails
                  from
                  the comfort of bean bags in Seminyak as you listen to live music and watch the sun set over
                  the
                  ocean. Kuta is THE place to party and surf, whilst Nusa Dua is ideal for some relaxation by
                  the
                  beach. Want something different? Head inland to beautiful Ubud and find your inner zen at a
                  yoga retreat, surrounded by rice terraces and coffee plantations.</p>
                </div>
                <p>
                  <h4>Pukul 07.00 wib</h4>
                  <p>Sarapan pagi lalu penjemputan untuk pergi ke pantai kuta </p>
                  <h4>Pukul 10.00</h4>
                  <p>
                    Berpindah ke tempat banana boat
                  </p>
                  <h4>Pukul 13.00</h4>
                  <p>
                    Makan siang
                  </p>
                  <h4>Pukul 13.30</h4>
                  <p>Menuju Kebun Binatang</p>
                  <h4>Pukul 16.00</h4>
                  <p>Menuju Pusat oleh-oleh</p>
                  <h3>Kunjungan Lokasi Trip
                  </h3>
                  <p>
                    - Pulau Dewata
                    - Kebun Binatang
                    - Pusat Oleh - Oleh
                  </p>
                  
                </div>
                
                <div class="col-lg-12 text-center">
                  <h2>Tempat Lainya</h2>
                  <hr>
                  <section class="center slider" >
                    <div>
                      <a href="#">
                        <img src="{{URL::asset('blog/images/b3.jpg')}}" alt="" >
                        <div class="card-wisata-detail text-center" >
                          <p >Gili Labak & Museum Keraton</p>
                          <p >Mulai dari Rp <small>430.000</small>/orang</p>
                        </div>
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b2.jpg')}}" alt="" >
                        <div class="card-wisata-detail text-center" >
                          <p >Gili Labak & Museum Keraton</p>
                          <p >Mulai dari Rp <small>430.000</small>/orang</p>
                        </div>
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b3.jpg')}}" alt="" > 
                        <div class="card-wisata-detail text-center" >
                          <p >Gili Labak & Museum Keraton</p>
                          <p >Mulai dari Rp <small>430.000</small>/orang</p>
                        </div>
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b2.jpg')}}" alt="">
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b3.jpg')}}" alt="">
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b2.jpg')}}" alt="">
                      </a>
                    </div>
                    <div>
                      <a href="">
                        <img src="{{URL::asset('blog/images/b3.jpg')}}" alt="">
                      </a>
                    </div>
                    <div>
                      <a href="">
                      <img src="{{URL::asset('blog/images/b2.jpg')}}" alt="">
                      </a>
                    </div>
                    <div>
                      <a href="">
                      <img src="{{URL::asset('blog/images/b3.jpg')}}" alt="">
                      </a>
                    </div>
                  </section>
                  <script type="text/javascript">
                    $(document).on('ready', function () {
                      $(".regular").slick({
                        dots: true,
                        infinite: true,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        responsive: [
                        {
                          breakpoint: 768,
                          settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3
                          }
                        },
                        {
                          breakpoint: 480,
                          settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                          }
                        }
                        ]
                      });
                      $(".center").slick({
                        dots: true,
                        infinite: true,
                        centerMode: true,
                        slidesToShow: 4,
                        slidesToScroll: 3,
                        responsive: [
                        {
                          breakpoint: 768,
                          settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3
                          }
                        },
                        {
                          breakpoint: 480,
                          settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                          }
                        }
                        ]
                      });
                    });
                    
                  </script>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-8">
                  <h2 class="page-header">Comments</h2>
                  <section class="comment-list">
                    <!-- First Comment -->
                    <article class="row">
                      <div class="col-md-2 col-sm-2 hidden-xs">
                        <figure class="thumbnail">
                          <img class="img-responsive" src="http://www.tangoflooring.ca/wp-content/uploads/2015/07/user-avatar-placeholder.png" />
                          <figcaption class="text-center">username</figcaption>
                        </figure>
                      </div>
                      <div class="col-md-10 col-sm-10">
                        <div class="panel panel-default arrow left">
                          <div class="panel-body">
                            <header class="text-left">
                              <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                              <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                                Dec 16, 2014</time>
                              </header>
                              <div class="comment-post">
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                  eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                  quis
                                  nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                  consequat.
                                </p>
                              </div>
                              <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                        <!-- Second Comment Reply -->
                        <article class="row">
                          <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                            <figure class="thumbnail">
                              <img class="img-responsive" src="http://www.tangoflooring.ca/wp-content/uploads/2015/07/user-avatar-placeholder.png" />
                              <figcaption class="text-center">username</figcaption>
                            </figure>
                          </div>
                          <div class="col-md-9 col-sm-9">
                            <div class="panel panel-default arrow left">
                              <div class="panel-heading right">Reply</div>
                              <div class="panel-body">
                                <header class="text-left">
                                  <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                                  <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                                    Dec 16, 2014</time>
                                  </header>
                                  <div class="comment-post">
                                    <p>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                      eiusmod tempor
                                      incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                      quis
                                      nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                      consequat.
                                    </p>
                                  </div>
                                  <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                    reply</a></p>
                                  </div>
                                </div>
                              </div>
                            </article>
                          </section>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                {{-- JS --}}
                <script type="text/javascript" src="{{URL::asset('slick/slick/slick.min.js')}}"></script>
                
                @endsection
                