<?php

namespace App\Http\Controllers;

use App\ApproveBlog;
use Illuminate\Http\Request;

class ApproveBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApproveBlog  $approveBlog
     * @return \Illuminate\Http\Response
     */
    public function show(ApproveBlog $approveBlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApproveBlog  $approveBlog
     * @return \Illuminate\Http\Response
     */
    public function edit(ApproveBlog $approveBlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApproveBlog  $approveBlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApproveBlog $approveBlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApproveBlog  $approveBlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApproveBlog $approveBlog)
    {
        //
    }
}
