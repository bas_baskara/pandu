<?php

namespace App\Http\Controllers;

use App\ApproveGuide;
use Illuminate\Http\Request;

class ApproveGuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApproveGuide  $approveGuide
     * @return \Illuminate\Http\Response
     */
    public function show(ApproveGuide $approveGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApproveGuide  $approveGuide
     * @return \Illuminate\Http\Response
     */
    public function edit(ApproveGuide $approveGuide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApproveGuide  $approveGuide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApproveGuide $approveGuide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApproveGuide  $approveGuide
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApproveGuide $approveGuide)
    {
        //
    }
}
