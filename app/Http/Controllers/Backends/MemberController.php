<?php

namespace App\Http\Controllers\Backends;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MemberController extends Controller
{
  /******************************************************************************
    HOMEPAGE
  *******************************************************************************/
  public function index()
  {
    return view('backends.community.login');
  }

  public function login()
  {
    return view('backends.community.login');
  }

  /******************************************************************************
    BLOG
  *******************************************************************************/
  
  public function blog(Request $request)
  {
    $categories = Category::all();
    
    $posts = Post::when($request->keyword, function ($query) use ($request) {
      $query->where('title', 'like', "%{$request->keyword}%")
      ->orWhere('subtitle', 'like', "%{$request->keyword}%")
      ->orWhere('description', 'like', "%{$request->keyword}%");
    })->orderBy('created_at','DESC')->paginate(13);
    
    $posts->appends($request->only('keyword'));

    return view('frontends.blog',compact('posts','categories'));
  }

  public function about()
  {
    return view('frontends.about');
  }

}
