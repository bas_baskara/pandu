<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Pelanggan\Users;
use App\MasterMember;
use App\Province;
use App\PrivateChat;
use App\Events\SendMessage;
use Illuminate\Http\Request;
use Auth;

class ChatController extends Controller
{
    public function indexCustomer()
    {
		$user = Auth::guard('pelanggan')->user();
    	$guides = MasterMember::all();
    	foreach ($guides as $guide) {
    		$asal[$guide->id] =  Province::find($guide->province);
    	}

    	return view('TEST.indexCustomer',['guides' => $guides, 'asal' => $asal, 'user' => $user]);


    }

    public function fetchMessages($id=0)
    {   $data = '';
        if($id > 0){
        	if(Auth::guard('pelanggan')->check()){
        		$data = PrivateChat::with('customer')->where('customer_id', Auth::guard('pelanggan')->user()->id)->get();
        	}
        }else{
            if(Auth::guard('member')->check()){
                $data = PrivateChat::with('guide')->where('member_id', Auth::guard('member')->user()->id)->get();
            }
        }
        return $data;
    }

    public function sendMessage(Request $request)
    {
    	
    		if(Auth::guard('pelanggan')->check()){
    		$sender = Auth::guard('pelanggan')->user();
    		$guide = MasterMember::find($request['member_id']);
	    	$message = $sender->userToGuide()->create(['message' => $request->input('message'), 'member_id' => $guide->id, 'sender' => $sender->first_name.' '.$sender->last_name, 'receiver' => $guide->first_name.' '.$guide->last_name]);
	    	broadcast(new SendMessage($sender, $guide, $message))->toOthers();
    	}elseif(Auth::guard('member')->check()){
    		$sender = Auth::guard('member')->user();
            $customer = Users::find($request['customer_id']);
	    	$message = $sender->guideToMember()->create(['message' => $request->input('message'), 'customer_id' => $customer->id, 'sender' => $sender->first_name.' '.$sender->last_name, 'receiver' => $customer->first_name.' '.$customer->last_name]);
	    	broadcast(new SendMessage($customer, $sender, $message))->toOthers();
    	}

    	return ['status' => 'Message sent !'];
    }

    public function start(Request $request)
    {
    	$chatData = PrivateChat::where('customer_id', $request['customer'])->get();
    	if(!$chatData->count()){
    		$insertData = new PrivateChat;
    		$insertData->customer_id = $request['customer'];
    		$insertData->member_id = $request['guide'];
    		$insertData->save();
    	}
        return ['status' => 'Chat has created'];

    }

    public function guide(Request $request)
    {
        $privChat = PrivateChat::where('customer_id', $request['customer'])->where('member_id', $request['guide'])->first()->id;
        return $privChat;
    }

    public function status(Request $request)
    {
        $guideChat = PrivateChat::where('member_id', Auth::guard('member')->user()->id)->where('customer_id', $request['custId'])->where('read_by_guide', '0')->get();
        foreach ($guideChat as $chat) {
            $chat->read_by_guide = '1';
            $chat->save();
        }
        return $guideChat->count().' updated';
    }

}
