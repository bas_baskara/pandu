<?php

namespace App\Http\Controllers;

use App\CustomerTeam;
use Illuminate\Http\Request;

class CustomerTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerTeam  $customerTeam
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerTeam $customerTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerTeam  $customerTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerTeam $customerTeam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerTeam  $customerTeam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerTeam $customerTeam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerTeam  $customerTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerTeam $customerTeam)
    {
        //
    }
}
