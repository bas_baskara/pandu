<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog\Usercareers;

class UsercareerController extends Controller
{
  public function index()
    {
      $usercareers = usercareers::all();
      return view('Admin.internship.listresume',compact('usercareers'));
    }
    public function create()
    {
        return view ('blog.formcareers');
    }
    public function store(Request $request)
    {
      $uploadedFile = $request->file('file');
      if (empty($uploadedFile)) {
        $filename = 'null';
      }else {
        $filename = $uploadedFile->getClientOriginalName();
        $path = $uploadedFile->move(public_path('/resume_internship'), $filename);
      }
      $usercareers = new usercareers;
      $usercareers->resume = $filename;
      $usercareers->name = $request->name;
      $usercareers->fullname = $request->fullname;
      $usercareers->email = $request->email;
      $usercareers->phone = $request->phone;
      $usercareers->graduated = $request->graduated;
      $usercareers->description = $request->description;
      $usercareers->save();

      return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
