<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog\Post;
use App\Blog\Category;
Use App\User;
use App\Blog\Careers;

class PageController extends Controller {

	public function index()
	{
		//$pages = Post::all(); //DB::table('posts')->where('pageataupost','page')->get();
		$pages = Post::all()->where('pageataupost','page');

		$users = User::all();
      	$categories = category::all();

		return view('blog.pages',compact('pages','users','categories'));
	}

	public function page($page)
  	{
  		$page = post::all()->where('slug', $page);

  		foreach ($page as $key => $value) {
  			$result = $value;
  		}

  		// var_dump($result->visitor); die();

     	$users = User::all();
     	$categories = category::all();
    
	    $count = $result->visitor + 1;
	    $previous = post::where('id', '<', $result->id)->orderBy('id')->first();
	    $next = post::where('id', '>', $result->id)->orderBy('id')->first();
	    if($count){
	        post::where('id',$result->id)->update(array('visitor'=> $count));
	    }
	    return view('blog.page_view',compact('result','categories','users'));
  	}
  
//   public function category(category $category)
//   {
//       $page = $category->posts();
//       return view('blog.category',compact('page'));
//   }

// public function careers(careers $careers)
//   {
//     $users = User::all();
//     return view('blog.post_careers',compact('careers','users'));
//   }
}