<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog\Post;
use App\Blog\Category;
use App\Blog\Careers;

class HomeController extends Controller
{
  public function index()
  {
    $posts = Post::where('pageataupost','post')->orderBy('created_at','desc')->limit(3)->get();

    //var_dump($posts); die();

    return view ('blog.home',compact('posts'));
  }
  
  public function blog(Request $request)
  {
    $categories = Category::all();
    $posts = Post::when($request->keyword, function ($query) use ($request) {
      $query->where('title', 'like', "%{$request->keyword}%");
    })->orderBy('created_at','DESC')->paginate(12);
    $posts->appends($request->only('keyword'));

    return view('blog.blog',compact('posts','categories'));
  }
  public function about()
  {
    return view('blog.about');
  }
  public function caraKerjaTourGuide(){
    return view('blog.carakerja.tourguide');
  }
  public function caraKerjaCariTourGuide(){
    return view('blog.carakerja.caritourguide');
  }
  public function caraKerjaKetuaKomunitas(){
    return view('blog.carakerja.ketuakomunitas');
  }
  public function caraKerjaMemberKomunitas(){
    return view('blog.carakerja.memberkomunitas');
  }
  public function syaratKetentuan(){
    return view('blog.kebijakan.syaratketentuan');
  }
  
  public function syaratPrivasi(){
    return view('blog.kebijakan.privasi');
  }
  public function career()
  {
    $careers = careers::all();
    return view('blog.careers',compact('careers'));
  }
  
  public function usercareer()
  {
    return view('blog.formcareers');
  }
}
