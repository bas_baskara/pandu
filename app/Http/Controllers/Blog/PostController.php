<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog\Post;
use App\Blog\Category;
Use App\User;
use App\Blog\Careers;

class PostController extends Controller
{
  public function index()
  {
     return view ('blog.home');
  }
  public function post(post $posts)
  {
      $users = User::all();
      $categories = category::all();
      $count = $posts->visitor + 1;
      $previous = Post::where('id', '<', $posts->id)->orderBy('id')->first();
      $next = Post::where('id', '>', $posts->id)->orderBy('id')->first();
      if($count){
          Post::where('id',$posts->id)->update(array('visitor'=> $count));
      }
      return view('blog.post_view',compact('posts','categories','users','previous','next'));
  }
  
  public function category(category $category)
  {
      $posts = $category->posts();
      return view('blog.category',compact('posts'));
  }
  
  public function careers(careers $careers)
  {
    $users = User::all();
    return view('blog.post_careers',compact('careers','users'));
  }
}
