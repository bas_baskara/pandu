<?php

namespace App\Http\Controllers;

use App\Post_Tag;
use Illuminate\Http\Request;

class PostTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post_Tag  $post_Tag
     * @return \Illuminate\Http\Response
     */
    public function show(Post_Tag $post_Tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post_Tag  $post_Tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Post_Tag $post_Tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post_Tag  $post_Tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post_Tag $post_Tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post_Tag  $post_Tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post_Tag $post_Tag)
    {
        //
    }
}
