<?php

namespace App\Http\Controllers\Master_System;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r = new General;
        $roles = $r->roles();
        $users = User::all();

        return view('master_system.user.show', compact('users','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $r = new General;
        $roles = $r->roles();

        return view('master_system.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadedFile = $request->file('file');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('/assets/profile_images'), $filename);
        }
        User::create([
            'name' => $request['name'],
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'phone' => $request['phone'],
            'address' => $request['address'],
            'status' => "1",
            'level' => $request['level'],
            'avatar' => $filename,
        ]);

        return redirect(url('rahasiadapur/user/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $r = new General;
        $roles = $r->roles();

        $user = User::where('id',$id)->first();
        // $roles = array(
        //     '1' => 'Super Administrator', 
        //     '2' => 'Author', 
        //     '3' => 'Editor', 
        //     '4' => 'Contributor', 
        //     '5' => 'Customer', 
        //     '6' => 'Member', 
        //     '7' => 'Guide', 
        //     '8' => 'Community Leader', 
        //     '9' => 'Subscriber',
        //     '10' => 'Admin',
        //     '11' => 'Customer Service');
        
        return view('master_system.user.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uploadedFile = $request->file('file');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('/assets/profile_images'), $filename);
        }
        $user = User::find($id);
        $user->name = $request->name;
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->password = isset($request->password) ? bcrypt($request->password) : $user->password;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->status = "1";
        $user->level = $request->level;
        $user->avatar = $filename;
        $user->save();

        return redirect(url('rahasiadapur/user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();

        return redirect()->back();
    }
}
