<?php

namespace App\Http\Controllers\Master_System;

use App\Setting;
use App\Post;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;


class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('pageataupost','=','post')->count();
        $setting = Setting::where('id',$id)->first();
        return view('master_system.system.settings',compact('setting','post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uploadedFile = $request->file('file');
            if (empty($uploadedFile)) {
                  $filename = 'null';
             } 
            else {
                $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('../www/assets/images'), $filename);
                
            }
      
        
        $setting = Setting::find($id);
        
        $setting->facebook = $request->facebook;
        $setting->instagram = $request->instagram;
        $setting->twitter = $request->twitter;
        $setting->instagram = $request->instagram;
        $setting->googleplus = $request->googleplus;
        $setting->youtube = $request->youtube;
        
        $setting->analytics = $request->analytics;
        $setting->webmaster = $request->webmaster;
        $setting->facebook_pixel = $request->facebook_pixel;
        
        $setting->copyright = $request->copyright;
        
        $setting->metakeyword = $request->metakeyword;
        $setting->metadescription = $request->metadescription;
        $setting->metatitle = $request->metatitle;
        
        $setting->meta_title_blog = $request->meta_title_blog;
        $setting->meta_desc_blog = $request->meta_desc_blog;
        $setting->meta_keyword_blog = $request->meta_keyword_blog;
        
        $setting->meta_title_about = $request->meta_title_about;
        $setting->meta_desc_about = $request->meta_desc_about;
        $setting->meta_keyword_about = $request->meta_keyword_about;

        $setting->save();
        $request->session()->flash('success', 'Sukses mengubah setting');
        return redirect()->back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
