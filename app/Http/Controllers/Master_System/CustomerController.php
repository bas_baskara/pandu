<?php

namespace App\Http\Controllers\Master_System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use Auth;

class CustomerController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:user');
    }


    public function index()
    {
        $customers = Customer::all();

        return view('master_system.customer.show',compact('customers')); 
    }


    
    public function edit($id)
    {
        $customers = Customer::where('id',$id)->first();
        
        return view('master_system.customer.edit',compact('customers'));
    }


    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        $customer->id = $request->id;
        $customer->created_at = $request->created_at;
        $customer->updated_at = $request->updated_at;
        $customer->save();

        return redirect(url('rahasiadapur/customer/index'));
    }
    public function destroy($id)
    {
        Customer::where('id',$id)->delete();

        return redirect()->back();
    }
}
