<?php

namespace App\Http\Controllers\Master_System;

use App\MasterMember;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MasterMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MasterMember::all();

        return view('master_system.member.show',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_system.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $member = new MasterMember;
      $member->first_name  = $request->firstname;
      $member->last_name = $request->last_name;
      $member->username = $request->username;
      $member->email = $request->email;
      $member->password = $request->password;
      $member->mobilephone = $request->mobilephone;
      $member->phone = $request->phone;
      $member->address = $request->address;
      $member->city = $request->city;
      $member->province = $request->province;
      $member->country = $request->country;
      $member->postalcode = $request->postalcode;
      $uploadedProfile = $request->file('profile');
      if (empty($uploadedProfile)) {
        $filename_profile = 'null';
      }else {
        $filename_profile = $uploadedProfile->getClientOriginalName();
        $uploadedProfile->move(public_path('/image_post'), $filename_profile);
      }
      $member->profile_img = $filename_profile;
      $member->description = $request->description;
      $member->biography = $request->biography;
      $member->license_id = $request->license_id;
      $uploadedLicense = $request->file('license_id_img');
      if (empty($uploadedLicense)) {
        $filename_license = 'null';
      }else {
        $filename_license = $uploadedLicense->getClientOriginalName();
        $uploadedLicense->move(public_path('/image_post'), $filename_license);
      }
      $member->license_id_img = $filename_license;
      $member->c_license = $request->c_license;
      $uploadedcLicense = $request->file('c_license_img');
      if (empty($uploadedcLicense)) {
        $filename_clicense = 'null';
      }else {
        $filename_clicense = $uploadedcLicense->getClientOriginalName();
        $uploadedcLicense->move(public_path('/image_post'), $filename_clicense);
      }
      $member->c_license_img = $filename_clicense;
      $member->a_license = $request->a_license;
      $uploadedaLicense = $request->file('a_license_img');
      if (empty($uploadedaLicense)) {
        $filename_alicense = 'null';
      }else {
        $filename_alicense = $uploadedaLicense->getClientOriginalName();
        $uploadedaLicense->move(public_path('/image_post'), $filename_alicense);
      }
      $member->a_license_img = $filename_alicense;
      $member->passport = $request->passport;
      $uploadedPassport = $request->file('passport');
      if (empty($uploadedPassport)) {
        $filename_passport = 'null';
      }else {
        $filename_passport = $uploadedPassport->getClientOriginalName();
        $uploadedPassport->move(public_path('/image_post'), $filename_passport);
      }
      $member->passport_img = $filename_passport;
      $member->status = "0";
      $member->isleader = $request->isleader;
      $member->activation_code = "-";
      $member->rating = "0";
      $member->facebook = $request->fb;
      $member->twitter = $request->twt;
      $member->googleplus = $request->g;
      $member->instagram = $request->ig;
      $member->linkedin = $request->li;
      $member->youtube = $request->yt;
      $member->comment_id = "1";
      $member->withdraw_id = "1";
      
      $member->save();

      return redirect(url('rahasiadapur/members/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = MasterMember::where('id',$id)->first();
      return view('master_system.member.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $member = MasterMember::find($id);
      $member->first_name  = $request->firstname;
      $member->last_name = $request->last_name;
      $member->username = $request->username;
      $member->email = $request->email;
      $member->password = $request->password;
      $member->mobilephone = $request->mobilephone;
      $member->phone = $request->phone;
      $member->address = $request->address;
      $member->city = $request->city;
      $member->province = $request->province;
      $member->country = $request->country;
      $member->postalcode = $request->postalcode;
      $uploadedProfile = $request->file('profile');
      if (empty($uploadedProfile)) {
        $filename_profile = 'null';
      }else {
        $filename_profile = $uploadedProfile->getClientOriginalName();
        $uploadedProfile->move(public_path('/image_post'), $filename_profile);
      }
      $member->profile_img = $filename_profile;
      $member->description = $request->description;
      $member->biography = $request->biography;
      $member->license_id = $request->license_id;
      $uploadedLicense = $request->file('license_id_img');
      if (empty($uploadedLicense)) {
        $filename_license = 'null';
      }else {
        $filename_license = $uploadedLicense->getClientOriginalName();
        $uploadedLicense->move(public_path('/image_post'), $filename_license);
      }
      $member->license_id_img = $filename_license;
      $member->c_license = $request->c_license;
      $uploadedcLicense = $request->file('c_license_img');
      if (empty($uploadedcLicense)) {
        $filename_clicense = 'null';
      }else {
        $filename_clicense = $uploadedcLicense->getClientOriginalName();
        $uploadedcLicense->move(public_path('/image_post'), $filename_clicense);
      }
      $member->c_license_img = $filename_clicense;
      $member->a_license = $request->a_license;
      $uploadedaLicense = $request->file('a_license_img');
      if (empty($uploadedaLicense)) {
        $filename_alicense = 'null';
      }else {
        $filename_alicense = $uploadedaLicense->getClientOriginalName();
        $uploadedaLicense->move(public_path('/image_post'), $filename_alicense);
      }
      $member->a_license_img = $filename_alicense;
      $member->passport = $request->passport;
      $uploadedPassport = $request->file('passport');
      if (empty($uploadedPassport)) {
        $filename_passport = 'null';
      }else {
        $filename_passport = $uploadedPassport->getClientOriginalName();
        $uploadedPassport->move(public_path('/image_post'), $filename_passport);
      }
      $member->passport_img = $filename_passport;
      $member->status = "0";
      $member->activation_code = "-";
      $member->rating = "0";
      $member->facebook = $request->fb;
      $member->twitter = $request->twt;
      $member->googleplus = $request->g;
      $member->instagram = $request->ig;
      $member->linkedin = $request->li;
      $member->youtube = $request->yt;
      $member->comment_id = "1";
      $member->withdraw_id = "1";
      $member->save();
      return redirect(url('rahasiadapur/members/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      MasterMember::where('id',$id)->delete();
      return redirect()->back();
    }
}
