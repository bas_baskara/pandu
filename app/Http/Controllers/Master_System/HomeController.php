<?php

namespace App\Http\Controllers\Master_System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['allpost'] = Post::all()->count();
        $data['pendingpost'] = Post::where('status','=','pending')->count();
        $data['publishpost'] = Post::where('status','=','publish')->count();
        $data['userpost'] = Post::where('posted_by', Auth::user())->count();
        $data['userviewpost'] = Post::where('posted_by', Auth::user())->sum('visitor');
      
      return view('master_system.dashboard',$data);
    }

    public function masterorder()
    {
        return view('master_system.masterorders.allorders');
    }
}
