<?php

namespace App\Http\Controllers\Master_System;

use App\MasterTour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MasterTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $t = MasterTour::all();

        return view('master_system.tour.index',compact('t'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterTour  $masterTour
     * @return \Illuminate\Http\Response
     */
    public function show(MasterTour $masterTour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterTour  $masterTour
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterTour $masterTour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterTour  $masterTour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterTour $masterTour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterTour  $masterTour
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterTour $masterTour)
    {
        //
    }
}
