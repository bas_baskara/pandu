<?php

namespace App\Http\Controllers\Master_System;

use App\MasterBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;


class MasterBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $b = MasterBank::find($id);

        $s = new General;
        $status = $s->status(); //array('1' => 'Active', '0' => 'Inactive');

        // var_dump($b); die();

        return view('master_system.payments.banks',compact('b','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function show(MasterBank $masterBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterBank $masterBank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $b = MasterBank::find($id);

        $b->bca_title = $request->bca_title;
        $b->bca_rek = $request->bca_rek;
        $b->bca_owner = $request->bca_owner;
        $b->bca_status = $request->bca_status;

        $b->mandiri_title = $request->mandiri_title;
        $b->mandiri_rek = $request->mandiri_rek;
        $b->mandiri_owner = $request->mandiri_owner;
        $b->mandiri_status = $request->mandiri_status;

        $b->bri_title = $request->bri_title;
        $b->bri_rek = $request->bri_rek;
        $b->bri_owner = $request->bri_owner;
        $b->bri_status = $request->bri_status;

        $b->bni_title = $request->bni_title;
        $b->bni_rek = $request->bni_rek;
        $b->bni_owner = $request->bni_owner;
        $b->bni_status = $request->bni_status;

        $b->panin_title = $request->panin_title;
        $b->panin_rek = $request->panin_rek;
        $b->panin_owner = $request->panin_owner;
        $b->panin_status = $request->panin_status;

        $b->mega_title = $request->mega_title;
        $b->mega_rek = $request->mega_rek;
        $b->mega_owner = $request->mega_owner;
        $b->mega_status = $request->mega_status;

        $b->permata_title = $request->permata_title;
        $b->permata_rek = $request->permata_rek;
        $b->permata_owner = $request->permata_owner;
        $b->permata_status = $request->permata_status;

        $b->bii_title = $request->bii_title;
        $b->bii_rek = $request->bii_rek;
        $b->bii_owner = $request->bii_owner;
        $b->bii_status = $request->bii_status;

        $b->dbs_title = $request->dbs_title;
        $b->dbs_rek = $request->dbs_rek;
        $b->dbs_owner = $request->dbs_owner;
        $b->dbs_status = $request->dbs_status;

        $b->save();

        return redirect(url('rahasiadapur/payment/banks/1'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterBank $masterBank)
    {
        //
    }
}
