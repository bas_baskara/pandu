<?php

namespace App\Http\Controllers\Master_System;

use App\Bankaccounts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankaccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bankaccounts  $bankaccounts
     * @return \Illuminate\Http\Response
     */
    public function show(Bankaccounts $bankaccounts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bankaccounts  $bankaccounts
     * @return \Illuminate\Http\Response
     */
    public function edit(Bankaccounts $bankaccounts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bankaccounts  $bankaccounts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bankaccounts $bankaccounts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bankaccounts  $bankaccounts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bankaccounts $bankaccounts)
    {
        //
    }
}
