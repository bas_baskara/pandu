<?php

namespace App\Http\Controllers\Master_System;

use App\Career;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $career = Career::all();
        
        return view('master_system.internship.showjob',compact('career'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_system.internship.createjob');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadedFile = $request->file('file');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('/image_post'), $filename);
        }
        $careers = new Career;
        $careers->image = $filename;
        $careers->title = $request->title;
        $careers->description = $request->description;
        $careers->slug = str_slug($request->title, '-');
        $careers->status = "0";
        $careers->posted_by = Auth::user()->name ;
        $careers->save();
        return redirect(url('rahasiadapur/career/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function show(Career $career)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career = Career::where('id',$id)->first();
        
        return view('master_system.internship.editjob',compact('career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uploadedFile = $request->file('file');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('/image_post'), $filename);
        }
        $careers = Career::find($id);
        $careers->image = $filename;
        $careers->title = $request->title;
        $careers->description = $request->description;
        $careers->slug = \Str::slug($request->title, '-');
        $careers->status = "pending";
        $careers->posted_by = Auth::user()->name ;
        
        $careers->save();
        return redirect(url('rahasiadapur/careers/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Career::where('id',$id)->delete();
        return redirect()->back();
    }
}
