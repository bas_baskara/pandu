<?php

namespace App\Http\Controllers\Master_System;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;
use App\Post;
use App\Category;
use Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = DB::table('posts')->where('pageataupost','page')->get(); //Post::all()->where('pageataupost','page');

        return view('master_system.page.show', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $s = new General;

        $status = $s->status(); //array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.page.create',compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Post;

        $page->title = $request->title;
        
        $page->slug = str_slug($request->title);
        // $page->category = isset($request->categories) ? implode(",",$request->categories) : '';
        $page->description = $request->description;
        // $page->tag = 'NULL';
        $page->pageataupost = 'page';
        $page->status = $request->status;
        $page->posted_by = Auth::user()->name;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
          $imagename = 'null';
        }else {
          $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
          $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        $page->image = $imagename;
        $page->visitor = 0;
        $page->metatitle = $request->metatitle;
        $page->metakeyword = $request->metakeyword;
        $page->metadescription = $request->metadescription;
        $page->ismember = 0;
        $page->isapprove = 1;

        $page->save();

        return redirect('rahasiadapur/page'); //->with('success','Page has been saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = Post::find($id);

        $s = new General;

        $status = $s->status();
        //$categories = array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.page.edit', compact('p','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $p = Post::find($id);

        $p->title = $request->title;

        $p->slug = str_slug($request->title);
        // $p->category = isset($request->categories) ? implode(",",$request->categories) : $p->category;
        $p->description = $request->description;
        // $p->tag = 'NULL'; 
        $p->pageataupost = 'page';
        $p->status = $request->status;
        $p->posted_by = Auth::user()->name;
        
        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
          $imagename = $p->image;
        }else {
          $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
          $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        $p->image = $imagename;
        $p->visitor = 0;
        $p->metatitle = $request->metatitle;
        $p->metakeyword = $request->metakeyword;
        $p->metadescription = $request->metadescription;
        $p->ismember = 0;
        $p->isapprove = 1;
        
        $p->save();

         return redirect('rahasiadapur/page'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::where('id',$id)->delete();

        return back()->with('success','Page deleted sucessfully');
    }
}
