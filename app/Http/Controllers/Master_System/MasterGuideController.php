<?php

namespace App\Http\Controllers;

use App\MasterGuide;
use Illuminate\Http\Request;

class MasterGuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterGuide  $masterGuide
     * @return \Illuminate\Http\Response
     */
    public function show(MasterGuide $masterGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterGuide  $masterGuide
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterGuide $masterGuide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterGuide  $masterGuide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterGuide $masterGuide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterGuide  $masterGuide
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterGuide $masterGuide)
    {
        //
    }
}
