<?php

namespace App\Http\Controllers;

use App\TourMasterInEx;
use Illuminate\Http\Request;

class TourMasterInExController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourMasterInEx  $tourMasterInEx
     * @return \Illuminate\Http\Response
     */
    public function show(TourMasterInEx $tourMasterInEx)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourMasterInEx  $tourMasterInEx
     * @return \Illuminate\Http\Response
     */
    public function edit(TourMasterInEx $tourMasterInEx)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourMasterInEx  $tourMasterInEx
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourMasterInEx $tourMasterInEx)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourMasterInEx  $tourMasterInEx
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourMasterInEx $tourMasterInEx)
    {
        //
    }
}
