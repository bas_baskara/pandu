<?php

namespace App\Http\Controllers\Master_System;

use App\TourDestination;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TourDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TourDestination::all();      
        return view('master_system.page.destination.show',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_system.page.destination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $td = new TourDestination;
        $td->country_id = 0;
        $td->province_id = 0;
        $td->city_id = 0;
        $td->main_city = $request->mc;
        $td->main_description = $request->md;
        $td->tour_category = 0;
        $td->place = "null";
        $td->location = "null";
        $td->info = "null";
        $td->qr_code = "null";
        $uploadedFile = $request->file('mic');

        if (empty($uploadedFile)) {
            $imagename = 'null';
        }else {
            $imagename = str_slug($request->mc) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/destinations'), $imagename);
        }
        $td->main_image_tours = $imagename;
        $td->meta_title = $request->metatitle;
        $td->meta_description = $request->metadescription;
        $td->meta_keyword = $request->metakeyword;

        $td->save();
        
        return redirect('rahasiadapur/citydestination/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function show(TourDestination $tourDestination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = TourDestination::where('id',$id)->first();
        return view('master_system.page.destination.edit',compact('p'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $td = TourDestination::find($id);
        $td->country_id = 0;
        $td->province_id = 0;
        $td->city_id = 0;
        $td->main_city = $request->mc;
        $td->main_description = $request->md;
        $td->tour_category = 0;
        $td->place = "null";
        $td->location = "null";
        $td->info = "null";
        $td->qr_code = "null";
        $uploadedFile = $request->file('mic');

        if (empty($uploadedFile)) {
            $imagename = $td->main_image_tours;
        }else {
            $imagename = str_slug($request->mc) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/destinations'), $imagename);
        }
        $td->main_image_tours = $imagename;
        $td->meta_title = $request->metatitle;
        $td->meta_description = $request->metadescription;
        $td->meta_keyword = $request->metakeyword;
        
        $td->save();
        
        return redirect('rahasiadapur/citydestination/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TourDestination::where('id',$id)->delete();
        
        return redirect()->back();
    }
}
