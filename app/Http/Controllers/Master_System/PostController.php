<?php

namespace App\Http\Controllers\Master_System;

use App\Library\General;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Tag;
use App\MasterMember;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('pageataupost','post')
                ->where('isapprove',1)
                ->orderBy('created_at','desc')
                ->get();

        return view('master_system.post.show',compact('posts'));
    }

    public function postmember()
    {
        $posts = Post::where('pageataupost','post')
                ->where('ismember',1)
                ->orderBy('created_at','desc')
                ->get();
                
        $member = MasterMember::all();

        return view('master_system.post.memberpost',compact('posts'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $s = new General;
        $data['userpost'] = Post::where('posted_by', Auth::user())->count();
        $data['userviewpost'] = Post::where('posted_by', Auth::user())->sum('visitor');
        $status = $s->status(); //array('1' => 'Published', '0' => 'Draft');

        $categories = Category::all();
        $tags = Tag::all();

        return view('master_system.post.create',compact('categories','tags','status',$data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $uploadedFile = $request->file('file');
        // if (empty($uploadedFile)) {
        //   $filename = 'null';
        // }else {
        //   $filename = $uploadedFile->getClientOriginalName();
        //   $path = $uploadedFile->move(public_path('/image_post'), $filename);
        // }

        $post = new Post;

        $post->title = $request->title;
        $post->slug = str_slug($request->title);

        $get_cats = $request->categories;
        $get_tags = $request->tags;
        
        if(isset($get_cats)){
            foreach($get_cats as $k => $v){
                $cats_collection[] = $v;
            }
        }
        else
        {
            $cats_collection[] = "0";
        }

        // if(empty($cats_collection))
            

        if(isset($get_tags)){
            foreach($get_tags as $k => $v){
                $tags_collection[] = $v;
            }
        }
        else
        {
            $tags_collection[] = "0";
        }
        // if(empty($tags_collection))
            

        $post->category = implode(",",$cats_collection);

        $post->description = $request->description;
        $post->tag = implode(",",$tags_collection);

        // var_dump($post->category, $post->tag); die();

        $post->pageataupost = 'post';
        $post->status = $request->status;
        $post->posted_by = Auth::user()->name;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
        $imagename = 'null';
        }else {
        $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
        $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        $post->image = $imagename;
        $post->visitor = 0;
        $post->metatitle = $request->metatitle;
        $post->metakeyword = $request->metakeyword;
        $post->metadescription = $request->metadescription;
        $post->ismember = 0;
        $post->isapprove = 0;
        $post->save();

        return redirect(url('rahasiadapur/post/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = new General;

        $posts = Post::where('id',$id)->first(); //with('categories')->
        $categories = Category::all();
        $tags = Tag::all();
        $status = $s->status(); //array('1' => 'Published', '0' => 'Draft');
        
        return view('master_system.post.edit',compact('categories','tags','posts','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
    
        $post->title = $request->title;
        $post->slug = str_slug($request->title);

        $get_cats = $request->categories;
        $get_tags = $request->tags;
        
        if(isset($get_cats)){
            foreach($get_cats as $k => $v){
                $cats_collection[] = $v;
            }
        }
        else
        {
            $cats_collection[] = "0";
        }            

        if(isset($get_tags)){
            foreach($get_tags as $k => $v){
                $tags_collection[] = $v;
            }
        }
        else
        {
            $tags_collection[] = "0";
        }

        $post->category = implode(",",$cats_collection);

        $post->description = $request->description;
        $post->tag = implode(",",$tags_collection);
        $post->pageataupost = 'post';
        $post->status = $request->status;
        // $post->posted_by = Auth::user()->name;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
        $imagename = $post->image;
        }else {
        $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
        $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        // var_dump($uploadedFile); die();

        $post->image = $imagename;
        $post->visitor = $post->visitor;
        $post->metatitle = $request->metatitle;
        $post->metakeyword = $request->metakeyword;
        $post->metadescription = $request->metadescription;
        $post->ismember = 0;
        $post->isapprove = 1;
        
        $post->save();
        
        return redirect(url('rahasiadapur/post/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::where('id',$id)->delete();
        
        return redirect()->back();
    }
}
