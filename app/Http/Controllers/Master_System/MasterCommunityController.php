<?php

namespace App\Http\Controllers\Master_System;

use App\MasterCommunity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Library\General;
use App\Library\Common;
use App\Country;
use App\Province;
use App\Regency;
use App\CommunityProvinceCity;

class MasterCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $communities = MasterCommunity::orderBy('created_at','desc')->get();

        // $provinces = Province::all();
        // $regencies = Regency::all();

        $communities = DB::table('master_communities')
            ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('master_communities.*','provinces.name as provinsi','regencies.name as kota', 'countries.country_name as negara')
            ->orderBy('master_communities.created_at','desc')
            ->get();

        $complement = DB::table('community_province_cities')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('community_province_cities.city_id as idkota','community_province_cities.community_id as idkomunitas','provinces.name as namaprovinsi','regencies.name as namakota','countries.id as idnegara','countries.country_name as negara')
            ->get();

            // var_dump($communities); die();

        return view('master_system.community.show', compact('communities'));
    }

    public function getprovinces($country)
    {
        $data = Province::where('country_id',$country)->get();

        return response()->json($data);
    }

    public function getcities($province)
    {
        $data = Regency::where('province_id',$province)->get();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $s = new General;
        $status = $s->status();

        $countries = Country::all();
        $provinces = Province::all();
        $regencies = Regency::all();

        return view('master_system.community.create', compact('status','countries','provinces','regencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mc = new MasterCommunity;

        $lastid = $mc->latest()->first();

        $mc->community_name = $request->title;
        $mc->community_identity = str_slug($request->title);
        $mc->description = $request->description;
        
        $get_provs = $request->province;
        $get_city = $request->city;
        $get_country = $request->country;

        $upload_profile_image = $request->file('profile_image');
        $upload_banner_image = $request->file('banner_image');

        if(empty($upload_profile_image))
        {
            $profile_imagename = "null";
        }
        else
        {
            $profile_imagename = str_slug($request->title).'-profile.'.$upload_profile_image->getClientOriginalExtension();
            $upload_profile_image->move(public_path('../www/assets/communities'), $profile_imagename);
        }

        if(empty($upload_banner_image))
        {
            $banner_imagename = "null";
        }
        else
        {
            $banner_imagename = str_slug($request->title).'-banner.'.$upload_banner_image->getClientOriginalExtension();
            $upload_banner_image->move(public_path('../www/assets/communities'), $banner_imagename);
        }

        $mc->profile_image = $profile_imagename;
        $mc->banner_image = $banner_imagename;
        $mc->status = $request->status;
        $mc->member = 0;
        $mc->guide = 0;
        $mc->leader = 0;
        $mc->review = 0;
        $mc->metatitle = $request->metatitle;
        $mc->metakeyword = $request->metakeyword;
        $mc->metadescription = $request->metadescription;

        // var_dump($mc); die();

        $mc->save();

        $lastInsertId = $mc->id;

        foreach ($get_city as $kgc) {
            
            $jc[] = [
                'community_id' => $lastInsertId,
                'city_id' => $kgc,
                'province_id' => $get_provs,
                'country_id' => $get_country
            ];

        }
// var_dump($jc); die();
        CommunityProvinceCity::insert($jc);

        return redirect(url('rahasiadapur/community/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterCommunity  $masterCommunity
     * @return \Illuminate\Http\Response
     */
    public function show($identity)
    {
        // $community = MasterCommunity::where('community_identity',$identity)->first();

        $community = DB::table('master_communities')
            ->join('regencies','master_communities.city','=','regencies.id')
            ->join('provinces','master_communities.province','=','provinces.id')
            ->select('master_communities.*','provinces.name as provinsi','regencies.name as kota')
            ->orderBy('master_communities.created_at','desc')
            ->where('master_communities.community_identity',$identity)
            ->first();

        $complement = DB::table('community_province_cities')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('community_province_cities.city_id as idkota','community_province_cities.community_id as idkomunitas','provinces.name as namaprovinsi','regencies.name as namakota','countries.id as idnegara','countries.country_name as negara')
            ->get();

            // var_dump($complement); die();


        return view('master_system.community.detail', compact('community','complement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterCommunity  $masterCommunity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = new General;
        $status = $s->status();

        $community = DB::table('master_communities')
            ->join('regencies','master_communities.city','=','regencies.id')
            ->join('provinces','master_communities.province','=','provinces.id')
            ->select('master_communities.*','provinces.name as provinsi','regencies.name as kota')
            ->orderBy('master_communities.created_at','desc')
            ->where('master_communities.id',$id)
            ->first();

        $complement = DB::table('community_province_cities')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('community_province_cities.city_id as idkota','community_province_cities.community_id as idkomunitas','provinces.name as namaprovinsi','regencies.name as namakota','countries.id as idnegara','countries.country_name as negara')
            ->get();

        $countries = Country::all();
        $provinces = Province::all();
        $regencies = Regency::all();

        return view('master_system.community.edit',compact('community','status','complement','countries','provinces','regencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterCommunity  $masterCommunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mc = MasterCommunity::find($id);

        $mc->community_name = $request->title;
        $mc->community_identity = str_slug($request->title);
        $mc->description = $request->description;
        
        $get_country = $request->country;
        $get_provs = $request->province;
        $get_city = $request->city;

        $upload_profile_image = $request->file('profile_image');
        $upload_banner_image = $request->file('banner_image');

        if(empty($upload_profile_image))
        {
            $profile_imagename = $mc->profile_image;
        }
        else
        {
            $profile_imagename = str_slug($request->title) .'-profile.'.$upload_profile_image->getClientOriginalExtension();
            $upload_profile_image->move(public_path('../www/assets/communities'), $profile_imagename);
        }

        if(empty($upload_banner_image))
        {
            $banner_imagename = $mc->banner_image;
        }
        else
        {
            $banner_imagename = str_slug($request->title).'-banner.'.$upload_banner_image->getClientOriginalExtension();
            $upload_banner_image->move(public_path('../www/assets/communities'), $banner_imagename);
        }

        $mc->profile_image = $profile_imagename;
        $mc->banner_image = $banner_imagename;
        $mc->status = $request->status;
        $mc->member = ($mc->member != 0) ? $mc->member : 0;
        $mc->guide = ($mc->guide != 0) ? $mc->guide : 0;
        $mc->leader = ($mc->leader != 0) ? $mc->leader : 0;
        $mc->review = ($mc->review != 0) ? $mc->review : 0;
        $mc->metatitle = $request->metatitle;
        $mc->metakeyword = $request->metakeyword;
        $mc->metadescription = $request->metadescription;

        $mc->save();

        $lastInsertId = $mc->id;

        // foreach ($get_city as $kgc) {
            
        //     $jc[] = [
        //         'community_id' => $lastInsertId,
        //         'province_id' => $get_provs['0'],
        //         'city_id' => $kgc,
        //     ];

        // }

        // CommunityProvinceCity::insert($jc);

        foreach ($get_city as $kgc) {
            DB::table('community_province_cities')
                ->where('community_id',$lastInsertId)
                ->update([
                    // 'community_id' => $lastInsertId,
                    'city_id' => $kgc,
                    // 'province_id' => $get_provs,
                    // 'country_id' => $get_country
            ]);
        }

        // for ($i=0; $i < sizeof($get_city,1); $i++) {

        //     $data[] = [
        //         'city_id' => $get_city[$i], 
        //     ];


        //     DB::table('community_province_cities')
        //         ->where('community_id',$lastInsertId)
        //         ->update([
        //             // 'community_id' => $lastInsertId,
        //             'city_id' => $get_city[$i],
        //             // 'province_id' => $get_provs,
        //             // 'country_id' => $get_country
        //     ]);

        //     // var_dump($data);

        //     // DB::update("update community_province_cities set community_id={$lastInsertId}, city_id={$data[$i]}, province_id={$get_provs}, country_id={$get_country} where community_id={$lastInsertId}");
        // }

        // die();

        // $lastquery = DB::getQueryLog();

        return redirect(url('rahasiadapur/community/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterCommunity  $masterCommunity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterCommunity::where('id',$id)->delete();
        CommunityProvinceCity::where('community_id',$id)->delete();

        return redirect()->back();
    }
}
