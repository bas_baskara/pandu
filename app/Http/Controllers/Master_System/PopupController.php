<?php

namespace App\Http\Controllers\Master_System;

use App\Popup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Popup::all();

        return view('master_system.popup.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_system.popup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Popup;
        
        $data->title = $request->title;
        $data->description = $request->description;
        $uploadedFile = $request->file('image');

        if (empty($uploadedFile)) {
            $imagename = 'null';
        }else {
            $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }
        
        $data->image = $imagename;
        $data->link = $request->link;
        $data->mode = 1;
        $data->status = 1;
        
        $data->save();

        return redirect('rahasiadapur/popup/index')->with(['success' => 'Modal berhasil di simpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function show(Popup $popup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Popup::where('id',$id)->first();

        return view('master_system.popup.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Popup::find($id);
        
        $data->title = $request->title;
        $data->description = $request->description;
        $uploadedFile = $request->file('image');

        if (empty($uploadedFile)) {
            $imagename = 'null';
        }else {
            $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/blog_images'), $imagename);
        }
        
        $data->image = $imagename;
        $data->link = $request->link;
        $data->mode = 1;
        $data->status = 1;
        
        $data->save();

        return redirect('rahasiadapur/popup/index')->with(['success' => 'Modal berhasil di update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Popup $popup)
    {
        //
    }
}
