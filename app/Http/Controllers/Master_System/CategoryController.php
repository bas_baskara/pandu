<?php

namespace App\Http\Controllers\Master_System;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('master_system.category.show',compact('categories')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        
        $category->title = $request->title;
        $category->slug = str_slug($request->title);
        $category->description = $request->description;
        $category->status = $request->status;
        $category->metatitle = $request->metatitle;
        $category->metakeyword = $request->metakeyword;
        $category->metadescription = $request->metadescription;

        // var_dump($category); die();

        $category->save();

        return redirect(url('rahasiadapur/category/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        $categories = array('1' => 'Active', '0' => 'Inactive');
 
        return view('master_system.category.edit',compact('category','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->title = $request->title;
        $category->slug = str_slug($request->title);
        $category->description = $request->description;
        $category->status = $request->status;
        $category->metatitle = $request->metatitle;
        $category->metakeyword = $request->metakeyword;
        $category->metadescription = $request->metadescription;
        
        $category->save();

        return redirect(url('rahasiadapur/category/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id',$id)->delete();

        return redirect()->back();
    }
}
