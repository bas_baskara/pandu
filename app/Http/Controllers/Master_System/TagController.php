<?php

namespace App\Http\Controllers\Master_System;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();

        return view('master_system.tag.show',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.tag.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = new Tag;
        
        $tag->title = $request->title;
        $tag->slug = str_slug($request->title);
        $tag->description = $request->description;
        $tag->status = $request->status;
        $tag->metatitle = $request->metatitle;
        $tag->metakeyword = $request->metakeyword;
        $tag->metadescription = $request->metadescription;

        // var_dump($tag); die();

        $tag->save();

        return redirect(url('rahasiadapur/tag/index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::where('id',$id)->first();
        $tags = array('1' => 'Active', '0' => 'Inactive');
 
        return view('master_system.tag.edit',compact('tag','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);

        $tag->title = $request->title;
        $tag->slug = str_slug($request->title);
        $tag->description = $request->description;
        $tag->status = $request->status;
        $tag->metatitle = $request->metatitle;
        $tag->metakeyword = $request->metakeyword;
        $tag->metadescription = $request->metadescription;
        
        $tag->save();

        return redirect(url('rahasiadapur/tag/index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::where('id',$id)->delete();

        return redirect()->back();
    }
}
