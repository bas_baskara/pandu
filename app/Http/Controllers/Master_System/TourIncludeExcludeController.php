<?php

namespace App\Http\Controllers\Master_System;

use Illuminate\Support\Facades\DB;
use App\Library\General;
use App\TourIncludeExclude;
use App\TourCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TourIncludeExcludeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //tourcategory
        $tie = TourIncludeExclude::all();

        // DB::table('tour_categories')
        //       ->rightjoin('tour_include_excludes', 'tour_categories.id','=','tour_include_excludes.tour_category_id')
        //       ->select('tour_include_excludes.*','tour_categories.title as judultourcategory','tour_categories.id as idtourcategory')
        //       ->get();

        $c = TourCategory::all();

        foreach ($tie as $ktie => $vtie) {
            $tc = $vtie->tour_category_id;
        }

        return view('master_system.tour.complement',compact('tie','c','tc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $s = new General;
        $status = $s->status();

        $inex = $s->includeexclude();

        //tourcategory
        $tc = TourCategory::all();

        return view('master_system.tour.createcomplement',compact('status','inex','tc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Include Exclude
        $ie = new TourIncludeExclude;

        $ie->title = $request->title;
        $ie->slug = str_slug($request->title);
        $ie->description = $request->description;
        $ie->tipe = $request->includeexclude;
        $ie->price = $request->price;
        $ie->status = $request->status;
        $ie->tour_category_id = implode(',', $request->tourcategory);
        $ie->ismember = 0;
        $ie->issystem = Auth::user()->id;

        $ie->save();

        return redirect(url('rahasiadapur/tour/complement'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function show(TourIncludeExclude $tourIncludeExclude)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = new General;
        $status = $s->status();

        $inex = $s->includeexclude();

        //tourincludeexclude
        $tie = TourIncludeExclude::find($id);

        $tc = explode(',', $tie->tour_category_id);

        //tourcategory
        $c = TourCategory::all();

        return view('master_system.tour.editcomplement',compact('status','inex','tie','tc','c'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Include Exclude
        $ie = TourIncludeExclude::find($id);

        $ie->title = $request->title;
        $ie->slug = str_slug($request->title);
        $ie->description = $request->description;
        $ie->tipe = $request->includeexclude;
        $ie->price = $request->price;
        $ie->status = $request->status;
        $ie->tour_category_id = implode(',',$request->tourcategory);
        $ie->ismember = 0;
        $ie->issystem = Auth::user()->id;

        $ie->save();

        return redirect(url('rahasiadapur/tour/complement'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TourIncludeExclude::where('id',$id)->delete();

        return redirect()->back();
    }
}
