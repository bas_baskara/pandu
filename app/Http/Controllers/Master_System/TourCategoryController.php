<?php

namespace App\Http\Controllers\Master_System;

use App\TourCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TourCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tourcategory = TourCategory::all();

        return view('master_system.tourcategory.show',compact('tourcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.tourcategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tc = new TourCategory;

        $tc->title = $request->title;
        $tc->slug = str_slug($request->title);
        $tc->description = $request->description;

        $uploadFile = $request->file('featured_image');

        if(empty($uploadFile)){
            $imagename = 'null';
        }else{
            $imagename = str_slug($request->title) . '.' . $uploadFile->getClientOriginalExtension();
            $uploadFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        $tc->featured_image = $imagename;
        $tc->status = $request->status;
        $tc->metatitle = $request->metatitle;
        $tc->metakeyword = $request->metakeyword;
        $tc->metadescription = $request->metadescription;
        
        $tc->save();

        return redirect(url('rahasiadapur/tourcategory'))->with(['success' => 'Category Tour had been added']);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourCategory  $tourCategory
     * @return \Illuminate\Http\Response
     */
    public function show(TourCategory $tourCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourCategory  $tourCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tc = TourCategory::where('id',$id)->first();
        $categories = array('1' => 'Active', '0' => 'Inactive');

        return view('master_system.tourcategory.edit',compact('tc','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourCategory  $tourCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tc = TourCategory::find($id);

        $tc->title = $request->title;
        $tc->slug = str_slug($request->title);
        $tc->description = $request->description;

        $uploadFile = $request->file('featured_image');

        if(empty($uploadFile)){
            $imagename = $tc->featured_image;
        }else{
            $imagename = str_slug($request->title) . '.' . $uploadFile->getClientOriginalExtension();
            $uploadFile->move(public_path('../www/assets/blog_images'), $imagename);
        }

        $tc->featured_image = $imagename;
        $tc->status = $request->status;
        $tc->metatitle = $request->metatitle;
        $tc->metakeyword = $request->metakeyword;
        $tc->metadescription = $request->metadescription;

        // var_dump($tc); die();

        $tc->save();

        return back()->with(['success' => 'Category Tour had been updated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourCategory  $tourCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TourCategory::where('id',$id)->delete();

        return back()->with('success','Tour Category had been deleted');
    }
}
