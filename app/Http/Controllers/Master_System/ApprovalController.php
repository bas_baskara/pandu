<?php

namespace App\Http\Controllers\Master_System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;
Use App\Blog\Post;
use App\MasterMember;
use App\Komunitas\Userkomunitas;
use App\Komunitas\Grupkomunitas;
Use App\Komunitas\Postkomunitas;
Use App\MasterCommunity;
Use App\JoinCommunity;

class ApprovalController extends Controller
{
    public function index()
    {
      $posts = Post::all();

      return view('master_system.approval.show', compact('posts'));
    }
     public function member()
    {
      $posts = Post::all();
      
      return view('master_system.approval.show_member', compact('posts'));
    }
    public function community(){
        
        $jc = JoinCommunity::all();
       
        $mm = MasterMember::all();
        $mc = MasterCommunity::all();        
     
      
      
        return view('master_system.approval.show_joincommunity',compact('jc','mm','mc'));
    }
    
    public function joincommunity($id){
        $jcupdate = JoinCommunity::where('id',$id)->first();
        var_dump($jcupdate);
        if ($jcupdate->status == '0') {
        JoinCommunity::where('id',$id)->update(array('admin_approve' => '1'));
        
        return redirect()->back();
      }  
    }
    
    public function guide(){
        $member = MasterMember::all();
        return view('master_system.approval.show_guide',compact('member'));
    }
    
    public function tour(){
        return view('master_system.approval.show_tour');
    }
    
    public function approve($id)
    {
      $data = Post::where('id',$id)->first();

      if ($data->isapprove == '0') {
        Post::where('id',$id)->update(array('isapprove' => '1'));
      }

      return redirect()->back();
    }
    
    public function approve_blog_member($id){
         $s = new General;
         $status = $s->status_blog_member();
         $data = Post::where('id',$id)->first();

        if ($data->ismember == '0') {
        Post::where('id',$id)->update(array('ismember' => '1'));
      }
       return redirect()->back();
    }
    
    public function approve_guide($id)
    {
      $data = MasterMember::where('id',$id)->first();

      if ($data->isguide == '0') {
        MasterMember::where('id',$id)->update(array('isguide' => '1','ismember'=>'0'));
      }

      return redirect()->back();
    }
    
    public function approve_tour($id){
        
    }
    
    
    public function reject($id)
    {
      $data = Post::where('id',$id)->first();

      return redirect()->back();
    }
    public function list_komunitas()
    {
        $data = Userkomunitas::all();

        return view('Admin.appro.list_ketuakomunitas', compact('data'));
    }

    public function approve_ketua($id)
    {
      $data = Userkomunitas::where('id',$id)->first();

      if ($data->status_akun == 'pending') {
        Userkomunitas::where('id',$id)->update(array('status_akun' => 'Terverifikasi'));
      }

      return redirect()->back();
    }

    public function reject_ketua($id)
    {
      $data = Userkomunitas::where('id',$id)->first();

      return redirect()->back();
    }

    public function list_grup_komunitas()
    {
      $data = grupkomunitas::all();

      return view('Admin.appro.list_grupkomunitas',compact('data'));
    }

    public function approve_grup($id)
    {
      $data = grupkomunitas::where('id',$id)->first();

      if ($data->status == 'belum terverify') {
        grupkomunitas::where('id',$id)->update(array('status' => 'Terverifikasi'));
      }

      return redirect()->back();
    }

    public function reject_grup($id)
    {
      $data = grupkomunitas::where('id',$id)->first();

      return redirect()->back();
    }

    public function artikel_komunitas()
    {
      $data = postkomunitas::all();

      return view('Admin.appro.list_artikelkomunitas', compact('data'));
    }

    public function approve_artikelkomunitas($id)
    {
      $data = postkomunitas::where('id',$id)->first();

      if ($data->status == 0) {
        postkomunitas::where('id',$id)->update(array('status' => 1));
      }

      return redirect()->back();
    }

    public function reject_artikelkomunitas($id)
    {
      $data = postkomunitas::where('id',$id)->first();
      
      return redirect()->back();
    }
}