<?php

namespace App\Http\Controllers;

use App\TourTimeline;
use Illuminate\Http\Request;

class TourTimelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourTimeline  $tourTimeline
     * @return \Illuminate\Http\Response
     */
    public function show(TourTimeline $tourTimeline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourTimeline  $tourTimeline
     * @return \Illuminate\Http\Response
     */
    public function edit(TourTimeline $tourTimeline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourTimeline  $tourTimeline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourTimeline $tourTimeline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourTimeline  $tourTimeline
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourTimeline $tourTimeline)
    {
        //
    }
}
