<?php

namespace App\Http\Controllers;

use App\MasterReview;
use Illuminate\Http\Request;

class MasterReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterReview  $masterReview
     * @return \Illuminate\Http\Response
     */
    public function show(MasterReview $masterReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterReview  $masterReview
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterReview $masterReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterReview  $masterReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterReview $masterReview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterReview  $masterReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterReview $masterReview)
    {
        //
    }
}
