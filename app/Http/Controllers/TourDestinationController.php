<?php

namespace App\Http\Controllers;

use App\TourDestination;
use Illuminate\Http\Request;

class TourDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function show(TourDestination $tourDestination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function edit(TourDestination $tourDestination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourDestination $tourDestination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourDestination  $tourDestination
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourDestination $tourDestination)
    {
        //
    }
}
