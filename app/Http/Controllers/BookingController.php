<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Auth;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontends.form_booking');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bk = new Booking;

        $bk->customer_id = Auth::user()->id;
        $bk->tour_id = 0;
        $bk->status = $request->status;
        $bk->email = $request->email;
        $bk->ktp_or_license = $request->ktp_or_license;
        $bk->nama_lengkap = $request->nama_lengkap;
        $bk->no_telp = $request->no_telp;
        $bk->alamat = $request->alamat;
        $bk->nama_peserta = $request->nama_peserta;
        $bk->no_telp_peserta = $request->no_telp_peserta;
        $bk->usia_peserta = $request->usia_peserta;
        $bk->alamat_peserta = $request->alamat_peserta;

        $bk->save();

        return redirect('');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
