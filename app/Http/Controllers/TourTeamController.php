<?php

namespace App\Http\Controllers;

use App\TourTeam;
use Illuminate\Http\Request;

class TourTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourTeam  $tourTeam
     * @return \Illuminate\Http\Response
     */
    public function show(TourTeam $tourTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourTeam  $tourTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(TourTeam $tourTeam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourTeam  $tourTeam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourTeam $tourTeam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourTeam  $tourTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourTeam $tourTeam)
    {
        //
    }
}
