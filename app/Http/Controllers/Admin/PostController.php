<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog\Post;
use App\Blog\Category;
use Auth;

class PostController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index()
  {
    $posts = post::orderBy('created_at','desc')->get();
    return view('Admin.post.show',compact('posts'));
  }
  
  public function create()
  {
    $categories = category::all();
    return view('Admin.post.create',compact('categories'));
  }
  
  public function store(Request $request)
  {
    // $uploadedFile = $request->file('file');
    // if (empty($uploadedFile)) {
    //   $filename = 'null';
    // }else {
    //   $filename = $uploadedFile->getClientOriginalName();
    //   $path = $uploadedFile->move(public_path('/image_post'), $filename);
    // }

    $post = new post;

    $post->title = $request->title;
    $post->slug = str_slug($request->title);
    $post->category = isset($request->categories) ? implode(",",$request->categories) : '';
    $post->description = $request->description;
    $post->tag = 'NULL';
    $post->pageataupost = 'post';
    $post->status = 1;
    $post->posted_by = Auth::user()->name;

    $uploadedFile = $request->file('image_post');

    if (empty($uploadedFile)) {
      $imagename = 'null';
    }else {
      $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalName();
      $path = $uploadedFile->move(public_path('/assets/images'), $imagename);
    }


    $post->image = $imagename;
    $post->visitor = 0;
    $post->metatitle = $request->metatitle;
    $post->metakeyword = $request->metakeyword;
    $post->metadescription = $request->metadescription;
    $post->save();
    // $post->categories()->sync($request->categories);
    return redirect(route('post.index'));
  }
  
  public function show($id)
  {
    
  }
  
  public function edit($id)
  {
    $posts = post::with('categories')->where('id',$id)->first();
    $categories = category::all();
    return view('Admin.post.edit',compact('categories','posts'));
  }
  
  public function update(Request $request, $id)
  {  
    $post = post::find($id);
    
    $post->title = $request->title;
    $post->slug = str_slug($request->title);

    $post->category = isset($request->categories) ? implode(",",$request->categories) : $post->category;

    // var_dump($post->category); die();

    $post->description = $request->description;
    $post->tag = 'NULL';
    $post->pageataupost = 'post';
    $post->status = 1;
    $post->posted_by = Auth::user()->name;

    $uploadedFile = $request->file('image_post');

    if (empty($uploadedFile)) {
      $imagename = $post->image;
    }else {
      $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalName();
      $path = $uploadedFile->move(public_path('/assets/images'), $imagename);
    }

    $post->image = $imagename;
    $post->visitor = $post->visitor;
    $post->metatitle = $request->metatitle;
    $post->metakeyword = $request->metakeyword;
    $post->metadescription = $request->metadescription;
    // $post->categories()->sync($request->categories);
    $post->save();
    
    return redirect(route('post.index'));
  }
  
  public function destroy($id)
  {
    post::where('id',$id)->delete();
    return redirect()->back();
  }
  
}
