<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Blog\Post;
use App\Blog\Category;
use Auth;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$title = 'This is page';
        $pages = DB::table('posts')->where('pageataupost','page')->get(); //Post::all()->where('pageataupost','page');

        // var_dump($pages); die();

        return view('Admin.page.show', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = category::all();
        return view('Admin.page.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->all(); die();
        $page = new Post;

        $page->title = $request->title;
        
        $page->slug = str_slug($request->title);
        $page->category = isset($request->categories) ? implode(",",$request->categories) : '';
        $page->description = $request->description;
        $page->tag = 'NULL';
        $page->pageataupost = 'page';
        $page->status = 1;
        $page->posted_by = Auth::user()->name;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
          $imagename = 'null';
        }else {
          $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalName();
          $path = $uploadedFile->move(public_path('/assets/images'), $filename);
        }

        $page->image = $imagename;
        $page->visitor = 0;
        $page->metatitle = $request->metatitle;
        $page->metakeyword = $request->metakeyword;
        $page->metadescription = $request->metadescription;

        $page->save();

        return Redirect::to('rahasiadapur/page'); //->with('success','Page has been saved');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = Post::find($id);
        $categories = category::all();

        return view('Admin.page.edit', compact('p','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $p = post::find($id);

        $p->title = $request->title;

        $p->slug = str_slug($request->title);
        $p->category = isset($request->categories) ? implode(",",$request->categories) : $p->category;
        $p->description = $request->description;
        $p->tag = 'NULL'; //implode(",",$request->hastag);
        $p->pageataupost = 'page';
        $p->status = 1;
        $p->posted_by = Auth::user()->name;

        // $this->validate($request, [
        //     'image_post' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        // ]);
        
        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
          $imagename = $p->image;
        }else {
          $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalName();
          $path = $uploadedFile->move(public_path('/assets/images'), $filename);
        }

        $p->image = $imagename;
        $p->visitor = 0;
        $p->metatitle = $request->metatitle;
        $p->metakeyword = $request->metakeyword;
        $p->metadescription = $request->metadescription;

        $p->save();

        return back()->with('success','Page has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::where('id',$id)->delete();

        return back()->with('success','Page deleted sucessfully');
    }
}
