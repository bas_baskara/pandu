<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Blog\Post;
use App\Komunitas\Userkomunitas;
use App\Komunitas\Grupkomunitas;
Use App\Komunitas\Postkomunitas;

class ApprovalController extends Controller
{
    public function index()
    {
      $posts = post::all();
      return view('Admin.appro.show', compact('posts'));
    }
    
    public function approve($id)
    {
      $data = post::where('id',$id)->first();
      if ($data->status == 'pending') {
        post::where('id',$id)->update(array('status' => 'publish'));
      }
      return redirect()->back();
    }
    
    public function reject($id)
    {
      $data = post::where('id',$id)->first();
      return redirect()->back();
    }
    public function list_komunitas()
    {
        $data = Userkomunitas::all();
        return view('Admin.appro.list_ketuakomunitas', compact('data'));
    }

    public function approve_ketua($id)
    {
      $data = Userkomunitas::where('id',$id)->first();
      if ($data->status_akun == 'pending') {
        Userkomunitas::where('id',$id)->update(array('status_akun' => 'Terverifikasi'));
      }
      return redirect()->back();
    }

    public function reject_ketua($id)
    {
      $data = Userkomunitas::where('id',$id)->first();
      return redirect()->back();
    }
    public function list_grup_komunitas()
    {
      $data = grupkomunitas::all();
      return view('Admin.appro.list_grupkomunitas',compact('data'));
    }
    public function approve_grup($id)
    {
      $data = grupkomunitas::where('id',$id)->first();
      if ($data->status == 'belum terverify') {
        grupkomunitas::where('id',$id)->update(array('status' => 'Terverifikasi'));
      }
      return redirect()->back();
    }

    public function reject_grup($id)
    {
      $data = grupkomunitas::where('id',$id)->first();
      return redirect()->back();
    }

    public function artikel_komunitas()
    {
      $data = postkomunitas::all();
      return view('Admin.appro.list_artikelkomunitas', compact('data'));
    }
    public function approve_artikelkomunitas($id)
    {
      $data = postkomunitas::where('id',$id)->first();
      if ($data->status == 0) {
        postkomunitas::where('id',$id)->update(array('status' => 1));
      }
      return redirect()->back();
    }

    public function reject_artikelkomunitas($id)
    {
      $data = postkomunitas::where('id',$id)->first();
      return redirect()->back();
    }
}