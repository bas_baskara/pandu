<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Blog\Post;
Use App\User;
Use Auth;

class HomeController extends Controller
{
  public function __construct()
  {
    // parent::__construct();
    $this->middleware('auth');
  }

  public function index()
  {
      $data['allpost'] = post::all()->count();
      $data['pendingpost'] = post::where('status','=','pending')->count();
      $data['publishpost'] = post::where('status','=','publish')->count();
      $data['userpost'] = post::where('posted_by', Auth::user())->count();
      $data['userviewpost'] = post::where('posted_by', Auth::user())->sum('visitor');
      
      return view('Admin.dashboard',$data);
  }
}
