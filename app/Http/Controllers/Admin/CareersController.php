<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Blog\Careers;

class CareersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
      $career = careers::all();
      return view('Admin.internship.showjob',compact('career'));
    }

    public function create()
    {
        return view('Admin.internship.createjob');
    }

    public function store(Request $request)
    {
      $uploadedFile = $request->file('file');
      if (empty($uploadedFile)) {
        $filename = 'null';
      }else {
        $filename = $uploadedFile->getClientOriginalName();
        $path = $uploadedFile->move(public_path('/image_post'), $filename);
      }
      $careers = new careers;
      $careers->image = $filename;
      $careers->title = $request->title;
      $careers->description = $request->description;
      $careers->slug = str_slug($request->title, '-');
      $careers->status = "pending";
      $careers->posted_by = Auth::user()->name ;
      $careers->save();
      return redirect(route('careers.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
      $career = careers::where('id',$id)->first();
      return view('admin.internship.editjob',compact('career'));
    }

    public function update(Request $request, $id)
    {
      $uploadedFile = $request->file('file');
      if (empty($uploadedFile)) {
        $filename = 'null';
      }else {
        $filename = $uploadedFile->getClientOriginalName();
        $path = $uploadedFile->move(public_path('/image_post'), $filename);
      }
      $careers = careers::find($id);
      $careers->image = $filename;
      $careers->title = $request->title;
      $careers->description = $request->description;
      $careers->slug = \Str::slug($request->title, '-');
      $careers->status = "pending";
      $careers->posted_by = Auth::user()->name ;
     
      $careers->save();
      return redirect(route('careers.index'));
    }
    
    public function destroy($id)
    {
      careers::where('id',$id)->delete();
      return redirect()->back();
    }
}
