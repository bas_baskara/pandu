<?php

namespace App\Http\Controllers;

use App\MasterRating;
use Illuminate\Http\Request;

class MasterRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterRating  $masterRating
     * @return \Illuminate\Http\Response
     */
    public function show(MasterRating $masterRating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterRating  $masterRating
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterRating $masterRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterRating  $masterRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterRating $masterRating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterRating  $masterRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterRating $masterRating)
    {
        //
    }
}
