<?php

namespace App\Http\Controllers;

use App\CommunityProvinceCity;
use Illuminate\Http\Request;

class CommunityProvinceCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CommunityProvinceCity  $communityProvinceCity
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityProvinceCity $communityProvinceCity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CommunityProvinceCity  $communityProvinceCity
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityProvinceCity $communityProvinceCity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CommunityProvinceCity  $communityProvinceCity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityProvinceCity $communityProvinceCity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CommunityProvinceCity  $communityProvinceCity
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityProvinceCity $communityProvinceCity)
    {
        //
    }
}
