<?php

namespace App\Http\Controllers;

use App\MasterComment;
use Illuminate\Http\Request;

class MasterCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterComment  $masterComment
     * @return \Illuminate\Http\Response
     */
    public function show(MasterComment $masterComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterComment  $masterComment
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterComment $masterComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterComment  $masterComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterComment $masterComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterComment  $masterComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterComment $masterComment)
    {
        //
    }
}
