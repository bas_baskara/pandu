<?php

namespace App\Http\Controllers;

use App\TourComment;
use Illuminate\Http\Request;

class TourCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourComment  $tourComment
     * @return \Illuminate\Http\Response
     */
    public function show(TourComment $tourComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourComment  $tourComment
     * @return \Illuminate\Http\Response
     */
    public function edit(TourComment $tourComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourComment  $tourComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourComment $tourComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourComment  $tourComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourComment $tourComment)
    {
        //
    }
}
