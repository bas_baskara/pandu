<?php

namespace App\Http\Controllers\Master_System;

use App\MasterBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MasterBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $b = MasterBank::find($id);

        return view('master_system.payments.banks',compact('b'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function show(MasterBank $masterBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterBank $masterBank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterBank $masterBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterBank  $masterBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterBank $masterBank)
    {
        //
    }
}
