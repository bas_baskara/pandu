<?php

namespace App\Http\Controllers\Pelanggan\Auth;

use App\Pelanggan\Users;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
    use AuthenticatesUsers;
    
    protected $redirectTo = '/';
    
    public function __construct()
    {
        $this->middleware('guest:pelanggan')->except('signout');
    }
    public function showLoginForm()
    {
        return view('users.login');
    }
    
    protected function guard()
    {
        return Auth::guard('pelanggan');
    }
    
    public function username()
    {
        $identity  = request()->get('email');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    public function signout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('user/signin');
    }
    
}
