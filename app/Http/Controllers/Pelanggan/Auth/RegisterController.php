<?php

namespace App\Http\Controllers\Pelanggan\Auth;
use App\Notifications\UserRegisteredSuccessfully;
use App\Pelanggan\Users;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = 'user/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:pelanggan');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
           
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

        protected function register(Request $request)
        {
        $user_pelanggan = Users::create([
            'first_name' => $request['namadepan'],
            'last_name' => $request['namabelakang'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'mobilephone' => $request['phone'],
            'activation_code' => str_random(30).time(),
              ]);
            //   $user_pelanggan->notify(new UserRegisteredSuccessfully($user_pelanggan));
             $request->session()->put('notif','Registrasi Berhasil Silahkan Login');
            return redirect()->back();
        }
    
    public function showRegistrationForm()
    {
        return view('users.register');
    }

    // public function activateUser(string $activationCode)
    // {
    //     try {
    //         $user_pelanggan = app(Users::class)->where('activation_code', $activationCode)->first();
    //         if (!$user_pelanggan) {
    //             return "The code does not exist for any user in our system.";
    //         }
    //         $user_pelanggan->status          = 1;
    //         $user_pelanggan->activation_code = null;
    //         $user_pelanggan->save();
    //         auth()->login($user_pelanggan);
    //     } catch (\Exception $exception) {
    //         logger()->error($exception);
    //         return "Whoops! something went wrong.";
    //     }
    //     return redirect()->to('users/signin');
    // }

    protected function guard()
    {
        return Auth::guard('pelanggan');
    }
}
