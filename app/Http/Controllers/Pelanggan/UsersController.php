<?php

namespace App\Http\Controllers\Pelanggan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Pelanggan\Users;
use App\Post;
use App\Category;
use App\Tag;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pelanggan');
    }

    public function index(){
        $posts = Post::where('pageataupost','post')
            ->where('pageataupost','post')
            ->where('status',1)
            ->where('isapprove',1)
            ->orderBy('created_at','desc')
            ->limit(3)
            ->get();
    
        //postmember
        $pm = Post::where('pageataupost','post')
                ->where('pageataupost','post')
                ->where('status',2)
                ->where('isapprove',1)
                ->where('ismember',1)
                ->orderBy('created_at','desc')
                ->limit(3)
                ->get();

        $categories = Category::all();
    
    // var_dump($homepage); die();

    return view('users.home',compact('posts','pm','categories'));

    }
    
    public function edit($username)
    {
    	$usr = Users::where('username',$username)->first();
    	
        return view('users.profile',compact('usr'));
    }

    public function update(Request $request,$id)
    {
        
    	$usr = Users::find($id);
    	
    	$usr->first_name = $request->first_name;
    	$usr->last_name = $request->last_name;
    	$usr->email = $request->email;
    	$usr->password = empty($request->password) ? $usr->password : $request->password;
    	$usr->mobilephone = $request->mobilephone;
    	$usr->address = $request->address;
    	
        $uploadedProfile = $request->file('profile_img');
        if (empty($uploadedProfile)) {
            $filename_profile = $usr->profile_img;
        }else {
            $filename_profile = str_slug($uploadedProfile->getClientOriginalName()) . '-' . $uploadedProfile->getClientOriginalExtension();
            $uploadedProfile->move(public_path('../www/assets/communities/avatars'), $filename_profile);
        }
    	$usr->profile_img = $filename_profile;
    	$usr->description = $request->description;

        // var_dump($usr); die();

    	$usr->save();
        
        \Session::put('message','Profile berhasil diupdate');
        return back();
    }
}
