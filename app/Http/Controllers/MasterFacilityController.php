<?php

namespace App\Http\Controllers;

use App\MasterFacility;
use Illuminate\Http\Request;

class MasterFacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterFacility  $masterFacility
     * @return \Illuminate\Http\Response
     */
    public function show(MasterFacility $masterFacility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterFacility  $masterFacility
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterFacility $masterFacility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterFacility  $masterFacility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterFacility $masterFacility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterFacility  $masterFacility
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterFacility $masterFacility)
    {
        //
    }
}
