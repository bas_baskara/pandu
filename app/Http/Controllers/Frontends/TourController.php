<?php
namespace App\Http\Controllers\Frontends;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;
use App\Library\Common;
use Illuminate\Support\Facades\DB;
use App\TourDestination;
use App\MasterMember;
use App\MasterTour;
use App\MasterCommunity;
use App\Country;
use App\Province;
use App\Regency;
use App\Post;
use App\Category;
use App\CommunityProvinceCity;
use App\Bankaccounts;
use App\TourIncludeExclude;
use App\TourCategory;
use App\TourAndDestination;
use App\Booking;
use DateTime;
use Auth;

class TourController extends Controller
{

        // public function __construct()
        // {
        //     $this->middleware('auth:pelanggan');
        // }

    public function alltours()
    {
        $alltour = MasterTour::all();
        $city = Regency::all();

        return view('frontends.alltours', compact('alltour','city'));
    }

    public function tourincities()
    {
        $tourincities = DB::table('master_tours')->groupBy('city_id')->get();
        $city = Regency::all();

        // var_dump($tourincities); die();

        return view('frontends.tourincities', compact('tourincities','city'));
    }

    public function tourinprovinces()
    {
        $tourinprovinces = DB::table('master_tours')->groupBy('province_id')->get();
        $province = Province::all();

        return view('frontends.tourinprovinces', compact('tourinprovinces','province'));
    }

    public function tourincountries()
    {
        $tourincountries = DB::table('master_tours')->groupBy('country_id')->get();
        $country = Country::all();

        // var_dump($tourincountries); die();

        return view('frontends.tourincountries', compact('tourincountries','country'));
    }

    public function tourcategory($slug){
        
        // $result = TourDestination::where('main_city',$id)->first();

        $tour_by_category = DB::table('master_tours')
        					->join('tour_categories','master_tours.category','=','tour_categories.id')
        					->select('master_tours.*','tour_categories.title as tourcattitle','tour_categories.slug as tourcatslug')
        					->where('tour_categories.slug', $slug)
        					->orderBy('created_at','desc')
        					->get();
        $city = Regency::all();
        $province = Province::all();
        $country = Country::all();
        $g = MasterMember::all();
        
        $pt = new General;
        $durasi = $pt->durasitour();

        $category = Category::all();

        $filtercity = DB::table('regencies')
                        ->select('*')
                        ->limit(10)
                        ->get();
        // var_dump($result); die();
        
        return view('frontends.tourcategory', compact('tour_by_category','city','province','country','g','durasi','category','filtercity'));
    }

    public function citytour($slug)
    {
    	$tour_by_city = DB::table('master_tours')
                        ->join('regencies','master_tours.city_id','=','regencies.id')
                        ->select('master_tours.*','master_tours.slug as tourslug','regencies.*','regencies.slug as cityslug')
                        ->where('regencies.slug',$slug)
                        // ->groupBy('master_tours.city_id')
                        ->get();
        $city = Regency::all();
        $province = Province::all();
        $country = Country::all();
        $g = MasterMember::all();

        $pt = new General;
        $durasi = $pt->durasitour();

        $category = Category::all();

        $filtercity = DB::table('regencies')
                        ->select('*')
                        ->limit(10)
                        ->get();

    	return view('frontends.tourcity', compact('tour_by_city','city','province','country','g','durasi','category','filtercity'));
    }

    public function provincetour($slug)
    {
    	$tour_by_province = DB::table('master_tours')
                            ->join('provinces','master_tours.province_id','=','provinces.id')
                            ->select('master_tours.*','master_tours.slug as tourslug','provinces.*','provinces.slug as provinceslug')
                            ->where('provinces.slug',$slug)
                            // ->groupBy('master_tours.province_id')
                            ->get();
        $city = Regency::all();
        $province = Province::all();
        $country = Country::all();
        $g = MasterMember::all();

        $pt = new General;
        $durasi = $pt->durasitour();

        $category = Category::all();

        $filtercity = DB::table('regencies')
                        ->select('*')
                        ->limit(10)
                        ->get();

    	return view('frontends.tourprovince', compact('tour_by_province','city','province','country','g','durasi','category','filtercity'));
    }

    public function countrytour($slug)
    {
    	$tour_by_country = DB::table('master_tours')
                            ->join('countries','master_tours.country_id','=','countries.id')
                            ->select('master_tours.*','master_tours.slug as tourslug','countries.*','countries.slug as countryslug')
                            ->where('countries.slug',$slug)
                            // ->groupBy('master_tours.country_id')
                            ->get();

        $city = Regency::all();
        $province = Province::all();
        $country = Country::all();
        $g = MasterMember::all();

        $pt = new General;
        $durasi = $pt->durasitour();

        $category = Category::all();

        $filtercity = DB::table('regencies')
                        ->select('*')
                        ->limit(10)
                        ->get();

    	return view('frontends.tourcountry', compact('tour_by_country','city','province','country','g','durasi','category','filtercity'));
    }

    public function communitytour($id)
    {
    	$tour_by_community = MasterTour::where('community_id',$id)->get();

    	return view('frontends.tourcommunity', compact('tour_by_community'));
    }

    public function ratingtour($rate)
    {

    }

    public function tourdetail($slug)
    {
        $s = new General;

        $durasi = $s->durasitour();

        $tipe = $s->tipetrip();

    	$detailtour = MasterTour::where('slug',$slug)->first();
        $city = Regency::where('id',$detailtour->city_id)->first();
        $province = Province::where('id',$detailtour->province_id)->first();
        $country = Country::where('id',$detailtour->country_id)->first();
        $member = MasterMember::where('id',$detailtour->member_id)->first();
        $community = MasterCommunity::where('id',$detailtour->community_id)->first();
        $join = DB::table('master_members')
                            ->join('join_communities','master_members.id','=','join_communities.member_id')
                            ->join('master_communities','join_communities.community_id','=','master_communities.id')
                            ->select('master_communities.*')
                            ->where('master_members.id','=',$detailtour->member_id)->first();
            //var_dump($join); die();        
                            
        $relatedtour_by_member = MasterTour::where('member_id',$member['id'])->get();
        $relatedtour_by_community = MasterTour::where('community_id',$community['id'])->get();

        return view('frontends.tourdetail', compact('detailtour','city', 'province','country','member','community','relatedtour_by_member','relatedtour_by_community','durasi','tipe','join'));
    }

    public function bookingtour(Request $request, $slug)
    {
        if(empty($request->customer_id))
        {
            return redirect('user/signin');
        }
        else
        {

            $detailtour = MasterTour::where('slug',$slug)->first()->toJson();

            $datasesi = session([
                'jml' => $request->jumlahpeserta, 
                'tgl' => $request->tanggalberangkat,
                'pel' => $request->customer_id,
                'sta' => $request->bookingstatus,
                'slu' => $request->slug,
            ]);

            // $booking = Booking::where('tour_id',$detailtour->id)->where('customer_id',Auth::user()->id)->first();

            // $jmlpeserta = $request->session()->put('jml',$jumlahpeserta);
            // $tglberangkat = $request->session()->put('tgl', $tanggalberangkat);
            
            $dt = new DateTime();

            // $b = new Booking;
            // $booking = array(
            //     'customer_id' => $request->session()->put('customer_id'),
            //     'tour_id' => $detailtour->session()->put('id'),
            //     // $tour_team_id = NULL;
            //     'number_of_team' => $request->session()->put('jumlahpeserta'),
            //     'status' => $request->session()->put('bookingstatus'),
            //     // $booking_date = $dt->format('Y-m-d H:i:s');
            //     'departure_date' => $request->session()->put('tanggalberangkat'),
            //     'slug' => $response->session()->put('slug')
            // );

            // Session::set('databooking',$booking);

            // $a = $request->session()->all();

            // var_dump(Session::get($databooking));
            // die();
            // var_dump($request->all()); die();
            // $paid_date = NULL;
            // $complete_date = NULL;
            // $canceled_date = NULL;
            // $whoscanceled = NULL;

            // var_dump($request->all()); 
            // die();
            // \Session::put('datasesi',$datasesi);
            return view('frontends.booking', compact('detailtour')); //view('frontends.booking', compact('detailtour'));
        }
    }

    function filterbycategory($category)
    {
        $pt = new General;

        $mt = MasterTour::where('category',$category)->orderBy('created_at','desc')->get();
        $r = Regency::all();
        $p = Province::all();
        $co = Country::all();
        $m = MasterMember::all();

        foreach ($mt as $kmt => $vmt) {
            
            foreach ($r as $kr => $vr) {
                if($vmt->city_id == $vr->id)
                {
                    $city = $vr;
                }
            }

            foreach ($p as $kp => $vp) {
                if($vmt->province_id == $vp->id)
                {
                    $province = $vp;
                }
            }

            foreach ($co as $kco => $vco) {
                if($vmt->country_id == $vco->id)
                {
                    $country = $vco;
                }
            }

            foreach ($m as $km => $vm) {
                if($vmt->member_id == $vm->id)
                {
                    $member = $vm;
                }
            }

        }

        $data = array(
            'rc' => MasterTour::where('category',$category)->orderBy('created_at','desc')->get(),
            'city' => $city,
            'province' => $province,
            'country' => $country,
            'g' => $member,
            'durasi' => $pt->durasitour()
        );

        return response()->json($data);
    }
    
}