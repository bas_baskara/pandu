<?php

namespace App\Http\Controllers\Frontends;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\MasterTour;

class SitemapController extends Controller
{
    public function index(){
        $posts = Post::all()->first();
        $tours = MasterTours::all()->first();
        
        return response()->view('sitemap.index', [
          'post' => $posts,
          'tours' => $tours,
      ])->header('Content-Type', 'text/xml');
    }
    public function blog()
    {
        $posts = Post::latest()->get();
        return response()->view('sitemap.blogs', [
            'post' => $posts,
        ])->header('Content-Type', 'text/xml');
    }
    
    public function tours()
    {
        $tours = MasterTour::latest()->get();
        return response()->view('sitemap.tours', [
            'tours' => $tours,
        ])->header('Content-Type', 'text/xml');
    }
}
