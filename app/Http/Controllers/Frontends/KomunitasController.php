<?php

namespace App\Http\Controllers\Frontends;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Library\Common;
use App\MasterMember;

class KomunitasController extends Controller
{
  /******************************************************************************
    HOMEPAGE
  *******************************************************************************/
  public function index()
  {
    return view('frontends.community.login');
  }

  public function login()
  {
    return view('frontends.community.login');
  }

  /******************************************************************************
    BLOG
  *******************************************************************************/
  
  public function blog(Request $request)
  {
    $categories = Category::all();
    
    
      $common = new Common;
      

        //postmember
        $pm = Post::where('ismember','1')->orderBy('created_at','desc')->get();
    
    // $posts = Post::when($request->keyword, function ($query) use ($request) {
    //   $query->where('title', 'like', "%{$request->keyword}%")
    //   ->orWhere('subtitle', 'like', "%{$request->keyword}%")
    //   ->orWhere('description', 'like', "%{$request->keyword}%");
    // })->orderBy('created_at','DESC')->paginate(13);
    
    // $posts->appends($request->only('keyword'));

    // return view('frontends.community.blog',compact('posts','categories'));
    return view('frontends.community.blog',compact('pm'));
  }
  
  public function detailpost($pm)
  {
     $query = DB::table('posts')->where('slug',$pm)->get();//Post::where('slug',$posts);
    
     $com = DB::table('posts')
            ->join('master_members', 'posts.posted_by', '=', 'master_members.username')
            ->join('join_communities', 'master_members.id', '=', 'join_communities.member_id')
            ->join('master_communities','master_communities.id','=','join_communities.community_id')
            ->select('posts.*', 'master_members.username as kk', 'master_communities.community_name as nm')
            
            ->get();    
    
      foreach ($com as $key => $value) {
        $result = $value;
      }    
         
      $users = MasterMember::all();

      $count = $result->visitor + 1;
      if($count){
          Post::where('id',$result->id)->update(array('visitor'=> $count));
      }
      return view('frontends.community.post_view',compact('result','users','com'));
  }

  public function about()
  {
    return view('frontends.about');
  }

}
