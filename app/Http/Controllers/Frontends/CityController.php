<?php
namespace App\Http\Controllers\Frontends;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\General;
use App\Library\Common;
use Illuminate\Support\Facades\DB;
use App\TourDestination;
use App\MasterMember;
use App\MasterTour;
use App\MasterCommunity;
use App\Country;
use App\Province;
use App\Regency;
use App\Post;
use App\CommunityProvinceCity;
use App\Bankaccounts;
use App\TourIncludeExclude;
use App\TourCategory;
use App\TourAndDestination;



class CityController extends Controller
{
    public function wisata($id){
        
        $result = TourDestination::where('main_city',$id)->first();

        $tour_by_city = MasterTour::where('city_id',291)->get();

        // var_dump($tour_by_city); die();

        $tour_by_province = MasterTour::where('province_id')->get();
        // var_dump($result); die();
        
        return view('frontends.view_wisata', compact('result','tour_by_city','tour_by_province'));
    }
}