<?php

namespace App\Http\Controllers\Frontends;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Post;
use App\MasterTour;
use App\Country;
use App\Province;
use App\Regency;
use App\Category;
use App\Tag;
use App\Careers;
use App\User;
use App\Wisata;
use App\Popup;
use App\Member;
use App\Setting;
use App\TourDestination;
use App\MasterSetting;
use App\MasterRating;

class GeneralController extends Controller
{
  /******************************************************************************
    HOMEPAGE
  *******************************************************************************/
        
    public function index()
    {
      // $datasesi = \Session::get('book');

      // var_dump($datasesi); die();

      // if(!empty($datasesi))
      // {
        // return redirect('');

        $posts = Post::where('pageataupost','post')
        ->where('pageataupost','post')
        ->where('status',1)
        ->where('isapprove',1)
        ->orderBy('created_at','desc')
        ->limit(3)
        ->get();
        
      //postmember
        $pm = Post::where('pageataupost','post')
        ->where('pageataupost','post')
        ->where('status',2)
        ->where('isapprove',1)
        ->where('ismember',1)
        ->orderBy('created_at','desc')
        ->limit(3)
        ->get();
        
        
        $categories = Category::all();
        $member = Member::all();

        $tour = DB::table('master_tours')
                ->select('*')
                ->groupBy('province_id')
                ->orderBy('created_at','desc')
                ->get();
        $city = Regency::all();
        $province = Province::all();
        $country = Country::all();

    // var_dump($homepage); die();

      return view('frontends.home',compact('posts','categories','member','pm','tour','city','province','country'));
    // return View::composer('frontends.home', function($view){
    //   $view->with('posts','categories','member','homepage');
    // });
    }

  /******************************************************************************
    ajax
  *******************************************************************************/
    public function getcities($province)
    {
        $data = Regency::where('province_id',$province)->get();

        return response()->json($data);
    }

    public function getprovinces($country)
    {
        $data = Province::where('country_id',$country)->get();

        return response()->json($data);
    }

    

  /******************************************************************************
    BLOG
  *******************************************************************************/
    
    public function blog(Request $request)
    {
      $categories = Category::all();
      $tags = Tag::all();
      $result = Setting::where('id',1);
      
      // $posts = Post::when($request->keyword, function ($query) use ($request) {
      //   $query->where('title', 'like', "%{$request->keyword}%")
      //   ->where('status',1)
      //   ->orWhere('description', 'like', "%{$request->keyword}%");
      // })->orderBy('created_at','DESC')->paginate(13);
      
      // $posts->appends($request->only('keyword'));

      $posts = Post::where('status',1)
              ->where('isapprove',1)
              ->where('pageataupost','post')
              ->where('ismember',0)
              ->orderBy('created_at','DESC')->paginate(12);

      return view('frontends.blog',compact('posts','categories','tags','blog','result'));
    }
    
  //autosuggest for blog
    
    public function fetch(Request $request){
      if($request->get('query')){
        $query = $request->get('query');
        $data = Post::where('title','LIKE','%'.$query.'%')
        ->where('pageataupost','post')->limit(5)->get();
        
        $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
        foreach($data as $row){
          $output .= '<li><a href="#">'.$row->title.'</a></li>';
        }
        $output .= '</ul>';
        echo $output;
      }
    }

    public function about()
    {
      $result = Setting::where('id',1);

      return view('frontends.about',compact('result'));
    }

  /******************************************************************************
    POSTS
  *******************************************************************************/

    public function post()
    {
     return view ('blog.home');
   }

   public function detailpost($posts)
   {
     $query = DB::table('posts')->where('slug',$posts)->get();//Post::where('slug',$posts);
     
     foreach ($query as $key => $value) {
      $result = $value;
    }
    
    $users = User::all();
    $categories = Category::all();
    
    $tags = Tag::all();
      //$posts = Post::where('tag','=',$get_tag->id)->get();


    $count = $result->visitor + 1;
    $previous = Post::where('id', '<', $result->id)->orderBy('id')->first();
    $next = Post::where('id', '>', $result->id)->orderBy('id')->first();
    if($count){
      Post::where('id',$result->id)->update(array('visitor'=> $count));
    }
    return view('frontends.post_view',compact('result','categories','users','previous','next','tags'));
  }
  
  public function category($slug)
  {
    $get_cat = Category::where('slug',$slug)->first();

    $posts = Post::where('category','=',$get_cat->id)->get();

    $categories = Category::all();

    // foreach($get_post as $kgp => $vgp){
    //   $throw_post_cats[] = explode(",",$vgp->category);
    // }

    // if(isset($throw_post_cats))
    // {
    //   foreach($throw_post_cats as $ktpc => $vtpc)
    //   {
    //     $get_cat[] = Category::where('slug',$vtpc)->first();
    //   }
    // }   

      // foreach($posts as $post)
      // {
      //   var_dump($post->description);
      // }

      // die();

    return view('frontends.category',compact('posts','categories','get_cat'));
  }
  
  
  public function careers(careers $careers)
  {
    $users = User::all();
    return view('blog.post_careers',compact('careers','users'));
  }

  /******************************************************************************
    PAGES
  *******************************************************************************/
    
    public function page()
    {
    //$pages = Post::all(); //DB::table('posts')->where('pageataupost','page')->get();
      $pages = Post::all()->where('pageataupost','page')->where('status',1);

      $users = User::all();
      $categories = category::all();

      return view('frontends.pages',compact('pages','users','categories'));
    }

    public function detailpage($page)
    {
      $page = Post::all()->where('slug', $page);

      foreach ($page as $key => $value) {
        $result = $value;
      }

      $users = User::all();
      $categories = category::all();
      
      $count = $result->visitor + 1;
      $previous = Post::where('id', '<', $result->id)->orderBy('id')->first();
      $next = Post::where('id', '>', $result->id)->orderBy('id')->first();
      if($count){
        Post::where('id',$result->id)->update(array('visitor'=> $count));
      }
      return view('frontends.page_view',compact('result','categories','users'));
    }
    
  /******************************************************************************
    WISATA
  *******************************************************************************/
    
    public function wisata($w)
    {
      $result = Wisata::all()->where('kota',$w);

        // var_dump($result); die();
      return view('frontends.wisata', compact('result'));
    }

    public function view_rating_wisata(){
      return view('frontends.rating_wisata');
    }
    
    public function view_rating_member(){
      return view('frontends.rating_member');
    }
    
    public function rating_wisata(Request $request){
      $mr = new MasterRating;
      $mr->tour_id = 1;
      $mr->review = $request->rates;
      $mr->save();

        return redirect()->back();
    }
    public function rating_member(Request $request){
      $mr = new MasterRating;
      $mr->member_id  = 4;
      $mr->review = $request->rates;
      $mr->save();

        return redirect()->back(); 
    }
    
    public function wis(Request $request){
     
     $td = TourDestination::when($request->search, function ($query) use ($request) {
       $query->where('main_city', 'like', "%{$request->search}%");
     })->paginate(1);
     
     $td->appends($request->only('search'));
     return view('frontends.city',compact('td'));
   }
   
   public function search_city(Request $request){
    $alltour = MasterTour::all();
    $city = Regency::all();
    if($alltour->city_id == $city->id){
     $td = TourDestination::when($request->search, function ($query) use ($request) {
       $query->where($alltour->city_id == $city->id ? $city->name : "", 'like', "%{$request->search}%");
     })->paginate(1);
     
     $td->appends($request->only('search'));
   }
   return view('frontends.alltours',compact('alltour','city'));
 }
 
 public function fetch_city(Request $request){
   $city = Regency::all();
   
   if($request->get('query')){
    $query = $request->get('query');
    $data = MasterTour::where('city_id','LIKE','%'.$query.'%')
    ->limit(1)->get();
    
    $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
    foreach($data as $row){
      if($row->city_id == $city->id){
        $output .= '<li><a href="#">'.$row->city_id == $city->id ? $city->name : "".'</a></li>';
      }
    }
    $output .= '</ul>';
    echo $output;
  }

}
}
