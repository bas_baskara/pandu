<?php

namespace App\Http\Controllers;

use App\TourIncludeExclude;
use Illuminate\Http\Request;

class TourIncludeExcludeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function show(TourIncludeExclude $tourIncludeExclude)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function edit(TourIncludeExclude $tourIncludeExclude)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourIncludeExclude $tourIncludeExclude)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourIncludeExclude  $tourIncludeExclude
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourIncludeExclude $tourIncludeExclude)
    {
        //
    }
}
