<?php

namespace App\Http\Controllers;

use App\TourAndDestination;
use Illuminate\Http\Request;

class TourAndDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourAndDestination  $tourAndDestination
     * @return \Illuminate\Http\Response
     */
    public function show(TourAndDestination $tourAndDestination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourAndDestination  $tourAndDestination
     * @return \Illuminate\Http\Response
     */
    public function edit(TourAndDestination $tourAndDestination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourAndDestination  $tourAndDestination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TourAndDestination $tourAndDestination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourAndDestination  $tourAndDestination
     * @return \Illuminate\Http\Response
     */
    public function destroy(TourAndDestination $tourAndDestination)
    {
        //
    }
}
