<?php

namespace App\Http\Controllers\Member;

use App\Library\General;
use App\Library\Common;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Auth;
use App\MasterMember;
use App\Customer;
use App\PrivateChat;
use App\MasterTour;
use App\MasterCommunity;
use App\Country;
use App\Province;
use App\Regency;
use App\Post;
use App\CommunityProvinceCity;
use App\Bankaccounts;
use App\TourIncludeExclude;
use App\TourCategory;
use App\TourAndDestination;
use App\TourDestination;

class MembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:member');
    }
    
    public function index()
    {
        $id = Auth::user()->id;
        $common = new Common;
        $c = MasterMember::where('id',$id)->first();
        $negara = $common->member_country($id);
        $provinsi = $common->member_province($id);
        $kota = $common->member_city($id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);


        return view('member.home',compact('negara','provinsi','kota','c','mr'));
    }

    public function message()
    {
        $id = Auth::user()->id;
        $common = new Common;
        $c = MasterMember::where('id',$id)->first();
        $negara = $common->member_country($id);
        $provinsi = $common->member_province($id);
        $kota = $common->member_city($id);

        //member review
        $mr = $common->member_rate_review($id);

        $member = DB::table('members')
                    ->join('provinces','members.province','=','provinces.id')
                    ->join('regencies','members.city','=','regencies.id')
                    ->select('members.*','provinces.name as p','regencies.name as r')
                    ->where('members.id',$c->id)
                    ->get();

                            // var_dump($mr); die();

        return view('member.message',compact('negara','provinsi','kota','member','c','mr'));
    }

    public function profile($id){

        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review($member_id);

        $member = MasterMember::find($id)->first();

        $country = Country::all();
        $province = Province::all();
        $regency = Regency::all();

        return view('member.profile',compact('negara','provinsi','kota','member','province','regency','country','c','mr'));
    }

    public function updateprofile(Request $request, $id)
    {
        $member = MasterMember::find($id);

        $member->first_name = $request->first_name;
        $member->last_name = $request->last_name;
        $member->email = $request->email;
        $member->mobilephone = $request->hp;
        $member->phone = $request->phone;
        $member->address = $request->address;
        $member->city = empty($request->regency) ? $member->city : $request->regency;
        $member->province = empty($request->province) ? $member->province : $request->province;
        $member->country = empty($request->country) ? $member->country : $request->country;
        $member->postalcode = $request->postalcode;

        $uploadedFile = $request->file('profile_img');

        if (empty($uploadedFile)) {
            $imagename = $member->profile_img;
        }else {
            $imagename = str_slug($request->first_name) .'-'. str_slug($request->last_name) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/communities/avatars'), $imagename);
        }

        $member->profile_img = $imagename;
        $member->description = $request->description;
        $member->biography = $request->biography;
        $member->facebook = $request->facebook;
        $member->twitter = $request->twitter;
        $member->googleplus = $request->googleplus;
        $member->instagram = $request->instagram;
        $member->linkedin = $request->linkedin;
        $member->youtube = $request->youtube;


        // var_dump($member); die();

        $member->save();

        \Session::put('message','Profile berhasil diupdate');
        return redirect(url('member/profile/'.$id));
    }

    public function joincommunity()
    {
        $member_id = Auth::user()->id;

        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review($member_id);

        $communities = DB::table('master_communities')
            ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('master_communities.*','countries.country_name as negara','provinces.name as provinsi','regencies.name as kota')
            ->where('community_province_cities.province_id',$c->province)
            ->groupBy('community_province_cities.community_id')
            ->orderBy('community_province_cities.created_at','desc')
            ->get();

        $complement = DB::table('community_province_cities')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->select('community_province_cities.community_id as komunitas','provinces.name as provinsi','regencies.name as kota')
            ->get();

        return view('member.communities',compact('negara','provinsi','kota','c','communities','complement','mr'));
    }

    public function blog()
    {
        $id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$id)->first();
        $negara = $common->member_country($id);
        $provinsi = $common->member_province($id);
        $kota = $common->member_city($id);
        
        //member review
        $mr = $common->member_rate_review(Auth::user()->id);
        
        //postmember
        $pm = Post::where('ismember','1')->orderBy('created_at','desc')->get();
        
        return view('member.blog', compact('negara','provinsi','kota','pm','c','mr'));
    }

    public function createblog()
    {
        $id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$id)->first();
        $negara = $common->member_country($id);
        $provinsi = $common->member_province($id);
        $kota = $common->member_city($id);


        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        $s = new General;
        $status = $s->status_blog_member();

        return view('member.createblog',compact('negara','provinsi','kota','status','c','mr'));
    }

    public function storeblog(Request $request)
    {
        $blog = new Post;

        $whois = $request->session()->all();

        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->category = 'null';
        $blog->description = $request->description;
        $blog->tag = 'null';
        $blog->pageataupost = 'post';
        $blog->status = $request->status;
        $blog->posted_by = Auth::user()->username;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
            $imagename = 'null';
        }else {
            $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/communities/blog_images'), $imagename);
        }

        $blog->image = $imagename;
        $blog->visitor = '0';
        $blog->ismember = isset($whois) ? '1' : '0';
        $blog->isapprove = '0';

        $blog->save();

        \Session::put('message','Blog berhasil disimpan');
        return redirect('member/blog');
    }

    public function editblog($id)
    {
        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);


        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        $s = new General;
        $status = $s->status_blog_member();

        $p = Post::where('id',$id)->first();

        return view('member.editblog',compact('negara','provinsi','kota','status','p','c','mr'));
    }

    public function updateblog(Request $request, $id)
    {
        $blog = Post::find($id);

        $whois = $request->session()->all();

        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->category = 'null';
        $blog->description = $request->description;
        $blog->tag = 'null';
        $blog->pageataupost = 'post';
        $blog->status = $request->status;
        $blog->posted_by = Auth::user()->username;

        $uploadedFile = $request->file('image_post');

        if (empty($uploadedFile)) {
        $imagename = $blog->image;
        }else {
        $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
        $uploadedFile->move(public_path('../www/assets/communities/blog_images'), $imagename);
        }

        $blog->image = $imagename;
        $blog->visitor = $blog->visitor == '0' ? '0' : $blog->visitor;
        $blog->ismember = isset($whois) ? '1' : '0';
        $blog->isapprove = $blog->isapprove == '1' ? '1' : '0';

        $blog->save();

        \Session::put('message','Blog berhasil diupdate');
        return redirect('member/blog');
    }

    public function upgrademember($id)
    {
        $member_id = Auth::user()->id;
        // $whois = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review($member_id);

        $member = MasterMember::where('id',$id)->first();

        return view('member.memberguide',compact('negara','provinsi','kota','member','c','mr'));
    }

    public function submitupgrademember(Request $request, $id)
    {
        $member = MasterMember::find($id);

        $uploadedFile = $request->file('profile_img');

        if (empty($uploadedFile)) {
            $imagename = $member->profile_img;
        }else {
            $imagename = str_slug($request->title) . '.' . $uploadedFile->getClientOriginalExtension();
            $uploadedFile->move(public_path('../www/assets/communities/avatars'), $imagename);
        }
        $member->profile_img = $imagename;


        $member->license_id = $request->ktp;
        $uploadeKTP = $request->file('ktp_member');
        if (empty($uploadeKTP)) {
            $imagektp = $member->license_id_img;
        }else {
            $imagektp = $request->ktp . '.' . $uploadeKTP->getClientOriginalExtension();
            $uploadeKTP->move(public_path('../www/assets/communities/licenses'), $imagektp);
        }
        $member->license_id_img = $imagektp;


        $member->c_license = $request->sim;
        $uploadeSIM = $request->file('sim_member');
        if (empty($uploadeSIM)) {
            $imagesim = $member->c_license_img;
        }else {
            $imagesim = $request->sim . '.' . $uploadeSIM->getClientOriginalExtension();
            $uploadeSIM->move(public_path('../www/assets/communities/licenses'), $imagesim);
        }
        $member->c_license_img = $imagesim;

        $member->save();

        \Session::put('message','Pengajuan menjadi Tour Guide berhasil dikirim');
        return redirect('member/home');
    }

    public function changepassword($id)
    {
        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        //member
        $m = MasterMember::find($id);

        return view('member.changepassword',compact('negara','provinsi','kota','m','c','mr'));
    }

    public function updatepassword(Request $request, $id)
    {
        //member
        $m = MasterMember::find($id);

        $m->password = Hash::make($request->new_pass);

        $m->save();

        \Session::put('message','Password berhasil diupdate');
        return redirect('member/home');
    }

    public function transaksi()
    {
        $member_id = Auth::user()->id;

        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        return view('member.transaksi',compact('negara','provinsi','kota','c','mr'));
    }

    public function customerbooking()
    {
        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        return view('member.tours.customerbooking',compact('negara','provinsi','kota','c','mr'));
    }

    public function getcities($province)
    {
        $data = Regency::where('province_id',$province)->get();

        return response()->json($data);
    }

    public function getprovinces($country)
    {
        $data = Province::where('country_id',$country)->get();

        return response()->json($data);
    }


    public function tour()
    {
        $member_id = Auth::user()->id;

        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review($member_id);

        $tours = MasterTour::where('member_id',$member_id)->get();

        $s = new General;
        $status = $s->status_tour_member();

        $durasi = $s->durasitour();

        $tipe = $s->tipetrip();

        $tc = TourCategory::all();

        $countries = Country::all();
        $provinces = Province::all();
        $city = Regency::all();

        $community = $common->member_community($member_id);

        return view('member.tours.tours',compact('negara','provinsi','kota','c','tours','tipe','tc','durasi','status','community', 'countries','provinces','city','mr'));
    }

    public function createtour()
    {
        $member_id = Auth::user()->id;

        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review($member_id);

        $community = DB::table('master_communities')
                     ->join('join_communities','master_communities.id','=','join_communities.community_id')
                     ->join('master_members','join_communities.member_id','master_members.id')
                     ->select('master_communities.id')
                     ->where('master_members.id','=',$member_id)
                     ->first();

        $s = new General;
        $status = $s->status_tour_member();

        $durasi = $s->durasitour();

        $tipe = $s->tipetrip();

        $ie = TourIncludeExclude::all();

        $tc = TourCategory::all();

        $destinasi = TourDestination::all();

        $countries = Country::all();
        $provinces = Province::all();
        $city = Regency::all();

        // var_dump($ie); die();

        // DB::table('tour_categories')
        //       ->rightjoin('tour_include_excludes', 'tour_categories.id','=','tour_include_excludes.tour_category_id')
        //       ->select('tour_include_excludes.*','tour_categories.title as judultourcategory','tour_categories.id as idtourcategory')
        //       ->get();


        return view('member.tours.createtour',compact('negara','provinsi','kota','c','status','destinasi','tc','durasi','tipe','ie','community', 'countries','provinces','city','mr'));
    }

    public function storetour(Request $request)
    {
        $t = new MasterTour();
        $t->title = $request->title;
        $t->slug = str_slug($request->title);
        $t->category = $request->categories;
        $t->description = $request->description;
        $t->destination = $request->destinasi;

        $uploadedFile = $request->file('image_tour');
        $n = 1;
        if (empty($uploadedFile)) {
            $imagename = '';
        } else {
            foreach ($uploadedFile as $tourimages) {
                $imagename = str_slug($request->title) . '-' . $n++ . '.' . $tourimages->getClientOriginalExtension();
                $tourimages->move(public_path('../www/assets/tour_images'), $imagename);
                $data[] = $imagename;
            }
        }

        $t->images = json_encode($data);
        $t->trippackage = $request->durasi;
        $t->pricedetail = $request->pricedetail;
        $t->tripplans = $request->tripplans;
        $t->actual_price = $request->actual_price;
        $t->promo_price = $request->promo_price;
        $t->fake_price = $request->fake_price;
        $t->tipe = $request->tipe;
        $t->min_kuota = $request->min_kuota;
        $t->max_kuota = $request->max_kuota;
        $t->country_id = $request->country;
        $t->province_id = $request->province;
        $t->city_id = $request->city;
        $t->rating = $request->rating;
        $t->member_id = $request->member_id;
        $t->community_id = $request->community_id;

        // var_dump($t); die();

        $t->save();

        \Session::put('message','Tour berhasil disimpan');
        return redirect('member/tour');
    }

    public function edittour(Request $request, $id)
    {
        $mastertours = MasterTour::where('id',$id)->first();

        $member_id = Auth::user()->id;

        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        $community = DB::table('master_communities')
                     ->join('join_communities','master_communities.id','=','join_communities.community_id')
                     ->join('master_members','join_communities.member_id','master_members.id')
                     ->select('master_communities.id')
                     ->where('master_members.id','=',$id)
                     ->first();

        $s = new General;
        $status = $s->status_tour_member();

        $durasi = $s->durasitour();

        $tipe = $s->tipetrip();

        $ie = TourIncludeExclude::all();

        $tc = TourCategory::all();

        $destinasi = TourDestination::all();

        $countries = Country::all();
        $provinces = Province::all();
        $city = Regency::all();

        return view('member.tours.edittour',compact('negara','provinsi','kota','mastertours','c','status','destinasi','tc','durasi','tipe','ie','community', 'countries','provinces','city','mr'));
    }

    public function updatetour(Request $request, $id)
    {
        $t = MasterTour::find($id);
        $t->title = $request->title;
        $t->slug = str_slug($request->title);
        $t->category = $request->categories;
        $t->description = $request->description;
        $t->destination = $request->destinasi;

        $uploadedFile = $request->file('image_tour');
        $n = 1;
        if (empty($uploadedFile)) {
            $data = json_decode($t->images);
        } else {
            foreach ($uploadedFile as $tourimages) {
                $imagename = str_slug($request->title) . '-' . $n++ . '.' . $tourimages->getClientOriginalExtension();
                $tourimages->move(public_path('../www/assets/tour_images'), $imagename);
                $data[] = $imagename;
            }
        }

        $t->images = json_encode($data);
        $t->trippackage = $request->durasi;
        $t->pricedetail = $request->pricedetail;
        $t->tripplans = $request->tripplans;
        $t->actual_price = $request->actual_price;
        $t->promo_price = $request->promo_price;
        $t->fake_price = $request->fake_price;
        $t->tipe = $request->tipe;
        $t->min_kuota = $request->min_kuota;
        $t->max_kuota = $request->max_kuota;
        $t->country_id = $request->country;
        $t->province_id = $request->province;
        $t->city_id = $request->city;
        $t->rating = $request->rating;
        $t->member_id = $request->member_id;
        $t->community_id = $request->community_id;

        // var_dump($t); die();

        $t->save();

        \Session::put('message','Tour berhasil diupdate');
        return redirect('member/tour');
    }

    public function deletetour($id)
    {
        MasterTour::where('id',$id)->delete();

        \Session::put('message','Tour berhasil dihapus');
        return redirect('member/tour');
    }

    public function seetour($id)
    {
        $mastertours = MasterTour::where('id',$id)->first();

        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);
        $city = Regency::where('id',$mastertours->city_id)->first();
        $province = Province::where('id',$mastertours->province_id)->first();
        $country = Country::where('id',$mastertours->country_id)->first();
        $community = MasterCommunity::where('id',$mastertours->community_id)->first();

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        return view('member.tours.detail', compact('negara','provinsi','kota','mastertours','c','mr','city','province','country','community'));
    }

    public function bankaccount($id)
    {
        $bank = MasterMember::where('id',$id)->first();

        $member_id = Auth::user()->id;
        $common = new Common;
        // $c = $common->member_origin($id);
        $c = MasterMember::where('id',$member_id)->first();
        $negara = $common->member_country($member_id);
        $provinsi = $common->member_province($member_id);
        $kota = $common->member_city($member_id);

        //member review
        $mr = $common->member_rate_review(Auth::user()->id);

        return view('member.bankaccount',compact('bank','c','negara','provinsi','kota','mr'));
    }

    public function updatebankaccount(Request $request, $id)
    {
        $bank = MasterMember::find($id);

        $bank->bank_name = $request->bank_name;
        $bank->account_name = $request->account_name;
        $bank->bank_acc_number = $request->bank_acc_number;
        $bank->bank_area = $request->bank_area;
        $bank->bank_additional = $request->bank_additional;

        $bank->save();

        \Session::put('message','Bank account berhasil disimpan');
        return redirect('member/bankaccount/'.$id);
    }

    public function chatfromcustomer()
    {
        $id = Auth::guard('member')->user()->id;
        $common = new Common;
        $c = MasterMember::find($id);
        $negara = $common->member_country($id);
        $provinsi = $common->member_province($id);
        $kota = $common->member_city($id);

        //member review
        $mr = $common->member_rate_review($id);


        $chats = PrivateChat::where('member_id', $id)->orderBy('updated_at', 'desc')->get();
        if($chats){
            foreach ($chats as $chat) {
                $customers[] = Customer::find($chat->customer_id);
                $chat_status[$chat->customer_id] = $chat->where('customer_id', $chat->customer_id)->get();
            }
        }


        return view('member.chatfromcustomer',compact('negara','provinsi','kota','c','mr', 'customers', 'chat_status'));
    }


    //  public function store(Request $request)

    // {

    //     $this->validate($request, [

    //             'filename' => 'required',
    //             'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

    //     ]);
        
    //     if($request->hasfile('filename'))
    //      {

    //         foreach($request->file('filename') as $image)
    //         {
    //             $name=$image->getClientOriginalName();
    //             $image->move(public_path().'/images/', $name);  
    //             $data[] = $name;  
    //         }
    //      }

    //      $form= new Form();
    //      $form->filename=json_encode($data);
         
        
    //     $form->save();

    //     return back()->with('success', 'Your images has been successfully');
    // }

    // public function dropzoneUploadFile(Request $request){
    //     $imageName = time().'.'.$request->file->getClientOriginalExtension();
    //     $request->file->move(public_path('images'), $imageName);
    //     return response()->json(['success'=>$imageName]);
    // }
}
