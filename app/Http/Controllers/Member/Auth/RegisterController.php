<?php

namespace App\Http\Controllers\Member\Auth;
use App\Notifications\MemberRegisteredSuccessfully;
use App\MasterMember;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator; 
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use App\Country;
use App\Province;
use App\Regency;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = 'member/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:member');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
           
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function register(Request $request)
    {
      $this->validate($request,[

        'email' => 'required',

        'username' => 'required'

      ]);
        
        $member = MasterMember::create([
            'first_name' => $request['namadepan'],
            'last_name' => $request['namabelakang'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'province' => $request['province'],
            'city' => $request['regency'],
            'mobilephone' => $request['phone'],
            'phone' => $request['phone'],
            'activation_code' => str_random(30).time()
            ]);
            
         $request->session()->put('notif','Registrasi Berhasil Silahkan Login');
         return redirect()->to('member/login');
        
    }
    
    public function showRegistrationForm()
    {
        $country = Country::all();
        $province = Province::all();
        $city = Regency::all();

        // $area = DB::table('regencies')
        //         ->join('provinces','regencies.province_id','=','provinces.id')
        //         ->join('countries','regencies.country_id','=','countries.id')
        //         ->select('regencies.name as kota','regencies.id as kota_id', 'provinces.name as provinsi','provinces.id as provinsi_id', 'countries.country_name as negara','countries.id as negara_id')->get();

                // var_dump($area); die();
                
        $regencies = Regency::all();                

        return view('member.register',compact('country','province','city','regencies'));
    }
    
    
      public function getcities($province)
      {
        $data = Regency::where('province_id',$province)->get();

        return response()->json($data);
      }
    

    public function activateUser(string $activationCode)
    {
        try {
            $member = app(Users::class)->where('activation_code', $activationCode)->first();
            if (!$member) {
                return "The code does not exist for any user in our system.";
            }
            $member->status          = 1;
            $member->activation_code = null;
            $member->save();
            auth()->login($member);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
        return redirect()->to('member/login');
    }

    protected function guard()
    {
        return Auth::guard('member');
    }
}
