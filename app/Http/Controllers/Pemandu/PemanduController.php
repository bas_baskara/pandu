<?php

namespace App\Http\Controllers\Pemandu;

use App\Pemandu\Pemandu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PemanduController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pemandu.login');
    }

    public function register()
    {
        return view('pemandu.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pemandu  $pemandu
     * @return \Illuminate\Http\Response
     */
    public function show(Pemandu $pemandu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pemandu  $pemandu
     * @return \Illuminate\Http\Response
     */
    public function edit(Pemandu $pemandu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pemandu  $pemandu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pemandu $pemandu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pemandu  $pemandu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemandu $pemandu)
    {
        //
    }
}
