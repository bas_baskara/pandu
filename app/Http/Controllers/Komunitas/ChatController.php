<?php

namespace App\Http\Controllers\Komunitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MasterMember;
use App\JoinCommunity;
use App\MasterCommunity;
use App\Library\Common;
use App\ChatGroup;
use App\Events\GroupChatSend;
use Auth;
use DB;
use Illuminate\Contracts\Auth\Guard;

class ChatController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:member');

	}
    public function chat()
    {
      $online = Auth::user()->id;
    	 $common = new Common;
      $sidebar = $common->member_community($online);

      $member = DB::table('master_members')
        ->join('join_communities','master_members.id','=','join_communities.member_id')
        ->join('master_communities','join_communities.community_id','=','master_communities.id')
        ->select('master_members.*','join_communities.*')
        ->get();

      //community
      $c = DB::table('master_communities')
             ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',$online)->first();

      //community_area
      
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
    	$comms_id = JoinCommunity::where('member_id', $online)->get();
      foreach($comms_id as $com_id) {
      $communities[] = MasterCommunity::find($com_id->community_id);
      }

    	return view('komunitas.chatGroup', compact('communities', 'chatGroup', 'c','member','ca','sidebar'));
    }

    public function sendchat(Request $request)
    {
    	$member = Auth::user();
    	$message = $member->chatGroup()->create(['message' => $request->input('message'), 'community_id' => $request['community_id'], 'sender' => $request['sender']]);
	    broadcast(new GroupChatSend($member, $message))->toOthers();

    	return ['status' => 'Message sent !'];
    }

    public function getchat()
    {
    	$member = Auth::user();
    	return ChatGroup::with('member')->get();

    }
    public function comId(Request $request)
    {
    	$member = Auth::user();

    	$chatGroup = ChatGroup::create(['member_id' => $member->id, 'community_id' => $request['comId']]);
    	return $request['comId'];
    }
}
