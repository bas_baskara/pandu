<?php

namespace App\Http\Controllers\Komunitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Library\Common;
use App\Member;
use App\MasterCommunity;
use App\Province;
use App\Regency;
use App\Post;
use App\JoinCommunity;
use App\MemberCommunityReviews;
Use Auth;
use DB;
use Hash;

class KomunitasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    public function index()
    {
      $online = Auth::user()->id;

      $common = new Common;
      $sidebar = $common->member_community($online);

      //community_area
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
      
      //community
      $c = DB::table('master_communities')
          ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',$online)->first();


          // var_dump($ca); die();

      return view('komunitas.home',compact('c','ca','sidebar'));
    }

    public function profile($id)
    {  
        $common = new Common;

        $sidebar = $common->member_community(Auth::user()->id);
        
        

      //community_area
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
      
      //community
      $c = DB::table('master_communities')
              ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',Auth::user()->id)->first();

      $community = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
            ->where('master_communities.id',$id)->first();
      
      $areas = DB::table('community_province_cities')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('master_communities','community_province_cities.community_id','=','master_communities.id')
            ->select('provinces.id as provinsi_id','provinces.name as provinsi_nama','regencies.id as kota_id','regencies.name as kota_nama')
            ->where('master_communities.id',$sidebar->id)
            ->get();

      $province = Province::all();
      $regency = Regency::all();

      return view('komunitas.profile',compact('community','province','regency','areas','sidebar','c','ca'));
    }

    public function updateprofile(Request $request, $id)
    {
          $community = MasterCommunity::find($id);
        
           $community->community_name = $request->community_name;
           $community->community_identity = str_slug($request->community_name);
          $community->description = $request->description;
          
        
           $uploadedFile = $request->file('profile_image');
        
           if(empty($uploadedFile)) {
             $imagename = $community->profile_image;
           } else {
             $imagename = str_slug($request->community_name) . '-profile.' . $uploadedFile->getClientOriginalExtension();
             $uploadedFile->move(public_path('../www/assets/communities'), $imagename);
          }
        
           $community->profile_image = $imagename;
        
           $uploadedFile = $request->file('banner_image');
        
           if(empty($uploadedFile)) {
             $imagename = $community->banner_image;
           } else {
             $imagename = str_slug($request->community_name) . '-banner.' . $uploadedFile->getClientOriginalExtension();
             $uploadedFile->move(public_path('../www/assets/communities'), $imagename);
           
           $community->banner_image = $imagename;
        
           $community->email = $request->email;
          $community->hp = $request->hp;
           $community->facebook = $request->facebook;
           $community->twitter = $request->twitter;
           $community->instagram = $request->instagram;
           $community->website = $request->website;
        
          $community->save();
        
          return redirect()->back();
        }
    }

    public function blog()
    {
      $online = Auth::user()->id;

      $common = new Common;
      $sidebar = $common->member_community($online);
            //community_area
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
      
      //community
      $c = DB::table('master_communities')
              ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',Auth::user()->id)->first();
    
     $com = DB::table('posts')
            ->join('master_members', 'posts.posted_by', '=', 'master_members.username')
            ->join('join_communities', 'master_members.id', '=', 'join_communities.member_id')
            ->join('master_communities','master_communities.id','=','join_communities.community_id')
            ->select('posts.*', 'master_members.username as kk', 'master_communities.community_name as nm')
            ->get();    

      $pm = Post::where('ismember','1')->orderBy('created_at','desc')->get();

      return view('komunitas.blog',compact('pm','c','ca','sidebar','com'));
    }

    public function members()
    {
      $online = Auth::user()->id;

      // $community = DB::table('master_communities')
      //       ->join('provinces','master_communities.province','=','provinces.id')
      //       ->join('regencies','master_communities.city','=','regencies.id')
      //       ->join('join_communities','master_communities.id','=','join_communities.community_id')
      //       ->join('members','join_communities.member_id','=','members.id')
      //       ->select('master_communities.*','provinces.name as p','regencies.name as r','members.*')
      //       ->where('members.id',$member_community)->first();

      $common = new Common;
      $sidebar = $common->member_community($online);

      $member = DB::table('master_members')
        ->join('join_communities','master_members.id','=','join_communities.member_id')
        ->join('master_communities','join_communities.community_id','=','master_communities.id')
        ->select('master_members.*','join_communities.*')
        ->get();

      //community
      $c = DB::table('master_communities')
             ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',$online)->first();

      //community_area
      
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();

                  // var_dump($member); die();

      return view('komunitas.members', compact('c','member','ca','sidebar'));
    }

    public function message(){
      $online = Auth::user()->id;

      $common = new Common;
      $sidebar = $common->member_community($online);
            //community_area
      $c = DB::table('master_communities')
             ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',$online)->first();

      //community_area
      
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
      return view('komunitas.message',compact('c','ca','sidebar'));
    }

    public function reviews()
    {
      $online = Auth::user()->id;

      $common = new Common;
      $sidebar = $common->member_community($online);

      //community_area
      $c = DB::table('master_communities')
             ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
           ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','master_members.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')
          ->where('master_members.id',$online)->first();

      //community_area
      
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();

      return view('komunitas.reviews',compact('c','ca','community','sidebar'));
    }

    public function storereviews(Request $request)
    {
      $review = new MemberCommunityReviews;

      $review->member_id = Auth::user()->id;
      $review->community_id = Auth::user()->community_id;
      $review->description = $request->description;
      // $sum = 0;
      // foreach ($request->rg1 as $countrating) {
      //   $sum += $countrating;
      // }

      $review->rating = intval($request->rg1);

      var_dump($review); die();
    }

    public function showblog()
    {
      return view('komunitas.bloglist');
    }

    public function transaksi()
    {
      $online = Auth::user()->id;

      $common = new Common;
      $sidebar = $common->member_community($online);

            //community_area
      $ca = DB::table('master_communities')
          ->join('community_province_cities','master_communities.id','=','community_province_cities.community_id')
          ->join('countries','community_province_cities.country_id','=','countries.id')
          ->join('provinces','community_province_cities.province_id','=','provinces.id')
          ->join('regencies','community_province_cities.city_id','=','regencies.id')
          ->select('master_communities.*','countries.country_name as n','provinces.name as p','regencies.name as r','community_province_cities.*')->limit(1)->get();
      //community
      $c = DB::table('master_communities')
          ->join('join_communities','master_communities.id','=','join_communities.community_id')
          ->join('master_members','join_communities.member_id','=','master_members.id')
          ->select('master_communities.*','master_members.*')
          ->where('master_members.id',$online)->first();

      return view('komunitas.transaksi',compact('community','c','ca','sidebar'));
    }

    public function community($identity)
    {
      $group = MasterCommunity::where('community_identity',$identity)->first();
      $join = JoinCommunity::all();
      $checkjoin = JoinCommunity::where('member_id',Auth::user()->id)->first();

      // var_dump($join); die();

      return view('komunitas.detailcommunity',compact('group','join','checkjoin'));
    }

    public function joincommunity(Request $request)
    {
      /**
      * Status
      * 0. Waiting Approval
      * 1. Approved
      * 2. Canceled
      **/
      $join = new JoinCommunity;

      // $c_id = MasterCommunity::where('community_identity',$identity)->first();

      $join->member_id = $request->member_id;
      $join->community_id = $request->community_id;
      $join->status = $request->status;
      $join->admin_approve = $request->admin_approve;
      $join->leader_approve = $request->leader_approve;

      // var_dump($join); die();

      $join->save();

      return redirect()->back(); //->with(['message' => '',''=>'']);
    }

    public function joinupdate(Request $request, $identity)
    {
      /**
      * Status
      * 0. Waiting Approval
      * 1. Approved
      * 2. Canceled
      **/

      //community_id
        $c_id = MasterCommunity::where('community_identity',$identity)->first();

        $join = JoinCommunity::find('id',$c_id->id);

        $join->member_id = $request->member_id;
        $join->community_id = $request->community_id;
        $join->status = $request->status;
        $join->admin_approve = $request->admin_approve;
        $join->leader_approve = $request->leader_approve;

      var_dump($join); die();

      // $join->save();

      return redirect()->back();
    }

    
    public function showTransaksi(){
        return view ('komunitas.showTransaksi');
    }

    // public function updateprofile($id)
    // {
    //     $user = Userkomunitas::where('id',$id)->first();

        

    //     return view('komunitas.profile',compact('user'));
    // }

    public function update(Request $request, $id)
    {
      $uploadedFiles = $request->file('image_profile');
      $filenames = $uploadedFiles->getClientOriginalName();
      $path = $uploadedFiles->move(public_path('komunitas/avatar'), $filenames);
      $user = Userkomunitas::find($id);
          $user->fullname = $request->fullname;
          $user->email = $request->email;
          $user->password = Hash::make($request->password);
          $user->no_ktp = $request->no_ktp;
          $user->place = $request->place;
          $user->date_of_birth = $request->date_of_birth;
          $user->address = $request->address;
          $user->domicile = $request->domicile;
          $user->phone = $request->phone;
          $user->description = $request->description;
          $user->review = "null";
          $user->url_facebook = $request->url_facebook;
          $user->level = "ketua";
          $user->image_profile = $filenames;
          $user->save();
          return redirect()->back();
    }
}
