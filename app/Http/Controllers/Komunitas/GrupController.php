<?php

namespace App\Http\Controllers\Komunitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Komunitas\Grupkomunitas;
use App\Komunitas\Userkomunitas;
Use Auth;

class GrupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:komunitas');
    }
    public function createViewGrup()
    {
        return view('blog.komunitas.creategrup');
    }
    public function createGrup(Request $request)
    {
        $uploadedFile = $request->file('img_profile');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('komunitas/grup'), $filename);
        }
        $grup = new grupkomunitas;
        $grup->name = $request->name;
        $grup->description = $request->description;
        $grup->ketua = Auth::user()->fullname;
        $grup->place = $request->place;
        $grup->img_profile = $filename;
        $grup->member = 1;
        $grup->save();
        return redirect(route('create-grup.index'))->with('message','Buat Grup Sukses, menunggu proses vertifikasi');
    }
    public function Grup(Request $request, $grup_id = NULL)
    {
        if($grup_id){
            $users = Userkomunitas::where('grup_id',$grup_id)->first();
            $grups = $users->grupkomunitas->first();
            $ingrups = Userkomunitas::all();
        }
        return view('blog.komunitas.grup',compact('users', 'grups','ingrups'));
    }
    public function terimaGrup($id)
    {
        $terima = Userkomunitas::where('id',$id)->first();
        if ($terima->inGrup == '0') {
            Userkomunitas::where('id',$id)->update(array('inGrup' => '1'));
        }
        return redirect()->back();
    }
    public function tolakGrup($id)
    {
        $terima = Userkomunitas::where('id',$id)->first();
        $currentGrup = Userkomunitas::where('grup_id',$id)->first();
        if ($terima->inGrup == '0') {
            Userkomunitas::where('id',$id)->update(array('grup_id' => NULL));
        }
        return redirect()->back();
    }

}
