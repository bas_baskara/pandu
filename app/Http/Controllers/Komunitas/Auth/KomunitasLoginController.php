<?php

namespace App\Http\Controllers\Komunitas\Auth;

use App\Komunitas\Userkomunitas;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KomunitasLoginController extends Controller
{
    
    use AuthenticatesUsers;
    
    protected $redirectTo = 'komunitas/home';
    
    public function __construct()
    {
        $this->middleware('guest:komunitas')->except('logout');
    }
    public function showLoginForm()
    {
        return view('komunitas.login');
    }
    
    protected function guard()
    {
        return Auth::guard('komunitas');
    }
    
    public function username()
    {
        $identity  = request()->get('email');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }


    // protected function credentials(Request $request)
    // {
    //     $admin = Userkomunitas::where('email',$request->email)->first();
    //     if (count($admin)) {
    //         if ($admin->status == 0) {
    //             return ['email'=>'inactive','password'=>'You are not an active person, please contact Admin'];
    //         }else{
    //             return ['email'=>$request->email,'password'=>$request->password,'status'=>1];
    //         }

    //     return $request->only($this->username(), 'password');
    //     }
    // }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     $request->session()->put('login_error', trans('auth.failed'));
    //     throw ValidationException::withMessages(
    //         [
    //             'error' => [trans('auth.failed')],
    //         ]
    //     );
    // }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('komunitas/login');
    }
    
    // protected function validateLogin(Request $request)
    // {
    //     $this->validate($request, [
    //         $this->username() => 'required|string',
    //         'password' => 'required|string',
    //         // 'activation_code' => 'required|exists|userkomunitas,activation_code',
    //     ]);
    // }
    
    
    
}
