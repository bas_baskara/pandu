<?php

namespace App\Http\Controllers\Komunitas\Auth;
use App\Notifications\UserKomunitasRegisteredSuccessfully;
use App\Komunitas\Userkomunitas;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = 'komunitas/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:komunitas');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fullname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function registerketua(Request $request)
    {
        $uploadedFile = $request->file('img_ktp');
        if (empty($uploadedFile)) {
            $filename = 'null';
        }else {
            $filename = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->move(public_path('komunitas/ktp'), $filename);
        }
        $user_komunitas = Userkomunitas::create([
            'id' => str_random(8),
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'no_ktp' => $request['no_ktp'],
            'place' => $request['place'],
            'date_of_birth' => $request['date_of_birth'],
            'address' => $request['address'],
            'domicile' => $request['domicile'],
            'img_ktp' => $filename,
            'phone' => $request['phone'],
            'description' => "null",
            'review' => "null",
            'inGrup' => 0,
            'url_facebook' => $request['url_facebook'],
            'level' => "ketua",
            'activation_code' => str_random(30).time(),
            'image_profile' => "null",
            ]);
              $user_komunitas->notify(new UserKomunitasRegisteredSuccessfully($user_komunitas));
            return redirect()->back()->with('message','Registrasi Sukses, silahkan login kembali');
        }

        protected function register(Request $request)
        {
        $user_komunitas = Userkomunitas::create([
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'place' => $request['place'],
            'date_of_birth' => $request['date_of_birth'],
            'address' => $request['address'],
            'description' => "null",
            'no_ktp' => "null",
            "img_ktp" => "null",
            'review' => "null",
            'domicile' => $request['domicile'],
            'level' => "member",
            'inGrup' => 0,
            'activation_code' => str_random(30).time(),
            'url_facebook' => $request['url_facebook'],
            'phone' => $request['phone'],
              ]);
              $user_komunitas->notify(new UserKomunitasRegisteredSuccessfully($user_komunitas));
            return redirect()->back()->with('message','Registrasi Sukses, silahkan login kembali');
        }
    
    public function showRegistrationForm()
    {
        return view('komunitas.register');
    }

    public function showRegistrationForm2()
    {
        return view('komunitas.register2');
    }
    
    public function activateUser(string $activationCode)
    {
        try {
            $user_komunitas = app(Userkomunitas::class)->where('activation_code', $activationCode)->first();
            if (!$user_komunitas) {
                return "The code does not exist for any user in our system.";
            }
            $user_komunitas->status          = 1;
            $user_komunitas->activation_code = null;
            $user_komunitas->save();
            auth()->login($user_komunitas);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
        return redirect()->to('komunitas/login');
    }

    public function updateProfile($id)
    {
        $user = Userkomunitas::where('id',$id)->first();
        return view('komunitas.profile',compact('user'));
    }

    public function update(Request $request, $id)
    {
      $uploadedFiles = $request->file('image_profile');
      $filenames = $uploadedFiles->getClientOriginalName();
      $path = $uploadedFiles->move(public_path('komunitas/profile'), $filenames);

      $user = Userkomunitas::find($id);
          $user->fullname = $request->fullname;
          $user->email = $request->email;
          $user->password = Hash::make($request->password);
          $user->no_ktp = $request->no_ktp;
          $user->place = $request->place;
          $user->date_of_birth = $request->date_of_birth;
          $user->address = $request->address;
          $user->domicile = $request->domicile;
          $user->phone = $request->phone;
          $user->description = $request->description;
          $user->review = "null";
          $user->url_facebook = $request->url_facebook;
          $user->level = "ketua";
          $user->image_profile = $filenames;
          $user->save();

          return redirect()->back();
    }



    protected function guard()
    {
        return Auth::guard('komunitas');
    }
}
