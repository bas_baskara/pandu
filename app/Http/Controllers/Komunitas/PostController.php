<?php

namespace App\Http\Controllers\Komunitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Komunitas\Postkomunitas;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $uploadedFile = $request->file('file');
      if (empty($uploadedFile)) {
        $filename = 'null';
      }else {
        $filename = $uploadedFile->getClientOriginalName();
        $path = $uploadedFile->move(public_path('/komunitas_artikel'), $filename);
      }
      $post = new postkomunitas;
      $post->image = $filename;
      $post->title = $request->title;
      $post->slug = str_slug($request->title);
      $post->description = $request->description;
      $post->status = 0;
      $post->posted_by = Auth::user()->fullname;
      $post->visitor = 0;
      $post->meta_desc = $request->meta_desc;
      $post->meta_title = $request->meta_title;
      $post->meta_keyword = $request->meta_keyword;
      $post->save();
      return redirect()->back()->with('message','Buat Artikel Sukses, Tunggu Approval Dari Admin');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $uploadedFile = $request->file('file');
      if (empty($uploadedFile)) {
        $filename = 'null';
      }else {
        $filename = $uploadedFile->getClientOriginalName();
        $path = $uploadedFile->move(public_path('/imagepost_komunitas'), $filename);
      }
      $post = postkomunitas::find($id);
      $post->image = $filename;
      $post->title = $request->title;
      $post->slug = str_slug($request->title);
      $post->description = $request->description;
      $post->status = "pending";
      $post->posted_by = "null";
      $post->comment = "null";
      $post->visitor = 0;
      $post->meta_desc = $request->meta_desc;
      $post->meta_title = $request->meta_title;
      $post->meta_keyword = $request->meta_keyword;
      $post->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      postkomunitas::where('id',$id)->delete();
      return redirect()->back();
    }
}
