<?php

namespace App\Http\Controllers;

use App\JoinCommunity;
use Illuminate\Http\Request;

class JoinCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JoinCommunity  $joinCommunity
     * @return \Illuminate\Http\Response
     */
    public function show(JoinCommunity $joinCommunity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JoinCommunity  $joinCommunity
     * @return \Illuminate\Http\Response
     */
    public function edit(JoinCommunity $joinCommunity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JoinCommunity  $joinCommunity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JoinCommunity $joinCommunity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JoinCommunity  $joinCommunity
     * @return \Illuminate\Http\Response
     */
    public function destroy(JoinCommunity $joinCommunity)
    {
        //
    }
}
