<?php

namespace App\Http\ViewComposers;

use App\Customer;

class CustomerComposer
{
	public function compose($view)
	{
		$view->with('customer', Customer::where('id',Auth::user()->id)->first());
	}
}