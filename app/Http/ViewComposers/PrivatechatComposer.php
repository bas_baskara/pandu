<?php

namespace App\Http\ViewComposers;

use App\PrivateChat;
use App\Customer;
use Auth;
use Illuminate\Contracts\Auth\Guard;
Use Illuminate\View\View;

class PrivatechatComposer
{

	protected $guide;

	public function __construct(){
		if(Auth::guard('member')->check()){
		$id = Auth::guard('member')->user()->id;
		$chats = PrivateChat::where('member_id', $id)->where('read_by_guide', '0')->whereNotNull('message')->get();
		$this->guide = $chats;
		}

	}

	public function compose(View $view)
	{
		$view->with('unread', $this->guide);
	}
}