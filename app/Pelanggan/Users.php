<?php

namespace App\Pelanggan;

use App\PrivateChat;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;
    protected $guard ='pelanggan';
    protected $table = 'customers';

    protected $fillable = [
        'email', 'username', 'first_name',
        'last_name','password','mobilephone','address','city','province','country','postalcode','profile_img','description','status','activation_code','rating','facebook','twitter','instagram','googleplus','youtube'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

     public function userToGuide()
    {
      return $this->hasMany(PrivateChat::class, 'customer_id');
    }
}
