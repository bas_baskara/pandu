<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
    public function userToGuide()
    {
      return $this->hasMany(PrivateChat::class);
    }
}
