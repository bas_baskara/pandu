<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  public function posts()
    {
    	return $this->belongsToMany('App\Blog\post','post__categories')->orderBy('created_at','DESC')->paginate(8);
    }

  public function getRouteKeyName()
  {
      return 'slug';
  }
}
