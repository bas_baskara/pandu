<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

  public function categories()
  {
    return $this->belongsToMany('App\Blog\Category','post__categories')->withTimestamps();
  }

  public function getRouteKeyName()
  {
      return 'slug';
  }
  
}
