<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class Usercareers extends Model
{
  protected $fillable = [
      'name', 'fullname','email', 'phone', 'graduated', 'description', 'resume',
  ];
  public function getRouteKeyName()
  {
      return 'slug';
  }
}
