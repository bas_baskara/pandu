<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function categories()
  {
    return $this->belongsToMany('App\Blog\Category','post_categories')->withTimestamps();
  }

  public function getRouteKeyName()
  {
      return 'slug';
  }
}
