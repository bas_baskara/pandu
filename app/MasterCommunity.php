<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterCommunity extends Model
{
    protected $table = 'master_communities';
}
