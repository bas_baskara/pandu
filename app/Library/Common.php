<?php
/***************************************************************
Author: Lombok Industries
Author URI: https://www.lombokindustries.com
Description: Library
Version: 1.0.0
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*****************************************************************/

namespace App\Library;

use Illuminate\Support\Facades\DB;
use App\Country;
use App\Regency;
use App\Province;
use App\MasterMember;

class Common {

	public function member_country($id)
	{
		$mm = MasterMember::where('id',$id)->first();
		$c = Country::where('id',$mm->country)->first();

		$result = $c['country_name'];

		return $result;
	}

	public function member_province($id)
	{
		$mm = MasterMember::where('id',$id)->first();
		$p = Province::where('id',$mm->province)->first();

		$result = $p['name'];

		return $result;
	}

	public function member_city($id)
	{
		$mm = MasterMember::where('id',$id)->first();
		$r = Regency::where('id',$mm->city)->first();

		$result = $r['name'];

		return $result;
	}

	public function member_origin($id)
	{
		$mo = DB::table('master_members')
			 ->join('countries','master_members.country','=','countries.id')
			 ->join('provinces','master_members.province','=','provinces.id')
			 ->join('regencies','master_members.city','=','regencies.id')
			 ->select('master_members.*','countries.country_name as negara','provinces.name as provinsi','regencies.name as kota')
			 ->where('master_members.id',$id)
			 ->first();

		return $mo;
	}

	public function member_community($id)
	{
		$mc = DB::table('master_members')
				->join('join_communities','master_members.id','=','join_communities.member_id')
				->join('master_communities','join_communities.community_id','=','master_communities.id')
				->select('master_communities.*')
				->where('master_members.id',$id)
				->first();

		return $mc;
	}

	public function community_origin()
	{
		$co = DB::table('community_province_cities')
            ->join('regencies','community_province_cities.city_id','=','regencies.id')
            ->join('provinces','community_province_cities.province_id','=','provinces.id')
            ->join('countries','community_province_cities.country_id','=','countries.id')
            ->select('community_province_cities.city_id as idkota','community_province_cities.community_id as idkomunitas','provinces.name as namaprovinsi','regencies.name as namakota','countries.country_name as negara')
            ->get();

        return $co;
	}

	public function member_community_review()
	{
		$rr = DB::table('master_members')
			->join('master_reviews','master_members.id','=','master_reviews.member_id')
			->join('member_community_reviews','master_members.id','=','member_community_reviews.member_id')
			->select('member_community_reviews.description as reviewkomunitas','member_community_reviews.rating as ratingkomunitas')
			->get();

		return $rr;
	}

	public function member_rate_review($id)
	{
		$mv = DB::table('master_members')
			->join('master_transactions','master_members.id','=','master_transactions.member_id')
			->select('master_members.*','master_transactions.rating as ratingtransaksi','master_transactions.review as reviewtransaksi')
			->where('master_transactions.member_id',$id)
			->get();

		return $mv;
	}
}