<?php
/***************************************************************
Author: Lombok Industries
Author URI: https://www.lombokindustries.com
Description: Library
Version: 1.0.0
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*****************************************************************/

namespace App\Library;

use Illuminate\Support\Facades\DB;
use App\Country;
use App\Regency;
use App\Province;
use App\MasterMember;
use App\MasterTour;
use App\TourCategory;

class Tours {

	public function tour_by_category()
	{

	}

	public function tour_by_tipe()
	{

	}

	public function tour_by_promo()
	{

	}

	public function tour_by_city()
	{

	}

	public function tour_by_province()
	{

	}

	public function tour_by_country()
	{

	}

	public function tour_by_rating()
	{

	}

	public function tour_by_price()
	{

	}

	public function tour_by_community()
	{

	}

	public function tour_by_includeexlude()
	{
		
	}

}