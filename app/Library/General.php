<?php
/***************************************************************
Author: Lombok Industries
Author URI: https://www.lombokindustries.com
Description: Library
Version: 1.0.0
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*****************************************************************/

namespace App\Library;

class General {

    public function status()
    {
        return $status = array(
            '1' => 'Active',
            '0' => 'In active'
        );
    }

    public function includeexclude()
    {
        return $inex = array(
            '1' => 'Include', 
            '2' => 'Exclude'); 
    }

    public function status_blog_member()
    {
        return $sbm = array(
            '2' => 'Pubslihed',
            '0' => 'Draft'
        );
    }

    public function status_tour_member()
    {
        return $status = array(
            '2' => 'Pubslihed',
            '0' => 'Draft'
        );
    }

    public function switch()
    {
        return $switch = array(
            '1' => 'Enable',
            '0' => 'Disable'
        );
    }

    public function tipetrip()
    {
        return $tipeperjalanan = array(
            '1' => 'Open Trip',
            '2' => 'Private Tour',
            '3' => 'Reguler Tour'
        );
    }

    public function durasitour()
    {
        return $program = array(
            '1' => '1 Hari',
            '2' => '1 Hari 1 Malam (1D1N)',
            '3' => '2 Hari 1 Malam (2D1N)',
            '4' => '2 Hari 2 Malam (2D2N)',
            '5' => '3 Hari 2 Malam (3D2N)',
            '6' => '4 Hari 3 Malam (4D3N)',
            '7' => '5 Hari 4 Malam (5D4N)',
            '8' => '6 Hari 5 Malam (6D5N)',
            '9' => '7 Hari 6 Malam (7D6N)',
            '10' => '8 Hari 6 Malam (8D7N)'
        );
    }

    public function roles()
    {
       return $roles = array(
            '1' => 'Super Administrator', 
            '2' => 'Author', 
            '3' => 'Editor', 
            '4' => 'Contributor', 
            '5' => 'Customer', 
            '6' => 'Member', 
            '7' => 'Guide', 
            '8' => 'Community Leader', 
            '9' => 'Subscriber',
            '10' => 'Admin',
            '11' => 'Customer Service'
        );
    }

    public function flash_message($msg, $status = 'success')
    {
        $response = '';
        $class = 'danger';
        
        if($status == 'success'){
            $class = 'success';
        }

        if(!empty($msg))
        {
            $flash = '<div class="alert alert-'.$class.' no-margin" style="margin-bottom:15px!important;">'.$msg.'</div>';
        }
        return $flash;
    }
}