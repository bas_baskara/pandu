<?php

namespace App\Members;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;
    protected $guard ='member';
    protected $table = 'members';

    protected $fillable = [
      'first_name','last_name','username','email','mobilephone','phone','address','city','province','country',
      'postalcode','profile_img','description','biography','licensi_id','licensi_id_img','c_license','c_license_img',
      'a_license','a_license_img','passport','passport_img','status','activation_code','rating','facebook','twitter',
      'googleplus','instagram','linkedin','youtube','created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
