<?php

namespace App\Providers;

use App\Customer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'customer', 'App\Http\ViewComposers\CustomerComposer' 
        );

        View::composer('customer', function($view)
        {
            $view->with('customer', Customer::where('id', Auth::user()->id()->first()));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
