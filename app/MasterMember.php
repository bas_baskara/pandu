<?php

namespace App;

use App\PrivateChat;
use App\ChatGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MasterMember extends Authenticatable
{
    use Notifiable;
    protected $guard ='member';
    protected $table = 'master_members';

    protected $fillable = [
      'first_name','last_name','username','email','password','mobilephone','phone','address','city','province','country',
      'postalcode','profile_img','description','biography','licensi_id','licensi_id_img','c_license','c_license_img',
      'a_license','a_license_img','passport','passport_img','status','activation_code','rating','facebook','twitter',
      'googleplus','instagram','linkedin','youtube','created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
      return $this->hasMany(Message::class);
    }

    public function guideToMember()
    {
      return $this->hasMany(PrivateChat::class, 'member_id');
    }

    public function chatGroup()
    {
      return $this->hasMany(ChatGroup::class, 'member_id');
    }
}
