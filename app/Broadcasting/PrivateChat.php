<?php

namespace App\Broadcasting;

use App\MasterMember;
use App\Pelanggan\Users;
use App\PrivateChat;
use Auth;

class PrivateChat
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    public function join(MasterMember $guide, Users $customer,PrivateChat $message)
    {
        if(Auth::guard('pelanggan')->check() || Auth::guard('member')->check()){
            return true;
        }else{
            return false;
        }
    }
}
