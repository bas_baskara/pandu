<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message'];

    /**
	 * A message belong to a user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
	  return $this->belongsTo(User::class);
	}

	public function customer()
	{
		return $this->belongsTo(Customers::class);
	}

	public function member()
	{
		return $this->belongsTo(Members::class);
	}

	public function master_member()
	{
		return $this->belongsTo(MasterMembers::class);
	}
}
