<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatGroup extends Model
{
    protected $fillable = ['message', 'community_id', 'member_id', 'sender'];

    public function member()
    {
    	return $this->belongsTo(MasterMember::class, 'member_id');
    }
}
