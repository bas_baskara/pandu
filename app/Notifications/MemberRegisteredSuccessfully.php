<?php

namespace App\Notifications;

use App\MasterMember;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MemberRegisteredSuccessfully extends Notification
{
    use Queueable;

    protected $master_member;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(MasterMember $master_member)
    {
        $this->master_member = $master_member;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $master_member = $this->master_member;

        return (new MailMessage)
                    ->from(env('ADMIN_MAIL'))
                    ->subject('Successfully created new account')
                    ->greeting(sprintf('Hello %s', $master_member->username))
                    ->line('You have successfully registered to our system. Please activate your account.')
                    ->action('Click Here', url('member/activate', $master_member->activation_code))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
