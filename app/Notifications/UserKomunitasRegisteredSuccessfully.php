<?php

namespace App\Notifications;
use App\Komunitas\Userkomunitas;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserKomunitasRegisteredSuccessfully extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $user_komunitas;

    public function __construct(Userkomunitas $user_komunitas)
    {
        $this->Userkomunitas = $user_komunitas;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $user */
        $user_komunitas = $this->Userkomunitas;
        return (new MailMessage)
           ->line('Please verify your account')
           ->action('verify account',route('activate.userkomunitas',$user_komunitas->activation_code))
           ->line('Thank you for activation');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
