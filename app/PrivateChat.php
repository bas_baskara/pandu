<?php

namespace App;

use App\Pelanggan\Users;
use Illuminate\Database\Eloquent\Model;

class PrivateChat extends Model
{
	protected $fillable = ['message', 'customer_id', 'member_id', 'sender', 'receiver'];
    //

    public function guide()
    {
    	return $this->belongsTo(MasterMember::class, 'member_id');
    }

    public function customer()
    {
    	return $this->belongsTo(Users::class);
    }
}
