<?php

namespace App\Events;

use App\MasterMember;
use App\ChatGroup;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GroupChatSend implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $member; public $chatGroup;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MasterMember $member, ChatGroup $chatGroup)
    {
        $this->member = $member;
        $this->chatGroup = $chatGroup;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('chatgroup');
    }
}
