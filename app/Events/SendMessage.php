<?php

namespace App\Events;

use App\Pelanggan\Users;
use App\MasterMember;
use App\PrivateChat;
use Auth;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $customer; public $member; public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Users $customer, MasterMember $member, PrivateChat $message)
    {
        $this->customer = $customer;
        $this->member = $member;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('toguide');
    }

    public function broadcastWhen()
    {
        $privChat = PrivateChat::find($this->message->id);
        return $privChat->customer_id === $this->customer->id && $privChat->member_id === $this->member->id;
    }
   
}
