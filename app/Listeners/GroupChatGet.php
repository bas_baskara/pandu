<?php

namespace App\Listeners;

use App\Events\GroupChatSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GroupChatGet
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GroupChatSend  $event
     * @return void
     */
    public function handle(GroupChatSend $event)
    {
        //
    }
}
