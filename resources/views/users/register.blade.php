@extends('layout')
@section('content')


<div class="bg">

<div class="container">
  <div class="row">
    <div class="col-md-6" style="margin-bottom:50px;">
        <div class="account-box"  style="border-radius : 15px;">
        
        <div class="logo">
          <img src="{{URL::asset('www/assets/images/logo.png')}}" alt="panduasia"/>
        </div>

            <div class="login-text">
              <h2>Register User Baru</h2>
            </div>
            
             <br>
                    @if(session()->has('notif'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{session()->get('notif')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

            <form class="login-form" method="POST" action="{{ url('user/register') }}">


              @csrf


            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nama Depan"  name="namadepan" required="required" />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nama Belakang"  name="namabelakang" required="required" />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username"  name="username" required="required" />
            </div>

            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email"  name="email" required="required" />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Phone" name="phone" required="required" />
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" required="required" />
            </div>

            <label class="clickcheckbox">
                <center>
                    <p class="small">Dengan mengklik Daftar, Anda menyetujui <a href="{{URL('/home/about/syaratketentuan')}}">syarat dan ketentuan</a> dan <a href="{{URL('/home/about/kebijakanpribadi')}}">Kebijakan pribadi kami.</a> </p>
                </center>
            </label>

            <button class="btn btn-lg btn-block purple-bg" type="submit">Buat Akun</button>

            </form>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
