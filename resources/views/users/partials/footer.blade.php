
		<footer id="gtco-footer" class="gtco-section" role="contentinfo">
			<div class="gtco-container">
				<div class="row row-pb-md">
					<div class="col-md-8 col-md-offset-2 gtco-cta text-center">
						<h3>We Love To Talk About Your Business</h3>
						<p>
							<a href="javascript:;" class="btn btn-white btn-outline">Contact Us</a>
						</p>
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-4 gtco-widget gtco-footer-paragraph">
						<h3>Panduasia</h3>
						<p>dengan adanya Panduasia, para pemandu dapat menciptakan suasana dimana para traveller merasa nyaman dan menikmati perjalananya
							sepanjang waktu. </p>
						</div>
						<div class="col-md-4 gtco-footer-link">
							<div class="row">
								<div class="col-md-6">
									<ul class="gtco-list-link">
									</ul>
								</div>
								<div class="col-md-6">
									<p>
										<a href="tel://081909522645">0819-0952-2645 / 0855-8856-739</a>
										<br>
										<a href="#">cs@panduasia.com</a>
										Alamat : Jl Kayu Manis No. 17, Balekembang,Kramat Jati, Jakarta Timur Kode Pos : 13530
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 gtco-footer-subscribe">
							<form class="form-inline">
								<div class="form-group">
									<label class="sr-only" for="exampleInputEmail3">Email address</label>
									<input type="email" class="form-control" id="" placeholder="Email">
								</div>
								<button type="submit" class="btn btn-primary">Send</button>
							</form>
						</div>
					</div>
				</div>
				<div class="gtco-copyright">
					<div class="gtco-container">
						<div class="row">
							<div class="col-md-6 text-left">
								<p>
									<small>&copy; 2018 Panduasia. All Rights Reserved. </small>
								</p>
							</div>
						</div>
					</div>
				</div>
			</footer>

    </div>

    <div class="gototop js-top">
			<a href="#" class="js-gotop">
				<i class="icon-arrow-up"></i>
			</a>
		</div>

    @section('footer')
    @show
