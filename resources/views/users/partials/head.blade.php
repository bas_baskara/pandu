
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Panduasia &mdash; Guide others to happiness</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<meta name="author" content="Panduasia" />

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">

	<!-- ionicon -->
	<script src="https://unpkg.com/ionicons@4.1.2/dist/ionicons.js"></script>

	<link rel="stylesheet" href="{{URL::asset('public/assets/vendors/fontawesome/css/font-awesome.min.css') }}">


	<!-- Animate.css -->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/icomoon.css')}}">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/themify-icons.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/bootstrap.css')}}">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/magnific-popup.css')}}">

	

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	  crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
	  crossorigin="anonymous"></script>
	

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/owl.theme.default.min.css')}}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/style.css')}}">

	<!-- Modernizr JS -->
	<script src="{{URL::asset('public/assets/blog/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

  <link href="{{URL::asset('public/assets/blog/css/rotating-card.css')}}" rel='stylesheet' />

	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


  <link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/style-font.css')}}">

  <!-- cardourteam -->
  <link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/card-ourteam.css')}}">


	<!-- jQuery -->
	<script src="{{URL::asset('public/assets/blog/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{URL::asset('public/assets/blog/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{URL::asset('public/assets/blog/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{URL::asset('public/assets/blog/js/jquery.waypoints.min.j')}}s"></script>
	<!-- Carousel -->
	<script src="{{URL::asset('public/assets/blog/js/owl.carousel.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{URL::asset('public/assets/blog/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{URL::asset('public/assets/blog/js/magnific-popup-options.js')}}"></script>
	<!-- Main -->
	<script src="{{URL::asset('public/assets/blog/js/main.js')}}"></script>
	<script src="{{URL::asset('public/assets/blog/js/index-search.js')}}"></script>

  @section('head')
            @show
