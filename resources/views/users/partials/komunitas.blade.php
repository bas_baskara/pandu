<div class="gtco-services gtco-section">
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        
    </div>
    
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-2">
                    <img src="{{URL::asset('blog/images/avatar.jpg')}}" class="img-responsive">
                </div>
               
                <div class="col-md-5">
                <h2>{{ Auth::user()->fullname}}</h2>
                    <h6>
                        <ion-icon name="locate"></ion-icon> {{ Auth::user()->domicile}}
                    </h6>
                    <a href="" class="btn btn-primary">Chat</a>
                    </div>
                    <ul class="list-unstyled no-mb">
                        <div class="col-sm-5">
                            Ulasan
                            <div class="rating iblocks">
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                            </div>
                            <div class="col-sm-12">
                                Memiliki anggota 15 dari 30 pemandu wisata
                            </div>
                    </div>
                    </ul>
                </div>
               
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <h1>Tentang ku</h1>
                        <p>
                                {{Auth::user()->description }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <h3>Link</h3>
                            <li>
                                   
                            <a href="{{Auth::user()->url_facebook}}">Facebook</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="list-group" >
                        <a href="#" class="list-group-item active">My Account</a>
                        <a href="{{ url('komunitas/transaksi')}}" class="list-group-item">Setup profile</a>
                        <a href="#" class="list-group-item">Inbox</a>
                        <a href="{{ url('komunitas/artikel')}}" class="list-group-item">Artikel</a>
                       
                        <?php $select_grup = Request::get('grup_id')  ?>
                       
                        @foreach(App\Komunitas\grupkomunitas::all() as $grup)
                        
                        @if ($grup->ketua == Auth::user()->fullname && $grup->status == "Terverifikasi")
                        
                        <a href="{{URL('komunitas/grup/info',['grup_id' => $grup->id])}}" class="list-group-item">{{$grup->name}}</a>
                        
                        @endif
                        
                        @endforeach
                       
                        @if(Auth::user()->level == "ketua")
                                    
                        <a href="{{ url('komunitas/create-grup')}}" class="list-group-item"> Buat Grup Komunitas</a>
                        <a href="{{ url('komunitas/mymember')}}" class="list-group-item"> My Member Komunitas</a>
                        <a href="#" class="list-group-item">Transaksi member komunitas</a>
                        <a href="#" class="list-group-item">Review</a>
                       
                        @endif
                    </div>
                </div>
             
                <div class="col-md-8">
                    <div class="row">
                    </div>
                </div>
                        
@section('komunitas')
@show
