@extends('layout')
@section('content')



<header role="banner">
  <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
    <!-- Overlay -->
    <div class="overlay"></div>
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#bs-carousel" data-slide-to="1"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item slides active">
        <div class="slide-1">
          
          <img src="http://blog.airyrooms.com/wp-content/uploads/2017/12/Pulau-Padar-tour-wisata-komodo.jpg" alt="bali" class="img-banner img-top-head">
          
          <div class="carousel-caption">
            <h3 class="title-carousel">The best decision of our lives...</h3>
            <p>We had such a great time in LA!</p>
            <button class="btn carousel__btn">Selengkapnya</button>
          </div>
        </div>
      </div>
      <div class="item slides">
        <div class="slide-2">
          <div class="img-top-head">
            <img src="https://wallpaperplay.com/walls/full/4/1/d/207149.jpg" alt="bali" class="img-banner img-top-head">
          </div>
          <div class="carousel-caption">
            <h3 class="title-carousel">The best decision of our lives...</h3>
            <p>We had such a great time in LA!</p>
            <button class="btn carousel__btn">Selengkapnya</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>



<div class="container">
  
  <div class="row">
    
    <div class="cardPostView" style="padding:15px; border-radius:15px;">
      <div class="gtco-heading ">
        <a href="#" ><span class="label label-danger pull-right">logout</span></a>
        <h3 style="font-weight:800;">Hello {{Auth::user()->username}}.</h3>
        <p> Nikmati liburan-mu bersama Panduasia.</p>
        <hr>
      </div>
      <ol class="breadcrumb text-center">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Wishlist</a></li>
        <li><a href="#">Transaksi </a></li>
        <li><a href="#">Chat</a></li>
        <li><a href="#">Menunggu Pembayaran</a></li>
        <li><a href="#">Ulasan</a></li>
      </ol>
      <br>
      <br>
      <br>
    </div>
    
    <div class="col-md-8 search-wisata">
      <div class="cardPostView"  >
        <div class="bs-callout bs-callout-primary">
          <h4>Pencarian</h4>
          <form method="GET" action="{{ url('search') }}" class="form-inline">
            <div class="form-group">
              <input type="text " class="form-control col-lg-6" placeholder=' misal: "Bandung"' name="cari" style="width: 100%;">
            </div>
            <div class="form-group" style="margin-left:55px;">
              <label for="pwd">Kategori</label>
              <select class="form-control" name="cat">
                @foreach($categories as $kc => $vc)
                  <option value="{{ $vc->id }}">{{ $vc->title }}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" class="btn btn-danger "><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
    </div>
    
    
    
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
      <h2 style="font-weight:800;">Wisata Terpopuler</h2>
      <hr>
    </div>
    
    <div class="col-md-3">
      <div class="card  bg-primary ">
        <div class="social-card-header align-middle text-center ">
          <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col">
              <span class="text-muted">Bandung</span>
              <div class="text-harga">12 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">130K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3">
      <div class="card">
        <div class="social-card-header align-middle text-center ">
          <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
          alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col">
              <span class="text-muted">Yogyakarta</span>
              <div class="text-harga">16 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">220K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3">
      <div class="card  ">
        <div class="social-card-header align-middle text-center ">
          <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
          class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              <span class="text-muted">Lombok</span>
              <div class="text-harga">16 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">420K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3  ">
      <div class="card">
        <div class="social-card-header align-middle text-center ">
          <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              <span class="text-muted">Makassar</span>
              <div class="text-harga">8 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">138K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3">
      <div class="card  ">
        <div class="social-card-header align-middle text-center ">
          <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
          class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              <span class="text-muted">Lombok</span>
              <div class="text-harga">16 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">420K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3  ">
      <div class="card">
        <div class="social-card-header align-middle text-center ">
          <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
          class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              <span class="text-muted">Banten</span>
              <div class="text-harga">5 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">220K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3  ">
      <div class="card">
        <div class="social-card-header align-middle text-center ">
          <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
          class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              
              <span class="text-muted">Pangandaran</span>
              <div class="text-harga">9 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">150K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-3  ">
      <div class="card">
        <div class="social-card-header align-middle text-center ">
          <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
          class="image" alt="">
        </div>
        <hr>
        <div class="card-body text-center">
          <div class="row">
            <div class="col border-right">
              <span class="text-muted">Surabaya</span>
              <div class="text-harga">24 Wisata</div>
            </div>
            <div class="col">
              <span class="text-muted">Start from</span>
              <div class="text-harga">220K</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
  
  <center>
    <a href="#" class="btn btn-primary">Lebih banyak lagi</a>
  </center>
  
</div>

<br>


<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2 style="font-weight:800;">Artikel terbaru</h2>
        <hr>
      </div>
      
      <div class="container">
        <div class="row">
          @foreach($posts as $post)
          <div class="col-md-4">
            <div class="column">
              <!-- Post-->
              <div class="post-module">
                <!-- Thumbnail-->
                <div class="thumbnail">
                  <div class="date">
                    <div class="day">{{$post->created_at->format('d')}}</div>
                    <div class="month">{{$post->created_at->format('M')}}</div>
                  </div>
                  <img src="{{ URL::asset('www/assets/blog_images/'.$post->image)}}" alt="{{$post->title}}">
                </div>
                <!-- Post Content-->
                <div class="post-content">
                  
                  <h1 class="title">
                    <a href="{{ url('blog', $post->slug) }}">{{$post->title}}</a>
                  </h1>
                  
                  <div class="post-meta">
                    <span class="timestamp">
                      <i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
                      
                      
                      <span class="views">
                        <i class="fa fa-user"></i>
                        <a href="#"> {{$post->visitor}} Views</a>
                      </span>
                      <a href="{{ url('blog',$post->slug) }}" style="color:red;">Selengkapnya</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            <div class="col-md-12 text-center">
              <p>
                <a href="{{ url('blog') }}" class="btn btn-special">Load More</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <br>
  
  <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
		<h2 style="font-weight:800;">Rekomendasi Destinasi Wisata</h2>
    
  </div>
  
  <div class="container">
    <div class="row">
      <div class="responsive slider " style="margin-top :50px;">
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Surabaya</span>
                  <div class="font-weight-bold">24 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5 ">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Pangandaran</span>
                  <div class="font-weight-bold">9 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">150K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Banten</span>
                  <div class="font-weight-bold">5 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Bandung</span>
                  <div class="font-weight-bold">12 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">130K</div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Makassar</span>
                  <div class="font-weight-bold">8 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">138K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
              alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Lombok</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">420K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
      </div>
      
    </div>
  </div>
  
  
  
  <div class="gtco-section gtco-products" >
    <div class="gtco-container">
      <div class="row row-pb-sm">
        <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
          <h2 style="font-weight:800;">Artikel Komunitas terbaru</h2>
          <hr>
        </div>
        
        <div class="container">
          <div class="row">
            @foreach($pm as $po)
            <div class="col-sm-6 col-lg-4">
              <div class="komunitas_card home__head">
                <img src="{{ URL::asset('www/assets/communities/blog_images/'.$po->image)}}" alt="{{$po->title}}" class="home__img">
                <h5 class="home__name" >{{$po->title}}</h5>
                
                <a class="btn home__btn" href="{{ url('komunitas/blog/'.$po->slug) }}">Selengkapnya</a>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        
      </div>
    </div>
  </div>
  
  
  <div class=" gtco-heading" style="margin-left:73px;">
		<h2 style="font-weight:800;">Tempat Wisata Baru </h2>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="responsive slider " style="margin-top :50px;">
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Surabaya</span>
                  <div class="font-weight-bold">24 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5 ">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Pangandaran</span>
                  <div class="font-weight-bold">9 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">150K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Banten</span>
                  <div class="font-weight-bold">5 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Bandung</span>
                  <div class="font-weight-bold">12 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">130K</div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Makassar</span>
                  <div class="font-weight-bold">8 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">138K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
              alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Lombok</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">420K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
      </div>
      
    </div>
  </div>
  
  
  <div class=" gtco-heading " style="margin-left:73px;">
		<h2 style="font-weight:800;">Liburan ke Pantai</h2>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="responsive slider " style="margin-top :50px;">
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Surabaya</span>
                  <div class="font-weight-bold">24 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5 ">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Pangandaran</span>
                  <div class="font-weight-bold">9 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">150K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Banten</span>
                  <div class="font-weight-bold">5 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Bandung</span>
                  <div class="font-weight-bold">12 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">130K</div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Makassar</span>
                  <div class="font-weight-bold">8 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">138K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
              alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Lombok</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">420K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
      </div>
      
    </div>
  </div>
  
  <div class="gtco-heading"  style="margin-left:73px;">
		<h2 style="font-weight:800;">Liburan Keluarga</h2>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="responsive slider " style="margin-top :50px;">
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Surabaya</span>
                  <div class="font-weight-bold">24 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5 ">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Pangandaran</span>
                  <div class="font-weight-bold">9 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">150K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Banten</span>
                  <div class="font-weight-bold">5 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Bandung</span>
                  <div class="font-weight-bold">12 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">130K</div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Makassar</span>
                  <div class="font-weight-bold">8 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">138K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center">
              <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
              alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  <span class="text-muted">Yogyakarta</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">220K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3  mt-5">
          <div class="card">
            <div class="social-card-header align-middle text-center ">
              <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
              class="image" alt="">
            </div>
            <hr>
            <div class="card-body text-center">
              <div class="row">
                <div class="col border-right">
                  
                  <span class="text-muted">Lombok</span>
                  <div class="font-weight-bold">16 Wisata</div>
                </div>
                <div class="col">
                  
                  <span class="text-muted">Start from</span>
                  <div class="font-weight-bold">420K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
      </div>
      
    </div>
  </div>
  
  
  
  @endsection
  