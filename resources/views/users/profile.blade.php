@extends('layout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3" style="margin-top:15px;">
            <ul class="list-group">
              <li class="list-group-item">Lihat Profil</li>
              <li class="list-group-item">Setting</li>
              <li class="list-group-item" >
                  <a href="{{URL::to('/user/home')}}">Kembali</a></li>
            </ul>
        </div>
        <div class="col-md-8 col-lg-8 pull-right">
            <div class="panel-group">
           
                @if(session('message'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                          <strong>{{ session('message') }}</strong>
                    </div>
                    {{ session()->forget('message') }}
                @endif
               
                <div class="panel panel-default">
                    <div class="panel-heading"  style="
                    background-color : #e74c3c;
                    color : white;
                 ">Edit Profile</div>
                    <div class="panel-body">
                        
                    <form action="{{ url('user/profile/'.$usr->id.'/update') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                                               
                    <div class="form-group">
                        <label for="exampleInputUsername">Username</label>
                        <input type="text" class="form-control" name="username" value="{{ $usr->username }}" data-toggle="tooltip" data-placement="bottom" title="Username tidak bisa diganti" disabled>
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputUsername">Nama Awal</label>
                        <input type="text" class="form-control" name="first_name" value="{{ $usr->first_name }}">
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputUsername">Nama Akhir</label>
                        <input type="text" class="form-control" name="last_name" value="{{ $usr->last_name }}">
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputUsername">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ $usr->email }}">
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputUsername">No HP</label>
                        <input type="text" class="form-control" name="mobilephone" value="{{ $usr->mobilephone }}">
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputKeterangan">Alamat</label>
                        {{-- <textarea class="form-control" id="summernote" name="address" rows="3"></textarea> --}}
                        <input type="text" class="form-control" name="address" value="{{ $usr->address }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputFile">Image profile</label>
                        
                        <input type="file" id="exampleInputFile" class="form-control-file" name="profile_img" onchange="readURL(this);" accept="image/*">
                        @if(empty($usr->profile_img) || $usr->profile_img == 'null')
                            <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
                    </div>
                        @else
                        <div class="form-group">
                        <img class="preview_featured_image" src="{{ asset('www/assets/communities/avatars/'.$usr->profile_img) }}" style="width:200px">
                        </div>
                        @endif
    
                     <div class="form-group">
                        <label for="exampleInputKeterangan">Keterangan</label>
                        <textarea class="form-control" id="summernote" name="description" rows="3">{{ $usr->description }}</textarea>
                    </div>
    
                    <div class="form-group">
                        <label for="exampleInputUsername">Password <small>(*Kosongkan jika tidak ingin diganti)</small></label>
                        <input type="password" class="form-control" name="password">
                    </div>
    
                    <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Ubah</button>
    
                        
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


@endsection
