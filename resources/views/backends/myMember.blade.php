@extends('blog/app') @section('head')


@endsection 
@section('main-content') 
@include('komunitas/_layouts/komunitas')


<div class="row">
    @foreach($users->where('grup_id',$grup); as $user);
    @if($user->inGrup == '1'))
    
    <div class="col-md-6">
        <div class="business-card">
            <div class="media">
                <div class="media-left">
                    <img class="media-object img-circle profile-img" src="{{
                        URL::asset('komunitas/avatar/'.$user->image_profile
                        )}}">
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading">{{$user->fullname}}</h2>
                        <div class="job">{{$user->level}}</div>
                        <div class="mail"><a href="mailto:{{$user->email}}">{{$user->email}}</a> </div>
                        <div class="mail"><a href="mailto:{{$user->email}}"><span class="glyphicon glyphicon-phone-alt"></span>{{$user->phone}}</a> </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach   
    </div>
    
    
</div>
</div>
</div>


@endsection
