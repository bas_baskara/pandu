@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
<meta name="keywords" content="Pandu Asia Blog" />
@endsection
@section('main-content')
@include('komunitas/_layouts/komunitas')
<div class="flex-container_1" style="padding:35px;">
  <form action="{{ route('updateprofile',$user->id) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <div class="form-group">
      <label>Nama Lengkap</label>
      <input type="text" name="fullname" class="form-control" value="{{ $user->fullname }}">
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" class="form-control" value="{{ $user->email }}">
    </div>
    <div class="form-group">
      <label>Password</label>
      <input type="password" name"password" class="form-control">
    </div>
    <div class="form-group">
      <label>No KTP</label>
      <input type="text" name="no_ktp" class="form-control" value="{{ $user->no_ktp }}">
    </div>
    <div class="form-group">
      <label>TTL(Place)</label>
      <input type="text" name="place" class="form-control" value="{{ $user->place }}">
    </div>
    <div class="form-group">
      <label>TTL(Date)</label>
      <input type="text" name="date_of_birth" class="form-control" value="{{ $user->date_of_birth }}">
    </div>
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="address" class="form-control" value="{{ $user->address }}">
    </div>
    <div class="form-group">
      <label>Domisili</label>
      <input type="text"name="domicile" class="form-control" value="{{ $user->domicile }}">
    </div>
    <div class="form-group">
      <label>No Telp</label>
      <input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
    </div>
    <div class="form-group">
      <label>Url Facebook</label>
      <input type="text" name="url_facebook" class="form-control" value="{{ $user->url_facebook }}">
    </div>
    <div class="form-group">
      <label>Description</label>
      <textarea class="form-control" name="description" rows="3"></textarea>
    </div>

    <div class="form-group">
      <label>Foto Profile</label>
      <input type="file" name="image_profile" value="">
    </div>

    <div class="form-group">
      <input type="submit" class="btn btn-success" value="Ubah profile">
    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection
