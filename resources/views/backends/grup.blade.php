@extends('blog/app') @section('head')

@endsection 
@section('main-content')
@include('komunitas/_layouts/komunitas')

<div class="col-md-8">
    <div class="panel panel-default">
        
        <div class="panel-body">
            <div class="col-md-4">
                <img src="{{$users->grupkomunitas->first()->img_profile}}" class="img-responsive">
            </div>
            <span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="float: right;"></span>
            <div class="col-md-5"> 
                
                <h2>{{$grups->name}} </h2> 
                <h5><span class="glyphicon glyphicon-map-marker"  aria-hidden="true"></span> {{$grups->place}}
                </h5>
                <h5><span class="glyphicon glyphicon-user"  aria-hidden="true"></span> {{$grups->ketua}} (Admin)
                </h5>
                
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="business-card">
        <div class="media">
            <div class="media-left">
                <img class="media-object img-circle profile-img" src="  {{
                    URL::asset('komunitas/avatar/'.$users->image_profile
                    )}}">
                </div>
                <div class="media-body">
                    <h2 class="media-heading"><strong>{{$users->fullname}}</strong></h2>
                    @if($users->level =="ketua")
                    <div class="job"><span class="glyphicon glyphicon-user"></span> Ketua</div>
                    @endif
                    <div class="mail"><span class="glyphicon glyphicon-envelope"></span> {{$users->email}}</div>
                </div>
            </div>
        </div>
    
    
    
        @foreach($ingrups->where('inGrup',0) as $ingrup)
        @if($ingrup->grup_id !== NULL)
        <div class="business-card">
            <div class="media">
                <div class="media-left">
                    <img class="media-object img-circle profile-img" src="{{
                        URL::asset('komunitas/avatar/'.$ingrup->image_profile
                        )}}">
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading"><strong>{{$ingrup->fullname}}</strong></h2>
                        <div class="mail"><span class="glyphicon glyphicon-envelope"></span> {{$ingrup->email}}</div>
                        <div class="mail"><span class="glyphicon glyphicon-map-marker"></span> {{$ingrup->place}}</div>
                        <div class="mail">{{$ingrup->description}}</div>
                        
                    </div>
                    <div style="float:right;">
                        <a href="{{URL('/komunitas/grup/info/'.$ingrup->id.'/terima')}}" class="glyphicon glyphicon-ok"></a>
                        <a href="{{URL('/komunitas/grup/info/'.$ingrup->id.'/tolak')}}" class="glyphicon glyphicon-remove"></a>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        
    </div>    
    
</div>
</div>
</div>
</div>
</div>

@endsection