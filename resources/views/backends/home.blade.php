@extends('blog/app') 
@section('head')

@endsection 
@section('main-content')
@include('komunitas/_layouts/komunitas')

<div class="col-md-8">
    <div class="card">
        
        @include('includes.messages')
        <div class="card-body" style="padding:25px;">
            <div class="card-header">Dashboard</div>
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <h2 style="color:  #273746  ;">You are logged in! Hi {{Auth::user()->fullname}}</h2> 
    <a href="{{URL('komunitas/create/post')}}" class="btn btn-primary" ><span class="glyphicon glyphicon-pencil"></span>  Buat artikel</a>
    </div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

@endsection
