@extends('blog/app')
@section('head')

	<meta name="title" content="{{$postskomunitas->meta_title}}" />
	<meta name="description" content="{{ $postskomunitas->meta_desc }}" />
	<meta name="keywords" content="{{$postskomunitas->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$postskomunitas->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$postskomunitas->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$postskomunitas->meta_desc}}" />
	
@endsection
@section('main-content')

<div class="containerPostView">

	<div class="cardPostView">
		<img src="{{ URL::asset('komunitas_artikel/'.$postskomunitas->image)}}" alt="{{$postskomunitas->title}}" class="bannerpost">
		<div class="containerPostView">
			<div class="texturl">
      
        <a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/komunitas')}}">Komunitas /</a>

				{{-- @foreach ($postskomunitas->categories as $category)
				<a href="{{ url('home/blog/'.$category->name)}}">{{$category->name}} </a>
				@endforeach --}}

			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title">{{$postskomunitas->title}}</h1>
				<h5 class="timetext">
					<span class="glyphicon glyphicon-time"></span>{{$postskomunitas->created_at}}</h5>
					<p class="bodycontent">
						{!! htmlspecialchars_decode($postskomunitas->description) !!}
					</p>
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
						@foreach ($users as $user)
						@if ($postskomunitas->posted_by == $user['fullname'])
						<img src="{{ URL::asset('komunitas/avatar/'.$user->img_profile)}}" alt="author" class="authorimg">
						@endif
						@endforeach
						<p>by {{$postskomunitas->posted_by}}</p>
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		<div class="commentBox">
			<div class="fb-comments" postskomunitass-href="{{ Request::url() }}" postskomunitass-numpostskomunitas="10" postskomunitass-width="100%"></div>
			</div>
			
		<br>
		<div class="container">
        <div class="row" >
        <div class="col-6">
		@if(isset($previous))
		<a href="{{ url('/home/blog/'.$previous->slug) }}" class="btn btn-success pull-left post-nav">Previous ({{$previous->title}})</a>
		@endif
		</div>
		<div class="col-6">
		@if(isset($next))
		<a href="{{ url('/home/blog/'.$next->slug) }}" class="btn btn-success pull-right post-nav">Next ({{$next->title}})</a>
		@endif
		</div>
		</div>
		</div>
	</div>
</div>


<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=214610849349025&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection
