<div class="gtco-loader"></div>

<div id="page">

  <nav class="gtco-nav" role="navigation">
    <div class="gtco-container">

      <div class="row">
        <div class="col-sm-2 col-xs-12">
          <div id="gtco-logo">
            <a href="{{URL::to('/')}}">
              <img src="{{URL::asset('public/assets/blog/images/logo.png')}}" alt="logo panduasia">
            </a>
          </div>
        </div>
        <div class="col-xs-10 text-right menu-1"  style="margin-top:15px;">
          <ul>
            <li class="active">
              <a href="{{URL::to('/')}}">Home</a>
            </li>
			
            <li class="has-dropdown">
              <a href="#">Cara kerja</a>
              <ul class="dropdown">
                <li><a href="">Sub menu 1</a></li>
                <li><a href="">Sub menu 2</a></li>
                <li><a href="">Sub menu 3</a></li>
              </ul>
            </li>
			
            <li>
              <a href="{{URL::to('/blog')}}">Blog</a>
            </li>
            <li>
              <a href="#">Komunitas</a>
            </li>
            <li>
              <a href="#">Be a Tourguide</a>
            </li>
            <li>
              <a href="{{URL::to('/about')}}">About</a>
            </li>
            <li class="has-dropdown">
              <a href="">Masuk</a>
              <ul class="dropdown">
                <li>
                  <a href="javascript:;">Login</a>
                  <a href="javascript:;">Register</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

@section('header')
@show
