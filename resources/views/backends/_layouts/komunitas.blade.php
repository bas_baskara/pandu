<div class="gtco-services gtco-section">
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        
    </div>
    
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-2">
                    @if(Auth::user()->image_profile == NULL)
                    <img src="{{URL::asset('blog/images/avatar.jpg')}}"
                     class="img-responsive">
                     @else 
                     <img src="{{
                     URL::asset('komunitas/avatar/'.Auth::user()->image_profile
                     )}}"
                     class="img-responsive">
                     @endif

                </div>
               
                <div class="col-md-5">
                <h2>{{ Auth::user()->fullname}}</h2>
                    <h6>
                        <span class="glyphicon glyphicon-map-marker"></span> {{ Auth::user()->domicile}}
                    </h6>
                    <a href="" class="btn btn-primary">Chat</a>
                    </div>
                    <ul class="list-unstyled no-mb">
                        <div class="col-sm-5">
                            Ulasan
                            <div class="rating iblocks">
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                                <ion-icon name="star-outline"></ion-icon>
                            </div>
                        
                            <div class="col-sm-12">
                                Grup {{Auth::user()->grup_id}} 
                                @if(!Auth::user()->grup_id == NULL)
                                {{-- member {{Auth::user()->grupkomunitas->id}} --}}
                                @endif
                            </div>
                    </div>
                    </ul>
                </div>
               
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <h1>Tentang ku</h1>
                        <p>
                                {{Auth::user()->description }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <h3>Link</h3>
                            <li>
                            <a href="{{Auth::user()->url_facebook}}">Facebook</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="list-group" >
                    <a href="{{URL('komunitas/home')}}" class="list-group-item active"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span>  My Account</a>
                        <a href="{{URL('komunitas/profile',Auth::user()->id)}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Setup profile</a>
                      
                        @if(Auth::user()->grup_id == NULL && Auth::user()->level == "member")
                       <a href="{{URL('komunitas/search-grup')}}" class="list-group-item"><span class="glyphicon glyphicon-search " aria-hidden="true"></span> Cari Grup</a>
                        @endif
                        <a href="{{ url('komunitas/create/post')}}" class="list-group-item"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Artikel</a>
                       
                       
                            @if(!Auth::user()->grup_id == NULL && Auth::user()->level == "member")
                                <a href="{{ url('komunitas/grup/mymembers')}}" class="list-group-item"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Grup</a>
                            @endif
                       
                        <?php $select_grup = Request::get('grup_id')  ?>
                       
                        @foreach(App\Komunitas\grupkomunitas::all() as $grup)
                            
                        @if ($grup->ketua == Auth::user()->fullname && $grup->status == "Terverifikasi")
                        
                        <a href="{{URL('komunitas/grup/info',['grup_id' => $grup->id])}}" class="list-group-item"><span class="glyphicon glyphicon-tag"></span> {{$grup->name}}</a>
                        
                         @endif
                        
                        @endforeach
                       
                        @if(Auth::user()->level == "ketua")
                            @if(Auth::user()->grup_id == NULL)            
                                <a href="{{ url('komunitas/create-grup')}}" class="list-group-item"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Buat Grup Komunitas</a>
                                
                            @endif
                        
                            @if(!Auth::user()->grup_id == NULL )
                         
                             <a href="{{ url('komunitas/grup/mymembers')}}" class="list-group-item"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Member Komunitas</a>
                          
                            @endif
                        
                            {{-- <a href="#" class="list-group-item">Transaksi member komunitas</a>
                        <a href="#" class="list-group-item">Review</a> --}}
                       
                        @endif
                    </div>
                </div>
             
                <div class="col-md-8">
                    <div class="row">
                    </div>
                </div>
                        
@section('komunitas')
@show
