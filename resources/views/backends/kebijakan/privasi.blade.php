@extends('blog/app')
@section('head')

	{{-- <meta name="title" content="{{$posts->meta_title}}" />
	<meta name="description" content="{{ $posts->meta_desc }}" />
	<meta name="keywords" content="{{$posts->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$posts->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$posts->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$posts->meta_desc}}" /> --}}
	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($posts as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('image_post/'.$posts->image)}}" alt="{{$posts->title}}" class="bannerpost"> --}}
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/blog/')}}">Blog /</a>

			
				<a href=""></a>
			
			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title"> Kebijakan Privasi</h1>
			
					<p class="bodycontent">
            Kebijakan Privasi
________________________________________
Adanya Kebijakan Privasi ini adalah komitmen nyata dari Panduasia.com untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs www.panduasia.com, situs-situs turunannya, serta aplikasi gawai Panduasia.com (selanjutnya disebut sebagai "Situs").
Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs Panduasia.com sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada Panduasia.com atau yang Panduasia.com kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya disebut sebagai "data").
Dengan mengakses dan/atau mempergunakan layanan Panduasia.com, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna memberikan persetujuan kepada Panduasia.com untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun 
<h6>Syarat dan Ketentuan Panduasia.com.</h6>

1.	Perolehan dan Pengumpulan Data Pengguna
2.	Penggunaan Data
3.	Pengungkapan Data Pribadi Pengguna
4.	Cookies
5.	Pilihan Pengguna dan Transparansi
6.	Penyimpanan dan Penghapusan Informasi
7.	Pembaruan Kebijakan Privasi
________________________________________
<h6>Perolehan dan Pengumpulan Data Pengguna</h6>
Panduasia.com mengumpulkan data Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:
1.	Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas pada data yang diserahkan pada saat Pengguna:
1.	Membuat atau memperbarui akun Panduasia.com, termasuk diantaranya nama pengguna (username), alamat email, nomor telepon, password, alamat, foto, dan lain-lain;
2.	Menghubungi Panduasia.com, termasuk melalui layanan konsumen;
3.	Mengisi survei yang dikirimkan oleh Panduasia.com atau atas nama Panduasia.com;
4.	Melakukan interaksi dengan Pengguna lainnya melalui fitur pesan, diskusi produk, ulasan, rating, Pusat Resolusi dan sebagainya;
5.	Mempergunakan layanan-layanan pada Situs, termasuk data transaksi yang detil, diantaranya jenis, jumlah dan/atau keterangan dari produk atau layanan yang dibeli, alamat pengiriman, kanal pembayaran yang digunakan, jumlah transaksi, tanggal dan waktu transaksi, serta detil transaksi lainnya;
6.	Mengisi data-data pembayaran pada saat Pengguna melakukan aktivitas transaksi pembayaran melalui Situs, termasuk namun tidak terbatas pada data rekening bank, kartu kredit, virtual account, instant payment, internet banking, gerai ritel; dan/atau
7.	Menggunakan fitur yang membutuhkan izin akses terhadap perangkat Pengguna.
2.	Data yang terekam pada saat Pengguna mempergunakan Situs, termasuk namun tidak terbatas pada:
1.	Data lokasi riil atau perkiraannya seperti alamat IP, lokasi Wi-Fi, geo-location, dan sebagainya;
2.	Data berupa waktu dari setiap aktivitas Pengguna, termasuk aktivitas pendaftaran, login, transaksi, dan lain sebagainya;
3.	Data penggunaan atau preferensi Pengguna, diantaranya interaksi Pengguna dalam menggunakan Situs, pilihan yang disimpan, serta pengaturan yang dipilih. Data tersebut diperoleh menggunakan cookies, pixel tags, dan teknologi serupa yang menciptakan dan mempertahankan pengenal unik;
4.	Data perangkat, diantaranya jenis perangkat yang digunakan untuk mengakses Situs, termasuk model perangkat keras, sistem operasi dan versinya, perangkat lunak, nama file dan versinya, pilihan bahasa, pengenal perangkat unik, pengenal iklan, nomor seri, informasi gerakan perangkat, dan/atau informasi jaringan seluler;
5.	Data catatan (log), diantaranya catatan pada server yang menerima data seperti alamat IP perangkat, tanggal dan waktu akses, fitur aplikasi atau laman yang dilihat, proses kerja aplikasi dan aktivitas sistem lainnya, jenis peramban, dan/atau situs atau layanan pihak ketiga yang Anda gunakan sebelum berinteraksi dengan Situs.
3.	Data yang diperoleh dari sumber lain, termasuk:
1.	Mitra usaha Panduasia.com yang turut membantu Panduasia.com dalam mengembangkan dan menyajikan layanan-layanan dalam Situs kepada Pengguna, antara lain mitra penyedia layanan pembayaran, logistik atau kurir, infrastruktur situs, dan mitra-mitra lainnya.
2.	Mitra usaha Panduasia.com tempat Pengguna membuat atau mengakses akun Panduasia.com, seperti layanan media sosial, atau situs/aplikasi yang menggunakan API Panduasia.com atau yang digunakan Panduasia.com;
3.	Penyedia layanan finansial
4.	Penyedia layanan pemasaran;
5.	Sumber yang tersedia secara umum.
Panduasia.com dapat menggabungkan data yang diperoleh dari sumber tersebut dengan data lain yang dimilikinya.
kembali ke atas
________________________________________
<h6>Penggunaan Data</h6>
Panduasia.com dapat menggunakan keseluruhan atau sebagian data yang diperoleh dan dikumpulkan dari Pengguna sebagaimana disebutkan dalam bagian sebelumnya untuk hal-hal sebagai berikut:
1.	Memproses segala bentuk permintaan, aktivitas maupun transaksi yang dilakukan oleh Pengguna melalui Situs, termasuk untuk keperluan pengiriman produk kepada Pengguna.
2.	Penyediaan fitur-fitur untuk memberikan, mewujudkan, memelihara dan memperbaiki produk dan layanan kami, termasuk:
1.	Menawarkan, memperoleh, menyediakan, atau memfasilitasi layanan marketplace, asuransi, pembiayaan, pinjaman, maupun produk-produk lainnya melalui Situs;
2.	Memungkinkan fitur untuk mempribadikan akun Panduasia.com Pengguna, seperti Wishlist; dan/atau
3.	Melakukan kegiatan internal yang diperlukan untuk menyediakan layanan pada situs/aplikasi Panduasia.com, seperti pemecahan masalah software, bug, permasalahan operasional, melakukan analisis data, pengujian, dan penelitian, dan untuk memantau dan menganalisis kecenderungan penggunaan dan aktivitas.
3.	Membantu Pengguna pada saat berkomunikasi dengan Layanan Pelanggan Panduasia.com, diantaranya untuk:
1.	Memeriksa dan mengatasi permasalahan Pengguna;
2.	Mengarahkan pertanyaan Pengguna kepada petugas Layanan Pelanggan yang tepat untuk mengatasi permasalahan; dan
3.	Mengawasi dan memperbaiki tanggapan Layanan Pelanggan Panduasia.com.
4.	Menghubungi Pengguna melalui email, surat, telepon, fax, dan lain-lain, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses transaksi maupun proses penyelesaian kendala.
5.	Menggunakan informasi yang diperoleh dari Pengguna untuk tujuan penelitian, analisis, pengembangan dan pengujian produk guna meningkatkan keamanan dan keamanan layanan-layanan pada Situs, serta mengembangkan fitur dan produk baru.
6.	Menginformasikan kepada Pengguna terkait produk, layanan, promosi, studi, survei, berita, perkembangan terbaru, acara dan lain-lain, baik melalui Situs maupun melalui media lainnya. Panduasia.com juga dapat menggunakan informasi tersebut untuk mempromosikan dan memproses kontes dan undian, memberikan hadiah, serta menyajikan iklan dan konten yang relevan tentang layanan Panduasia.com dan mitra usahanya.
7.	Melakukan monitoring ataupun investigasi terhadap transaksi-transaksi mencurigakan atau transaksi yang terindikasi mengandung unsur kecurangan atau pelanggaran terhadap Syarat dan Ketentuan atau ketentuan hukum yang berlaku, serta melakukan tindakan-tindakan yang diperlukan sebagai tindak lanjut dari hasil monitoring atau investigasi transaksi tersebut.
8.	Dalam keadaan tertentu, Panduasia.com mungkin perlu untuk menggunakan ataupun mengungkapkan data Pengguna untuk tujuan penegakan hukum atau untuk pemenuhan persyaratan hukum dan peraturan yang berlaku, termasuk dalam hal terjadinya sengketa atau proses hukum antara Pengguna dan Panduasia.com.
kembali ke atas
________________________________________
<h6>Pengungkapan Data Pribadi Pengguna</h6>
Panduasia.com menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda, kecuali dalam hal-hal sebagai berikut:
1.	Dibutuhkan adanya pengungkapan data Pengguna kepada mitra atau pihak ketiga lain yang membantu Panduasia.com dalam menyajikan layanan pada Situs dan memproses segala bentuk aktivitas Pengguna dalam Situs, termasuk memproses transaksi, verifikasi pembayaran, pengiriman produk, dan lain-lain.
2.	Panduasia.com dapat menyediakan informasi yang relevan kepada mitra usaha sesuai dengan persetujuan Pengguna untuk menggunakan layanan mitra usaha, termasuk diantaranya aplikasi atau situs lain yang telah saling mengintegrasikan API atau layanannya, atau mitra usaha yang mana Panduasia.com telah bekerjasama untuk mengantarkan promosi, kontes, atau layanan yang dikhususkan
3.	Dibutuhkan adanya komunikasi antara mitra usaha Panduasia.com (seperti penyedia logistik, pembayaran, dan lain-lain) dengan Pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.
4.	Panduasia.com dapat menyediakan informasi yang relevan kepada vendor, konsultan, mitra pemasaran, firma riset, atau penyedia layanan sejenis.
5.	Pengguna menghubungi Panduasia.com melalui media publik seperti blog, media sosial, dan fitur tertentu pada Situs, komunikasi antara Pengguna dan Panduasia.com mungkin dapat dilihat secara publik.
6.	Panduasia.com dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama Panduasia.com.
7.	Panduasia.com mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat penegak hukum.
kembali ke atas
________________________________________
<h6>Cookies</h6>
1.	Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.
2.	Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.
3.	Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke Situs).
4.	Panduasia.com menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna dan lain-lain, akan kami gunakan untuk pengembangan Situs dan konten Panduasia.com. Jika tidak ingin data Anda terlacak oleh Google Analytics, Anda dapat menggunakan Add-On Google Analytics Opt-Out Browser.
5.	Panduasia.com dapat menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan layanan dan konten Panduasia.com, termasuk diantaranya ialah penyesuaian dan penyajian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat mengaturnya melalui browser.
kembali ke atas
________________________________________
<h6>Pilihan Pengguna dan Transparansi</h6>
1.	Perangkat seluler pada umumnya (iOS, Android, dan sebagainya) memiliki pengaturan sehingga aplikasi Panduasia.com tidak dapat mengakses data tertentu tanpa persetujuan dari Pengguna. Perangkat iOS akan memberikan pemberitahuan kepada Pengguna saat aplikasi Panduasia.com pertama kali meminta akses terhadap data tersebut, sedangkan perangkat Android akan memberikan pemberitahuan kepada Pengguna saat aplikasi Panduasia.com pertama kali dimuat. Dengan menggunakan aplikasi dan memberikan akses terhadap aplikasi, Pengguna dianggap memberikan persetujuannya terhadap Kebijakan Privasi.
2.	Setelah transaksi jual beli melalui marketplace berhasil, Penjual maupun Pembeli memiliki kesempatan untuk memberikan penilaian dan ulasan terhadap satu sama lain. Informasi ini mungkin dapat dilihat secara publik dengan persetujuan Pengguna.
3.	Pengguna dapat mengakses dan mengubah informasi berupa alamat email, nomor telepon, tanggal lahir, jenis kelamin, daftar alamat, metode pembayaran, dan rekening bank melalui fitur Pengaturan pada Situs.
4.	Pengguna dapat menarik diri dari informasi atau notifikasi berupa buletin, ulasan, diskusi produk, pesan pribadi, atau pesan pribadi dari Admin yang dikirimkan oleh Panduasia.com melalui fitur Pengaturan pada Situs. Panduasia.com tetap dapat mengirimkan pesan atau email berupa keterangan transaksi atau informasi terkait akun Pengguna.
5.	Sejauh diizinkan oleh ketentuan yang berlaku, Pengguna dapat menghubungi Panduasia.com untuk melakukan penarikan persetujuan terhadap perolehan, pengumpulan, penyimpanan, pengelolaan dan penggunaan data Pengguna. Apabila terjadi demikian maka Pengguna memahami konsekuensi bahwa Pengguna tidak dapat menggunakan layanan Situs maupun layanan Panduasia.com lainnya.
kembali ke atas
________________________________________
<h6>Penyimpanan dan Penghapusan Informasi </h6>
Panduasia.com akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.
kembali ke atas
________________________________________
<h6>Pembaruan Kebijakan Privasi</h6>
Panduasia.com dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. Panduasia.com menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs maupun layanan Panduasia.com lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.

              
          </p>
          
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
					
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		
		<br>
	
	</div>
</div>


@endsection
