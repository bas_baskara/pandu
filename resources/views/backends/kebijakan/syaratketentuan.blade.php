@extends('blog/app')
@section('head')

	{{-- <meta name="title" content="{{$posts->meta_title}}" />
	<meta name="description" content="{{ $posts->meta_desc }}" />
	<meta name="keywords" content="{{$posts->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$posts->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$posts->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$posts->meta_desc}}" /> --}}
	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($posts as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('image_post/'.$posts->image)}}" alt="{{$posts->title}}" class="bannerpost"> --}}
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/blog/')}}">Blog /</a>

			
				<a href=""></a>
			
			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title"> Syarat & Ketentuan</h1>
					<p class="bodycontent">
________________________________________
<h6>Selamat datang di www.panduasia.com</h6>
Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh Pandu Asia Indonesia terkait penggunaan situs www.panduasia.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
Dengan mendaftar dan/atau menggunakan situs www.panduasia.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyutujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan Pandu Asia Indonesia. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.panduasia.com.
A.	Definisi
B.	Akun, Saldo Panduasia, Password dan Keamanan
C.	Transaksi Pembelian
D.	Transaksi Penjualan
E.	Penataan Open trip
F.	Komisi
G.	Harga
H.	Konten
I.	Kartu Kredit
J.	Promo
K.	Penarikan Dana
L.	Ketentuan Lain
M.	Penolakan Jaminan Dan Batasan Tanggung Jawab
N.	Pelepasan
O.	Ganti Rugi
P.	Pilihan Hukum
Q.	Pembaharuan
________________________________________
<h6>A. DEFINISI</h6>
1.	Pandu Asia Indonesia adalah suatu startup yang menjalankan kegiatan usaha jasa web portal www.panduasia.com, yakni situs pencarian pemandu wisata di berbagai wilayah di Indonesia. Selanjutnya disebut Panduasia.
2.	Situs Panduasia adalah www.panduasia.com.
3.	Syarat & ketentuan adalah perjanjian antara Pengguna dan Panduasia yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab pengguna dan Panduasia, serta tata cara penggunaan sistem layanan Panduasia.
4.	Pengguna adalah pihak yang menggunakan layanan Panduasia, termasuk namun tidak terbatas pada pembeli, penjual ataupun pihak lain yang sekedar berkunjung ke Situs Panduasia.
5.	Pembeli adalah Pengguna terdaftar yang melakukan permintaan atas jasa yang dijual oleh Penjual di Situs Panduasia.
6.	Penjual adalah Pengguna terdaftar yang melakukan tindakan buka jasa pemandu dan/atau melakukan penawaran atas suatu jasa kepada para Pengguna Situs Panduasia.
7.	Rekening Resmi Panduasia adalah rekening bersama yang disepakati oleh Panduasia dan para pengguna untuk proses transaksi jual beli di Situs Panduasia. Rekening resmi Panduasia dapat ditemukan di halaman https://www.panduasia.com/bantuan/nomor-rekening-panduasia/.
 kembali ke atas
________________________________________
<h6>B. AKUN, SALDO PANDUASIA, PASSWORD DAN KEAMANAN</h6>
1.	Pengguna dengan ini menyatakan bahwa pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.
2.	Panduasia tidak memungut biaya pendaftaran kepada Pengguna.
3.	Pengguna yang telah mendaftar berhak bertindak sebagai:
o	- Pembeli
o	- Penjual, dengan memanfaatkan layanan buka jasa pemandu.
4.	Pengguna yang akan bertindak sebagai Penjual diwajibkan memilih pilihan menggunakan layanan buka jasa pemandu. Setelah menggunakan layanan buka jasa, Pengguna berhak melakukan pengaturan terhadap open trip yang akan diperdagangkan di open trip pribadi Pengguna.
5.	Panduasia tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat & ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa penghapusan Jasa, moderasi jasa, penutupan jasa, pembatalan listing, suspensi akun, dan/atau penghapusan akun pengguna.
6.	Panduasia memiliki kewenangan untuk menutup jasa atau akun Pengguna baik sementara maupun permanen apabila didapati adanya tindakan kecurangan dalam bertransaksi dan/atau pelanggaran terhadap syarat dan ketentuan Panduasia.
7.	Pengguna dilarang untuk menciptakan dan/atau menggunakan perangkat, software, fitur dan/atau alat lainnya yang bertujuan untuk melakukan manipulasi pada sistem Panduasia, termasuk namun tidak terbatas pada : (i) manipulasi data Jasa; (ii) kegiatan perambanan (crawling/scraping); (iii) kegiatan otomatisasi dalam transaksi, jual beli, promosi, dsb; (v) penambahan open trip ke open trip; dan/atau (vi) aktivitas lain yang secara wajar dapat dinilai sebagai tindakan manipulasi sistem.
8.	Panduasia memiliki kewenangan untuk melakukan penyesuaian jumlah transaksi jasa, penyesuaian jumlah reputasi, dan/atau melakukan proses moderasi/menutup akun Pengguna, jika diketahui atau diduga adanya kecurangan oleh Pengguna yang bertujuan memanipulasi data transaksi Pengguna demi meningkatkan reputasi jasa (review dan atau jumlah transaksi). Contohnya adalah melakukan proses belanja ke jasa sendiri dengan menggunakan akun pribadi atau akun pribadi lainnya.
9.	Panduasia memiliki kewenangan untuk melakukan pembekuan saldo Pengguna apabila ditemukan / diduga adanya tindak kecurangan dalam bertransaksi dan/atau pelanggaran terhadap syarat dan ketentuan Panduasia.
10.	Penjual dilarang melakukan duplikasi jasa, duplikasi open trip, atau tindakan-tindakan lain yang dapat diindikasikan sebagai usaha persaingan tidak sehat.
11.	Pengguna tidak memiliki hak untuk mengubah nama akun, nama jasa dan/atau domain jasa Pengguna.
12.	Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password untuk semua aktivitas yang terjadi dalam akun Pengguna.
13.	Panduasia tidak akan meminta username, password maupun kode SMS verifikasi atau kode OTP milik akun Pengguna untuk alasan apapun, oleh karena itu Panduasia menghimbau Pengguna agar tidak memberikan data tersebut maupun data penting lainnya kepada pihak yang mengatasnamakan Panduasia atau pihak lain yang tidak dapat dijamin keamanannya.
14.	Pengguna setuju untuk memastikan bahwa Pengguna keluar dari akun di akhir setiap sesi dan memberitahu Panduasia jika ada penggunaan tanpa izin atas sandi atau akun Pengguna.
15.	Pengguna dengan ini menyatakan bahwa Panduasia tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun Pengguna yang diakibatkan oleh kelalaian Pengguna, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi (OTP), password atau email kepada pihak lain, maupun kelalaian Pengguna lainnya yang mengakibatkan kerugian ataupun kendala pada akun Pengguna.
16.	Pengguna memahami dan menyetujui bahwa untuk mempergunakan fasilitas keamanan one time password (OTP) maka penyedia jasa telekomunikasi terkait dapat sewaktu-waktu mengenakan biaya kepada Pengguna dengan nominal sebagai berikut (i) Rp 500 ditambah pajak 10% untuk Indosat, Tri, XL, Smartfren, dan Esia; (ii) Rp 200 ditambah pajak 10% untuk Telkomsel.
17.	Penjual dilarang mempromosikan jasa dan/atau Open trip secara langsung menggunakan fasilitas pesan pribadi, diskusi open trip, ulasan open trip yang dapat mengganggu kenyamanan Pengguna lain.
 kembali ke atas
________________________________________
<h6>C. TRANSAKSI PEMBELIAN</h6>
1.	Pembeli wajib bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh Panduasia. Pembeli melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya telah dipilih oleh Pembeli, dan kemudian Panduasia akan meneruskan dana ke pihak Penjual apabila tahapan transaksi jual beli pada sistem Panduasia telah selesai.
2.	Saat melakukan order jasa pemandu, Pembeli menyetujui bahwa:
o	- Pembeli bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan Open trip (termasuk didalamnya namun tidak terbatas pada lokasi, tambahan lainnya pada open trip) sebelum membuat tawaran atau komitmen untuk membeli.
o	- Pengguna masuk ke dalam kontrak yang mengikat secara hukum untuk membeli Open trip ketika Pengguna membeli suatu open trip.
o	- Panduasia tidak mengalihkan kepemilikan secara hukum atas open trip-open trip dari Penjual kepada Pembeli.
3.	Pembeli memahami dan menyetujui bahwa ketersediaan jasa (jadwal) merupakan tanggung jawab Penjual yang menawarkan Jasa tersebut. Terkait ketersediaan Jasa dapat berubah sewaktu-waktu, sehingga dalam keadaan full schedule open trip, maka penjual akan menolak order, dan pembayaran atas open trip yang bersangkutan dikembalikan kepada Pembeli.
4.	Pembeli memahami sepenuhnya dan menyetujui bahwa segala transaksi yang dilakukan antar Pembeli dan Penjual selain melalui Rekening Resmi Panduasia dan/atau tanpa sepengetahuan Panduasia (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs Panduasia atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pembeli.
5.	Panduasia memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih dahulu.
6.	Pembayaran oleh Pembeli wajib dilakukan segera (selambat-lambatnya dalam batas waktu 2 hari) setelah Pembeli melakukan check-out. Jika dalam batas waktu tersebut pembayaran atau konfirmasi pembayaran belum dilakukan oleh pembeli, Panduasia memiliki kewenangan untuk membatalkan transaksi dimaksud. Pengguna tidak berhak mengajukan klaim atau tuntutan atas pembatalan transaksi tersebut.
7.	Konfirmasi pembayaran dengan setoran tunai wajib disertai dengan berita pada slip setoran berupa nomor invoice dan nama. Konfirmasi pembayaran dengan setoran tunai tanpa keterangan tidak akan diproses oleh Panduasia.
8.	Pembeli menyetujui untuk tidak memberitahukan atau menyerahkan bukti pembayaran dan/atau data pembayaran kepada pihak lain selain Panduasia. Dalam hal terjadi kerugian akibat pemberitahuan atau penyerahan bukti pembayaran dan/atau data pembayaran oleh Pembeli kepada pihak lain, maka hal tersebut akan menjadi tanggung jawab Pembeli.
9.	Pembeli wajib melakukan konfirmasi pelaksanaan open trip, setelah menerima jasa open trip. Panduasia memberikan batas waktu 2 (dua) hari setelah open trip berstatus "selesai" pada sistem Panduasia, untuk Pembeli melakukan konfirmasi penyelesaian open trip. Jika dalam batas waktu tersebut tidak ada konfirmasi atau klaim dari pihak Pembeli, maka dengan demikian Pembeli menyatakan menyetujui dilakukannya konfirmasi selesainya open trip secara otomatis oleh sistem Panduasia.
10.	Setelah adanya konfirmasi jasa open trip atau konfirmasi penyelesaian open trip otomatis, maka dana pihak Pembeli yang dikirimkan ke Rekening resmi Panduasia akan di lanjut kirimkan ke pihak Penjual (transaksi dianggap selesai).
11.	Pembeli memahami dan menyetujui bahwa setiap klaim yang dilayangkan setelah adanya konfirmasi / konfirmasi otomatis penyelesaian jasa open trip adalah bukan menjadi tanggung jawab Panduasia. Kerugian yang timbul setelah adanya konfirmasi/konfirmasi otomatis penyelesaian jasa open trip menjadi tanggung jawab Pembeli secara pribadi.
12.	Pembeli memahami dan menyetujui bahwa setiap masalah pembatalan open trip yang disebabkan keterlambatan pembayaran adalah merupakan tanggung jawab dari Pembeli.
13.	Pembeli memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang Pembeli pergunakan dengan bank Rekening resmi Panduasia adalah tanggung jawab Pembeli secara pribadi.
14.	Pengembalian dana dari Panduasia kepada Pembeli hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
o	- Kelebihan pembayaran dari Pembeli atas harga Open trip,
o	- Penjual tidak bisa menyanggupi order karena full schedule open trip, maupun penyebab lainnya,
o	- Penjual sudah menyanggupi open trip
o	- Penyelesaian permasalahan melalui Pusat Resolusi berupa keputusan untuk pengembalian dana kepada Pembeli atau hasil keputusan dari pihak Panduasia.
15.	Apabila terjadi proses pengembalian dana, maka pengembalian akan dilakukan melalui Saldo Panduasia milik Pengguna yang akan bertambah sesuai dengan jumlah pengembalian dana. Jika Pengguna menggunakan pilihan metode pembayaran kartu kredit maka pengembalian dana akan merujuk pada ketentuan bagian M terkait Kartu Kredit.
16.	Panduasia berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat bukti-bukti yang ada. Keputusan Panduasia adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan Pembeli untuk mematuhinya.
17.	Apabila Pembeli memilih menggunakan metode pembayaran transfer bank, maka total pembayaran akan ditambahkan kode unik untuk mempermudah proses verifikasi. Dalam hal pembayaran telah diverifikasi maka kode unik akan dikembalikan ke Saldo Panduasia Pembeli.
18.	Pembeli wajib melakukan pembayaran dengan nominal yang sesuai dengan jumlah tagihan beserta kode unik (apabila ada) yang tertera pada halaman pembayaran. Pandu Asia Indonesia tidak bertanggungjawab atas kerugian yang dialami Pembeli apabila melakukan pembayaran yang tidak sesuai dengan jumlah tagihan yang tertera pada halaman pembayaran.
19.	Pembeli memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama Penjual.
 kembali ke atas
________________________________________
<h6>D. TRANSAKSI PENJUALAN</h6>
1.	Penjual dilarang memanipulasi harga Jasa Open Trip dengan tujuan apapun.
2.	Penjual dilarang melakukan penawaran / berdagang Jasa /Open trip terlarang sesuai dengan yang telah ditetapkan pada ketentuan 
3.	Penjual wajib memberikan foto dan informasi open trip dengan lengkap dan jelas sesuai dengan kondisi dan kualitas open trip yang dijualnya. Apabila terdapat ketidaksesuaian antara foto dan informasi open trip yang diunggah oleh Penjual dengan open trip yang diterima oleh Pembeli, maka Panduasia berhak membatalkan/menahan dana transaksi.
4.	Dalam menggunakan Fasilitas "Judul Open trip", "Foto Open trip", "Catatan" dan "Deskripsi Open trip", Penjual dilarang membuat peraturan bersifat klausula baku yang tidak memenuhi peraturan perundang-undangan yang berlaku di Indonesia, termasuk namun tidak terbatas pada (i) tidak menerima komplain, (ii) tidak menerima retur (penukaran open trip), (iii) tidak menerima refund (pengembalian dana), (iv) jasa open trip tidak bergaransi, (v) pengalihan tanggung jawab (termasuk tidak terbatas pada penanggungan ongkos). Jika terdapat pertentangan antara catatan jasa dan/atau deskripsi open trip dengan Syarat & Ketentuan Panduasia, maka peraturan yang berlaku adalah Syarat & Ketentuan Panduasia.
5.	Penjual wajib memberikan balasan untuk menerima atau menolak pesanan Open Trip pihak Pembeli dalam batas waktu 2 hari terhitung sejak adanya notifikasi pesanan Open trip dari Panduasia. Jika dalam batas waktu tersebut tidak ada balasan dari Penjual maka secara otomatis pesanan akan dibatalkan.
6.	Demi menjaga kenyamanan Pembeli dalam bertransaksi, Penjual memahami dan menyetujui bahwa Panduasia berhak melakukan moderasi jasa Penjual apabila Penjual melakukan penolakan, pembatalan dan/atau tidak merespon pesanan Open trip milik Pembeli dengan dugaan untuk memanipulasi transaksi, pelanggaran atas Syarat dan Ketentuan, dan/atau kecurangan atau penyalahgunaan lainnya.
7.	Penjual wajib memasukan no approval Open trip dalam batas waktu 4 x 24 jam (tidak termasuk hari Sabtu/Minggu/libur Nasional) terhitung sejak adanya notifikasi pesanan Open trip dari Panduasia. Jika dalam batas waktu tersebut pihak Penjual tidak memasukan nomor approval Open trip maka secara otomatis pesanan dianggap dibatalkan. Jika Penjual tetap mengirimkan Open trip setelah melebihi batas waktu pengiriman sebagaimana dijelaskan diatas, maka Penjual memahami bahwa transaksi akan tetap dibatalkan untuk kemudian Penjual dapat melakukan penarikan Open trip pada kurir tempat Open trip dikirimkan.
8.	Penjual memahami dan menyetujui bahwa pembayaran atas harga Open trip dan biaya lainnya (diluar biaya transfer / administrasi) akan dikembalikan sepenuhnya ke Pembeli apabila transaksi dibatalkan dan/atau transaksi tidak berhasil dan/atau ketentuan lain yang diatur dalam Syarat & Ketentuan Poin D. 14.
9.	Dalam keadaan Penjual hanya dapat memenuhi sebagian dari jumlah Open trip yang dipesan oleh Pembeli, maka Penjual wajib memberikan keterangan kepada Panduasia sebelum menerima pesanan dimaksud. Pembeli memiliki kewenangan penuh untuk menyetujui melanjutkan transaksi/membatalkan transaksi dan Penjual dilarang melanjutkan transaksi tanpa mendapat persetujuan dari Pembeli. Apabila telah disetujui ulang oleh Pembeli sesuai dengan jumlah pesanan yang disanggupi oleh Penjual, maka selisih dana total harga Open trip akan dikembalikan kepada pihak Pembeli. Namun apabila penjual tetap melanjutkan transaksi tersebut tanpa persetujuan dari Pembeli, maka Penjual menyetujui bahwa hal ini akan menjadi tanggung jawab Penjual sepenuhnya (termasuk namun tidak terbatas pada pengembalian ongkos kirim kepada Pembeli).
10.	Panduasia memiliki kewenangan untuk menahan pembayaran dana di Rekening Resmi Panduasia sampai waktu yang tidak ditentukan apabila terdapat permasalahan dan klaim dari pihak Pembeli terkait proses pengiriman dan kualitas Open trip. Pembayaran baru akan dilanjut kirimkan kepada Penjual apabila permasalahan tersebut telah selesai dan/atau Open trip telah diselesaikan oleh Pembeli.
11.	Panduasia berwenang untuk membatalkan dan/atau menahan dana transaksi dalam hal: (i) Nomor approval  open trip yang diberikan oleh Penjual tidak sesuai dan/atau diduga tidak sesuai dengan transaksi yang terjadi di Situs Panduasia; (ii) jika nama open trip dan deskripsi open trip tidak sesuai/tidak jelas dengan open trip yang dikirim; (iii) jika ditemukan adanya manipulasi transaksi.
12.	Penjual memahami dan menyetujui bahwa Pajak Penghasilan Penjual akan dilaporkan dan diurus sendiri oleh masing-masing Penjual sesuai dengan ketentuan pajak yang berlaku di peraturan perundang-undangan di Indonesia.
13.	Panduasia berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat bukti-bukti yang ada. Keputusan Panduasia adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan Pembeli untuk mematuhinya.
14.	Penjual memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama Penjual.
 kembali ke atas
________________________________________
<h6>E. PENATAAN OPEN TRIP</h6>
1.	Penjual dilarang mempergunakan open trip (termasuk dan tidak tebatas pada informasi jasa dan informasi open trip) sebagai media untuk beriklan atau melakukan promosi ke halaman situs lain diluar situs Panduasia.
2.	Penjual dilarang memberikan data kontak pribadi dengan maksud untuk melakukan transaksi secara langsung kepada Pembeli / calon Pembeli.
3.	Penjual dilarang memberikan keterangan (informasi jasa dan/atau Open trip) selain/diluar daripada keterangan jasa dan/atau Open trip yang bersangkutan.
4.	Penamaan Open trip harus dilakukan sesuai dengan informasi detail, spesifikasi, dan kondisi Open trip, dengan demikian Pengguna tidak diperkenankan untuk mencantumkan nama dan/atau kata yang tidak berkaitan dengan Open trip tersebut.
5.	Penamaan Open trip dan informasi open trip harus sesuai dengan kondisi Open trip yang ditampilkan dan Pengguna tidak diperkenankan mencantumkan nama dan informasi yang tidak sesuai dengan kondisi Open trip.
6.	Penjual wajib memisahkan tiap-tiap Open trip yang memiliki ukuran dan harga yang berbeda.
7.	Panduasia memiliki kewenangan untuk mengubah nama dan/atau memakai nama Jasa dan/atau domain Pengguna untuk kepentingan internal Panduasia.
 kembali ke atas
________________________________________
<h6>F. KOMISI</h6>
1.	Hingga saat ini, Panduasia belum memberlakukan sistem komisi untuk setiap transaksi yang dilakukan melalui Rekening Resmi Panduasia.
2.	Apabila dikemudian hari akan diberlakukan sistem komisi dalam transaksi melalui Rekening Resmi Panduasia, maka akan dilakukan sosialisasi terlebih dahulu jangka waktu selambat-lambatnya 1 (satu) pekan sebelum sistem komisi dinyatakan berlaku oleh Panduasia.
 kembali ke atas
________________________________________
<h6>G. HARGA</h6>
1.	Harga Open trip yang terdapat dalam situs Panduasia adalah harga yang ditetapkan oleh Penjual. Penjual dilarang memanipulasi harga open trip dengan cara apapun.
2.	Penjual dilarang menetapkan harga yang tidak wajar pada Open trip yang ditawarkan melalui Situs Panduasia. Panduasia berhak untuk melakukan tindakan pemeriksaan, penundaan, atau penurunan konten atas dasar penetapan harga yang tidak wajar.
3.	Pembeli memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaharuinya halaman situs Panduasia dikarenakan browser/ISP yang dipakai Pembeli adalah tanggung jawab Pembeli.
4.	Penjual memahami dan menyetujui bahwa kesalahan ketik yang menyebabkan keterangan harga atau informasi lain menjadi tidak benar/sesuai adalah tanggung jawab Penjual. Perlu diingat dalam hal ini, apabila terjadi kesalahan pengetikan keterangan harga Open trip yang tidak disengaja, Penjual berhak menolak pesanan Open trip yang dilakukan oleh pembeli.
5.	Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat ketidaksepahaman antara Penjual dan Pembeli tentang harga bukanlah merupakan tanggung jawab Panduasia.
6.	Dengan melakukan pemesanan melalui Panduasia, Pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana tertera dalam halaman pembayaran, yang terdiri dari harga open trip, ongkos kirim, dan biaya-biaya lain yang mungkin timbul dan akan diuraikan secara tegas dalam halaman pembayaran. Pengguna setuju untuk melakukan pembayaran melalui metode pembayaran yang telah dipilih sebelumnya oleh Pengguna.
7.	Batasan harga maksimal satuan untuk Open trip yang dapat ditawarkan adalah Rp. 100.000.000,-
8.	Situs Panduasia untuk saat ini hanya melayani transaksi jual beli Open trip dalam mata uang Rupiah.
 kembali ke atas
________________________________________
<h6>H. KONTEN</h6>
1.	Dalam menggunakan setiap fitur dan/atau layanan Panduasia, Pengguna dilarang untuk mengunggah atau mempergunakan kata-kata, komentar, gambar, atau konten apapun yang mengandung unsur SARA, diskriminasi, merendahkan atau menyudutkan orang lain, vulgar, bersifat ancaman, atau hal-hal lain yang dapat dianggap tidak sesuai dengan nilai dan norma sosial. Panduasia berhak melakukan tindakan yang diperlukan atas pelanggaran ketentuan ini, antara lain penghapusan konten, moderasi jasa, pemblokiran akun, dan lain-lain.
2.	Pengguna dilarang mempergunakan foto/gambar Open trip yang memiliki watermark yang menandakan hak kepemilikan orang lain.
3.	Penguna dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Pengguna adalah tanggung jawab Pengguna secara pribadi.
4.	Penjual tidak diperkenankan untuk mempergunakan foto/gambar Open trip atau logo jasa sebagai media untuk beriklan atau melakukan promosi ke situs-situs lain diluar Situs Panduasia, atau memberikan data kontak pribadi untuk melakukan transaksi secara langsung kepada pembeli / calon pembeli.
5.	Ketika Pengguna menggungah ke Situs Panduasia dengan konten atau posting konten, Pengguna memberikan Panduasia hak non-eksklusif, di seluruh dunia, secara terus-menerus, tidak dapat dibatalkan, bebas royalti, disublisensikan ( melalui beberapa tingkatan ) hak untuk melaksanakan setiap dan semua hak cipta, publisitas , merek dagang , hak basis data dan hak kekayaan intelektual yang Pengguna miliki dalam konten, di media manapun yang dikenal sekarang atau di masa depan. Selanjutnya , untuk sepenuhnya diizinkan oleh hukum yang berlaku , Anda mengesampingkan hak moral dan berjanji untuk tidak menuntut hak-hak tersebut terhadap Panduasia.
6.	Pengguna menjamin bahwa tidak melanggar hak kekayaan intelektual dalam mengunggah konten Pengguna kedalam situs Panduasia. Setiap Pengguna dengan ini bertanggung jawab secara pribadi atas pelanggaran hak kekayaan intelektual dalam mengunggah konten di Situs Panduasia.
7.	Panduasia menyediakan fitur "Diskusi Open trip" untuk memudahkan pembeli berinteraksi dengan penjual, perihal Open trip yang ditawarkan. Penjual tidak diperkenankan menggunakan fitur tersebut untuk tujuan dengan cara apa pun menaikkan harga Open trip dagangannya, termasuk di dalamnya memberi komentar pertama kali atau memberi komentar selanjutnya / terus menerus secara berkala (flooding / spam).
8.	Meskipun kami mencoba untuk menawarkan informasi yang dapat diandalkan, kami tidak bisa menjanjikan bahwa katalog akan selalu akurat dan terbarui, dan Pengguna setuju bahwa Pengguna tidak akan meminta Panduasia bertanggung jawab atas ketimpangan dalam katalog. Katalog mungkin termasuk hak cipta, merek dagang atau hak milik lainnya.
9.	Konten atau materi yang akan ditampilkan atau ditayangkan pada Situs Panduasia melalui Panduasia Website Feed akan tunduk pada Ketentuan Situs, peraturan hukum, serta etika pariwara yang berlaku.
10.	KOL bertanggungjawab terhadap konten atau materi yang ditampilkan melalui program Panduasia Website Feed.
11.	Panduasia berhak untuk sewaktu-waktu menurunkan konten atau materi yang terdapat pada Panduasia Website Feed yang dianggap melanggar Syarat dan Ketentuan Situs, peraturan hukum yang berlaku, serta etika pariwara yang berlaku.
 kembali ke atas
________________________________________
<h6>I. KARTU KREDIT</h6>
1.	Pengguna dapat memilih untuk mempergunakan pilihan metode pembayaran menggunakan kartu kredit untuk transaksi pembelian open trip melalui Situs Panduasia.
2.	Transaksi pembelian open trip dengan menggunakan kartu kredit dapat dilakukan untuk transaksi pembelian dengan nilai total belanja minimal Rp. 50.000 (lima puluh ribu rupiah) hingga maksimal Rp 50.000.000 (lima puluh juta rupiah) untuk transaksi marketplace dan/atau digital, transaksi dapat dilakukan hingga Rp500.000.000,- (lima ratus juta Rupiah) khusus untuk pembelian open trip mobil pada Official Store.
3.	Transaksi pembelian open trip dengan menggunakan kartu kredit  wajib mengikuti syarat dan ketentuan yang diatur oleh Panduasia dan mempergunakan kurir/logistik yang disediakan dan terhubung dengan Situs Panduasia.
4.	Pengguna dilarang untuk mempergunakan metode pembayaran kartu kredit di luar peruntukan sebagai alat pembayaran.
5.	Apabila terdapat transaksi pembelian open trip dengan menggunakan kartu kredit yang melanggar ketentuan hukum dan/atau syarat ketentuan Panduasia, maka Panduasia berwenang untuk:
o	- menahan dana transaksi selama diperlukan oleh Panduasia, pihak Bank, maupun mitra payment gateway terkait untuk melakukan investigasi yang diperlukan, sekurang-kurangnya 14 hari;
o	- melakukan pemotongan dana sebesar 15% (lima belas persen) dari nilai transaksi, serta menarik kembali nilai subsidi sehubungan penggunaan kartu kredit.
6.	Panduasia akan menghentikan kerjasama dengan Penjual yang melakukan tindakan yang dapat merugikan prinsipal, penerbit, acquirer dan /atau pemegang kartu kredit, antara lain memproses penarikan / gesek tunai (cash withdrawal transaction).
7.	Apabila transaksi pembelian tidak berhasil dan/atau dibatalkan, maka tagihan atas transaksi tersebut akan dibatalkan dan dana transaksi akan dikembalikan ke limit kartu kredit pembeli di tagihan berikutnya. Ketentuan pada ayat ini tidak berlaku untuk transaksi pembelian open trip dengan menggunakan kartu kredit yang melanggar ketentuan hukum dan/atau syarat ketentuan Panduasia.
8.	Kartu kredit yang dapat digunakan untuk bertransaksi di Panduasia hanya kartu kredit yang diterbitkan di Indonesia.
9.	Transaksi menggunakan metode pembayaran Kartu Kredit akan dikenakan biaya administrasi sebesar 1,5% dari total biaya yang harus dibayarkan. Apabila seluruh transaksi dalam satu pembayaran yang menggunakan kartu kredit dibatalkan, maka biaya administrasi tersebut akan dikembalikan ke limit kartu kredit Pengguna. Namun apabila hanya sebagian transaksi dalam satu pembayaran yang menggunakan kartu kredit dibatalkan, maka biaya administrasi tersebut tidak akan ikut serta dikembalikan.
10.	Pembayaran yang menggunakan program cicilan 0% akan mengikuti ketentuan yang terdapat pada halaman https://www.panduasia.com/bantuan/217202526-cicilan/
 kembali ke atas
________________________________________
<h6>J. PROMO</h6>
1.	Panduasia sewaktu-waktu dapat mengadakan kegiatan promosi (selanjutnya disebut sebagai “Promo”) dengan Syarat dan Ketentuan yang mungkin berbeda pada masing-masing kegiatan Promo. Pengguna dihimbau untuk membaca dengan seksama Syarat dan Ketentuan Promo tersebut.
2.	Apabila terdapat transaksi Promo yang mempergunakan metode pembayaran kartu kredit yang melanggar Syarat dan Ketentuan Panduasia, maka akan merujuk pada Syarat dan Ketentuan Poin M. Kartu Kredit.
3.	Panduasia berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan termasuk namun tidak terbatas pada menarik subsidi atau cashback, membatalkan benefit Jasapoints, pencabutan Promo, membatalkan transaksi, menahan dana, menurunkan reputasi jasa, menutup jasa atau akun, serta hal-hal lainnya jika ditemukan adanya manipulasi, penggunaan resi yang tidak valid pada mayoritas transaksi, pelanggaran maupun pemanfaatan Promo untuk keuntungan pribadi Pengguna, maupun indikasi kecurangan atau pelanggaran pelanggaran Syarat dan Ketentuan Panduasia dan ketentuan hukum yang berlaku di wilayah negara Indonesia.
4.	Pengguna hanya boleh menggunakan 1 (satu) akun Panduasia untuk mengikuti setiap promo Panduasia. Jika ditemukan pembuatan lebih dari 1 (satu) akun oleh 1 (satu) Pengguna yang sama dan/atau nomor handphone yang sama dan/atau alamat yang sama dan/atau ID pelanggan yang sama dan/atau identitas pembayaran yang sama dan/atau riwayat transaksi yang sama, maka Pengguna tidak berhak mendapatkan manfaat dari promo Panduasia.
 kembali ke atas
________________________________________
<h6>K. PENARIKAN DANA</h6>
1.	Penarikan dana sesama bank akan diproses dalam waktu 1x24 jam hari kerja, sedangkan penarikan dana antar bank akan diproses dalam waktu 2x24 jam hari kerja.
2.	Untuk penarikan dana dengan tujuan nomor rekening di luar bank BCA, Mandiri, dan BNI apabila ada biaya tambahan yang dibebankan akan menjadi tanggungan dari Pengguna.
3.	Dalam hal ditemukan adanya dugaan pelanggaran terhadap Syarat dan Ketentuan Panduasia, kecurangan, manipulasi atau kejahatan, Pengguna memahami dan menyetujui bahwa Panduasia berhak melakukan tindakan pemeriksaan, pembekuan, penundaan dan/atau pembatalan terhadap penarikan dana yang dilakukan oleh Pengguna.
4.	Pemeriksaan, pembekuan atau penundaan penarikan dana sebagaimana dimaksud dalam poin 3 dapat dilakukan dalam jangka waktu selama yang diperlukan oleh pihak Panduasia.
 kembali ke atas
________________________________________
<h6>L. KETENTUAN LAIN</h6>
1.	Apabila Pengguna mempergunakan fitur-fitur yang tersedia dalam situs Panduasia  maka Pengguna dengan ini menyatakan memahami dan menyetujui segala syarat dan ketentuan yang diatur khusus sehubungan dengan fitur-fitur tersebut di bawah ini, yakni:
o	- Penggunaan fitur pemesanan dan pembelian tiket kereta api melalui situs Panduasia akan diatur lebih lanjut dalam ketentuan berikut
2.	Segala hal yang belum dan/atau tidak diatur dalam syarat dan ketentuan khusus dalam fitur tersebut maka akan sepenuhnya merujuk pada syarat dan ketentuan Panduasia secara umum.
 kembali ke atas
________________________________________
<h6>M. PENOLAKAN JAMINAN DAN BATASAN TANGGUNG JAWAB</h6>
Panduasia adalah portal web dengan model Costumer to Customer Marketplace, yang menyediakan layanan kepada Pengguna untuk dapat menjadi Penjual maupun Pembeli di website Panduasia. Dengan demikian transaksi yang terjadi adalah transaksi antar member Panduasia, sehingga Pengguna memahami bahwa batasan tanggung jawab Panduasia secara proporsional adalah sebagai penyedia jasa portal web
Panduasia selalu berupaya untuk menjaga Layanan Panduasia aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi terus-menerus atau akses ke Layanan kami dapat selalu sempurna. Informasi dan data dalam situs Panduasia memiliki kemungkinan tidak terjadi secara real time.
Pengguna setuju bahwa Anda memanfaatkan Layanan Panduasia atas risiko Pengguna sendiri, dan Layanan Panduasia diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".
Sejauh diizinkan oleh hukum yang berlaku, Panduasia (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan Anda setuju untuk tidak menuntut Panduasia bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari :
•	- Penggunaan atau ketidakmampuan Pengguna dalam menggunakan Layanan Panduasia.
•	- Harga, Pengiriman atau petunjuk lain yang tersedia dalam layanan Panduasia.
•	- Keterlambatan atau gangguan dalam Layanan Panduasia.
•	- Kelalaian dan kerugian yang ditimbulkan oleh masing-masing Pengguna.
•	- Kualitas Open trip.
•	- Pelaksanaan Open trip.
•	- Pelanggaran Hak atas Kekayaan Intelektual.
•	- Perselisihan antar pengguna.
•	- Pencemaran nama baik pihak lain.
•	- Setiap penyalahgunaan Open trip yang sudah disepakatii pihak Pengguna.
•	- Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke Rekening Resmi Panduasia, yang dengan cara apa pun mengatas-namakan Panduasia ataupun kelalaian penulisan rekening dan/atau informasi lainnya dan/atau kelalaian pihak bank.
•	- Virus atau perangkat lunak berbahaya lainnya (bot, script, automation tool selain fitur Gold Merchant, hacking tool) yang diperoleh dengan mengakses, atau menghubungkan ke layanan Panduasia.
•	- Gangguan, bug, kesalahan atau ketidakakuratan apapun dalam Layanan Panduasia.
•	- Kerusakan pada perangkat keras Anda dari penggunaan setiap Layanan Panduasia.
•	- Isi, tindakan, atau tidak adanya tindakan dari pihak ketiga, termasuk terkait dengan Open trip yang ada dalam situs Panduasia yang diduga palsu.
•	- Tindak penegakan yang diambil sehubungan dengan akun Pengguna.
•	- Adanya tindakan peretasan yang dilakukan oleh pihak ketiga kepada akun pengguna.
 kembali ke atas
________________________________________
<h6>N. PELEPASAN</h6>
Jika Anda memiliki perselisihan dengan satu atau lebih pengguna, Anda melepaskan Panduasia (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya , yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut. Dengan demikian maka Pengguna dengan sengaja melepaskan segala perlindungan hukum (yang terdapat dalam undang-undang atau peraturan hukum yang lain) yang akan membatasi cakupan ketentuan pelepasan ini.
 kembali ke atas
________________________________________
<h6>O. GANTI RUGI</h6>
Pengguna akan melepaskan Panduasia dari tuntutan ganti rugi dan menjaga Panduasia (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian ini, penggunaan Layanan Panduasia yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak ketiga.
 kembali ke atas
________________________________________
<h6>P. PILIHAN HUKUM</h6>
Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.
 kembali ke atas
________________________________________
<h6>Q. PEMBAHARUAN</h6>
Syarat & ketentuan mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Panduasia menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat & ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Panduasia, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & ketentuan.


					</p>
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
					
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		
		<br>
	
	</div>
</div>


@endsection
