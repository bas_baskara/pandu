@extends('blog/app') @section('head')

{{-- <meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
<meta name="keywords" content="Pandu Asia Blog" />  --}}
@endsection 
@section('main-content')
@include('komunitas/_layouts/komunitas')


<form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
    <div class="search-wrapper" style="margin-top:25px;">
        <div class="input-holder">
            <input type="text" class="search-input" name="keyword" placeholder="Cari grup ..." />
            <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
                <span></span>
            </button>
        </div>
        <span class="close" onclick="searchToggle(this, event);"></span>
        <div class="result-container">
        </div>
    </div>
</form>



<div class="col-md-6">
    @foreach($grups->where('status','Terverifikasi') as $grup)
    <div class="business-card">
        <div class="media">
            <div class="media-left">
                <img class="media-object img-circle profile-img" src="{{
                    URL::asset('komunitas/grup/'.$grup->img_profile
                    )}}">
                </div>
                <div class="media-body">
                    <h2 class="media-heading"><strong>{{$grup->name}}</strong></h2>
                    <p name="id">#{{$grup->id}}</p> 
                    <div class="job">
                        {{$grup->ketua}} (Admin)
                    </div>
                    Anggota : {{$grup->member}}
                    
                    <a href="{{ route('join.grup',$grup->id) }}" class="btn btn-info float-right"  style="margin-top:45px;">Selengkapnya</a>
                    
                </div>
            </div>
        </div>
        @endforeach 
    </div>
    
    
    
    
</div>
</div>
</div>
</div>

@endsection