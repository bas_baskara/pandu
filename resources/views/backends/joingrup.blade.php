@extends('blog/app') @section('head')


@endsection 
@section('main-content')
@include('komunitas/_layouts/komunitas')

<div class="col-md-8">
  <div class="panel panel-default">
    <form role="form" action="{{ route('masuk.grup',$grups->id) }}" method="post" enctype="multipart/form-data">
      
      @include('includes.messages')
      @csrf
      {{ method_field('PATCH') }}
      <div class="panel-body">
        <div class="col-md-4">
          <img src="{{
            URL::asset('komunitas/grup/'.$grups->img_profile
            )}}" class="img-responsive">
        </div>
        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="float: right;"></span>
        <div class="col-md-5"> 
          
          <h2>{{$grups->name}} </h2>
          <p name="grup">{{$grups->id}}</p> 
          <h5><span class="glyphicon glyphicon-map-marker"  aria-hidden="true"></span> {{$grups->place}}
          </h5>
          <h5><span class="glyphicon glyphicon-user"  aria-hidden="true"></span> {{$grups->ketua}} (Admin)
          </h5>
          
          <input type="submit" value="Gabung" class="btn btn-success">
        </div>
      </div>
    </form>
  </div>
</div>

</div>
</div>
</div>
</div>
</div>

@endsection