@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
{{-- <link href="https://blackrockdigital.github.io/startbootstrap-scrolling-nav/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}

<meta name="keywords" content="Pandu Asia Blog" /> @endsection @section('main-content')

<header id="gtco-header" role="banner">
    <img src="{{URL::asset('blog/images/blog.jpg')}}" alt="panduasia blog" class="gtco-cover">
</header>
<!-- END #gtco-header -->

<div class="container">
    <div class="row">
        <div class="gtco-services gtco-section">
            <div class="gtco-heading text-center">
                <h2>New Articles</h2>
                <br>
                <form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" name="keyword" placeholder="Cari artikel menarik disini ..." />
                            <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
                                <span></span>
                            </button>
                        </div>
                        <span class="close" onclick="searchToggle(this, event);"></span>
                        <div class="result-container">
                        </div>
                    </div>
                </form>
                <h4>Search By kategori : </h4>
                
                <div class="category-button col-md-12">
                    <a href="" class="flex-item btn btn-info"></a>
                    <a class="btn btn-dark flex-item" href="">All</a>
                </div>
                
            </div>
        </div>
    </div>
    
    <br>   
    

    <div id="komunitas" class="container-fluid text-center" style="
    background: -webkit-gradient(linear, left top, right bottom, from(#20b8e6), to(rgb(52, 212, 124)));
    opacity: .8;
    " >
        <h2 style="
            font-size: 1.875em;
            font-weight: bold;
            color:  #f4f6f7  ;
            margin-top:40px;
            
        ">Gabung bersama di komunitas kami</h2>
        <div class="row">
            <div class="col-md-12" style="margin-bottom:40px;">
                
            <a href="{{URL('/komunitas/register-member')}}" class="btn btn-white btn-outline">Daftar</a>
            </div>

        </div>
    </div>

    
    <br><br>


    <div class="row">
        
        <aside class="col-sm-4">
            @foreach($data as $datas) @if($datas->status == 0)
            <?php echo " " ?> @else
            <div class="card-banner-komunitas align-items-end">
                <img src="{{ URL::asset('komunitas_artikel/'.$datas->image)}}" alt="{{$datas->title}}">
                
                <article class="overlay-komunitas overlay-cover d-flex align-items-center justify-content-center">
                    <h5 class="card-title">{{$datas->title}}</h5>
                    <a href="{{route('postskomunitas',$datas->slug)}}" class="btn btn-info btn-sm"> Selengkapnya </a>
                </article>
            </div>
            @endif
            @endforeach
        </aside>
    </div>

    <!-- Pager -->
    <div class="row">
        <!--<div class="next">-->
            {{$data->links()}}
        <!--</div>-->
    </div>

</div>

    <br>
    <br>

    
    
    
    


</div>

@endsection
