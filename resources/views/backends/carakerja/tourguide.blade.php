@extends('blog/app')
@section('head')

	{{-- <meta name="title" content="{{$posts->meta_title}}" />
	<meta name="description" content="{{ $posts->meta_desc }}" />
	<meta name="keywords" content="{{$posts->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$posts->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$posts->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$posts->meta_desc}}" /> --}}
	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($posts as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('image_post/'.$posts->image)}}" alt="{{$posts->title}}" class="bannerpost"> --}}
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/blog/')}}">Blog /</a>

			
				<a href=""></a>
			
			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title">Panduan Menjadi Tour Guide di Panduasia</h1>
			
					<p class="bodycontent">
              <h3>Registrasi sebagai Tour Guide</h3> 

              Saugi mempunyai banyak pengalaman dalam dunia wisata di kota yang Saugi tempati saat ini. Dia ingin mencari penghasilan lebih dari pekerjaan yang bisa dia lakukan sambil bersenang-senang wisata ke tempat-tempat yang ingin dia kunjungi. Oleh karena itu Saugi mencari pekerjaan sebagai Tour Guide dengan menggunakan Panduasia.
              
              Saugi daftar sebagai Tourguide di Panduasia. GRATIS, tanpa ikatan, tanpa biaya!
              
              pada saat akan mendaftar, Saugi perlu team yang bisa saling berbagi informasi seputar dunia wisata, oleh karena itu sebelum mendaftar sebagai Tour Guide, Saugi perlu terdaftar di Komunitas Tour Guide di area/kota saugi berasal. Daftar sebagai member Komunitas Kamu disini (buat Link).
              
              Dengan adanya komunitas, saugi sangat terbantu untuk mendapatkan update informasi-informasi terbaru seputar wisata di kota Saugi berasal.
              
              Setelah terdaftar sebagai member komunitas, saugi dapat dengan mudah mendaftar sebagai tour guide di Panduasia, silahkan klik disini (Link)
              
                 <h3>Lengkapi profil Tourguide Anda</h3> 
              
              Saugi mengisi profil tourguidenya dengan lengkap dan sedetil mungkin, dimulai dari data pribadi, pengalaman, lokasi tempat wisata yang diketahui, hingga informasi lainnya yang telah tersedia di kolom profil. Saugi juga memastikan bahwa profilnya menarik dan menonjol bagi klien.
              
                  <h3>Mencari pekerjaan yang tepat</h3> 
              
              Setelah melengkapi profil, Saugi segera berburu job di Panduasia dengan cara mengklik halaman Open Trip sesuai dengan tempat wisata dan tambahan keuntungan yang dia miliki (bisa antar jemput bandara / hotel, bisa menyediakan homestay, bisa menyewakan sepeda ke traveller,dll). Selain itu, Saugi juga akan rutin mendapatkan notifikasi job via email.
              
              Saugi melamar sebagai tourguide daripada traveller yang mencari jasa pemandu dan Saugi dapat membuat Open Trip sebanyak-banyaknya sesuai dengan tempat-tempat wisata dikota Saugi yang Saugi ketahui dengan baik sebanyak - banyaknya melalui platform online yang sudah tersedia secara GRATIS oleh Panduasia.
              
                  <h3>Bekerja dengan traveller. Dapat ekstra income tak terbatas dan menikmati travelling sesuka hati di kota kamu berasal</h3>
              
              Traveller tertarik dengan Open Trip yang dibuat oleh Saugi. Lalu Traveller dan Saugi mulai chatting dan negosiasi harga Open Trip. Setelah setuju dengan harga dan scope maka Traveller membayar deposit Open Trip tersebut.
              
              Saugi menemui traveller pada waktu yang telah ditentukan, ketika bertemu, traveller akan mengklik "DownPayment", sehingga uang DP sebesar 30% akan langsung di transfer ke Saugi pada saat pertemuan oleh Panduasia. Setelah bersenang-senang bersama-sama traveller berwisata ke tempat yang dituju, maka Traveller akan mengklik "Complete", dan uangpun akan ditransfer ke Saugi sesuai dengan kesepakatan yang telah dibuat dengan traveller dan traveller pun akan memberikan rating yang akan membantu reputasi Saugi sebagai Tourguide terpercaya
              
              Dapatkan banyak penghasilan melalui tourguide di Panduasia.
              
              
              
					</p>
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
					
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		
		<br>
	
	</div>
</div>


@endsection
