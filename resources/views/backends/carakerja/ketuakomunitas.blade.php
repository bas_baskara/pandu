@extends('blog/app')
@section('head')

	{{-- <meta name="title" content="{{$posts->meta_title}}" />
	<meta name="description" content="{{ $posts->meta_desc }}" />
	<meta name="keywords" content="{{$posts->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$posts->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$posts->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$posts->meta_desc}}" /> --}}
	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($posts as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('image_post/'.$posts->image)}}" alt="{{$posts->title}}" class="bannerpost"> --}}
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/blog/')}}">Blog /</a>

			
				<a href=""></a>
			
			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title"> Panduan Menjadi Ketua Komunitas di Panduasia</h1>
			
					<p class="bodycontent">
             
            
<h3>1. Registrasi sebagai Ketua Komunitas</h3>
Budi adalah Ketua Komunitas yang aktif didunia wisata, Budi ingin lebih mengenalkan komunitasnya di Dunia Digital dengan harapan bisa memberikan berbagai informasi menarik mengenai dunia wisata, maka Panduasia adalah hal yang tepat buat Budi.
Budi daftar sebagai Ketua Komunitas di panduasia. GRATIS, tanpa ikatan, tanpa biaya
<h3>2. Approval dari Panduasia</h3>
Pada saat mendaftar sebagai ketua, Budi akan diberitahukan untuk menunggu prose Approval dari panduasia.com. via email. Setelah di approve oleh Panduasia, Budi secara resmi dapat membentuk komunitasnya di panduasia.com, mengajak para member komunitasnya untuk bergabung bersama Budi (maksimal 300 member) dan dapat menceritakan berbagai pengalamannya di Dunia wisata di  Panduasia.com yang dilihat oleh banyak para traveler dari berbagai wilayah di Indonesia.
Komunitas Budi menjadi lebih banyak dikenal oleh para Traveller, Budi dapat mempromosikan Komunitasnya kepada para traveler melalui panduasia.com
<h3>3. Buat Nama Komunitas, Lengkapi Profile dan Sharing Pengalaman Komunitasmu</h3>
Budi membentuk nama komunitasnya dengan menarik, dengan deskripsi yang jelas terkait komunitas yang dibuatnya. Budi pun mengisi profil komunitasnya  dengan lengkap dan sedetil mungkin, dimulai dari nama Komunitas, lokasi tempat wisata yang diketahui, hingga informasi lainnya yang telah tersedia di kolom profil. Budi juga memastikan bahwa profile komunitas menarik dan selalu terupdate dengan berbagai Informasi Artikel seputar Wisata
<h3>4. Nikmati Profit Sharing 2% </h3>
Setiap para member yang bergabung dikomunitas Budi dan menjadi tour guide di panduasia.com,maka panduasia.com akan memberikan profit sharing 2% untuk setiap transaksi yang berhasil dilakukan oleh member tersebut, profit sharing ini dapat digunakan untuk berbagai keperluan Komunitas yang telah Budi buat, seperti biaya keperluan acara gathering Komunitas, pembelian peralatan pendukung Travelling hingga biaya untuk para member yang membutuhkan. Semua pengaturan Profit Sharing sepenuhnya menjadi Hak Budi sebagai Ketua Komunitas.
Dengan adanya profit Sharing ini, dapat membantu memaksimalkan Budi dalam mengembangkan komunitas yang dimiliki oleh Budi untuk terus menerus berkembang.
<h3>5. Nikmati Berbagai Promo Menarik</h3>
Di Panduasia, Budi pun bisa menjadi Tour Guide (klik disini untuk menjadi Tour Guide) dan dapat menikmati promo-promo menarik yang Budi bisa tukarkan dengan point-point yang terkumpul selama transaksi menjadi Tour Guide 
<h3>6. Nikmati Penghasilan Tanpa Batas dengan Menjadi bagian dari Panduasia.com</h3>
Budi yang telah terdaftar sebagai Ketua Komunitas, dapat memiliki income tanpa batas dengan menjadi Tour Guide di panduasia.com. Dengan Profit Sharing sebesar 90% untuk Budi (2% untuk Komunitas, dan 8% untuk Panduasia.com) di setiap transaksi yang berhasil dilakukan oleh Budi, 
Dengan bergabungnya Budi sebagai Ketua dan tourguide, Budi dapat menikmati Proses Travellingnya menemani para traveler dari berbagai wilayah dengan cara dibayar sebagai tourguide, serta mendapatkan profit sharing 2% untuk mengembangkan komunitasnya,  so Nikmati perjalananmu, kembangkan komunitasmu dan dapatkan incomemu tanpa batas…


              
					</p>
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
					
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		
		<br>
	
	</div>
</div>


@endsection
