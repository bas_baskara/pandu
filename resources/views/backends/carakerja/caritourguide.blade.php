@extends('blog/app')
@section('head')

	{{-- <meta name="title" content="{{$posts->meta_title}}" />
	<meta name="description" content="{{ $posts->meta_desc }}" />
	<meta name="keywords" content="{{$posts->meta_keyword}}" />
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$posts->meta_title}}" />
	<meta property="og:image" content="{{ URL::asset('core/public/image_post/'.$posts->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$posts->meta_desc}}" /> --}}
	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($posts as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('image_post/'.$posts->image)}}" alt="{{$posts->title}}" class="bannerpost"> --}}
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/blog/')}}">Blog /</a>

			
				<a href=""></a>
			
			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title"> Panduan Mencari Tour Guide di Panduasia</h1>
			
					<p class="bodycontent">
             

 

    <h3>Cara Pertama : Klik Search by City and Destinasi wisata</h3>

Dina sedang dinas singkat keluar kota, selesai bekerja, Dina ingin memanfaatkan sisa waktunya untuk berjalan-jalan di kota tersebut, Dina bingung mau pergi kemana dan tidak ada perencanaan sama sekali, mau mengunjungi banyak tempat di kota tersebut perlu persiapan yang cukup.

Dina mencari opsi lain menggunakan jasa Tour Guide di kota tersebut melalui Panduasia, dengan mengklik www.panduasia.com, dan search kota yang saat ini Dina kunjungi, Dina bisa melihat banyak tourguide yang tersedia dikota tersebut dengan menawarkan berbagai open trip yang menarik. Apabila Dina ada yang tertarik, maka Dina perlu melakukan Register dan Login dahulu di Panduasia, mudah dan cepat kok Registrasi dan Loginnya (Buatkan Link)

Dina tertarik dengan salah satu Open Trip Tourguide, maka Dina bisa melakukan chat dengan Tourguide tersebut, apabila cocok, Dina bisa Payment Deposit uangnya dengan Aman ke Panduasia.  Pembayaran menggunakan transfer BCA, Mandiri, kartu kredit, atau Paypal. Semua pembayaran di Panduasia di jamin dan di garansi 100% aman

    <h3> Cara Kedua : Klik Request Open Trip
      </h3>
Dina yang sedang Dinas ternyata membawa team yang ramai-ramai ingin mengunjungi suatu tempat (lebih dari 6 orang), Dina bisa mengklik request Open trip dan menjelaskan Deskripsi open Trip yang Diminta, lalu Send Request, dalam hitungan Jam, para Pemandu akan memberikan Rincian Harga dari request Open Trip tersebut.

Apabila tertarik dengan salah satu Open Trip Tourguide, Dina bisa melakukan chat dengan Tourguide tersebut, apabila cocok, Dina bisa Payment Deposit uangnya dengan Aman ke Panduasia.  Pembayaran menggunakan transfer BCA, Mandiri, kartu kredit, atau Paypal. Semua pembayaran di Panduasia di jamin dan di garansi 100% aman

 

 <h3> Menemui Tour Guide sesuai schedule dan Lokasi</h3>  

Dihari yang telah ditentukan, Dina menemui Tour Guide, sesuai dengan lokasi dan jam yang telah ditentukan bersama, ketika bertemu, Dina bisa mengklik "Telah Bertemu Pemandu", lalu panduasia akan mentransfer DP ke Pemandu sebesar 25% dari Total Transaksi.

    <h3>Travelling bersama Pemandu (Aman, Nyaman dan Menyenangkan)</h3>

Selama travelling Dina bisa menikmati suasana yang aman, nyaman dan menyenangkan karena Dina ditemani dengan  Tour Guide yang ramah dan memahami kultur kota tersebut dengan baik, Dina bisa menikmati berbagai keuntungan tambahan lainnya seperti Makan Siang/Malam, Homestay yang murah dan nyaman, sewa kendaraan, fasilitas antar jemput dari Bandara/Hotel sesuai dengan fasilitas yang dimiliki oleh Tour Guide tersebut, dalam arti kata Dina tinggal terima beres dan bersenang-senang selama travelling berlangsung.

    <h3>Selesai Travelling " Say Thank You " & Payment ke Tour Guide</h3>

Setelah selesai Travelling and Say Thank you, Dina bisa klik "Selesai Open Trip", sehingga uang deposit yang berada di Panduasia akan ditransfer dengan segera ke Tour Guide, Dina pun bisa memberikan rating kepada Tour Guide sesuai dengan pelayanan dan fasilitas yang diberikan kepada Dina..(beri bintang 5 ya Dinaaa....)

 

    <h3>Keep Connected melalui Panduasia.</h3>

Dina bisa tetap terkoneksi dengan Panduasia dan Tour Guide yang telah ditemuinya, setiap kali travelling dengan Panduasia, Dina bisa mengkoleksi point yang bisa ditukarkan dengan merchandise-merchandise menarik ataupun dengan open trip menarik dari Panduasia, so..stay connected ya Dina bersama Panduasia....
              
              
					</p>
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
					
					</div>
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		
		<br>
	
	</div>
</div>


@endsection
