@extends('blog/app') @section('head')


@endsection 
@section('main-content')
@include('komunitas/_layouts/komunitas')

<div class="col-md-8">
    <div class="card" style="padding:20px;">
        <div class="card-header"><h3>Buat Grup Baru</h3> </div>
        @include('includes.messages')
        <div class="card-body">
            <form action="{{ route('komunitas.grup.create') }}" method="post" enctype="multipart/form-data"> 
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Beri nama grup Anda</label>
                    <input type="text" class="form-control" name="name" placeholder="contoh: Komunitas Jakarta"
                    >
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label for="exampleInputEmail1">Deskripsi Grup Anda</label>
                    </div>    
                    <textarea name="description" style="width: 325px; height: 108px;"></textarea>
                </div>
                <label for="exampleInputEmail1">Lokasi</label>
                <input type="text" class="form-control" placeholder="" name="place"
                >
                
                <div class="form-group">
                    <label for="exampleInputEmail1">Foto Grup</label>
                    
                    <input type="image" id="image" alt="Login"  class="img-responsive"
                    src="{{URL::asset('blog/images/avatar.jpg')}}" />
                    
                    <input type="file" name="img_profile" accept="image/*" required>
                </div>
                
                <input type="submit" class="btn btn-success float-right" value="Buat grup">
                
            </form>
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
</div>

@endsection
