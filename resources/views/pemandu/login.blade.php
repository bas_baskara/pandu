@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Login" />
<meta name="description" content="Login panduasia" />
<meta name="keywords" content="Pandu Asia Blog" /> 
<link rel="stylesheet" href="{{URL::asset('blog/css/login.css')}}">
@endsection 

@section('main-content')

<section class="login-block">
    <div class="container">
        <div class="row">
            <div class="col-md-4 login-sec">
                <h2 class="text-center">Login Now</h2>
                <form class="login-form" method="POST" action="">
                    {{-- {{ csrf_field() }} --}}
                    {{-- @if(session()->has('login_error')) --}}
                    <div class="alert alert-success">
                      {{-- {{ session()->get('login_error') }} --}}
                    </div>
                     {{-- @endif --}}
                    <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <label for="exampleInputEmail1" class="text-uppercase">E-mail</label>
                        <input type="email" class="form-control" name="email">
                        {{-- @if ($errors->has('email')) --}}
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                {{-- @endif --}}
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <label for="exampleInputPassword1" class="text-uppercase">Password</label>
                        <input type="password" class="form-control" placeholder="*****" name="password">
                        {{-- @if ($errors->has('password')) --}}
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    {{-- @endif --}}
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <small for="remember">Remember Me</small>
                        </label>                        
                    </div>
                    <br>
                    <center>
                            {{-- value="login" --}}
                        <input type="submit" class="btn btn-login" value="login">
                    </center>
                </form>
                <br>
                <div class="copy-text">Belum punya akun ? daftar 
                    <i class="fa fa-sign-in"></i> 
                    <a href="{{URL::to('/home/komunitas/register')}}">Disini</a>
                </div>
            </div>
            <div class="col-md-8 banner-sec">
                
                <img class="bannerpost" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <div class="banner-text">
                        <h2>Panduasia</h2>
                        <p>Dengan adanya Panduasia, para pemandu dapat menciptakan suasana dimana para traveller merasa nyaman dan menikmati perjalanannya
                            sepanjang waktu.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @endsection
    