@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
<meta name="keywords" content="Pandu Asia Blog" /> 
@endsection 
@section('main-content')
@include('blog/_layouts/komunitas')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 {{ __('Logout') }}
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
               <h2>You are logged in!</h2>
            <a href="#" class="btn btn-primary" >Buat artikel</a>
            </div>
        </div>
    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    