@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
<meta name="keywords" content="Pandu Asia Blog" /> 
@endsection 
@section('main-content')

@include('blog/_layouts/komunitas')
                        <div class="flex-container_1">
                            <div class="table-responsive">
                                <table class="table">
                                    <caption>Daftar Member Komunitas</caption>
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Fullname</th>
                                        <th scope="col">Umur</th>
                                        <th scope="col">Asal</th>
                                        <th scope="col">Chat</th>
                                        <th scope="col">Hapus</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                      </tr>
                                      <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                      </tr>
                                      <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                      </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection
    