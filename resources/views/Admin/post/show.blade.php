@extends('Admin.app')
@section('main-content')
<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/listpost')}}">List Post</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Post</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Demo</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Author</th>
                    <th>Demo</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <?php $n = 1; ?>
                @foreach($posts as $post)
                @if($post['pageataupost'] == 'post')
                <tbody>
                  <tr>
                    <td>{{ $n++ }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ ($post->status == 1) ? "Published" : "Draft"  }}</td>
                    <td><?php echo date('D, d-m-Y g:i A', strtotime($post->created_at)) ?></td>
                    <td>{{ $post->posted_by }}</td>
                    <td><a href="{{ url('blog/'.$post->slug) }}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                    <td>
                      <a href="{{ route('post.edit',$post->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                    |
                      {{-- {{ route('post.destroy',$post->id) }} --}}
                      <form id="delete-form-{{ $post->id }}" method="post" action="{{ route('post.destroy',$post->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                      <a href="" onclick="
                      if(confirm('Are you sure, You Want to delete this?'))
                      {
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $post->id }}').submit();
                      }
                      else{
                        event.preventDefault();
                      }" ><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                </tbody>
                @endif
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="page-footer">
    <p class="no-s">2018 &copy; Panduasia .</p>
  </div>
</div><!-- Page Inner -->
</main><!-- Page Content -->
<nav class="cd-nav-container" id="cd-nav">
  <header>
    <h3>Navigation</h3>
    <a href="#0" class="cd-close-nav">Close</a>
  </header>
  <ul class="cd-nav list-unstyled">
    <li class="cd-selected" data-menu="index">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-home"></i>
        </span>
        <p>Dashboard</p>
      </a>
    </li>
    <li data-menu="profile">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-user"></i>
        </span>
        <p>Profile</p>
      </a>
    </li>
    <li data-menu="inbox">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-envelope"></i>
        </span>
        <p>Mailbox</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-tasks"></i>
        </span>
        <p>Tasks</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-cog"></i>
        </span>
        <p>Settings</p>
      </a>
    </li>
    <li data-menu="calendar">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-calendar"></i>
        </span>
        <p>Calendar</p>
      </a>
    </li>
  </ul>
</nav>
<div class="cd-overlay"></div>
@endsection
