@extends('Admin.app')
@section('main-content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Create Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/post/create')}}">Create Post</a></li>
      </ol>
    </div>
  </div>
  
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ route('post.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" />
          </div>
          <h4 class="no-m m-b-sm m-t-lg">Pilih Kategori</h4>
          <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="categories[]">
            <optgroup label="Pilih Kategori">
              @foreach ($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </optgroup>
          </select>
          
          <div class="form-group">
            <label>Body description</label>
            <textarea name="description" id="editor1" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>Upload Image (Main Image)</label>
            <input type="file" name="image_post">
            <span class="help-block text-danger"></span>
          </div>
          
          <h2>SEO Tools</h2>
          <br>
          <div class="form-group">
            <label for="">Meta Title</label>
            <input type="text" class="form-control" required="required" name="metatitle" />
          </div>
          <div class="form-group">
            <label for="">Meta Keyword</label>
            <input type="text" class="form-control" required="required" name="metakeyword" />
          </div>
          <div class="form-group">
            <label for="">Meta Description</label>
            <input type="text" class="form-control" required="required" name="metadescription" />
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-success" value="Tambah baru">
          </div>
          
        </div>
      </div><!-- Main Wrapper -->
      <div class="page-footer">
        <p class="no-s">2018 &copy; Panduasia .</p>
      </div>
    </div><!-- Page Inner -->
  </main><!-- Page Content -->
  <nav class="cd-nav-container" id="cd-nav">
    <header>
      <h3>Navigation</h3>
      <a href="#0" class="cd-close-nav">Close</a>
    </header>
    <ul class="cd-nav list-unstyled">
      <li class="cd-selected" data-menu="index">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-home"></i>
          </span>
          <p>Dashboard</p>
        </a>
      </li>
      <li data-menu="profile">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-user"></i>
          </span>
          <p>Profile</p>
        </a>
      </li>
      <li data-menu="inbox">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-envelope"></i>
          </span>
          <p>Mailbox</p>
        </a>
      </li>
      <li data-menu="#">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-tasks"></i>
          </span>
          <p>Tasks</p>
        </a>
      </li>
      <li data-menu="#">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-cog"></i>
          </span>
          <p>Settings</p>
        </a>
      </li>
      <li data-menu="calendar">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-calendar"></i>
          </span>
          <p>Calendar</p>
        </a>
      </li>
    </ul>
  </nav>
  <div class="cd-overlay"></div>
  @include('Admin._layouts.footer')
  @endsection
  
  
  