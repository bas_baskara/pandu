@extends('Admin.app')
@section('main-content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Create Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/post/edit')}}">Edit Post</a></li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ route('user.update',$user->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" required="required" name="name" value="{{ $user->name}}"  />
          </div>
          <div class="form-group">
            <label>Fullname</label>
            <input type="text" class="form-control" required="required" name="fullname" value="{{ $user->fullname}}"/>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required="required" name="email" value="{{ $user->email}}"/>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" required="required" name="password" value=""/>
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="required" name="phone" value="{{ $user->phone}}"/>
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" required="required" name="address" value="{{ $user->address}}"/>
          </div>
          <div class="form-group">
              <label>Upload Image (avatar)</label>
              <input type="file" name="file">
              <span class="help-block text-danger"></span>
            </div>
          <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control">
              <option value=""></option>
              <option value="Admin">Admin</option>
              <option value="Content Writer">Content Writer</option>
            </select> 
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary">
          </div>
          
        </div>
      </div><!-- Main Wrapper -->
      <div class="page-footer">
        <p class="no-s">2018 &copy; Panduasia .</p>
      </div>
    </div><!-- Page Inner -->
  </main><!-- Page Content -->
  <nav class="cd-nav-container" id="cd-nav">
    <header>
      <h3>Navigation</h3>
      <a href="#0" class="cd-close-nav">Close</a>
    </header>
    <ul class="cd-nav list-unstyled">
      <li class="cd-selected" data-menu="index">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-home"></i>
          </span>
          <p>Dashboard</p>
        </a>
      </li>
      <li data-menu="profile">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-user"></i>
          </span>
          <p>Profile</p>
        </a>
      </li>
      <li data-menu="inbox">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-envelope"></i>
          </span>
          <p>Mailbox</p>
        </a>
      </li>
      <li data-menu="#">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-tasks"></i>
          </span>
          <p>Tasks</p>
        </a>
      </li>
      <li data-menu="#">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-cog"></i>
          </span>
          <p>Settings</p>
        </a>
      </li>
      <li data-menu="calendar">
        <a href="javsacript:void(0);">
          <span>
            <i class="glyphicon glyphicon-calendar"></i>
          </span>
          <p>Calendar</p>
        </a>
      </li>
    </ul>
  </nav>
  <div class="cd-overlay"></div>
  @endsection