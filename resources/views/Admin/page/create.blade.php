@extends('Admin.app')
@section('main-content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Create Page</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/page/create')}}">Create Page</a></li>
      </ol>
    </div>
  </div>
  
  <form role="form" action="{{ url('rahasiadapur/page/store') }}" method="POST" enctype="multipart/form-data" class=" form-horizontal">
          {{ csrf_field() }}
    <div id="main-wrapper">
      <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control" required="required" name="title" id="title" />
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Category</h4>
              <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="categories[]" id="categories">
                  <optgroup label="Pilih Kategori">
                 @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                  </optgroup>
              </select>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="description" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
              <label>Upload Image (Main Image)</label>
              <input type="file" name="image_post">
              <span class="help-block text-danger"></span>
            </div>

           <?php /*  <div class="form-group">
                  <label for="post_status">Featured Image</label>
                  <input type="hidden" name="featured_image" value="<?php echo set_value('featured_image', isset($page['featured_image']) ? $page['featured_image'] : '')?>" id="featured_image">
                  <div class="preview_featured_image">
                    <?php if(!empty($page['featured_image'])): ?>
                      <img src="<?php echo base_url('assets/uploads/').$page['featured_image']?>" class="img-responsive thumbnail" onclick="removeFeaturedImage()" style="width:150px;height:150px;cursor:pointer">
                    <?php endif; ?>
                  </div>
                  <input type="file" name="featured_image" id="featured_image" value="<?php echo set_value('featured_image');?>" onchange="readURL(this);" accept="image/*">
                </div> */ ?>

        </div>
      </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" maxlength="120"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" id="metakeyword" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" maxlength="300"/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

    </div><!-- Main Wrapper -->

</form>

    <div class="page-footer">
      <p class="no-s">2018 &copy; Panduasia .</p>
    </div>
  </div><!-- Page Inner -->
</main><!-- Page Content -->

<nav class="cd-nav-container" id="cd-nav">
  <header>
    <h3>Navigation</h3>
    <a href="#0" class="cd-close-nav">Close</a>
  </header>
  <ul class="cd-nav list-unstyled">
    <li class="cd-selected" data-menu="index">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-home"></i>
        </span>
        <p>Dashboard</p>
      </a>
    </li>
    <li data-menu="profile">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-user"></i>
        </span>
        <p>Profile</p>
      </a>
    </li>
    <li data-menu="inbox">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-envelope"></i>
        </span>
        <p>Mailbox</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-tasks"></i>
        </span>
        <p>Tasks</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-cog"></i>
        </span>
        <p>Settings</p>
      </a>
    </li>
    <li data-menu="calendar">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-calendar"></i>
        </span>
        <p>Calendar</p>
      </a>
    </li>
  </ul>
</nav>
<div class="cd-overlay"></div>
@endsection
