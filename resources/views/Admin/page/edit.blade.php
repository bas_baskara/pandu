@extends('Admin.app')
@section('main-content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Update Page</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/page/edit/'. Request::segment(4)); ?>">Edit Post</a></li>
      </ol>
    </div>
  </div>

  <form role="form" action="{{ URL::to('rahasiadapur/page/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
  {{ csrf_field() }}
{{--   {{ method_field('PATCH') }} --}}

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" value="{{ $p->title }}"/>
          </div>

          <h4 class="no-m m-b-sm m-t-lg">Category</h4>
          <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%"  name="categories[]">
            <optgroup label="Pilih Kategori">

              <?php

              $result = explode(",",$p->category);

              $selected = 'selected="selected"';

              foreach ($categories as $category){ ?>
             <option value="<?php echo $category->id; ?>"
              <?php
              foreach ($result as $postCategory){

                if ($postCategory == $category->id){
                  echo $selected;
                }
              }
        
            ?>
              ><?php echo $category->name; ?></option>
              <?php } ?>
            </optgroup>
          </select>
          
            <div class="form-group">
              <label>Description</label>
                 <textarea name="description" id="" cols="30" rows="10"> {{ $p->description }}</textarea>
            </div>
          <div class="form-group">
            <label>Upload Image (Main Image)</label>
            <input type="file" name="image_post">
            <img src="{{ asset('public/assets/images/'.$p->image) }}" alt="{{ $p->title }}" width="300"/>
            <span class="help-block text-danger"></span>
          </div>

      </div>
    </div>

      <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" maxlength="70" value="{{ $p->metatitle }}" />
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" value="{{ $p->metakeyword }}" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" maxlength="300" value="{{ $p->metadescription }}" />
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>
    </div><!-- Main Wrapper -->

  </form>

    <div class="page-footer">
      <p class="no-s">2018 &copy; Panduasia .</p>
    </div>
  </div><!-- Page Inner -->
</main><!-- Page Content -->

<nav class="cd-nav-container" id="cd-nav">
  <header>
    <h3>Navigation</h3>
    <a href="#0" class="cd-close-nav">Close</a>
  </header>
  <ul class="cd-nav list-unstyled">
    <li class="cd-selected" data-menu="index">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-home"></i>
        </span>
        <p>Dashboard</p>
      </a>
    </li>
    <li data-menu="profile">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-user"></i>
        </span>
        <p>Profile</p>
      </a>
    </li>
    <li data-menu="inbox">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-envelope"></i>
        </span>
        <p>Mailbox</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-tasks"></i>
        </span>
        <p>Tasks</p>
      </a>
    </li>
    <li data-menu="#">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-cog"></i>
        </span>
        <p>Settings</p>
      </a>
    </li>
    <li data-menu="calendar">
      <a href="javsacript:void(0);">
        <span>
          <i class="glyphicon glyphicon-calendar"></i>
        </span>
        <p>Calendar</p>
      </a>
    </li>
  </ul>
</nav>
<div class="cd-overlay"></div>
@endsection