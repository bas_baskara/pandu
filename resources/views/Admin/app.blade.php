<!DOCTYPE html>
<html>
<head>
  @include('Admin._layouts.head')
</head>
<body class="page-header-fixed">

  <div class="wrapper">
    @include ('Admin._layouts.header')
    @yield('main-content')
  </div>

@include('Admin._layouts.footer');
</body>
</html>
