<title>Blog | Admin Panduasia</title>

<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta charset="UTF-8">
<meta name="robots" content="noindex, nofollow"/>
<meta name="googlebot" content="noindex"/>

        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>


        <link href="{{URL::asset('public/assets/vendors/datatables/css/jquery.datatables.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/datatables/css/jquery.datatables_themeroller.css')}}" rel="stylesheet" type="text/css"/>

        {{-- {{ Html::style('public/assets/vendors/uniform/css/uniform.default.min.css') }} --}}
        <link href="{{URL::asset('public/assets/vendors/pace-master/themes/blue/pace-theme-flash.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('public/assets/vendors/uniform/css/uniform.default.min.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('public/assets/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/fontawesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/line-icons/simple-line-icons.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/offcanvasmenueffects/css/menu_cornerbox.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/waves/waves.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/3d-bold-navigation/css/style.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/slidepushmenus/css/component.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/summernote-master/summernote.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/bootstrap-colorpicker/css/colorpicker.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="{{URL::asset('public/assets/css/modern.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/css/themes/green.css')}}" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="{{URL::asset('public/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>

        <script src="{{URL::asset('public/assets/vendors/3d-bold-navigation/js/modernizr.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/offcanvasmenueffects/js/snap.svg-min.js')}}"></script>


        <!-- Javascripts -->
        <script src="{{URL::asset('public/assets/vendors/jquery/jquery-2.1.4.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/pace-master/pace.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/jquery-blockui/jquery.blockui.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/switchery/switchery.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/uniform/jquery.uniform.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/offcanvasmenueffects/js/classie.js')}}"></script>
        {{-- <script src="{{URL::asset('public/assets/vendors/offcanvasmenueffects/js/main.js')}}"></script> --}}
        <script src="{{URL::asset('public/assets/vendors/waves/waves.min.js')}}"></script>
        {{-- <script src="{{URL::asset('public/assets/vendors/3d-bold-navigation/js/main.js')}}"></script> --}}
        <script src="{{URL::asset('public/assets/vendors/jquery-mockjax-master/jquery.mockjax.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/moment/moment.js')}}"></script>

         <script src="{{URL::asset('public/assets/vendors/datatables/js/jquery.datatables.min.js')}}"></script>
         <script src="{{URL::asset('public/assets/vendors/datatables/js/jquery.datatables.min.js')}}"></script>
         <script src="{{URL::asset('public/assets/vendors/x-editable/bootstrap3-editable/js/bootstrap-editable.js')}}"></script>
         <script src="{{URL::asset('public/assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
         <script src="{{URL::asset('public/assets/js/pages/table-data.js')}}"></script>

          <!-- Javascripts -->
        <script src="{{URL::asset('public/assets/vendors/summernote-master/summernote.min.js')}}"></script>
        
        <script src="{{URL::asset('public/assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/js/modern.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/js/pages/form-elements.js')}}"></script>


        <script src="{{URL::asset('public/assets/vendors/select2/js/select2.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/js/pages/form-select2.js')}}"></script>


    
         @section('head')
                   @show
