@extends('layout')
@section('content')

<div class="containerPostView">
	{{-- @foreach ($page as $post) --}}
	<div class="cardPostView">
		@if(empty($result->image) || $result->image == "null")
		<img src="{{ URL::asset('www/assets/images/default.png')}}" alt="{{$result->title}}" style="width: 100%">
        @else
		<img src="{{ URL::asset('www/assets/blog_images/'.$result->image)}}" alt="{{$result->title}}" class="bannerpost" style="width: 100%;">
		@endif

		<div class="containerPostView">

			<br>
			<div class="post-content-view">
				<h1 class="title">{{$result->title}}</h1>

					{{-- <p class="bodycontent"> --}}
						{!! htmlspecialchars_decode($result->description) !!}
						{{-- </p> --}}
				</div>
				<hr>
				<div class="foot">
					{{-- <div class="col-sm-8">
						@foreach ($users as $user)
						@if ($result->posted_by == $user['name'])
						<img src="{{ URL::asset('www/assets/profile_images/'.$user->avatar)}}" alt="author" class="authorimg">
						@endif
						@endforeach
						
						<p>by {{$result->posted_by}}</p>
					</div>
					
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div> --}}
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}
			
		<br>
{{-- 		<div class="container">
        <div class="row"> --}}
        <div class="col-sm-6">
		@if(isset($previous))
		<a href="{{ url('/blog/'.$previous->slug) }}" class="btn btn-success pull-left post-nav">Previous</a>
		@endif
		</div>
		<div class="col-sm-6">
		@if(isset($next))
		<a href="{{ url('/blog/'.$next->slug) }}" class="btn btn-success pull-right post-nav">Next</a>
		@endif
		{{-- </div>
		</div> --}}
		</div>
	</div>
</div>
</div>

@endsection
