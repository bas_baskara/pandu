@extends('layout')
@section('content')

<header id="gtco-header" role="banner">
	<img src="{{URL::asset('www/assets/blog_images/blog.jpg')}}" alt="panduasia blog" class="gtco-cover">
</header>
<!-- END #gtco-header -->

<div class="container">
	<div class="row">
		<div class="gtco-services gtco-section">
			<div class="gtco-heading text-center">
				<h2 style="font-weight:500;" >Artikel Terbaru</h2>
				<hr>
				<form autocomplete="off" action="{{ url()->current() }}">
					<div class="row row-pb-sm">
						<div class="col-md-4 col-md-offset-4">
							<div class="form-group text-center">
								<input type="text" class="form-control" id="search" name="keyword" placeholder="Search..">
								<div id="articlelist"></div>
								<br>
								<input type="submit" value="Search" class="btn btn-info btn-lg">
							</div>
							{{ csrf_field() }}
						</div>
					</div>
				</form>
				<h4>Search By kategori : </h4>

				<div class="category-button col-md-12">
					@foreach($categories as $category)		
					<a href="{{ url('category/'.$category->slug)}}"  class="flex-item btn btn-info" >{{$category->title}}</a>
					@endforeach


					<a class="btn btn-dark flex-item" href="">All</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		@foreach($posts as $post)
		{{-- @if($post->isapprove == 1 && $post->pageataupost == "post" && $post->ismember == 0) --}}
		<?php //var_dump($post); ?>
		<div class="col-md-4">
			<div class="column">
				<!-- Post-->
				<div class="post-module">
					<!-- Thumbnail-->
					<div class="thumbnail">
						<div class="date">
							<div class="day">{{$post->created_at->format('d')}}</div>
							<div class="month">{{$post->created_at->format('M')}}</div>
						</div>

						@if(empty($post->image) || $post->image == "null")
						<img src="{{ URL::asset('www/assets/images/default.png')}}" alt="{{$post->title}}">
						@else
						<img src="{{ URL::asset('www/assets/blog_images/'.$post->image)}}" alt="{{$post->title}}">
						@endif

					</div>
					<!-- Post Content-->
					<div class="post-content">
						{{-- @foreach ($post->categories as $category) --}}
						{{-- <div class="category">{{$category->name}}</div> --}}
						{{-- @endforeach --}}

						<h1 class="title">
							<a href="{{ url('blog/'.$post->slug) }}">{{$post->title}}</a>
						</h1>
						
						{{-- <p class="description">
							{!! htmlspecialchars_decode(str_limit($post->description,150)) !!}
						</p> --}}
						<div class="post-meta">
							<span class="timestamp">
								<i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
								
								<span class="views">
									<i class="fa fa-user"></i>
									<a href="#"> {{$post->visitor}} Views</a>
								</span>
								<a href="{{ url('blog/'.$post->slug) }}" style="color:red;">Selengkapnya</a>
							</div>
						</div>
					</div>
				</div>
			</div>		
			{{-- @endif --}}
			@endforeach
			<?php //die(); ?>
		</div>
		
		<!-- Pager -->
		<div style="margin: 0 auto;">
			<!--<div class="next">-->
			{{ $posts->links() }}	            
			<!--</div>-->
		</div>

		</div>

	</div>

</div>


@endsection
