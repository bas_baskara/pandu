@extends('layout')
@section('content')

<header id="gtco-header" class="gtco-cover gtco-cover-xs gtco-inner" role="banner">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-12 col-md-offset-0 text-left">
        <div class="display-t">
          <div class="display-tc">
            <div class="row">
              <div class="col-md-8">
                <h1 class="no-margin">Panduasia</h1>
                <p>Panduasia membantu para traveller dalam mencari tempat destinasi wisata yang menarik di berbagai wilayah di Indonesia
                  dengan cara mempertemukan traveller dengan para pemandu / tourguide yang berkualitas dari berbagai wilayah dan
                  komunitas.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- END #gtco-header -->

<!-- END #gtco-header -->
<div class="gtco-services gtco-section" style="background-color: white;">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <img src="{{ URL::asset('www/assets/blog_images/about.jpg') }}" alt="image_about" width="250px" height="250px">
        <h2>Tentang Panduasia</h2>
        <p>Platform marketplace di bidang wisata terdepan di Indonesia, panduasia, membantu para pemandu dari berbagai wilayah,
          komunitas dan traveller untuk berpartisipasi dalam pertumbuhan pariwisata di indonesia.
          <b>Visi Panduasia adalah meningkatkan dunia pariwisata di indonesia dengan menciptakan koneksi antara para traveller
            dengan para pemandu wisata dan komunitasnya.</b>
          Panduasia percaya dengan adanya marketplace di bidang wisata ini dapat menumbuhkan perekonomian para pemandu dari berbagai
          wilayah dan menciptakan komunitas-komunitas yang aktif berperan di dalam meningkatkan industri pariwisata di Indonesia
        </p>
      </div>
    </div>
    <div class="row row-pb-md">
      <div class="row">
        <div class="service ">
          <img src="{{ URL::asset('www/assets/images/panduasia.png') }}" alt="logo_panduasia" width="390px" height="150px">
          <p class="gtco-heading text-center">Panduasia yang berdiri pada tanggal <b> 30 April 2018</b> memiliki komitmen yang kuat untuk membangun dunia pariwisata yang attraktif, menakjubkan, menarik, indah, dan menjadikan moment yang tak terlupakan bagi para traveller dengan dibantu oleh para pemandu wisata yang ramah, berpengetahuan dan berpengalaman di berbagai destinasi wisata.
            </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="gtco-services gtco-section" style="background-color: white;">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Misi Panduasia</h2>
        <p>Panduasia berusaha mewujudkan misi pemberdayaan para pemandu beserta komunitasnya untuk berpartisipasi meningkatkan
          industri pariwisata di Indonesia. Dalam rangka menumbuhkan misi besar ini, panduasia mengajak para pemandu/tour guide,
          para komunitas dan para talenta-talenta terbaik yang ingin bergabung bersama kami, siapapun dan dimanapun untuk menggapai
          mimpi dan cita-cita.
        </p>
      </div>
      <div class="row row-pb-md" >
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3 >
              <i class="ti-pie-chart"></i>Join Tour Guide</h3>
            <p style="color: #2f2a2a;"> Join sebagai tour guide bersama panduasia.</p>
            <p style="color: #2f2a2a;">klik register
              <a href="">Tour Guide</a>
            </p>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3>
              <i class="ti-ruler-pencil"></i>Komunitas</h3>
            <p style="color: #2f2a2a;"> Mari ciptakan komunitas-komunitas yang dapat meningkatkan dunia industri pariwisata di Indonesia
            klik register komunitas<a href=""> disini</a></p>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3>
              <i class="ti-settings"></i>Program Internship</h3>
            <p style="color: #2f2a2a;">Ingin belajar banyak tentang Dunia Teknologi? gabung bersama Panduasia.</p>
            <p style="color: #2f2a2a;">
              klik program
               <a href="{{URL::to('/careers')}}">Internship</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="gtco-services gtco-section">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2 class="description">Our Team</h2>

      </div>
    </div>

    <section class="team">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="col-lg-12">

              <div class="row pt-md">
                <div class="col-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto1.jpg') }}" class="img-responsive">
                    <ul class="text-center">
                    </ul>
                    <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                    <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                  </div>
                  <h3>Abu Tholib</h3>
                  <h5>C E O - Founder </h5>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto7.jpeg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Wageningtyas</h3>
                  <h5>Marketing Advisor</h5>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto2.jpg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Ilham Nur Hakim</h3>
                  <h5>Fullstack Developer</h5>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/danar.jpg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Danar Winanda</h3>
                  <h5>Back-end Developer</h5>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto4.jpeg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Dina Nisa</h3>
                  <h5>Digital Marketing</h5>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto6.jpeg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Saugi</h3>
                  <h5>Vlog</h5>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{ URL::asset('www/assets/profile_images/foto5.jpeg') }}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h3>Faisal</h3>
                  <h5>Finance &amp; Accounting</h5>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



  </div>
</div>
<!-- END .gtco-services -->

<div id="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.8294639971077!2d106.84823551415512!3d-6.286134695450274!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3f1638ab025%3A0x5b58a1ac6635f66!2sPandu+Asia+Indonesia!5e0!3m2!1sen!2sid!4v1547817793845" 
      width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

@endsection