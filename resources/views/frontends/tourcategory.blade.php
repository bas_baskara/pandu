@extends('layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                <h2>Destinasi berdasarkan Kategori</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
           <?php /*  <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;">  */ ?>
        </div>
            <div class=" col-md-8" style="padding:15px;">
                <?php /* <h2 class="card-title">{{ $result->main_city }}</h2>
                
                <small>{{ str_limit($result->main_description, 700) }}</small> */ ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4" style="margin-top:15px;float:left; ">
                    <div class="card-filter" style="position:sticky;">
                        <div class="card-body">
                            <h5 class="card-title">Urutkan berdasarkan</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Harga Terendah</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Review</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Populer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Jemput di Hotel</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Makan Siang / Malam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Foto Dokumentasi</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Tiket gratis umur dibawah 6 tahun</label>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Kategori</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pantai</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Rekreasi Keluarga</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Romantis</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pegunungan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Alam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Budaya</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Perkotaan</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-top:15px;">
                    @foreach($tour_by_category as $ktbc => $vtbc)
                    <div class="list-group-item wisata">
                       <div  class="list-group-item-wisata">
                                                    
                            <div class="media col-lg-3">
                                <figure class="pull-left">
                                    @php 
                                        $pics = json_decode($vtbc->images); 
                                    @endphp
                                    <div class="tourslide">
                                    @foreach($pics as $pic)
                                      <div><img class="media-list-img" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 160px;"></div>                                    
                                    @endforeach
                                    </div>
                                </figure>
                            </div>
                            
                            <div class="col-md-6">
                                <a href="{{ url('tour/'.$vtbc->slug) }}"><h4 class="list-group-item-heading text-center">{{ $vtbc->title }}</h4></a>
                                <p class="list-group-item-text"> 
                                <small><i class="glyphicon glyphicon-map-marker"></i> 
                                    @foreach($city as $kc => $vc)
                                        @if($vtbc->city_id == $vc->id)
                                            {{ $vc->name }}
                                        @endif
                                    @endforeach
                                             , 
                                    @foreach($province as $kp => $vp)
                                        @if($vtbc->province_id == $vp->id)
                                            {{ $vp->name }}
                                        @endif
                                    @endforeach , 
                                    @foreach($country as $kco => $vco)
                                        @if($vtbc->country_id == $vco->id)
                                            {{ $vco->country_name }}
                                        @endif
                                    @endforeach </small>
                                         
                                <hr>
                               <small class="pull-right">
                                    <button type="button" class="btn btn-outline-warning btn-sm">
                                    @foreach($durasi as $kd => $vd)
                                        @if($vtbc->category == $kd)
                                            {{ $vd }}
                                        @endif
                                    @endforeach
                                    </button>
                                </small>

                                <div class="entry">
                                    <a href="{{ url('tour/'.$vtbc->slug) }}" style="color:#000;">{{ str_limit($vtbc->description, 129, '...') }}</a>
                                </div>
  
                                <small>Pemandu : @foreach($g as $kg => $vg)
                                    @if($vtbc->member_id == $vg->id)
                                        {{ $vg->first_name . " " . $vg->last_name }}
                                    @endif
                                @endforeach </small>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h4> Rp. {{ empty($vtbc->promo_price) ? number_format($vtbc->actual_price,0,",",".") : number_format($vtbc->promo_price,0,",",".") }}  <small> / orang </small></h4>
                                <a href="{{ url('tour/'.$vtbc->slug) }}" class="btn btn-book" style="
                                "> Book Now! </a>
                                <div class="stars">
                                    @php $rating = $vtbc->rating; @endphp 

                                @foreach(range(1,5) as $i)
                                    <span class="fa-stack rating" style="width:1em">
                                        
                                        @if($rating > 0)
                                            @if($rating > 0.5)
                                                <i class="fa fa-star" style="color: orange;"></i>
                                            @else
                                                <i class="fa fa-star-half" style="color: orange;"></i>
                                            @endif
                                        @else
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @php $rating-- @endphp
                                    </span>
                                @endforeach
                                </div>
                                <p class="rating-wisata"> Rate {{ $vtbc->rating / count($vtbc->rating) }} <small> / </small> 5 </p>     
                            </div>
                            
                            <hr>

                       </div>
                    </div>
                    <br>
                    @endforeach

                </div>
            </div>
        </div>
        @endsection