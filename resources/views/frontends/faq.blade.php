@extends('layout')
@section('content')

<header id="gtco-header" class="gtco-cover gtco-cover-xs gtco-inner" role="banner">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-12 col-md-offset-0 text-left">
        <div class="display-t">
          <div class="display-tc">
            <div class="row">
              <div class="col-md-8">
                <h1 class="no-margin">Panduasia</h1>
                <p>Bantuan
                </p>
              </div>
              @foreach(App\Post::where('pageataupost','page')->get() as $v   )
              <div class="col-md-6">
                  <ul>
                      <li><a href="{{url('/p/'.$v->slug)}}" style="color:white;">{{$v->title}}</a></li>
                  </ul>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- END #gtco-header -->

@endsection