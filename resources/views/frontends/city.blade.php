@extends('layout')
@section('content')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="gtco-client" >
      <div class="row row-pb-sm">
        <div class="col-md-4 col-md-offset-4">
          <form action="{{ url()->current() }}" autocomplete="off">
            <div class="form-group text-center">
              <label for="name"><b>Mau Ke Kota Mana ?</b></label>
              <input type="text" class="form-control " id="search_city" name="search" placeholder=" misal : Bandung ">
              <div id="citylist"></div>
              <br>
              <br>
              <input type="submit" value="Search" class="btn btn-info btn-lg">  
            </div>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
    <div class="row">
     <?php 
        $result = DB::table('tour_destinations')
                ->orderBy('main_city', 'asc')
                ->get();
     ?>
     @foreach($result as $td )    
      
      <div class="col-md-3">
        <div class="card  ">
          <div class="social-card-header align-middle text-center ">
            <img src="{{URL::asset('www/assets/destinations/'.$td->main_image_tours)}}"
              class="image" alt=" {{ $td->main_city }} ">
          </div>
          <hr>
          <a href="{{URL::to('wisata/'. $td->main_city )}}">
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">{{ $td->main_city }}</span>
                  </div>
                  <div class="col">
                    <span class="text-muted">Start from</span>
                    <div class="text-harga">420K</div>
                  </div>
                </div>
              </div>
          </a>
        </div>
      </div>
     
      @endforeach
      
    </div>
  </div>
</div>

@endsection
