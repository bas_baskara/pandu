@extends('layout')
@section('head')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
var memberId = '{{$member->id}}';
</script>  
@endsection
@section('content')

<div class="col-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(1)))) }}</a></li>
          <li class="breadcrumb-item">{{ str_replace('-', ' ', ucwords(trans(Request::segment(2)))) }}</li>
      </ol>
  </nav>
</div>

<div class="container">
  <div class="row">

        <div class="col-8">
          <h2 class="card-title-wisata">{{ $detailtour->title }}</h2>
          <p><i class="glyphicon glyphicon-map-marker"></i>{{ $detailtour->city_id == $city->id ? $city->name : "" }}, {{ $detailtour->province_id == $province->id ? $province->name : "" }}, {{ $detailtour->country_id == $country->id ? $country->country_name : "" }}</p>  

            @php 
                $pics = json_decode($detailtour->images); 
            @endphp

            <div class="main-image-wisata">
                @foreach($pics as $pic)
                  <div><img class="img-responsive" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 100px;"></div>
                @endforeach
            </div>
            <hr>
          </div>

          <div class="col-4">
            <div class="card">
              {{-- <div class="panel-heading">Panel heading without title</div> --}}
                <div class="card-body">
                  <div class="text-center">  

                    <h2> Rp. {{ empty($detailtour->promo_price) ? number_format($detailtour->actual_price,0,",",".") : number_format($detailtour->promo_price,0,",",".") }} <small> / orang </small></h2>

                    <form action="{{ url('tour/booking/'.Request::segment(2)) }}" method="post">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <input type="number" class="form-control" name="jumlahpeserta" id="jumlahpeserta" min="{{ $detailtour->min_kuota }}" max="{{ $detailtour->max_kuota }}" onkeyup="tourprice()" autofocus placeholder="Jumlah Peserta" />
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" name="tanggalberangkat" id="tanggalberangkat" placeholder="Tanggal Berangkat" />
                      </div>
                      <input type="hidden" name="customer_id" id="customer_id" value="{{ isset(Auth::user()->id) ? Auth::user()->id : NULL }}" />
                      <input type="hidden" name="bookingstatus" id="bookingstatus" value="2" />
                      <input type="hidden" name="slug" id="slug" value="{{ Request::segment(2) }}" />
<!--  -->

<!--  -->





                      {{-- <div class="form-group">
                        <select class="form-control" name="pakettrip">
                          <option value="0">-- Paket Trip --</option>
                          @foreach($durasi as $kd => $vd)
                            <option value="{{ $kd }}">{{ $vd }}</option>
                          @endforeach
                        </select>
                      </div> --}}

                      <table>
                        <thead>
                          <tr>
                            <th><h2>Total : </h2></th>
                            <th><h2><div class="showfinalprice"></div></h2></th>
                          </tr>
                        </thead>
                      </table>

                      <input type="submit" value="Book Now!"  class="btn btn-book btn-lg" style="width: 100%;">
                    </form>
                                       
                  </div>
                </div>
              </div>

              <div class="card bg-default">
                <div class="card-body text-center">
                  @if(empty($member->profile_img))
                  <img src="{{ asset('www/assets/images/default.png') }}" alt="" class="img-responsive" style="width: 100px;height:100px;" />
                  @else
                  <img src="{{ asset('www/assets/communities/avatars/'.$member->profile_img) }}" alt="" class="img-responsive" style="width: 100px;height:100px;" />
                  @endif
                  <br>
                  <small>Guide : </small><label for="">{{ $detailtour['member_id'] == $member['id'] ? $member['first_name'] ." ". $member['last_name'] : "" }}</label>
                  <small>from </small><label for="">{{ $detailtour['community_id'] == $community['id'] ? $community['community_name'] : "" }} </label> Community
                  <br>
                  <small>Bahasa : </small><label for="">Indonesia, English</label>
                  <div class="text-center">
                    <small>Rating</small>
                    <div class="stars">
                      @php $rating = $detailtour->rating; @endphp 

                      @foreach(range(1,5) as $i)
                        <span class="fa-stack rating" style="width:1em">
                            
                            @if($rating > 0)
                                @if($rating > 0.5)
                                    <i class="fa fa-star" style="color: orange;"></i>
                                @else
                                    <i class="fa fa-star-half" style="color: orange;"></i>
                                @endif
                            @else
                                <i class="fa fa-star-o"></i>
                            @endif
                            @php $rating-- @endphp
                        </span>
                      @endforeach
                    </div>
                    <p class="rating-wisata"> Rate {{ $detailtour->rating / 1 }} <small> / </small> 5 </p>
                    
                  </div>
                </div>
              </div>
            </div>
                      
            <div class="col-md-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Deskripsi
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        {!! $detailtour->description !!}
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#priceDetail" aria-expanded="false" aria-controls="priceDetail">
                          Rincian Harga
                        </a>
                      </h4>
                    </div>
                    <div id="priceDetail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        {!! $detailtour->pricedetail !!}
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#tripPlans" aria-expanded="false" aria-controls="tripPlans">
                          Rencana Perjalanan
                        </a>
                      </h4>
                    </div>
                    <div id="tripPlans" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        {!! $detailtour->tripplans !!}
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="" id="chat" aria-expanded="false" aria-controls="tripPlans">
                          Chat
                        </a>
                      </h4>
                    </div>
                  </div>

                </div>

            </div>
              
            <div class="col-sm-12">
              <h2 class="page-header">Comments</h2>
              <section class="comment-list">
                <!-- First Comment -->
                <article class="row">
                  <div class="col-md-2 col-sm-2">
                    <figure class="thumbnail">
                      <img class="img-responsive" src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" />
                      <figcaption class="text-center">username</figcaption>
                    </figure>
                  </div>
                  <div class="col-md-10 col-sm-10">
                    <div class="card bg-default arrow left">
                      <div class="card-body">
                        <header class="text-left">
                          <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                          <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                            Dec 16, 2014</time>
                          </header>
                          <div class="comment-post">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                              eiusmod tempor
                              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              quis
                              nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat.
                            </p>
                          </div>
                          <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                          </div>
                        </div>
                      </div>
                    </article>
                    <!-- Second Comment Reply -->
                    <article class="row">
                      <div class="col-md-2 col-sm-2">
                        <figure class="thumbnail">
                          <img class="img-responsive" src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" />
                          <figcaption class="text-center">username</figcaption>
                        </figure>
                      </div>
                      <div class="col-md-10 col-sm-10">
                        <div class="card bg-default arrow left">
                          <div class="card-heading right">Reply</div>
                          <div class="card-body">
                            <header class="text-left">
                              <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                              <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                                Dec 16, 2014</time>
                              </header>
                              <div class="comment-post">
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                  eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                  quis
                                  nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                  consequat.
                                </p>
                              </div>
                              <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                      </section>
                    </div>


                    <div class="col-sm-12">
                      <br><br><br>
                      <div class="text-center">
                        <h2>Trip Lainya dari Guide <strong>{{ $member['first_name'] . " " . $member['last_name'] }}</strong></h2>
                      </div>
                      <hr>
                      
                      @foreach($relatedtour_by_member as $kbm => $vbm)
                        @if($vbm->slug != Request::segment(2))
                          <div class="col-sm-4 col-md-3">
                            <div class="thumbnail">
                              @php 
                                  $pics = json_decode($vbm->images); 
                              @endphp
                              <div class="tourslide">
                              @foreach($pics as $pic)
                                <div><img class="media-list-img" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt=""></div>                                    
                              @endforeach
                            </div>

                              <div class="caption">
                                <h3>{{ $vbm->title }}</h3>
                                <p>Rp. {{ empty($vbm->promo_price) ? number_format($vbm->actual_price,0,",",".") : number_format($vbm->promo_price,0,",",".") }}</p>
                                <p><a href="{{ url('tour/'.$vbm->slug) }}" class="btn btn-primary" role="button" style="width: 100%;">Detail</a></p>
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="text-center">
                            <br>
                              <p>Belum ada trip lain</p>
                            <br>
                          </div>
                        @endif
                      @endforeach

                    </div>

                </div>
              </div>

  <!-- Chat -->
@if(Auth::guard('pelanggan')->check())
<!-- Modal -->
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Pesan dari {{$member->first_name.' '.$member->last_name}} </h5>
      </div>
      <div class="modal-body" id="chatToGuide">
        <div class="panel-body">
          <terima-pesan :messages="messagesforCustomer" :customer_id="{{Auth::guard('pelanggan')->user()->id}}" :member_id="{{$member->id}}"></terima-pesan>
        </div>
        <div class="panel-footer">
          <kirim-pesan v-on:messagesent="addMessage" :sender="'{{Auth::guard('pelanggan')->user()->first_name.' '.Auth::guard('pelanggan')->user()->last_name}}'" :customer_id="{{Auth::guard('pelanggan')->user()->id}}" :member_id="{{$member->id}}"></kirim-pesan>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- endchat -->
@endif


@endsection

@section('script')    
@if(Auth::guard('pelanggan')->check())

<script>
  $(function(){
    $('#chat').click(function(){

        var customer = "{{ Auth::guard('pelanggan')->user()->id }}";
        var guide = '{{$member->id}}';
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          method: 'post',
          url: '/chat/start',
          data:{guide:guide, customer:customer},
          success: function(data){
            console.log(data);
            var privateChatId = data;
          }
        });
        $('#chatModal').modal('show');
      
    });
  });
  
</script>
@else
<script type="text/javascript">
 $(function(){
    $('#chat').click(function(){
      window.location.href = '{{asset("/user/signin")}}';
    });
});
</script>
@endif
<script>

  function tourprice()
  {
    var peserta = $('#jumlahpeserta').val();
    var harga = "{{ empty($detailtour->promo_price) ? $detailtour->actual_price : $detailtour->promo_price }}";

    var total = harga * peserta;

    $('.showfinalprice').text("Rp. "+total);
  }

</script>
@endsection