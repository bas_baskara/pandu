@extends('frontends.layout')
@section('content')
<div class="containerPostView">
	{{-- @foreach ($result as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('www/assets/images/'.$result->image)}}" alt="{{$result->title}}" class="bannerpost"> --}}
			@if(empty($result->image) || $result->image == "null")
            <img src="{{ URL::asset('www/assets/images/default.png')}}" alt="{{$result->title}}" style="width: 100%;">
            @else
            <img src="{{ URL::asset('www/assets/blog_images/'.$result->image)}}" alt="{{$result->title}}" style="width: 100%;">
            @endif

		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('/')}}">Home /</a>
				<a href="{{ url('blog/')}}">Blog /</a>

				{{-- @foreach ($result->categories as $category)
					@if(empty($category))
						<a href="{{ url('blog/category/uncategorized')}}">uncategorized</a>
					@else
						<a href="{{ url('blog/category/'.$category->name)}}">{{$category->name}} </a>
					@endif
				@endforeach --}}

			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title">{{$result->title}}</h1>
				<h5 class="timetext">
					<ion-icon name="time" size="small" style="position: relative;top: 5px;right: 3px;"></ion-icon>
					{{ date("D, d M Y - h:i A", strtotime($result->created_at)) }}</h5>
					{{-- <p class="bodycontent"> --}}
						{!! htmlspecialchars_decode($result->description) !!}
						{{-- </p> --}}
				</div>
				<hr>
				<div>
				    <h3>Tag Terkait :</h3>
				    @foreach($tags as $t)
				    @if($result->tag == $t->id)
				    <h3><a href="#">{{$t->title}} </a></h3>
				    @endif
				    @endforeach
				</div>
				</br>
				<div class="foot">
					<div class="col-sm-8">
						@foreach ($users as $user)
						@if ($result->posted_by == $user->name)
						<img src="{{ URL::asset('www/assets/profile_images/'.$user->avatar)}}" alt="author" class="authorimg">
						@endif
						@endforeach
						
						<p>by {{$result->posted_by}}</p>
					</div>
					
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/metro/26/000000/facebook.png"></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/ios/26/000000/google-plus.png"></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/metro/26/000000/twitter.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		<div class="commentBox">
			<div class="fb-comments" data-href="{{ Request::url() }}" data-numresult="10" data-width="100%"></div>
			</div>
			
		<br>
{{-- 		<div class="container">
        <div class="row"> --}}
        <div class="col-sm-6">
		@if(isset($previous))
		<a href="{{ url('/blog/'.$previous->slug) }}" class="btn btn-success pull-left post-nav">Previous</a>
		@endif
		</div>
		<div class="col-sm-6">
		@if(isset($next))
		<a href="{{ url('/blog/'.$next->slug) }}" class="btn btn-success pull-right post-nav">Next</a>
		@endif
		{{-- </div>
		</div> --}}
		</div>
	</div>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=214610849349025&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
@endsection