@extends('layout')
@section('content')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Tour berdasarkan Kota</h2>
      </div>
    </div>
    <div class="row"> 
      @foreach($tourincities as $ktic => $vtic)
        @foreach($city as $kc => $vc)
          @if($vtic->city_id == $vc->id)
            <div class="col-md-3">
              <div class="card">
                  @php 
                    $pics = json_decode($vtic->images); 
                    $rpic = array_slice($pics,0,1);
                  @endphp

                  {{-- <div class="tourslide"> --}}
                    @foreach($rpic as $pic)
                      <div><img class="card-img-top" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 100%;"></div>                                    
                    @endforeach
                  {{-- </div> --}}

                <a href="{{ url('tour/city/'. $vc->slug )}}">
                    <div class="card-body text-center">
                      <div class="row">
                        <div class="col-12">
                          <span class="text-muted">{{ $vtic->city_id == $vc->id ? $vc->name : "" }}</span>
                        <br>
                          <span class="text-muted">Start from</span>
                          <div class="text-harga">Rp. {{ empty($vtic->promo_price) ? number_format($vtic->actual_price,0,",",".") : number_format($vtic->promo_price,0,",",'.') }}</div>
                        </div>
                      </div>
                    </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      @endforeach
    </div>
  </div>
</div>

@endsection
