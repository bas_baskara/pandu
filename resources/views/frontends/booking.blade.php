@extends('layout')
@section('content')

<div class="col-sm-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(1)))) }}</a></li>
          <li class="breadcrumb-item">{{ str_replace('-', ' ', ucwords(trans(Request::segment(2)))) }}</li>
          <li class="breadcrumb-item ">{{ str_replace('-', ' ', ucwords(trans(Request::segment(3)))) }}</li>
      </ol>
  </nav>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-muted">Ringkasan Pemesanan</h4>
                    <div class="col-md-4">
                        @php 
                            $pics = json_decode($detailtour->images); 
                        @endphp
                        <div class="tourslide">
                            @foreach($pics as $pic)
                              <div><img class="img-booking" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt=""></div>                                    
                            @endforeach
                        </div>
                    </div>

                    <div class="col-sm-8 pull-right">
                        <small class="small-booking">{{ $detailtour->title }}</small>
                        <hr>
                    </div>

                    <div class="col-sm-12">
                        <div class="include-desc">
                           <div class="row">
                            {{-- <div class="col-md-2">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAuYMJ0yTQq3XoYeHjrYstcCJnxKJEtlWnWP8aQviBS_lXnF64" style="width:35px;height:35px; margin-right:15px;" >
                            </div> --}}
                            
                            <div class="col" style="margin-top:15px;">
                                  <h4>Pemandu : {{ $detailtour->member_id }}</h4>
                            </div>  
                         </div>
                            <h4>Jumlah perserta : 4</h4>
                            <h4>Tgl keberangkatan : 29-04-2019</h4>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-muted">Detail Harga</h4>
                    <hr>
                    <h4 class="text-muted">Harga Normal - 3 Pax  <p class="pull-right">1,950,000</p></h4>
                   
                    <h4 class="text-muted">Harga /orang <p class="pull-right">650,000 </p></h4>
                    <hr>
                    <h4 class="text-muted">Total Pembayaran <p class="pull-right">1,950,000</p>      </h4>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="cardPostView">
                <div class="bs-callout bs-callout-primary">
                    <h4>Formulir Pendaftaran Tour</h4>
                    <form action="{{ url('') }}" method="POST">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp" name="email">
                            <small id="emailHelp" class="form-text text-muted">* penting dalam proses pemesanan tour</small>
                        </div>
                        <div class="form-group">
                            <label>No KTP/SIM</label>
                            <input type="text" class="form-control" name="ktp_or_license">
                        </div>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_lengkap">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input type="text" class="form-control" name="no_telp">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="alamat"> </textarea>
                        </div>
                    </form>
                </div>
            </div>

            <div class="cardPostView">
                <div class="bs-callout bs-callout-danger">
                    <h4>Data Peserta</h4>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_peserta[]" id="nama_peserta_">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input type="text" class="form-control" name="no_telp_peserta[]" id="no_telp_peserta_">
                            <small id="emailHelp" class="form-text text-muted">*penting jika ada terjadi masalah.</small>
                        </div>
                        <div class="form-group">
                            <label>Usia</label>
                            <input type="text" class="form-control" name="usia_peserta[]" id="usia_peserta">
                        </div>
                        <div class="form-group">
                            <label>No. KTP/SIM</label>
                            <input type="text" class="form-control" name="ktp_sim[]" id="ktp_sim_">
                        </div>
                </div>
            </div>
        </div>

    </div>
    
    <a href="#" class="btn btn-primary pull-right">Konfirmasi Pesanan</a>
    <br>
    <br>
    <br>
    
</div>

@endsection