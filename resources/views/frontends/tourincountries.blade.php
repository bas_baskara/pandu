@extends('layout')
@section('content')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-12 gtco-heading text-center">
        <h2>Tour berdasarkan Negara</h2>
      </div>
    </div>
    <div class="row"> 
      @foreach($tourincountries as $ktic => $vtic)
        @foreach($country as $kc => $vc)
          @if($vtic->country_id == $vc->id)
            <div class="col-md-3">
              <div class="card">

                @php 
                  $pics = json_decode($vtic->images); 
                  $rpic = array_slice($pics,0,1);
                @endphp

                @foreach($rpic as $pic)
                  <div><img class="img-responsive" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 100%;"></div>                                    
                @endforeach

                <a href="{{ url('tour/country/'. $vc->slug )}}">
                  <div class="card-body text-center">
                    <div class="row">
                      <div class="col border-right">
                        <span class="text-muted">{{ $vtic->country_id == $vc->id ? $vc->country_name : "" }}</span>
                      <br>
                        <span class="text-muted">mulai dari</span>
                        <div class="text-harga">Rp. {{ number_format($vtic->actual_price,0,",",".") }}</div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      @endforeach
    </div>
  </div>
</div>

@endsection
