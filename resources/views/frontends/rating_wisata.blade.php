@extends('layout')
@section('content')

<div class="stars">
      <form role="form" autocomplete="off" action="{{ url('rateswisata') }}" method="post" class="form-horizontal">
        {{ csrf_field() }}

                <input class="star star-5" id="star-5" type="radio" name="rates" value="5"/>
        <label class="star star-5" title="Outstanding" for="star-5"></label>
        <input class="star star-4" id="star-4" type="radio" name="rates" value="4"/>
        <label class="star star-4" title="Very Good" for="star-4"></label>
        <input class="star star-3" id="star-3" type="radio" name="rates" value="3"/>
        <label class="star star-3" title="Good" for="star-3"></label>
        <input class="star star-2" id="star-2" type="radio" name="rates" value="2"/>
        <label class="star star-2" title="Poor" for="star-2"></label>
        <input class="star star-1" id="star-1" type="radio" name="rates" value="1"/>
        <label class="star star-1" title="Very Poor" for="star-1"></label>
        <div class="form-group">
              <center>
              <button type="submit" value="submit" class="btn btn-primary btn-lg">Submit</button>
              </center>
        </div>
      </form>
</div>
@endsection


