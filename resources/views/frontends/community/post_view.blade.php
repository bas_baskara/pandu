@extends('layout')
@section('content')
<div class="containerPostView">
	{{-- @foreach ($result as $post) --}}
	<div class="cardPostView">
		{{-- <img src="{{ URL::asset('www/assets/images/'.$result->image)}}" alt="{{$result->title}}" class="bannerpost"> --}}
			@if(empty($result->image) || $result->image == "null")
            <img src="{{ URL::asset('www/assets/images/default.png')}}" alt="{{$result->title}}" style="width: 100%;">
            @else
            <img src="{{ URL::asset('www/assets/communities/blog_images/'.$result->image)}}" alt="{{$result->title}}" style="width: 100%;>
            @endif

		<div class="containerPostView">
		<div class="texturl"  style="padding:15px;">
				<a href="{{ url('/')}}">Home /</a>
				<a href="{{ url('blog/')}}">Blog /</a>

			</div>
			<br>
			<div class="post-content-view" >
				<h1 class="title">{{$result->title}}</h1>
				<h5 class="timetext"  style="padding:15px;">
					<ion-icon name="time" size="small" style="position: relative;top: 5px;right: 3px;"></ion-icon>
					{{ date("D, d M Y - h:i A", strtotime($result->created_at)) }}</h5>
					<p class="bodycontent"  style="padding:15px;">
						{!! htmlspecialchars_decode($result->description) !!}
					</p>
				</div>
				<hr>
				</br>
				<div class="foot">
					<div class="col-sm-8">
						<p>by {{$result->posted_by}}</p>
						<small> {{$result->nm}}</small>
						
					</div>
					
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/metro/26/000000/facebook.png"></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/ios/26/000000/google-plus.png"></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><img src="https://img.icons8.com/metro/26/000000/twitter.png"></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		<div class="commentBox">
			<div class="fb-comments" data-href="{{ Request::url() }}" data-numresult="10" data-width="100%"></div>
			</div>
			
		<br>
	</div>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=214610849349025&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
@endsection