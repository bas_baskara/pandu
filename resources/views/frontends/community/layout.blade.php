<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		$s1 = Request::segment(1);
		$slug = Request::segment(2);
	?>

	@if(isset($slug) == isset($result->slug))
		<title>{{ $result->metatitle }}</title>
		<meta name="description" content="{{ $result->metadescription }}" />
		<meta name="keywords" content="{{ $result->metakeyword }}" />
	@elseif(isset($s1) == 'blog')
		<title>{{ $homepage->meta_title_blog }}</title>
		<meta name="description" content="{{ $homepage->meta_desc_blog }}" />
		<meta name="keywords" content="{{ $homepage->meta_keyword_blog }}" />
	@elseif(isset($s1) == 'about')
		<title>{{ $homepage->meta_title_about }}</title>
		<meta name="description" content="{{ $homepage->meta_desc_about }}" />
		<meta name="keywords" content="{{ $homepage->meta_keyword_about }}" />
	@else
		<title>{{ $homepage->metatitle }}</title>
		<meta name="description" content="{{ $homepage->metadescription }}" />
		<meta name="keywords" content="{{ $homepage->metakeyword }}" />
	@endif

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">

    <!-- Bootstrap  -->
    <link href="{{ asset('www/assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Owl Carousel  -->
    <link href="{{ asset('www/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('www/assets/css/owl.theme.default.min.css') }}" rel="stylesheet">

	<!-- Animate.css -->
	<link href="{{ asset('www/assets/css/animate.css') }}" rel="stylesheet">
	<!-- Icomoon Icon Fonts-->
	<link href="{{ asset('www/assets/css/icomoon.css') }}" rel="stylesheet">
	<!-- Themify Icons-->
	<link href="{{ asset('www/assets/css/themify-icons.css') }}" rel="stylesheet">

    <link href="{{ asset('www/assets/vendors/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
	
	<link href="{{ asset('www/assets/css/login.css') }}" rel="stylesheet">

	<!-- Theme style  -->
	<link href="{{ asset('www/assets/css/styles.css') }}" rel="stylesheet">

	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<link href="{{ asset('www/assets/js/respond.min.js') }}" rel="stylesheet">
	<![endif]-->

	<link href="{{ asset('www/assets/old/css/rotating-card.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125409594-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125409594-1');
</script>


</head>

<body>

	<div class="gtco-loader"></div>

	<div id="page">
        <nav class="gtco-nav" role="navigation">
            @include('partials.header')
        </nav>

        <div class="content">
            @yield('content')
        </div>

    	<br>

    	<footer id="gtco-footer" class="gtco-section" role="contentinfo">
    		@include('partials.footer')
    	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop">
			<i class="icon-arrow-up"></i>
		</a>
	</div>

	<!-- jQuery -->
	<script src="{{ asset('www/assets/js/jquery.min.js') }}"></script>
    {{-- <script src="{{ asset('www/assets/plugins/jquery/jquery.min.js') }}"></script> --}}
    <!-- Bootstrap -->
    <script src="{{ asset('www/assets/js/bootstrap.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('www/assets/old/js/jquery.waypoints.min.js') }}"></script>
	<!-- Carousel -->
	<script src="{{ asset('www/assets/js/owl.carousel.min.js') }}"></script>
	<!-- Main -->
	<script src="{{ asset('www/assets/js/main.js') }}"></script>

	<script type="text/javascript">
		$().ready(function () {
			$('[rel="tooltip"]').tooltip();

		});

		function rotateCard(btn) {
			var $card = $(btn).closest('.card-container');
			console.log($card);
			if ($card.hasClass('hover')) {
				$card.removeClass('hover');
			} else {
				$card.addClass('hover');
			}
		}

	</script>

</body>

</html>