@extends('layout')
@section('content')

<header  role="banner">
    <img src="{{URL::asset('www/assets/blog_images/blog.jpg')}}" alt="panduasia blog" class="bannerpost">
</header>
    <!-- END #gtco-header -->

<div class="container">
    <div class="row">
        <div class="gtco-services gtco-section">
            <div class="gtco-heading text-center">
                <h2>New Articles</h2>
                <br>
                <form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" name="keyword" placeholder="Cari artikel menarik disini ..." />
                            <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
                                <span></span>
                            </button>
                        </div>
                        <span class="close" onclick="searchToggle(this, event);"></span>
                        <div class="result-container">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <br>


    <div id="komunitas" class="container-fluid text-center" style="
    background: -webkit-gradient(linear, left top, right bottom, from(#20b8e6), to(rgb(52, 212, 124)));
    opacity: .8;
    ">
        <h2 style="
            font-size: 1.875em;
            font-weight: bold;
            color:  #f4f6f7  ;
            margin-top:40px;
            
        ">Gabung
            bersama di komunitas kami</h2>
        <div class="row">
            <div class="col-md-12" style="margin-bottom:40px;">

                <a href="{{URL('/member/register')}}" class="btn btn-white btn-outline">Daftar</a>
            </div>

        </div>
    </div>


    <br><br>


    <div class="row">
     @foreach($pm as $post)
      @if($post->isapprove == 1)
        <aside class="col-sm-4" style="padding-top:30px;">
            <div class="card-banner-komunitas align-items-end">
                
                @if(empty($post->image) || $post->image == "null")
	            <img src="{{ URL::asset('www/assets/images/default.png')}}" alt="{{$post->title}}">
	            @else
	           
	            <img src="{{ URL::asset('www/assets/communities/blog_images/'.$post->image)}}" alt="{{$post->title}}">
	            @endif
	            
                <td></td>
                <article class="overlay-komunitas overlay-cover d-flex align-items-center justify-content-center">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <a href="{{ url('komunitas/blog/'.$post->slug) }}" class="btn btn-info btn-sm" > Selengkapnya </a>
                </article>
            </div>
        </aside>
        @endif
    @endforeach
    
    </div>

    <!-- Pager -->
    <div class="row">
        <!--<div class="next">-->
     
        <!--</div>-->
    </div>

</div>

<br>
<br>


</div>

@endsection
