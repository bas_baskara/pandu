@extends('layout')
@section('content')

<div class="col-sm-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(1)))) }}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour/country') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(2)))) }}</a></li>
          <li class="breadcrumb-item ">{{ str_replace('-', ' ', ucwords(trans(Request::segment(3)))) }}</li>
      </ol>
  </nav>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="gtco-heading text-center">
                <h2>Destinasi Negara</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
           <?php /*  <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;">  */ ?>
        </div>
            <div class=" col-md-8" style="padding:15px;">
                <?php /* <h2 class="card-title">{{ $result->main_city }}</h2>
                
                <small>{{ str_limit($result->main_description, 700) }}</small> */ ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4" style="margin-top:15px;float:left; ">
                    <div class="card-filter" style="position:sticky;">
                        <div class="card-body">
                            <h5 class="card-title">Urutkan berdasarkan</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Harga Terendah</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Review</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Populer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Jemput di Hotel</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Makan Siang / Malam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Foto Dokumentasi</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Tiket gratis umur dibawah 6 tahun</label>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Kategori</h5>
                            @foreach($category as $kcat => $vcat)
                                <div class="checkbox">
                                    <label><input type="checkbox" id="tourcategory" name="tourcategory" value="{{ $vcat->id }}">{{ $vcat->title }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-top:15px;">
                    @foreach($tour_by_country as $kcity => $vcountry)
                    <div class="list-group-item wisata">
                       <div  class="list-group-item-wisata">
                                                    
                            <div class="media col-lg-3">
                                <figure class="pull-left">
                                    @php 
                                        $pics = json_decode($vcountry->images);
                                        $rpic = array_slice($pics,0,1);
                                    @endphp
                                    <div class="tourslide">
                                    @foreach($rpic as $pic)
                                      <div><img class="media-list-img" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 160px;"></div>                                    
                                    @endforeach
                                    </div>
                                </figure>
                            </div>
                            
                            <div class="col-md-6">
                                <a href="{{ url('tour/'.$vcountry->slug) }}"><h4 class="list-group-item-heading text-center">{{ $vcountry->title }}</h4></a>
                                <p class="list-group-item-text"> 
                                 <small><i class="glyphicon glyphicon-map-marker"></i> 
                                    @foreach($city as $kc => $vc)
                                        @if($vcountry->city_id == $vc->id)
                                            {{ $vc->name }}
                                        @endif
                                    @endforeach
                                             , 
                                    @foreach($province as $kp => $vp)
                                        @if($vcountry->province_id == $vp->id)
                                            {{ $vp->name }}
                                        @endif
                                    @endforeach , 
                                    @foreach($country as $kco => $vco)
                                        @if($vcountry->country_id == $vco->id)
                                            {{ $vco->country_name }}
                                        @endif
                                    @endforeach </small>
                                         
                                <hr>
                                <small class="pull-right">
                                    <button type="button" class="btn btn-outline-warning btn-sm">
                                    @foreach($durasi as $kd => $vd)
                                        @if($vcountry->category == $kd)
                                            {{ $vd }}
                                        @endif
                                    @endforeach
                                    </button>
                                </small>

                                <div class="entry">
                                    <a href="{{ url('tour/'.$vcountry->slug) }}" style="color:#000;">{{ str_limit($vcountry->description, 129, '...') }}</a>
                                </div>
  
                                {{-- <ul class="list-group-item-include">Include :
                                    <li> Makan</li>
                                    <li> Tiket Masuk</li>
                                    <li> Homestay</li>
                                </ul> --}}
                                <small>Pemandu : @foreach($g as $kg => $vg)
                                    @if($vcountry->member_id == $vg->id)
                                        {{ $vg->first_name . " " . $vg->last_name }}
                                    @endif
                                @endforeach </small>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h4> Rp. {{ empty($vcountry->promo_price) ? number_format($vcountry->actual_price,0,",",".") : number_format($vcountry->promo_price,0,",",".") }}  <small> / orang </small></h4>
                                <a href="{{ url('tour/'.$vcountry->tourslug) }}" class="btn btn-book" style="
                                "> Book Now! </a>
                                <div class="stars">
                                    @php $rating = $vcountry->rating; @endphp 

                                @foreach(range(1,5) as $i)
                                    <span class="fa-stack rating" style="width:1em">
                                        
                                        @if($rating > 0)
                                            @if($rating > 0.5)
                                                <i class="fa fa-star" style="color: orange;"></i>
                                            @else
                                                <i class="fa fa-star-half" style="color: orange;"></i>
                                            @endif
                                        @else
                                            <i class="fa fa-star-o"></i>
                                        @endif
                                        @php $rating-- @endphp
                                    </span>
                                @endforeach
                                </div>
                                <p class="rating-wisata"> Rate {{ $vcountry->rating / count($vcountry->rating) }} <small> / </small> 5 </p>    
                            </div>
                            
                            <hr>

                       </div>
                    </div>
                    <br>
                    @endforeach


                </div>
            </div>
        </div>

<script type="text/javascript">

               var test = $('#tourcategory:checked').val();

               $('#tourcategory').click(function(){
console.log(test);
               });

               

        </script>

        @endsection