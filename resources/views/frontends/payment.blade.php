@extends('layout')
@section('content')

<div class="container">
    <div class="row">
        
       <div class="col-lg-4" style="margin-top:50px;position:sticky;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-muted">Detail Harga</h4>
                    <hr>
                    <h4 class="text-muted">Harga Normal - 3 Pax  <p class="pull-right">1,950,000</p></h4>
                   
                    <h4 class="text-muted">Harga /orang <p class="pull-right">650,000 </p></h4>
                    <hr>
                    <h4 class="text-muted">Total Pembayaran <p class="pull-right">1,950,000</p>      </h4>
                    
                </div>
            </div>
        </div>
        
         <div class="col-md-8" style="margin-top:50px;">
          <div class="panel panel-default">
             <div class="panel-body">
                <h4 class="text-muted">Konfirmasi Pembayaran</h4>
                <hr>
                <p>Kode pembayaran  : <small>B-2019-03-BJ004-04-001 </small> </p> 
                    <label class="text-muted">Nama Pemesan : Ilham Nur Hakim</label>
                    <br>
                    <label class="text-muted">Email : ilham@mail.com</label>
             </div>
          </div>
         </div>
    
    <div class="col-md-8" style="margin-top:10px;">
      <div class="panel panel-default">
         <div class="panel-body">
            <h4 class="text-muted">Data Peserta</h4>
            <hr>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">#ID</th>
                  <th scope="col">Nama Peserta</th>
                  <th scope="col">Alamat</th>
                  <th scope="col">Usia</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>1</td>
                  <td>Ilham</td>
                  <td>8</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Nur</td>
                  <td>Thornton</td>
                  <td>7</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Hakim</td>
                  <td>Bogor</td>
                  <td>11</td>
                </tr>
              </tbody>
            </table>
            <a class="btn btn-primary pull-right">Download invoice</a>
           </div>
       </div>    
    </div>

    <!--    <div id="accordion">-->
    <!--      <div class="card">-->
    <!--        <div class="card-header" id="headingOne">-->
    <!--          <h5 class="mb-0">-->
    <!--            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->
    <!--              Transfer-->
    <!--            </button>-->
    <!--          </h5>-->
    <!--        </div>-->
        
    <!--        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">-->
    <!--          <div class="card-body">-->
    <!--            <h4 class="text-muted">Ketentuan pembayaran:</h4>-->
    <!--            <p>-->
    <!--Pembayaran dapat dilakukan melalui transfer ke rekening Bank BCA, Bank Mandiri, Bank Syariah Mandiri, Bank BNI, atau Bank BRI.-->
    <!--Total belanja kamu belum termasuk kode pembayaran untuk keperluan proses verifikasi otomatis-->
    <!--Mohon transfer tepat sampai 3 digit terakhir</p>-->
    <!--          </div>-->
    <!--        </div>-->
    <!--      </div>-->
    <!--      <div class="card">-->
    <!--        <div class="card-header" id="headingTwo">-->
    <!--          <h5 class="mb-0">-->
    <!--            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">-->
    <!--        Transfer Bank Virtual-->
    <!--            </button>-->
    <!--          </h5>-->
    <!--        </div>-->
    <!--        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">-->
    <!--          <div class="card-body">-->
            
    <!--        <h2>Metode Pembayaran</h2>-->
    <!--        <label for="sel1">Pilih pembayaran Virtual Bank:</label>-->
    <!--          <select class="form-control" id="sel1">-->
    <!--            <option>Virtual Bank BCA</option>-->
    <!--            <option>Virtual Bank BRI</option>-->
    <!--            <option>Virtual Bank Mandiri</option>-->
    <!--            <option>Virtual Bank BNI</option>-->
    <!--        </select>-->
            
    <!--          </div>-->
    <!--        </div>-->
    <!--      </div>-->
    <!--      <div class="card">-->
    <!--        <div class="card-header" id="headingThree">-->
    <!--          <h5 class="mb-0">-->
    <!--            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">-->
    <!--              Collapsible Group Item #3-->
    <!--            </button>-->
    <!--          </h5>-->
    <!--        </div>-->
    <!--        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">-->
    <!--          <div class="card-body">-->
    <!--            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.-->
    <!--          </div>-->
    <!--        </div>-->
    <!--      </div>-->
    <!--    </div>-->
    

    </div>
</div>

@endsection
