@extends('layout')
@section('content')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-12 gtco-heading text-center">
        <h2>Tour berdasarkan Provinsi</h2>
      </div>
    </div>
    <div class="row"> 
      @foreach($tourinprovinces as $ktip => $vtip)
        @foreach($province as $kp => $vp)
          @if($vtip->province_id == $vp->id)
            <div class="col-md-3">
              <div class="card">

                  @php 
                    $pics = json_decode($vtip->images);
                    $rpic = array_slice($pics,0,1); 
                  @endphp

                  {{-- <div class="tourslide"> --}}
                    @foreach($rpic as $pic)
                      <div><img class="img-responsive" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 100%;"></div>                                    
                    @endforeach
                  {{-- </div> --}}
                <a href="{{ url('tour/province/'. $vp->slug )}}">
                    <div class="card-body text-center">
                      <div class="row">
                        <div class="col-12">
                          <span class="text-muted">{{ $vtip->province_id == $vp->id ? $vp->name : "" }}</span>
                        <br>
                          <span class="text-muted">Start from</span>
                          <div class="text-harga">Rp. {{ number_format($vtip->actual_price,0,",",".") }}</div>
                        </div>
                      </div>
                    </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      @endforeach
    </div>
  </div>
</div>

@endsection
