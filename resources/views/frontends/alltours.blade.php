@extends('layout')
@section('content')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Semua Tour</h2>
      </div>
    </div>
    <div class="row"> 
      @foreach($alltour as $kat => $vat)
        @foreach($city as $kc => $vc)
          @if($vat->city_id == $vc->id)
            <div class="col-md-3">
              <div class="card  ">
                <div class="social-card-header align-middle text-center ">

                  @php 
                    $pics = json_decode($vat->images); 
                    $rpic = array_slice($pics,0,1);
                  @endphp

                  <div class="tourslide">
                    @foreach($rpic as $pic)
                      <div><img class="image" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt=""></div>                                    
                    @endforeach
                  </div>

                </div>
                <hr>
                <a href="{{ url('tour/city/'. $vc->slug )}}">
                    <div class="card-body text-center">
                      <div class="row">
                        <div class="col border-right">
                          <span class="text-muted">{{ $vat->city_id == $vc->id ? $vc->name : "" }}</span>
                        </div>
                        <div class="col">
                          <span class="text-muted">mulai dari</span>
                          <div class="text-harga">Rp. {{ number_format($vat->actual_price,0,",",".") }}</div>
                        </div>
                      </div>
                    </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      @endforeach
    </div>
  </div>
</div>

@endsection
