@extends('layout')
@section('content')

<div class="container">
    <div class="row">

        <div class="col-lg-4" style="margin-top:50px;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-muted">Ringkasan Pemesanan</h4>
                    <div class="col-md-4">
                        <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="img-booking"
                            alt="bandung">
                    </div>

                    <div class="col-md-8 pull-right">
                        <small class="small-booking">
                            OPEN TRIP
                            GREEN CANYON
                            Open Trip Green Canyon 2019 Part 6 </small>
                        <hr>
                    </div>

                    <div class="col-md-12">
                        <div class="include-desc">
                           <div class="row">
                            <div class="col-md-2">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAuYMJ0yTQq3XoYeHjrYstcCJnxKJEtlWnWP8aQviBS_lXnF64" style="width:35px;height:35px; margin-right:15px;" >
                            </div>
                            
                            <div class="col" style="margin-top:15px;">
                                  <h4>Pemandu : Ilham</h4>
                            </div>  
                         </div>
                            <h4>Jumlah perserta : 4</h4>
                            <h4>Tgl keberangkatan : 29-04-2019</h4>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="cardPostView">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">Isi Form</a></li>
                        <li class="breadcrumb-item"><a href="#">Pembayaran</a></li>
                        <li class="breadcrumb-item ">Selesai</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="col-md-8">
            <div class="cardPostView">
                <div class="bs-callout bs-callout-primary">
                    <h4>Formulir Pendaftaran Tour</h4>
                    <form>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp" name="email">
                            <small id="emailHelp" class="form-text text-muted">* penting dalam proses pemesanan tour</small>
                        </div>
                        <div class="form-group">
                            <label>No KTP/SIM</label>
                            <input type="text" class="form-control" name="ktp_or_license">
                        </div>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_lengkap">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input type="text" class="form-control" name="no_telp">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="alamat"> </textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-4" style="margin-top:50px;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="text-muted">Detail Harga</h4>
                    <hr>
                    <h4 class="text-muted">Harga Normal - 3 Pax  <p class="pull-right">1,950,000</p></h4>
                   
                    <h4 class="text-muted">Harga /orang <p class="pull-right">650,000 </p></h4>
                    <hr>
                    <h4 class="text-muted">Total Pembayaran <p class="pull-right">1,950,000</p>      </h4>
                </div>
            </div>
        </div>

        <div class="col-md-8 pull-right">
            <div class="cardPostView">
                <div class="bs-callout bs-callout-danger">
                    <h4>Data Peserta</h4>
                    <form>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_peserta">
                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input type="text" class="form-control" name="no_telp_peserta">
                            <small id="emailHelp" class="form-text text-muted">*penting jika ada terjadi masalah.</small>
                        </div>
                        <div class="form-group">
                            <label>Usia</label>
                            <input type="text" class="form-control" name="usia_peserta">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="alamat_peserta"> </textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <a href="#" class="btn btn-primary pull-right">Konfirmasi Pesanan</a>
    <br>
    <br>
    <br>
    
</div>

@endsection