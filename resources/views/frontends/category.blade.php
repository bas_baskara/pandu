@extends('layout')
@section('content')

  <div class="container">
    <div class="row">
        <div class="gtco-services gtco-section">
            <div class="gtco-heading text-center">
            <h2>Articles by Category {{ $get_cat->title }}</h2>
            <br>
            </div>
        </div>

      @foreach($posts as $post)
      @if($post->status == "0" || $post->category == "0")
        <?php 
          echo "<br/>";
          echo "<h4>Sorry, Post not available...</h4>";
          echo "<br/>";
        ?>
      @else
      
      <div class="col-md-3">
        <div class="column">
          <!-- Post-->
          
          <div class="post-module">
            <!-- Thumbnail-->
            <div class="thumbnail">
              <div class="date">
                  <div class="day">{{ $post->created_at->format('d') }}</div>
                  <div class="month">{{ $post->created_at->format('M') }}</div>
              </div>
              <img src="{{ URL::asset('www/assets/blog_images/'.$post->image) }}" alt="">
            </div>
            <!-- Post Content-->
            <div class="post-content">
                {{-- @foreach ($categories as $category)
              <div class="category">{{$category->title}}</div>
              @endforeach --}}
              <h1 class="title">
                <a href="{{ url('blog',$post->slug) }}">{{$post->title,50}} </a> 
              </h1>
              
              

              <div class="post-meta">
                <span class="timestamp">
                   <i class="fa fa-clock-"></i>  {{ $post->created_at->diffForHumans() }}
                </span>
                <br>
                <span class="views">
                  <i class="fa fa-user"></i>
                  <a href="#"> {{$post->visitor}} Views</a>
                </span>
                <a href="{{ url('blog/',$post->slug) }}" style="color:red;">Selengkapnya</a>
              </div>

            </div> <!-- Post Content -->
          </div>
      
        </div>
        
      </div>
    @endif
      @endforeach
    
      <!-- Pager -->
		<ul class="pager">
			<li class="next">
				{{-- {{ $post->links() }}	             --}}
			</li>
    </ul>
     
    </div>
  </div>

@endsection
