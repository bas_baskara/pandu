@extends('layout')
@section('content')
<div class="containerPostView">

	<div class="cardPostView">
		<div class="containerPostView">

			<br>
			<div class="post-content-view">
				<h1 class="title">Wisata Area {{ Request::segment(2) }}</h1>

				<div class="bodycontent">
					<table class="table table-striped">
					  <thead>
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">Nama Tempat</th>
					      <th scope="col">Nama Lokasi</th>
					      <th scope="col">Keterangan</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php $no = 1; ?>
					@foreach ($result as $v)
					@if($v == "" ||  $v->kota != Request::segment(2))
						<tr>
					      <th scope="row">Tidak Tersedia</th>
					    </tr>
					@else
					    <tr>
					      <th scope="row">{{ $no++ }}</th>
					      <td>{{ $v->nama_tempat }}</td>
					      <td>{{ $v->lokasi_tempat }}</td>
					      <td>{{ $v->biaya }}</td>
					    </tr>
					  @endif
					 @endforeach
					  </tbody>
					</table>
				</div>


			</div>
			<hr>
			<div class="foot">
				<div class="col-sm-8">
					
				</div>
				
			</div>
		</div>
			
	</div>	
</div>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=214610849349025&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
@endsection