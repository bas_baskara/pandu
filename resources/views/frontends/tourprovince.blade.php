@extends('layout')
@section('content')

<div class="col-sm-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(1)))) }}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('/tour/province') }}">{{ str_replace('-', ' ', ucwords(trans(Request::segment(2)))) }}</a></li>
          <li class="breadcrumb-item ">{{ str_replace('-', ' ', ucwords(trans(Request::segment(3)))) }}</li>
      </ol>
  </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="gtco-heading text-center">
                <h2>Destinasi Provinsi</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
           <?php /*  <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;">  */ ?>
        </div>
        <div class=" col-md-8" style="padding:15px;">
        <?php /* <h2 class="card-title">{{ $result->main_city }}</h2>
        
        <small>{{ str_limit($result->main_description, 700) }}</small> */ ?>
        </div>
    </div>
            
    <div class="row">
        <div class="col-4" style="margin-top:15px;float:left; ">
            <div class="card-filter" style="position:sticky;">
                <div class="card-body">
                    <h5 class="card-title">Urutkan berdasarkan</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="radio">
                                <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="optradio">Harga Terendah</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="radio">
                                <label><input type="radio" name="optradio" checked>Review</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="optradio">Populer</label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <h5 class="card-title">Destinasi</h5>
                    @foreach($filtercity as $kfc => $vfc)
                        <div class="checkbox">
                            <label><input type="checkbox" name="tourcity" value="{{ $vfc->id }}">{{ $vfc->name }}</label>
                        </div>
                    @endforeach
                </div>
                <hr>
                <div class="card-body">
                    <h5 class="card-title">Kategori</h5>
                    @foreach($category as $kcat => $vcat)
                        <div class="checkbox">
                            <label><input type="checkbox" id="tourcategory" name="tourcategory" value="{{ $vcat->id }}" onclick="catTour()">{{ $vcat->title }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-8" style="margin-top:15px;">

            @foreach($tour_by_province as $kprovince => $vprovince)
                <div class="card mb-3" style="max-width: 100%;">
                  <div class="row no-gutters">
                    <div class="col-md-4">
                      @php 
                        $pics = json_decode($vprovince->images); 
                    @endphp
                    <div class="tourslide">
                    @foreach($pics as $pic)
                      <img class="card-img" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="">                              
                    @endforeach
                    </div>
                    </div>
                    <div class="col-md-8">
                      <div class="card-body">
                        <a href="{{ url('tour/'.$vprovince->tourslug) }}"><h5 class="card-title">{{ $vprovince->title }}</h5></a>
                        <a href="{{ url('tour/'.$vprovince->tourslug) }}" class="btn btn-book" style="
                        ">Book Now!</a>
                        <h3>Rp. {{ empty($vprovince->promo_price) ? number_format($vprovince->actual_price,0,",",".") : number_format($vprovince->promo_price,0,",",".") }}  <small> / orang </small></h3>
                        <p class="card-text">{{ str_limit($vprovince->description, 129, '...') }}</p>
                        <p class="card-text">
                            <small><i class="glyphicon glyphicon-map-marker"></i> 
                            @foreach($city as $kc => $vc)
                                @if($vprovince->city_id == $vc->id)
                                    {{ $vc->name }}
                                @endif
                            @endforeach
                                     , 
                            @foreach($province as $kp => $vp)
                                @if($vprovince->province_id == $vp->id)
                                    {{ $vp->name }}
                                @endif
                            @endforeach , 
                            @foreach($country as $kco => $vco)
                                @if($vprovince->country_id == $vco->id)
                                    {{ $vco->country_name }}
                                @endif
                            @endforeach </small>
                            <br>
                            <div class="stars">
                        @php $rating = $vprovince->rating; @endphp 

                        @foreach(range(1,5) as $i)
                            <span class="fa-stack rating" style="width:1em">
                                
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    @else
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    @endif
                                @else
                                    <i class="fa fa-star-o"></i>
                                @endif
                                @php $rating-- @endphp
                            </span>
                        @endforeach
                        </div>

                        <p class="rating-wisata"> Rate {{ $vprovince->rating / count($vprovince->rating) }} <small> / </small> 5 </p> 
                            <hr>
                            <small class="pull-right">
                                <button type="button" class="btn btn-outline-warning btn-sm">
                                @foreach($durasi as $kd => $vd)
                                    @if($vprovince->category == $kd)
                                        {{ $vd }}
                                    @endif
                                @endforeach
                                </button>
                            </small>
                            <small>Pemandu : 
                                @foreach($g as $kg => $vg)
                                    @if($vprovince->member_id == $vg->id)
                                        {{ $vg->first_name }} {{ $vg->last_name }} 
                                    @endif
                                @endforeach
                            </small>
                            <br>
                            <small class="text-muted">Last updated 3 mins ago</small>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

<script type="text/javascript">
    function catTour(){
        var cat = document.getElementByTagName('tourcategory');

       if(cat.checked == true){
            alert(cat.value);
       }
    }
</script>

@endsection