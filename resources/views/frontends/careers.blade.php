@extends('blog/app')
@section('head')

<meta name="title" content="Panduasia Career | Informasi tentang program internship" />
<meta name="description" content="Temukan program magang untuk studi anda dan dapatkan pengalaman kerja professional bagi persiapan karir kamu. Mulai internship kamu bersama Panduasia." />
<meta name="keywords" content="Panduasia | Career " />
@endsection

@section('main-content')

<header id="gtco-header" class="gtco-cover gtco-cover-xs gtco-inner" role="banner">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
  </div>
</header>
<!-- END #gtco-header -->

<div class="gtco-services gtco-section">
  <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
    <h2>Internships</h2>
    <br>
    <form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
      <div class="search-wrapper">
        <div class="input-holder">
            <input type="text" class="search-input" name="keyword" placeholder="Cari internship disini ..." />
            <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
              <span></span>
            </button>
        </div>
        <span class="close" onclick="searchToggle(this, event);"></span>
        <div class="result-container">
        </div>
      </div>
    </form>
  </div>

  <div class="container">
    <div class="row">
      @foreach($careers as $career)
      <div class="col-md-4">
        <div class="column">
          <!-- Post-->
          <div class="post-module">
            <!-- Thumbnail-->
            <div class="thumbnail">
              <div class="date">
                  <div class="day">{{$career->created_at->format('d')}}</div>
                <div class="month">{{$career->created_at->format('F')}}</div>
              </div>
              <img src="{{ URL::asset('public/assets/image_post/'.$career->image)}}" alt="{{$career->title}}">
            </div>
            <!-- Post Content-->
            <div class="post-content">


              <h1 class="title">
                <a href="{{route('careers',$career->slug)}}">{{ $career->title}}
                </a>
              </h1>

                <p class="description">
                  {!! htmlspecialchars_decode(str_limit($career->description,150)) !!}
                  
                </p>
                
                <div class="post-meta">
                  <span class="timestamp">
                    <i class="fa fa-clock-"></i></span>
                  <a href="{{route('careers',$career->slug)}}" style="color:red;" >Selengkapnya</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
      <!-- Pager -->
		<ul class="pager">
			<li class="next">
				{{-- {{$posts->links()}}	             --}}
			</li>
    </ul>

    </div>

  </div>

</div>

@endsection
