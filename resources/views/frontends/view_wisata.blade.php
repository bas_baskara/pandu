@extends('layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                <h2>Destinasi Kota</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
            <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;"> 
        </div>
            <div class=" col-md-8" style="padding:15px;">
                <h2 class="card-title">{{ $result->main_city }}</h2>
                
                <small>{{ str_limit($result->main_description, 700) }}</small>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4" style="margin-top:15px;float:left; ">
                    <div class="card-filter" style="position:sticky;">
                        <div class="card-body">
                            <h5 class="card-title">Urutkan berdasarkan</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Harga Terendah</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Review</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Populer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Jemput di Hotel</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Makan Siang / Malam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Foto Dokumentasi</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Tiket gratis umur dibawah 6 tahun</label>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Kategori</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pantai</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Rekreasi Keluarga</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Romantis</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pegunungan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Alam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Budaya</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Perkotaan</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-top:15px;">
                    @foreach($tour_by_city as $kcity => $vcity)
                    <div class="list-group-item wisata">
                       <div  class="list-group-item-wisata">
                                                    
                            <div class="media col-lg-3">
                                <figure class="pull-left">
                                    @php 
                                        $pics = json_decode($vcity->images); 
                                    @endphp
                                    <div class="tourslide">
                                    @foreach($pics as $pic)
                                      <div><img class="media-list-img" src="{{ asset('www/assets/tour_images/'.$pic) }}" alt="" style="width: 160px;"></div>                                    
                                    @endforeach
                                    </div>
                                </figure>
                            </div>
                            
                            <div class="col-md-6">
                                <a href="{{ url('tour/'.$vcity->slug) }}"><h4 class="list-group-item-heading text-center">{{ $vcity->title }}</h4></a>
                                <p class="list-group-item-text"> 
                                 <small><i class="glyphicon glyphicon-map-marker"></i> {{ $vcity->city_id }} , {{ $vcity->province_id }} , {{ $vcity->country_id }} </small>
                                         
                                <hr>
                                <small class="pull-right">
                                    <button type="button" class="btn btn-outline-warning btn-sm">{{ $vcity->category }}</button>
                                </small>

                                <div class="entry">
                                    <a href="{{ url('tour/'.$vcity->slug) }}" style="color:#000;">{{ str_limit($vcity->description, 129, '...') }}</a>
                                </div>
  
                                {{-- <ul class="list-group-item-include">Include :
                                    <li> Makan</li>
                                    <li> Tiket Masuk</li>
                                    <li> Homestay</li>
                                </ul> --}}
                                <small>Pemandu : {{ $vcity->member_id }} </small>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h4> Rp. {{ $vcity->actual_price }}  <small> / orang </small></h4>
                                <a href="{{ url('tour/booking') }}" class="btn btn-book" style="
                                "> Book Now! </a>
                                <div class="stars">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </div>
                                <p class="rating-wisata"> Average 4.5 <small> / </small> 5 </p>    
                            </div>
                            
                            <hr>

                       </div>
                    </div>
                    <br>
                    @endforeach


                </div>
            </div>
        </div>
        @endsection