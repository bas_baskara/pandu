@extends('layout')
@section('content')
<div class="gtco-services gtco-section">
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
    </div>
    <br/>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="col-2">

                        @if(Auth::user()->profile_img == NULL)
                        <img src="{{URL::asset('www/assets/communities/avatars/ai.jpg')}}"
                         class="img-responsive" style="width: 200px;">
                         @else 
                         <img class="img-responsive" src="{{
                         URL::asset('www/assets/communities/avatars/'.$c->profile_img
                         )}}" style="width: 200px;">
                         @endif
                </div>
               
                <div class="col-5">
                    <h2>{{ $c->first_name }} {{ $c->last_name }}</h2>
                    
                    <h6>
                        <span class="glyphicon glyphicon-map-marker"></span> {{ $kota }} - {{ $provinsi }}
                    </h6>
                    <a href="{{ url('member/message') }}" class="btn btn-primary">Chat</a>
                </div>

                <div class="col-5"> 
                @if($c->ismember == 1)
                    
                @elseif($c->isguide == 1)
                <ul class="list-unstyled no-mb">
                    <div class="col-5">
                        Rating

                        <span class="label label-warning">{{ $c->rating }}</span>
                         @php $rating = $c->rating; @endphp 

                        @foreach(range(1,5) as $i)
                            <span class="fa-stack rating" style="width:1em">
                                
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    @else
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    @endif
                                @else
                                    <i class="fa fa-star-o"></i>
                                @endif
                                @php $rating--; @endphp
                            </span>
                        @endforeach

                        <p>(<a href="#">
                            @foreach($mr as $kmr => $vmr)
                            {{-- @php var_dump($vmr); @endphp --}}
                                @if(empty($vmr))
                                   {{ "<h1>Belum ada review</h1>" }}
                                @else
                                   {{ count($vmr->review) }} review(s)
                                @endif
                            @endforeach
                            {{-- @php die(); @endphp --}}
                            </a>)
                        </p>

                        <p>
                            @foreach($mr as $kmr => $vmr)
                                @if($vmr->status == 0)
                                    
                                @php echo "Belum ada pekerjaan selesai" @endphp
                                @else
                                    {{ "Menyelesaikan " . count($mr->complete_date) }} dari {{ count($mr->complete_date) + count($mr->canceled_date) }} tour ({{ $mr->complete_date / 100 }} %)
                                @endif
                            @endforeach
                        </p>
                        <div class="col-12">

                        </div>
                    </div>
                </ul>
                @elseif($c->isleader == 1)
                <ul class="list-unstyled no-mb">
                    <div class="col-5">
                        Rating

                        <span class="label label-warning">{{ $c->rating }}</span>
                         @php $rating = $c->rating; @endphp 

                        @foreach(range(1,5) as $i)
                            <span class="fa-stack rating" style="width:1em">
                                
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    @else
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    @endif
                                @else
                                    <i class="fa fa-star-o"></i>
                                @endif
                                @php $rating--; @endphp
                            </span>
                        @endforeach

                        <p>(<a href="#">
                            @if(empty($mr))
                                  {{ 'Belum ada review' }}
                                @else
                                    @foreach($mr as $kmr => $vmr)
                                        {{ count($vmr->review) }} review(s)
                                    @endforeach
                            @endif
                            </a>)</p>

                        <p>
                            @foreach($mr as $kmr => $vmr)
                                @if(empty($vmr->status))
                                    Belum ada pekerjaan selesai
                                @else
                                    Menyelesaikan {{ count($mr->complete_date) }} dari {{ count($mr->complete_date) + count($mr->canceled_date) }} tour ({{ $mr->complete_date / 100 }} %)
                                @endif
                            @endforeach
                        </p>
                        <!-- <div class="rating iblocks">
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                        </div> -->
                    
                        <div class="col-12">

                        </div>
                    </div>
                </ul>
                @endif
            </div>

            </div>
               
            </div>

            <div class="row">
                <div class="col-3">
                    @include('member/partials/sidebar')
                </div>
             
                <div class="col-9">
                    <div class="panel panel-default">
                        @yield('membercontent')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
    