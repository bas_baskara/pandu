@extends('member.home')
@section('membercontent')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Komunitas di Kota Anda</h2>
        <p>Bergabunglah ke komunitas yang anda minati</p>
        <hr>
      </div>
    </div>

    <div class="row">

      @foreach ($communities as $kc => $vc)
        <div class="col-md-4 col-sm-4">
          <a href="{{ url('community/id/'.$vc->community_identity )}}" class="gtco-item two-row bg-img animate-box" >
            @if($vc->profile_image == 'null')
            <img src="{{ asset('www/assets/images/logo-community-default.png')}}" class="gtco-item two-row bg-img animate-box" alt="{{ $vc->community_name }}">
            @else
            <img src="{{ asset('www/assets/communities/'.$vc->profile_image)}}" class="gtco-item two-row bg-img animate-box" alt="{{ $vc->community_name }}">
            @endif
            <!-- <h1 class="text-kota">Jakarta</h1> -->
            <div class="overlay">
              <i class="ti-arrow-top-right"></i>
              <div class="copy">
                <h3>{{ strip_tags($vc->community_name) }}</h3>
                <p>{{-- strip_tags($vc->description) --}}</p>
              </div>
            </div>
          </a>
        </div>
     @endforeach

    </div>
  </div>
</div>

@endsection
    