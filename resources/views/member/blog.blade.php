@extends('member.home')
@section('membercontent')

@if(session('message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ session('message') }}</strong>
    </div>
    {{ session()->forget('message') }}
@endif

<div class="panel-heading clearfix">
  <h4 class="panel-title">Daftar Blog</h4>
  <a href="{{ url('member/blog/create') }}" class="btn btn-primary btn-xs" style="float: right;">Buat Blog Baru</a>
  <small>*Ayo berbagi pengalaman kamu di dunia travelling dan jadilah terkenal dengan cerita-mu.</small>
</div>
 <br>
              
<div class="panel-body">
  <div class="table-responsive">
    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
      <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Status</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Status</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </tfoot>

      <tbody>
        <?php $n = 1; ?>
            @foreach($pm as $kpm => $vpm)
            @if($vpm->posted_by == Auth::user()->username)
                <tr>
                  <td>{{ $n++ }}</td>
                  <td>{{ $vpm->title }}</td>
                  <td>
                    {{ ($vpm->status == 1) ? "Published" : "" }}
                    {{ ($vpm->status == 2) ? "Waiting Approval" : "Draft" }}</td>
                  <td>{{ $vpm->created_at->diffForHumans() }}</td>
                  <td>
                    <a href="{{ url('member/blog/edit/'.$vpm->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                  |
                    <a href="{{ url('member/blog/delete/'.$vpm->id) }}" onclick="return confirm('Yakin mau hapus artikel ini?')
                    "><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr>
            @endif
            @endforeach

      </tbody>

    </table>
  </div>
</div>
@endsection