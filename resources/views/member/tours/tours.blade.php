@extends('member.home')
@section('membercontent')

<div class="panel-heading clearfix">
  <h4 class="panel-title">Daftar Tours</h4>
  <a href="{{ url('member/tour/create') }}" class="btn btn-primary btn-xs" style="float: right;">Buat Tour Baru</a>
</div>

@if ($message = Session::get('success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
  </div>
@endif

<div class="panel-body">
  <div class="table-responsive">
    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
      <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Tipe</th>
          <th>Category</th>
          <th>Durasi</th>
          <th>Status</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Tipe</th>
          <th>Category</th>
          <th>Durasi</th>
          <th>Status</th>
          <th>Date</th>
          <th>Action</th>
        </tr>
      </tfoot>

      <tbody>
        @php $n = 1 @endphp
        @foreach($tours as $kt => $vt)
        <tr>
          <td>{{ $n++ }}</td>
          <td>{{ $vt->title }}</td>
          <td>
            @foreach($tipe as $ktipe => $vtipe)
              @if($ktipe == $vt->tipe)
              {{ $vtipe}}
              @endif
            @endforeach
          </td>
          <td>
            @foreach($tc as $ktc => $vtc)
              @if($ktc == $vt->category)
                {{ $vtc->title }}
              @endif
            @endforeach
          </td>
          <td>
            @foreach($durasi as $kd => $vd)
              @if($kd == $vt->trippackage)
                {{ $vd }}
              @endif
            @endforeach
          </td>
          <td>
            {{ $vt->status == 1 ? "Active" : "Inactive" }}</td>
          <td>{{ date('d M Y') }}</td>
          <td>
            <a href="{{ url('member/tour/detail/'.$vt->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a>
            |
            <a href="{{ url('member/tour/edit/'.$vt->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
          |
            <a href="{{ url('member/tour/delete/'.$vt->id) }}" onclick="return confirm('Yakin mau hapus paket tour ini?')
            "><span class="glyphicon glyphicon-trash"></span></a>
          </td>
        </tr>
        @endforeach
      </tbody>

    </table>
  </div>
</div>

@endsection