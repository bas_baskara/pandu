@extends('member.home')
@section('membercontent')

<div class="panel-heading clearfix">
    <h2 class="panel-title">Buat Paket Tour Baru</h2>
    <small>*Wisata yang bisa Anda jangkau di Wilayah Anda</small>
    <a href="{{ url('member/tour') }}" class="btn btn-danger btn-xs" style="float: right;">Kembali</a>
</div>

<div class="panel-body">
  <div class="col-sm-12">
    <form action="{{ url('member/tour/store') }}" method="post" class="form-horizontal" enctype="multipart/form-data" >
    {{ csrf_field() }}

      <div class="form-group">
       <label>Tipe Perjalanan</label>
          <select name="tipe" class="js-example-tokenizer js-states form-control" tabindex="-1" style="display: none; width: 100%">
            <optgroup label="Pilih Tipe Trip">
              @foreach($tipe as $kt => $vt)
              <option value="{{ $kt }}">{{ $vt }}</option>
              @endforeach
            </optgroup>
          </select>
      </div>

      <div class="form-group">
          <h4 class="no-m m-b-sm m-t-lg">Kategori</h4>
          <select class="js-example-tokenizer js-states form-control" id="tourcategory" name="categories" tabindex="-1" style="display: none; width: 100%">
            <optgroup label="Pilih Kategori">
              @foreach ($tc as $ktc)
              <option value="{{ $ktc->id }}">{{ $ktc->title }}</option>
              @endforeach
            </optgroup>
          </select>
      </div>

      <div class="form-group">
        <label>Judul Tour yang Menarik</label>
        <input type="text" class="form-control" name="title" required="required"/>
      </div>
      
      <div class="form-group">
        <label>Deskripsi Tour</label>
        <textarea name="description" class="summernote" cols="30" rows="10"></textarea>
      </div>

      <div class="form-group">
        <label>Keterangan Rincian Harga</label>
        <textarea name="pricedetail" class="summernote" cols="30" rows="10"></textarea>
      </div>

      <div class="form-group">
        <label>Rencana Perjalanan</label>
        <textarea name="tripplans" class="summernote" cols="30" rows="10"></textarea>
      </div>

      <div class="form-group">
        <label>Harga</label>
        <input type="number" class="form-control" name="actual_price" required="required" placeholder="contoh: 249000" />
      </div>

      <div class="form-group">
        <label>Harga Promo</label>
        <input type="number" class="form-control" name="promo_price" placeholder="Biarkan kosong jika tidak ada promo">
      </div>

      <div class="form-group">
          <label>Upload Image (Image Utama)</label>
          <input type="file" id="upload_files" name="image_tour[]" class="form-control output" onchange="readURL(this);" accept="image/*" multiple>
          <span class="help-block text-danger"></span>
          <div class="image_preview"></div>
          <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
      </div>

      <div class="form-group">
         <label>Durasi Perjalanan</label>
            <select class="js-example-tokenizer js-states form-control" tabindex="-1" style="display: none; width: 100%" name="durasi">
              <optgroup label="Pilih Durasi Trip">
                @foreach($durasi as $kd => $vd)
                <option value="{{ $kd }}">{{ $vd }}</option>
                @endforeach
              </optgroup>
            </select>
        </div>

      <div class="form-group">
        <label>Minimal Kuota</label>
       <input type="text" class="form-control" name="min_kuota" required="required"/>
      </div>

      <div class="form-group">
        <label>Max Kuota</label>
       <input type="text" class="form-control" name="max_kuota" required="required"/>
      </div>
      
      <input type="hidden" class="form-control" name="country" value="{{ $c->country }}" />
      <input type="hidden" class="form-control" name="province" value="{{ $c->province }}" />
      <input type="hidden" class="form-control" name="city" value="{{ $c->city }}" />
      <input type="hidden" class="form-control" name="rating" value="{{ "0" }}" />
      <input type="hidden" class="form-control" name="member_id" value="{{ Auth::user()->id }}" />
      <input type="hidden" class="form-control" name="community_id" value="{{ $community->id }}" />

      <br>

      <div class="form-group">
          <input type="submit" value="Simpan" class="btn btn-primary btn-lg" style="width: 100%;">
      </div>

    </form>
  </div>
</div>

  <script type="text/javascript">

  function filterProvince()
  {
    var countryID = $('#country').val();

    if(countryID)
    {
      $.ajax({
        url: '<?php echo url("rahasiadapur/tourdestination/getprovinces/") ?>/' + countryID,
        type: "GET",
        dataType: "json",
        success: function(data) {
          $('#province').empty();
          $.each(data, function(key, value){
            $('#province').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      })
    }
    else
    {
      $('#province').empty();
    }
  }

  function filterCity()
  {
    var provinceID = $('#province').val();
    if(provinceID)
    {
      $.ajax({
        url: 'getcities/'+provinceID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#city').empty();
          $.each(data, function(key, value){
            $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#city').empty();
    }
    // console.log(provinceID);
  }
</script>
 <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: -25.344, lng: 131.036};
  // The map, centered at Uluru
  var mapplace = new google.maps.Map(
      document.getElementById('mapmapplace'), {zoom: 4, center: uluru});

  var maplocation = new google.maps.Map(
      document.getElementById('maplocation'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: mapplace});

  var marker2 = new google.maps.Marker({position: uluru, map: maplocation});
}
    </script>

<script type="text/javascript">
  // $(document).ready(function() {

  //   $(".btn-success").click(function(){ 
  //       var html = $(".clone").html();
  //       $(".increment").after(html);
  //   });

  //   $("body").on("click",".btn-danger",function(){ 
  //       $(this).parents(".control-group").remove();
  //   });

  // });
  function preview_images() 
  {
   var total_file = document.getElementById("upload_files").files.length;
   for(var i=0;i<total_file;i++)
   {
    $('.image_preview').append("<div class='col-sm-3'><img src='"+URL.createObjectURL(event.target.files[i])+"' style='width:150px;'></div>");
   }
 }
</script>


@endsection