@extends('member.home')
@section('membercontent')

@if(session('message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ session('message') }}</strong>
    </div>
    {{ session()->forget('message') }}
@endif

<div class="panel-heading clearfix">
    <h2 class="panel-title">Change Password</h2>
</div>

<div class="panel-body">
    <form action="{{ url('member/profile/'.$m->id.'/updatepassword') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputUsername">New Password</label>
            <input type="password" class="form-control" id="new_pass" name="new_pass" required>
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Confirm Password</label>
            <input type="password" class="form-control" id="confirm_pass" name="confirm_pass" required>
        </div>

        <hr/>

        <div class="form-group">
            <label for="exampleInputUsername">Old Password</label>
            <input type="password" class="form-control" name="old_pass" required>
        </div>

      <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Submit</button>
    </form>
</div>

<script type="text/javascript">
    var password = document.getElementById("new_pass")
  , confirm_password = document.getElementById("confirm_pass");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Tidak Sama");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>
@endsection