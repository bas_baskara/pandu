@extends('member.home')
@section('membercontent')

@if(session('message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ session('message') }}</strong>
    </div>
    {{ session()->forget('message') }}
@endif

<div class="panel-heading clearfix">
    <h2 class="panel-title">Profilku</h2>
</div>

<div class="panel-body">
    <form action="{{ url('member/profile/'.$member->id.'/update') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputUsername">Username</label>
            <input type="text" class="form-control" name="username" value="{{ $member->username }}" data-toggle="tooltip" data-placement="bottom" title="Username tidak bisa diganti" disabled>
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Nama Awal</label>
            <input type="text" class="form-control" name="first_name" value="{{ $member->first_name }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Nama Akhir</label>
            <input type="text" class="form-control" name="last_name" value="{{ $member->last_name }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Email</label>
            <input type="text" class="form-control" name="email" value="{{ $member->email }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">No HP</label>
            <input type="text" class="form-control" name="hp" value="{{ $member->mobilephone }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">No Telpon</label>
            <input type="text" class="form-control" name="phone" value="{{ $member->phone }}">
        </div>

        <div class="form-group">
            <label for="exampleInputKeterangan">Alamat</label>
            {{-- <textarea class="form-control" id="summernote" name="address" rows="3"></textarea> --}}
            <input type="text" class="form-control" name="address" value="{{ $member->address }}">
        </div>

        <div class="form-group">
       <label for="exampleInputEmail1" class="no-m m-b-sm">Negara</label>
            <select class="js-states form-control" name="country" tabindex="-1" style="display: none; width: 100%" disabled="disabled">
                <optgroup label="Indonesia">
                    @foreach($country as $kc => $vc)
                        <?php $selected = 'selected="selected"'; ?>
                        @if($vc->id == $member->country)
                            <option value="{{ $vc->id }}" {{ $selected }}>{{ $vc->country_name }}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
      </div>

      <div class="form-group">
       <label for="exampleInputEmail1" class="no-m m-b-sm">Provinsi</label>
            <select class="js-states form-control" name="province" tabindex="-1" style="display: none; width: 100%" disabled="disabled">
                <optgroup label="Indonesia">
                    @foreach($province as $kp => $vp)
                        <?php $selected = 'selected="selected"'; ?>
                        @if($vp->id == $member->province)
                            <option value="{{ $vp->id }}" {{ $selected }}>{{ $vp->name }}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
      </div>
      <div class="form-group">
       <label for="exampleInputEmail1" class="no-m m-b-sm">Kota</label>
            <select class="js-states form-control" name="regency" tabindex="-1" style="display: none; width: 100%" disabled="disabled">
                <optgroup label="Indonesia">
                    @foreach($regency as $kr => $vr)
                        <?php $selected = 'selected="selected"'; ?>
                        @if($vr->id == $member->city)
                            <option value="{{ $vr->id }}" {{ $selected }}>{{ $vr->name }}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
      </div>

        <div class="form-group">
            <label for="exampleInputUsername">Kodepos</label>
            <input type="text" class="form-control" name="postalcode" value="{{ $member->postalcode }}">
        </div>

        <div class="form-group">
            <label for="exampleInputFile">Image profile</label>
            <input type="file" id="exampleInputFile" class="form-control output" name="profile_img" onchange="readURL(this);" accept="image/*">
            @if(empty($member->profile_img) || $member->profile_img == 'null')
                <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
            @else
            <img class="preview_featured_image" src="{{ asset('www/assets/communities/avatars/'.$member->profile_img) }}" style="width:200px">
            @endif
        </div>

        <div class="form-group">
            <label for="exampleInputKeterangan">Keterangan</label>
            <textarea class="form-control" id="summernote" name="description" rows="3">{{ $member->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="exampleInputKeterangan">Biography</label>
            <textarea class="form-control" id="summernote1" name="biography" rows="3">{{ $member->biography }}</textarea>
        </div>

@if(!empty($member->license_id) && !empty($member->license_id_img) || !empty($member->c_license) && !empty($c_license_img))
        <div class="form-group">
          <label>No KTP</label>
          <input type="text" class="form-control" name="ktp" value="{{ $member->license_id }}" required="required" maxlength="16" readonly />
      </div>

      <div class="form-group">
        @if(empty($member->license_id_img) || $member->license_id_img == "null")
          <img class="preview_ktp_image" src="{{ asset('www/assets/images/default.png') }}" width="200">
          @else
          <img src="{{ asset('www/assets/communities/licenses/'.$member->license_id_img) }}" width="300"/>
        @endif
      </div>

      <div class="form-group">
          <label>No SIM C/A</label>
          <input type="text" class="form-control" name="sim" value="{{ $member->c_license }}" required="required" maxlength="12" readonly />
      </div>

      <div class="form-group">
        @if(empty($member->c_license_img) || $member->c_license_img == "null")
          <img class="preview_sim_image" src="{{ asset('www/assets/images/default.png') }}" width="200">
          @else
          <img src="{{ asset('www/assets/communities/licenses/'.$member->c_license_img) }}" width="300"/>
        @endif
      </div>
@endif

        <div class="form-group">
            <label for="exampleInputUsername">Facebook</label>
            <input type="text" class="form-control" name="facebook" value="{{ $member->facebook }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Twitter</label>
            <input type="text" class="form-control" name="twitter" value="{{ $member->twitter }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Google+</label>
            <input type="text" class="form-control" name="googleplus" value="{{ $member->googleplus }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Instagram</label>
            <input type="text" class="form-control" name="instagram" value="{{ $member->instagram }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Youtube</label>
            <input type="text" class="form-control" name="youtube" value="{{ $member->youtube }}">
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Linkedin</label>
            <input type="text" class="form-control" name="linkedin" value="{{ $member->linkedin }}">
        </div>


      <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Submit</button>
    </form>
</div>
@endsection