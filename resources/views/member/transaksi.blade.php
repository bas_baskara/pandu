@extends('member.home')
@section('membercontent')

  <div class="panel-heading clearfix">
    <h4 class="panel-title">Transaksi</h4>
    <h3><span class="label label-success" style="float: right;">Sudah Dibayar</span></h3>
  </div>

  <div class="panel-body">
    <h4>Tagihan - #PIVJ-GQD-1805-0096</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

    <p><i class="fa fa-globe"></i> Kota &nbsp;&nbsp; <i class="fa fa-calendar"></i> 04-10-2018 &nbsp;&nbsp; <i class="fa fa-money"></i> Rp.500.000 &nbsp;&nbsp; <i class="fa fa-user"></i> Member</p>

    <div class="col-sm-12">
      <div class="btn btn-info">Booking</div>
      <div class="btn btn-success">Progress</div>
      <div class="btn btn-warning">Complete</div>
    </div>
  </div>

  <div class="panel-heading clearfix">
    <h4 class="panel-title">Transaksi</h4>
    <h3><span class="label label-success" style="float: right;">Sudah Dibayar</span></h3>
  </div>

  <div class="panel-body">
    <h4>Tagihan - #PIVJ-GQD-1805-0096</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

    <p><i class="fa fa-globe"></i> Kota &nbsp;&nbsp; <i class="fa fa-calendar"></i> 04-10-2018 &nbsp;&nbsp; <i class="fa fa-money"></i> Rp.500.000 &nbsp;&nbsp; <i class="fa fa-user"></i> Member</p>

    <div class="col-sm-12">
      <div class="btn btn-info">Booking</div>
      <div class="btn btn-success">Progress</div>
      <div class="btn btn-warning">Complete</div>
    </div>

</div>
@endsection