@extends('member.home')
@section('head')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection
@section('membercontent')
<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h4>Pesan Masuk dari Customer</h4>

        <hr>
      </div>
    </div>


    <ul class="list-group list-group-flush">
      @if(isset($customers))
      @foreach(array_unique($customers) as $customer)
        <?php
        $i = count($chat_status[$customer->id]) - 1;
        ?>
        <li class="list-group-item customerChat" custId="{{$customer->id}}" style="cursor: pointer">Pesan dari {{$customer->first_name}} @if($chat_status[$customer->id][count($chat_status[$customer->id]) - 1]->read_by_guide == 0)<sup class="alert-danger" id="status_{{$customer->id}}">unread</sup>@endif</li>
        <!-- Chat -->

<!-- Modal -->
<div class="modal fade" id="chatModal_{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Pesan dari {{$customer->first_name.' '.$customer->last_name}}</h5>
      </div>
      <div class="modal-body" id="chatToGuide">
        <div class="panel-body">
        <chat-dari-customer :messages="messagesforGuide" :customer_id="{{$customer->id}}" :member_id="{{$c->id}}"></chat-dari-customer>
        </div>
        <div class="panel-footer">
          <chat-ke-customer v-on:messagesent="addMessage" :sender="'{{$c->first_name.' '.$c->last_name}}'" :customer_id="{{$customer->id}}" :member_id="{{$c->id}}"></chat-ke-customer>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- endchat -->
      @endforeach
      @endif
    </ul>
     

      
  </div>
</div>
@endsection
@section('script')
  <script>
var privateChatId;
    $(function(){
      $('.customerChat').each(function(){
        $(this).click(function(){
          var custId = $(this).attr('custId');
          $('#status_'+custId).html('');
          $('#chatModal_'+custId).modal('show');
          cek_status();
            function cek_status(){
            var cek_status = setInterval(function(){
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
                method: 'post',
                url: '/chat/status',
                data:{custId:custId},
                success: function(data){
                  console.log(data);
                }
              });
            }, 1000);

            window.onclick = function(e){
              if(e.target.className == 'modal fade'){
                clearInterval(cek_status);
              }
            }
          }
        });
      });

   
    });

  </script>
@endsection