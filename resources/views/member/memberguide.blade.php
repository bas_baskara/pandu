@extends('member.home')
@section('membercontent')
    
<div class="panel-heading clearfix">
    <h2 class="panel-title">Pengajuan menjadi Pemandu</h2>
</div>

@if(empty($member->license_id) && empty($member->license_id_img) || empty($member->c_license) && empty($c_license_img))
<div class="panel-body">
  <div class="col-sm-12">
    <form action="{{ url('member/upgrademember/'.$member->id.'/update') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
      {{ csrf_field() }}
      
      <div class="form-group">
          <label>No KTP</label>
          <input type="text" class="form-control" name="ktp" value="{{ $member->license_id }}" required="required" maxlength="16" />
      </div>

      <div class="form-group">
        <label for="exampleInputFile">Upload KTP</label>
        <input type="file" name="ktp_member" class="form-control output" value="" onchange="readKTP(this);" accept="image/*">
        <p class="help-block">*Upload foto/scan ID Anda yang asli</p>
        @if(empty($member->license_id_img) || $member->license_id_img == "null")
          <img class="preview_ktp_image" src="{{ asset('www/assets/images/default.png') }}" width="200">
          @else
          <img src="{{ asset('www/assets/communities/licenses/'.$member->license_id_img) }}" width="300"/>
        @endif
      </div>

      <div class="form-group">
          <label>No SIM C/A</label>
          <input type="text" class="form-control" name="sim" value="{{ $member->c_license }}" required="required" maxlength="12"/>
      </div>

      <div class="form-group">
        <label for="exampleInputFile">Upload SIM C/A</label>
        <input type="file" name="sim_member" class="form-control output" value="" onchange="readSIM(this);" accept="image/*">
        <p class="help-block">*Upload foto/scan SIM C/A Anda yang asli</p>
        @if(empty($member->c_license_img) || $member->c_license_img == "null")
          <img class="preview_sim_image" src="{{ asset('www/assets/images/default.png') }}" width="200">
          @else
          <img src="{{ asset('www/assets/communities/licenses/'.$member->c_license_img) }}" width="300"/>
        @endif
      </div>

      <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Submit</button>
    </form>
  </div>
</div>
@else
<div class="panel-body">
  <div class="col-sm-12">
    <h3>
      Anda telah mengajukan diri sebagai Pemanadu, Mohon menunggu review dari admin.
    </h3>
    <h1>Terimakasih</h1>
  </div>
</div>
@endif

@endsection