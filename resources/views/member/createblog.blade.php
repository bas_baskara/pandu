@extends('member.home')
@section('membercontent')

<div class="panel-heading clearfix">
    <h2 class="panel-title">Buat Blog Baru</h2>
    <a href="{{ url('member/blog') }}" class="btn btn-danger btn-xs" style="float: right;">Kembali</a>
</div>

<div class="panel-body">
  <div class="col-sm-12">
      
      
     <form action="{{ url('member/blog/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
      {{ csrf_field() }}

        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" required="required"/>
        </div>
        
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" class="form-control summernote" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
      
        <div class="form-group">
          <label>Upload Image (Image Utama)</label>
          <input type="file" name="image_post" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
          <span class="help-block text-danger"></span>
          <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
        </div>

        <div class="form-group">
          <label for="status">Status</label>
          <select class="form-control" name="status" id="status">
            @foreach ($status as $key => $value)
              <option value="{{ $key }}">{{ $value }}</option>
           @endforeach
          </select>
        </div>

        <div class="form-group">
          <input type="submit" value="submit" class="btn btn-primary btn-lg" style="width: 100%;">
        </div>
    </form>
  </div>
</div>

@endsection