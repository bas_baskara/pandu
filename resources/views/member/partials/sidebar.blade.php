@if(Auth::user()->ismember == 1)
    <div class="list-group">
        
        {{-- <a href="{{ url('community/home')}}" class="list-group-item active"> <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> KOMUNITAS DASHBOARD</a> --}}
        <a href="{{ url('member/profile/'.Auth::user()->id) }}" class="list-group-item {{ Request::segment(2) == "profile" ? "active" : "" }}"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Profile</a>

        <a href="{{ url('member/message') }}" class="list-group-item {{ Request::segment(2) == "message" ? "active" : "" }}"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>

        <a href="{{ url('member/blog') }}" class="list-group-item {{ Request::segment(2) == "blog" ? "active" : "" }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Blog</a>

        <a href="{{ url('member/joincommunity')}}" class="list-group-item {{ Request::segment(2) == "joincommunity" ? "active" : "" }}"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Yuk Gabung Komunitas</a>

        <a href="{{ url('member/upgrademember/'.Auth::user()->id) }}" class="list-group-item {{ Request::segment(2) == "upgrademember" ? "active" : "" }}"><span class="glyphicon glyphicon-tower" aria-hidden="true"></span>  Pengajuan jadi Pemandu</a>
    </div>
@elseif(Auth::user()->isguide == 1 )
    <div class="list-group">
        <a href="{{ url('community/home')}}" class="list-group-item"> <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> KOMUNITAS DASHBOARD</a>
        <a href="{{ url('member/tour')}}" class="list-group-item {{ Request::segment(2) == 'tour' ? 'active' : '' }}"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Paket Tour</a>
        <a href="{{ url('member/customerbooking')}}" class="list-group-item {{ Request::segment(3) == 'customerbooking' ? 'active' : '' }}"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Booking/Order</a>
        <a href="{{ url('member/transactions')}}" class="list-group-item {{ Request::segment(2) == 'transactions' ? 'active' : '' }}"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Transaksi</a>

        <a href="{{ url('member/message')}}" class="list-group-item {{ Request::segment(2) == 'message' ? 'active' : '' }}"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>
<!--  -->
        <a href="{{ url('member/chatfromcustomer') }}" class="list-group-item {{ Request::segment(2) == "chatformcustomer" ? "active" : "" }}"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Chat Dari Customer @if(isset($unread) && count($unread) > 0)<sup class="text-danger">*unread({{count($unread)}})</sup>@endif</a>
<!--  -->
        <a href="{{ url('member/blog')}}" class="list-group-item {{ Request::segment(2) == 'blog' ? 'active' : '' }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Blog</a>
        <a href="{{ url('member/profile/'.Auth::user()->id)}}" class="list-group-item {{ Request::segment(2) == 'profile' ? 'active' : '' }}"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Profile</a>
        <a href="{{ url('member/bankaccount/'.Auth::user()->id) }}" class="list-group-item {{ Request::segment(2) == 'bankaccount' ? 'active' : '' }}"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Akun Bank</a>
    </div>
@elseif(Auth::user()->isleader == 1)
    <div class="list-group">
        <a href="{{ url('community/home')}}" class="list-group-item active"> <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> KOMUNITAS DASHBOARD</a>
        <a href="{{ url('member/home')}}" class="list-group-item"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Paket Tour</a>
        <a href="{{ url('member/message')}}" class="list-group-item"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>
        <a href="{{ url('member/transactions')}}" class="list-group-item"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Transaksi</a>
        <a href="{{ url('member/profile/'.Auth::user()->id)}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Change Profile</a>
        <a href="{{ url('member/profile/'.Auth::user()->id.'/password')}}" class="list-group-item"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Change Password</a>
    </div>
    @else
    <div class="list-group">
        <p>Anda tidak credible untuk halaman ini.!</p>
    </div>
@endif