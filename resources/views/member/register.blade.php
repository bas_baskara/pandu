@extends('layout')
@section('content')


<div class="bg">
    

    <div class="container">
        <div class="row">
            <div class="col-md-6" style="margin-bottom:50px;">
                <div class="account-box"  style="border-radius : 15px;">
                    
                    <div class="logo">
                        <img src="{{URL::asset('www/assets/images/logo.png')}}" alt="panduasia"/>
                    </div>
                    
                    <div class="login-text">
                        <h2>Register Member</h2>
                    </div>
                    <br>
                    @if(session('notif'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{session('notif')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    
                    <form class="login-form" method="POST" action="{{ route('members.register') }}" enctype="multipart/form-data">
                      
                        @csrf
                        
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nama Depan"  name="namadepan" required/>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nama Belakang"  name="namabelakang" required />
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username"  name="username" required />
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email"  name="email" required/>
                        </div>

          

                        <div class="form-group">
                      <h4 class="no-m m-b-sm m-t-lg">Provinsi</h4>
                      <select class="js-example-tokenizer js-states form-control" tabindex="-1" name="province" id="province" onchange="filterCityInMember()"  style="display: none; width: 100%">
                        <optgroup label="Pilih Provinsi" required>
                          <option value="0">-- Pilih Provinsi --</option>
                           @foreach($province as $kp => $vp)
                            <option value="{{ $vp->id }}">{{ $vp->name }}</option>
                            @endforeach
                        </optgroup>
                      </select>
                    </div>

                    <div class="form-group">
                      <h4 class="no-m m-b-sm m-t-lg">Kota</h4>
                      <select class="js-example-tokenizer js-states form-control" tabindex="-1" style="display: none; width: 100%" name="regency" id="regency" required>
                        <optgroup label="Pilih Kabupaten/Kota">

                          {{-- @foreach ($regencies as $regency)
                            <option value="{{ $regency->id }}">{{ $regency->name }}</option>
                          @endforeach --}}
                        </optgroup>
                      </select>
                    </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" name="phone" required />
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password" required />
                        </div>

                        <label class="clickcheckbox">
                            <center>
                                <p class="small">Dengan mengklik Daftar, Anda menyetujui <a href="{{URL('/home/about/syaratketentuan')}}">syarat dan ketentuan</a> dan <a href="{{URL('/home/about/kebijakanpribadi')}}">Kebijakan pribadi kami.</a> </p>
                            </center>
                        </label>

                        <button class="btn btn-lg btn-block purple-bg" type="submit">Create Account</button>
                        <a href="{{ url('member/login') }}">Sudah Punya Akun? Login langsung</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

  function fiterProvincInMember()
  {
    var countryID = $('#country').val();
    if(countryID)
    {
      $.ajax({
        url: 'getprovinces/'+countryID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#province').empty();
          $.each(data, function(key, value){
            $('#province').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#province').empty();
    }

  }

  function filterCityInMember()
  {
    var provinceID = $('#province').val();

    if(provinceID)
    {
      $.ajax({
        url:  '{{ url('/') }}/member/tour/getcities/'+provinceID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#regency').empty();
          $.each(data, function(key, value){
            $('#regency').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#regency').empty();
    }
    // console.log(provinceID);
  }
</script>

@endsection
