@extends('member.home')
@section('membercontent')


@if(session('message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ session('message') }}</strong>
    </div>
    {{ session()->forget('message') }}
@endif

<div class="panel-heading clearfix">
    <h2 class="panel-title">Bank Account</h2>
</div>

<div class="panel-body">
    <form action="{{ url('member/bankaccount/'.$bank->id.'/update') }}" method="POST"/>
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputUsername">Nama Bank</label>
            <input type="text" class="form-control" name="bank_name" value="{{ $bank->bank_name }}" required />
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Nama Pemilik Akun Bank</label>
            <input type="text" class="form-control" name="account_name" value="{{ $bank->account_name }}" required />
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">No Rekening</label>
            <input type="text" class="form-control" name="bank_acc_number" value="{{ $bank->bank_acc_number }}" required />
        </div>

        <div class="form-group">
            <label for="exampleInputUsername">Cabang Bank</label>
            <input type="text" class="form-control" name="bank_area" value="{{ $bank->bank_area }}" placeholder="KCP Nama Kota Anda" required />
        </div>

        <input type="hidden" class="form-control" name="bank_additional" value="{{ $bank->bank_additional }}"/>

      <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Submit</button>
    </form>
</div>
@endsection