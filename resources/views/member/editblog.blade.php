@extends('member.home')
@section('membercontent')

<div class="panel-heading clearfix">
    <h2 class="panel-title">Edit Blog</h2>
    <a href="{{ url('member/blog') }}" class="btn btn-danger btn-xs" style="float: right;">Kembali</a>
</div>

<div class="panel-body">
    <br>
     @if(session()->has('update_artikel'))
        <div class="alert alert-success">
            <strong>Success!</strong> {{session()->get('update_artikel')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
  <div class="col-sm-12">
     <form action="{{ url('member/blog/update/'.$p->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
      {{ csrf_field() }}

        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" value="{{ $p->title }}" required="required"/>
        </div>
        
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="summernote" cols="30" rows="10">{{ $p->description }}</textarea>
        </div>

        <div class="form-group">
          <label>Upload Image (Image Utama)</label>
          <input type="file" name="image_post" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
          <span class="help-block text-danger"></span>
          @if(empty($p->image) || $p->image == "null")
            <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" alt="{{ $p->title }}" width="300">
            @else
            <img src="{{ asset('www/assets/communities/blog_images/'.$p->image) }}" alt="{{ $p->title }}" width="300"/>
          @endif
        </div>

        <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php 
                $selected = 'selected="selected"'; 

                foreach ($status as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($p->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

        <div class="form-group">
          <input type="submit" value="submit" class="btn btn-primary btn-lg" style="width: 100%;">
        </div>
    </form>
  </div>
</div>

@endsection