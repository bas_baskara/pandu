<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	@foreach(App\Setting::all() as $hasil)
	@if(Request::segment(1) == 'blog')
	<title>{{ $hasil->meta_title_blog }}</title>
	<meta name="description" content="{{ $hasil->meta_desc_blog }}" />
	<meta name="keywords" content="{{ $hasil->meta_keyword_blog }}" />
	@elseif(Request::segment(1) == 'about')
	<title>{{ $hasil->meta_title_about }}</title>
	<meta name="description" content="{{ $hasil->meta_desc_about }}" />
	<meta name="keywords" content="{{ $hasil->meta_keyword_about }}" />
	@else
	<title>{{ $hasil->metatitle }}</title>
	<meta name="description" content="{{ $hasil->metadescription }}" />
	<meta name="keywords" content="{{ $hasil->metakeyword }}" />
	
	@endif
	
	@endforeach
	

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">

	<!-- Bootstrap  -->
{{-- 	<link href="{{ asset('www/assets/css/bootstrap.min.css') }}" rel="stylesheet"> --}}
	<link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

	<!-- Owl Carousel  -->
	<link href="{{ asset('www/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
	<link href="{{ asset('www/assets/css/owl.theme.default.min.css') }}" rel="stylesheet">
	<link href="{{ asset('www/assets/vendors/jquery-ui/jquery-ui.min.css') }}"  rel="stylesheet">

	<!-- Animate.css -->
	<link href="{{ asset('www/assets/css/animate.css') }}" rel="stylesheet">
	<!-- Icomoon Icon Fonts-->
	<link href="{{ asset('www/assets/css/icomoon.css') }}" rel="stylesheet">
	<!-- Themify Icons-->
	<link href="{{ asset('www/assets/css/themify-icons.css') }}" rel="stylesheet">

	<link href="{{ asset('www/assets/vendors/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
	
	<link href="{{ asset('www/assets/css/login.css') }}" rel="stylesheet">
	
	<link href="{{ asset('www/assets/css/loginuser.css') }}" rel="stylesheet">

	<link href="{{ asset('www/assets/vendors/summernote-master/summernote.css') }}" rel="stylesheet"/>
	
	<link href="{{ asset('www/assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet"/>
	
	<!-- Font  -->
	<link href="https://fonts.googleapis.com/css?family=Thasadith:700" rel="stylesheet">
	
	<!-- Theme style  -->
	<link href="{{ asset('www/assets/css/styles.css') }}" rel="stylesheet">

	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<link href="{{-- asset('www/assets/js/respond.min.js') --}}" rel="stylesheet">
<![endif]-->

{{-- <link href="{{ asset('www/assets/css/rotating-card.css') }}" rel="stylesheet"> --}}

<link href="{{ asset('www/assets/slick/slick/slick.css') }}" rel="stylesheet">

<link href="{{ asset('www/assets/slick/slick/slick-theme.css') }}" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Montserrat:600" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125409594-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	
	gtag('config', 'UA-125409594-1');
</script>

<style type="text/css">

/*.slider {*/
/*	width: 100%;*/
/*	margin: 100px auto;*/
/*}*/

.slick-slide {
	margin: 0px 5px;
}

.slick-slide img {
	width: 85%;
}

.slick-prev:before,
.slick-next:before {
	color: black;
}


.slick-slide {
	transition: all ease-in-out .3s;
}

.slick-current {
	opacity: 1;
}

/*For Rating*/
@import url(https://fonts.googleapis.com/css?family=Roboto:500,100,300,700,400);
div.stars{
  width: 300px;
  display: inline-block;
}
 
input.star{
  display: none;
}
 
label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}
 
input.star:checked ~ label.star:before {
  content:'\f005';
  color: #FD4;
  transition: all .25s;
}
 
input.star-5:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
 
input.star-1:checked ~ label.star:before {
  color: #F62;
}
 
label.star:hover{
  transform: rotate(-15deg) scale(1.3);
}
 
label.star:before{
  content:'\f006';
  font-family: FontAwesome;
}

</style>
@section('head')
@show
</head>

<body>
	<div class="gtco-loader"></div>

	<div id="page">
		<nav class="gtco-nav" role="navigation">
			@include('partials.header')
		</nav>

		<div class="content" id="app">
			@yield('content')
		</div>
			@section('script')
			@show

		<?php /* <div>
  

        @foreach(App\Popup::all() as $popup )
        <div class="modal fade" id="setting" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
              <div class='modal-header'>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <a class='modal-title' href="#">
                        <img src="{{asset('/www/assets/blog_images/') }}" alt="">
                        </a>
             </div>
            
          </div>
        </div>
        @endforeach
        
        <script>
            //$("#setting").modal('show');
        </script>
    

        </div> */?>
		
	
		<footer id="gtco-footer" class="gtco-section" role="contentinfo">
			@include('partials.footer')
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop">
			<i class="icon-arrow-up"></i>
		</a>
	</div>
	
	
	
	
	<!-- Scripts -->

	{{-- <script src="/js/app.js"></script> --}}

	<!-- jQuery -->
	<script src="{{ asset('www/assets/js/jquery.min.js') }}"></script>
{{-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script> --}}
	<!-- Bootstrap -->
	{{-- <script src="{{ asset('www/assets/js/bootstrap.min.js') }}"></script> --}}
	<script src="{{ asset('public/js/app.js') }}"></script>
	<script src="{{ asset('www/assets/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('www/assets/js/jquery.waypoints.min.js') }}"></script>
	<!-- Carousel -->
	<script src="{{ asset('www/assets/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('www/assets/vendors/summernote-master/summernote.min.js') }}"></script>
	<script src="{{URL::asset('www/assets/vendors/select2/js/select2.min.js')}}"></script>
	<script src="{{URL::asset('www/assets/js/pages/form-select2.js')}}"></script>
	<!-- Main -->
	<script src="{{ asset('www/assets/js/main.js') }}"></script>

	<script src="{{ asset('www/assets/slick/slick/slick.min.js') }}"></script>

	
	<script type="text/javascript">
	$(document).ready(function() {
	  $('#summernote').summernote({
	  	height: 150
	  });
	  $('#summernote1').summernote({
	  	height: 150
	  });
	  $('#summernote2').summernote({
	  	height: 150
	  });
	  $('#summernote3').summernote({
	  	height: 150
	  });

	  $('.summernote').summernote({
	  	height: 150
	  });

	  	$('.tourslide').slick({
		    dots: false,
		    arrows: false,
			infinite: false,
			fade: true,
			cssEase: 'linear',
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000
		  });


	$('.main-image-wisata').slick({
		    dots: true,
		    arrows: false,
			infinite: false,
			fade: true,
			cssEase: 'linear',
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000
		  });

	setTimeout(function() {
		    $('.alert').fadeOut('slow');
		}, 3000);
	  	
	});
	</script>
	
	<script type="text/javascript">
		$().ready(function () {
			$('[rel="tooltip"]').tooltip();

		});

		function rotateCard(btn) {
			var $card = $(btn).closest('.card-container');
			console.log($card);
			if ($card.hasClass('hover')) {
				$card.removeClass('hover');
			} else {
				$card.addClass('hover');
			}
		}

	</script>

	<script type="text/javascript">
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.preview_featured_image')
              .attr('src', e.target.result)
              .width(250)
              .height(200);
          };
          
          reader.readAsDataURL(input.files[0]);
      }
  }

  function readKTP(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('.preview_ktp_image')
            .attr('src', e.target.result)
            .width(250)
            .height(200);
          };
          
          reader.readAsDataURL(input.files[0]);
      }
  }

  function readSIM(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
	          $('.preview_sim_image')
	          .attr('src', e.target.result)
	          .width(250)
	          .height(200);
          };
          
          reader.readAsDataURL(input.files[0]);
      }
  }
  
</script>

	
	<script>
		$('.responsive').slick({
			dots: true,
			speed: 3000,
			slidesToShow: 4,
			slidesToScroll: 4,
			autoplay: true,
			autoplaySpeed: 1000,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
	</script>
	
	<script>
    $(".variable").slick({
      dots: true,
      infinite: true,
      variableWidth: true
    });
    </script>

	
	<!--auto suggest for blog-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('#search').keyup(function(){
				var query = $(this).val();
				if(query != ''){
					var _token = $('input[name="_token"]').val();
					$.ajax({
						url:"{{ route('autosuggest.fetch') }}",
						method:"POST",
						data:{query:query, _token:_token},
						success:function(data){
							$('#articlelist').fadeIn();
							$('#articlelist').html(data);
						}
					})
				}
			});
			$(document).on('click','li',function(){
				$('#search').val($(this).text());
				$('#articlelist').fadeOut();
			});
		});
	</script>
	
	<!--auto suggest city-->
	<script type="text/javascript">
		$(document).ready(function(){
			$('#search_city').keyup(function(){
				var query = $(this).val();
				if(query != ''){
					var _token = $('input[name="_token"]').val();
					$.ajax({
						url:"{{ route('autosuggest.city') }}",
						method:"POST",
						data:{query:query, _token:_token},
						success:function(data){
							$('#citylist').fadeIn();
							$('#citylist').html(data);
						}
					})
				}
			});
			$(document).on('click','li',function(){
				$('#search_city').val($(this).text());
				$('#citylist').fadeOut();
			});
		});
	</script>
	
		 <script type="text/javascript">
            $('#warning-login').modal('show');
         </script>
         
    <script type="text/javascript">

  function fiterProvince()
  {
    var countryID = $('#country').val();
    if(countryID)
    {
      $.ajax({
        url: 'getprovinces/'+countryID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#province').empty();
          $.each(data, function(key, value){
            $('#province').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#province').empty();
    }

  }

  function filterCity()
  {
    var provinceID = $('#province').val();

    if(provinceID)
    {
      $.ajax({
        url: 'member/register/getcities/'+provinceID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#city').empty();
          $.each(data, function(key, value){
            $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#city').empty();
    }
    // console.log(provinceID);
  }
</script>
<script>
    $( function() {
      $( "#tanggalberangkat" ).datepicker({
        changeMonth: true,
        changeYear: true
      });
    } );
    </script>

	
</body>

</html>