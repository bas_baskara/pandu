<!DOCTYPE html>
<html>
<head>

  <!-- Title -->
  <title>Panduasia | Login - Sign in</title>

  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta charset="UTF-8">
<!--   <meta name="description" content="Admin Dashboard Template" />
  <meta name="keywords" content="admin,dashboard" />
  <meta name="author" content="Steelcoders" /> -->

  <!-- Styles -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
  <link href="{{ URL::asset('www/assets/vendors/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
  <link href="{{ URL::asset('www/assets/vendors/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
  <link href="{{ URL::asset('www/assets/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/fontawesome/css/fontawesome.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/offcanvasmenueffects/css/menu_cornerbox.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/vendors/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>

  <!-- Theme Styles -->
  <link href="{{ URL::asset('www/assets/css/modern.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/css/themes/green.css') }}" class="theme-color" rel="stylesheet" type="text/css"/>
  <link href="{{ URL::asset('www/assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>

  <script src="{{ URL::asset('www/assets/vendors/3d-bold-navigation/js/modernizr.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="page-login">
  <main class="page-content">
    <div class="page-inner">
      <div id="main-wrapper">
        <div class="row">
          <div class="col-md-3 center">
            <div class="login-box">
              <a href="{{ route('login')}}" class="logo-name text-lg text-center">Admin Panduasia</a>
              <p class="text-center m-t-md">Please login into your account.</p>
              <form action="{{ route('login') }}" method="post" autocomplete="off">
                {!! csrf_field() !!}
                <div class="form-group">
                  <input type="email" class="form-control" name="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <button type="submit" class="btn btn-success btn-block">Login</button>
                <a href="{{ route('password.request') }}" class="display-block text-center m-t-md text-sm">Forgot Password?</a>
              </form>
              <p class="text-center m-t-xs text-sm">2018 &copy; Panduasia.</p>
            </div>
          </div>
        </div><!-- Row -->
      </div><!-- Main Wrapper -->
    </div><!-- Page Inner -->
  </main><!-- Page Content -->


  <!-- Javascripts -->
  <script src="{{ URL::asset('www/assets/js/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/pace-master/pace.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/jquery-blockui/jquery.blockui.js') }}"></script>
  <script src="{{ URL::asset('www/assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/switchery/switchery.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/uniform/jquery.uniform.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/offcanvasmenueffects/js/classie.js') }}"></script>
  <script src="{{ URL::asset('www/assets/vendors/waves/waves.min.js') }}"></script>
  <script src="{{ URL::asset('www/assets/js/modern.min.js') }}"></script>

</body>
</html>
