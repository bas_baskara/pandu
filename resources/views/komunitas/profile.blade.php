@extends('komunitas.home')
@section('communitycontent')

<div class="panel-heading clearfix">
    <h2 class="panel-title">Profil Komunitas</h2>
</div>

<div class="panel-body">
    <form action="{{ url('community/profile/'.$sidebar->id.'/update') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        
        
        
      <div class="form-group">
        <div class="form-group">
            <label for="exampleInputHarga">Nama Komunitas</label>
            <input type="text" name="community_name" class="form-control" value="{{ $community->community_name }}">
        </div>
        <div class="form-group">
            <label for="exampleInputHarga">Komunitas Identity</label>
            <input type="text" class="form-control" value="{{ $community->community_identity }}" disabled="disabled">
        </div>
        <div class="form-group">
            <label for="exampleInputKeterangan">Keterangan</label>
            <textarea class="form-control" name="description" id="summernote" rows="3">{{ $community->description }}</textarea>
        </div>

       <label for="exampleInputEmail1" class="no-m m-b-sm">Provinsi</label>
            <select class="js-states form-control" name="province" tabindex="-1" style="display: none; width: 100%" disabled="disabled">
                 <optgroup label="Indonesia">
                    @foreach($province as $kp => $vp)
                        <?php $selected = 'selected="selected"'; ?>
                        @if($vp->id == $c->province)
                            <option value="{{ $vp->id }}" {{ $selected }}>{{ $vp->name }}</option>
                        @endif
                    @endforeach
                </optgroup>
            </select>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Kota</label>
        @foreach($areas as $va)
        <select class="js-example-tokenizer js-states form-control" tabindex="-1" style="display: none; width: 100%;" multiple="multiple" disabled="disabled">
          <?php $selected = 'selected="selected"'; ?>
        
             <td><option {{ $selected }}>{{ $va->kota_nama }}</option></td>
        
        </select>
        @endforeach
      </div>

      <div class="form-group">
          <label>Profile Image</label>
          <input type="file" name="profile_image" class="form-control output" value="" onchange="readProfileURL(this);" accept="image/*">
          @if(empty($community->profile_image) || $community->profile_image == "null")
          <img class="preview_profile_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
          @else
          <img src="{{ asset('www/assets/communities/'.$community->profile_image) }}" alt="{{ $community->community_name }}" width="300"/>
          @endif
          <p class="help-block">*ukuran rekomendasi 150x150px</p>
      </div>

      <div class="form-group">
          <label>Banner Image</label>
          <input type="file" name="banner_image" class="form-control output" value="" onchange="readBannerURL(this);" accept="image/*">
          @if(empty($community->banner_image) || $community->banner_image == "null")
          <img class="preview_banner_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
          @else
          <img src="{{ asset('www/assets/communities/'.$community->banner_image) }}" alt="{{ $community->community_name }}" width="300"/>
          @endif
          <p class="help-block">*ukuran rekomendasi 1366x480px</p>
      </div>

      <div class="form-group">
        <label for="exampleInputHarga">Email</label>
        <input type="email" name="email" class="form-control" value="{{ $community->email }}">
      </div>
      <div class="form-group">
        <label for="exampleInputHarga">HP</label>
        <input type="text" name="hp" class="form-control" value="{{ $community->hp }}" maxlength="14">
      </div>

      <div class="form-group">
        <label for="exampleInputHarga">Facebook</label>
        <input type="text" name="facebook" class="form-control" value="{{ $community->facebook }}">
      </div>
      <div class="form-group">
        <label for="exampleInputHarga">Twitter</label>
        <input type="text" name="twitter" class="form-control" value="{{ $community->twitter }}">
      </div>
      <div class="form-group">
        <label for="exampleInputHarga">Instagram</label>
        <input type="text" name="instagram" class="form-control" value="{{ $community->instagram }}">
      </div>
      <div class="form-group">
        <label for="exampleInputHarga">Website</label>
        <input type="url" name="website" class="form-control" value="{{ $community->website }}" placeholder="http://example.com"
       pattern="http://.*">
      </div>

      <button type="submit" class="btn btn-primary btn-lg" style="width: 100%;">Submit</button>
    </form>
</div>
@endsection