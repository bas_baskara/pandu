@extends('layout')
@section('content')

<div class="gtco-services gtco-section">
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">

    </div>
    <br/>
    
    <div class="container">
       
        @foreach($ca as $kca => $vca)
      
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-2">
                    @if($c->profile_img == 'null' || $c->profile_img == NULL)
                    <img src="{{ asset('www/assets/communities/default.png') }}" class="img-responsive">
                    @else 
                    <!-- <img src="{{ asset('www/assets/communities/'.$c->profile_image) }}" class="img-responsive"> -->
                    @endif
                </div>
               
                <div class="col-md-5">
                    <h2>
                        {{ $c->community_name }}
                        
                    </h2>

                    <h6>
                        <span class="glyphicon glyphicon-map-marker">
                        </span> {{$c->p}}
                        
                    </h6>
                    <a href="{{ url('community/message') }}" class="btn btn-primary">Chat</a>
                </div>

                <div class="col-sm-5">
                    <ul class="list-unstyled no-mb">
                        Ulasan
                        <span class="label label-warning">{{ $c->review }}</span>

                        @php $rating = $c->review; @endphp 

                        @foreach(range(1,5) as $i)
                            <span class="fa-stack rating" style="width:1em">
                                
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    @else
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    @endif
                                @else
                                    <i class="fa fa-star-o"></i>
                                @endif
                                @php $rating-- @endphp
                            </span>
                        @endforeach


                        <p>(<a href="#">{{ $c->review == 0 ? 'Belum ada review' : 'review(s)' }} </a>)</p>

                        <p>
                            @if($c->transaksi_sukses == 0 && $c->transaksi_gagal == 0)
                            Belum ada pekerjaan selesai
                            @else
                            Menyelesaikan {{ $c->transaksi_sukses }} dari {{ $c->transaksi_sukses + $c->transaksi_gagal }} pekerjaan ({{ $c->transaksi_sukses / 100 }} %)
                            @endif
                        </p>
                        <!-- <div class="rating iblocks">
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                            <ion-icon name="star-outline"></ion-icon>
                        </div> -->
                    </ul>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-5">
                    <h3>Description</h3>
                   {!!html_entity_decode($vca->description)!!}
                   
                </div>
               
                <div class="col-md-7">
                    <h3>Social Medias</h3>
                    <ul>
                        <li>Facebook  : <a href="https://wwww.facebook.com/{{ $c->facebook }}" target="_blank">{{ $c->facebook }}</a></li>
                        <li>Twitter   : <a href="https://twitter.com/{{ $c->twitter }}" target="_blank">{{ $c->twitter }}</a></li>
                        <li>Instagram : <a href="https://instagram.com/{{ $c->instagram }}" target="_blank">{{ $c->instagram }}</a></li>
                        <li>Email     : <a href="mailto:{{ $c->email }}">{{ $c->email }}</a></li>
                        <li>Website : <a href="{{ $c->website }}" target="_blank">{{ substr($c->website, 7) }}</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                @include('komunitas/partials/sidebar')
            </div>
             
            <div class="col-sm-9">
                <div class="panel panel-default">
                    @yield('communitycontent')
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
    