@if(Auth::user()->ismember == 1)
    <div class="list-group">
        <a href="{{ url('community/profile/'.Auth::user()->community_id)}}" class="list-group-item active"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Profile Komunitas</a>
        <a href="{{ url('community/message/')}}" class="list-group-item"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>
        <a href="{{ url('community/chat')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Chat Grup</a>
        <a href="{{ url('community/reviews')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Review Member</a>
        <a href="{{ url('community/members')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Member</a>
        <a href="{{ url('community/blog')}}" class="list-group-item"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Blog Komunitas</a>
        <a href="{{ url('member/logout') }}" class="list-group-item list-group-item-danger"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sign Out</a>
    </div>
    @elseif(Auth::user()->isguide == 1)
    <div class="list-group">
        <a href="{{ url('community/profile/'.$sidebar->id) }}" class="list-group-item active"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Profile Komunitas</a>
        <a href="{{ url('community/message') }}" class="list-group-item"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>
        <a href="{{ url('community/chat')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Chat Grup</a>
        <a href="{{ url('community/reviews') }}" class="list-group-item"><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span> Review Member</a>
        <a href="{{ url('community/members') }}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Member</a>
        <a href="{{ url('community/blog') }}" class="list-group-item"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Blog Komunitas</a>
        <a href="{{ url('member/logout') }}" class="list-group-item list-group-item-danger"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sign Out</a>
    </div>
    @elseif(Auth::user()->isleader == 1)
    <div class="list-group">
        <a href="{{ url('community/profile/'.$sidebar->id) }}" class="list-group-item active"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>  Profile Komunitas</a>
        <a href="{{ url('community/message')}}" class="list-group-item"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Message</a>
        <a href="{{ url('community/chat')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Chat Grup</a>
        <a href="{{ url('community/transaksi')}}" class="list-group-item"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Transaksi Member</a>
        <a href="{{ url('community/reviews') }}" class="list-group-item"><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span> Review Member</a>
        <a href="{{ url('community/members')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Member</a>
        <a href="{{ url('community/blog')}}" class="list-group-item"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Blog Komunitas</a>
        <a href="{{ url('member/logout') }}" class="list-group-item list-group-item-danger"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sign Out</a>
    </div>
    @else
    <div class="list-group">
        <p>Anda tidak credible untuk halaman ini.!</p>
    </div>
@endif