@extends('komunitas.home')
@section('communitycontent')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Members</h2>
        <hr>
      </div>
    </div>
    
    
    <div class="row">

      @foreach($member as $km => $vm)
      @if($vm->community_id == $sidebar->id)
      <div class="col-md-4 col-sm-4">
        <a href="{{ url('member/home' )}}" class="gtco-item two-row bg-img animate-box" >
          @if($vm->profile_img == 'null')
          <img src="{{URL::asset('www/assets/communities/avatars/ai.jpg')}}"
                         class="img-responsive">
          @else
          <img src="{{ asset('www/assets/communities/avatars/'.$vm->profile_img)}}" class="gtco-item two-row bg-img animate-box" >
          @endif
          
          <div class="overlay">
            <i class="ti-arrow-top-right"></i>
            <div class="copy">
              <h3>{{ $vm->first_name }} {{ $vm->last_name }}</h3>
              <p>sajkdls</p>
            </div>
          </div>
        </a>
      </div>
      @endif
      @endforeach

    </div>
  </div>
</div>

@endsection
