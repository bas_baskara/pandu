@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik"
/>
<meta name="keywords" content="Pandu Asia Blog" />

@endsection
@section('main-content')
@include('komunitas/_layouts/komunitas')

<div class="flex-container_1" style="padding:35px;">
  
  @include('includes.messages')
  <form role="form" action="{{ route('komunitas-artikel.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
    {{ csrf_field() }}
    <div class="form-group">
      <label>Title</label>
      <input type="text" class="form-control" required="required" name="title" />
    </div>
    
    <div class="form-group">
      <label>Body description</label>
      <textarea name="description" id="editor1" cols="30" rows="10"></textarea>
    </div>
    <div class="form-group">
      <label>Upload Image (Main Image)</label>
      <input type="file" name="file">
      <span class="help-block text-danger"></span>
    </div>
    
    <h2>SEO Tools</h2>
    <br>
    <div class="form-group">
      <label for="">Meta Title</label>
      <input type="text" class="form-control" required="required" name="meta_title" />
    </div>
    <div class="form-group">
      <label for="">Meta Description</label>
      <input type="text" class="form-control" required="required" name="meta_desc" />
    </div>
    <div class="form-group">
      <label for="">Meta Keyword</label>
      <input type="text" class="form-control" required="required" name="meta_keyword" />
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-success" value="Tambah baru">
    </div>
    
    
    
    @include('komunitas/_layouts/script')
    
  </div>
</div>
</div>
</div>
</div>


@endsection
