@extends('komunitas.home')
@section('communitycontent')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Chat</h2>
        <hr>
      </div>
    </div>

    <div class="row">
      <div id="app">
        <Chat :user="{{auth()->user()}}"></Chat>
      </div>

      <!--<div class="col-sm-2">
        <div class="row">
          <div class="col-sm-12">
            <a href="#" class="thumbnail">
              <img src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" alt="...">
            </a>
            <a href="#" class="thumbnail">
              <img src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" alt="...">
            </a>
            <a href="#" class="thumbnail">
              <img src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" alt="...">
            </a>
            <a href="#" class="thumbnail">
              <img src="{{ asset('www/assets/profile_images/rotating_card_profile.png') }}" alt="...">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-10">
        <div class="row">
          <div class="alert alert-success" role="alert">Halloooh</div>
          <div class="alert alert-info" role="alert">Helooooh</div>
          <div class="alert alert-warning" role="alert">Hollllaaaah</div>
          <div class="alert alert-danger" role="alert">Hiiliiih</div>
        </div>
      </div>-->

    </div>
  </div>
</div>

@endsection
