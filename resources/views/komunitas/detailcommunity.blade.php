@extends('layout')
@section('content')

<div class="gtco-services gtco-section">
    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        
    </div>
    <br/>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-2">
                    @if($group->profile_image == 'null')
                    <img src="{{ asset('www/assets/communities/avatars/ai.jpg') }}" class="img-responsive">
                    @else 
                    <img src="{{ asset('www/assets/communities/'.$group->profile_image) }}" class="img-responsive">
                    @endif
                </div>
               
                <div class="col-md-5">
                    <h3>{{ $group->community_name }}</h3>
                    <h5>
                        <span class="glyphicon glyphicon-map-marker"></span> {{ $group->province }} - {{ $group->city }}
                    </h5>
                    <h5>
                        <span class="glyphicon glyphicon-user"></span> {{ count($group->member) }} Orang anggota
                    </h5>
                    @if(empty($checkjoin))
                        <form action="{{ url('community/id/'.$group->community_identity.'/join') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="member_id" value="{{ Auth::user()->id }}" />
                            <input type="hidden" name="community_id" value="{{ $group->id }}"/>
                            <input type="hidden" name="status" value="0"/>
                            <input type="hidden" name="leader_approve" value="0"/>
                            <input type="hidden" name="admin_approve" value="0"/>

                            <button type="submit" class="btn btn-primary">Mari bergabung</button>
                        </form>
                    @endif

                    @foreach ($join as $kj => $vj)
                        @if($vj->member_id != Auth::user()->id && $vj->community_id != $group->id)
                            <form action="{{ url('community/id/'.$group->community_identity.'/join') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="member_id" value="{{ Auth::user()->id }}" />
                                <input type="hidden" name="community_id" value="{{ $group->id }}"/>
                                <input type="hidden" name="status" value="0"/>
                                <input type="hidden" name="leader_approve" value="0"/>
                                <input type="hidden" name="admin_approve" value="0"/>

                                
                            </form>
                        @endif

                        @if($vj->member_id == Auth::user()->id && $vj->community_id == $group->id)
                        <a href="javascript::void(0);" class="btn btn-default" disabled>Menunggu Approval</a>
                        @endif
                    @endforeach
                </div>

                <div class="col-sm-5">
                    <ul class="list-unstyled no-mb">
                        Ulasan
                        <span class="label label-warning">{{ $group->review }}</span>

                        @php $rating = $group->review; @endphp 

                        @foreach(range(1,5) as $i)
                            <span class="fa-stack rating" style="width:1em">
                                
                                @if($rating > 0)
                                    @if($rating > 0.5)
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    @else
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    @endif
                                @else
                                    <i class="fa fa-star-o"></i>
                                @endif
                                @php $rating--; @endphp
                            </span>
                        @endforeach


                        <p>(<a href="#">{{ $group->review == 0 ? 'Belum ada review' : 'review(s)' }} </a>)</p>
                        
                    </ul>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-sm-8">
                    <h3>Deskripsi</h3>
                    <p>{{ strip_tags($group->description) }}</p>
                </div>
               
                <div class="col-md-4">
                    <h3>Social Medias</h3>
                    <ul>
                        <li>Facebook  : <a href="https://wwww.facebook.com/{{ $group->facebook }}" target="_blank">{{ $group->facebook }}</a></li>
                        <li>Twitter   : <a href="https://twitter.com/{{ $group->twitter }}" target="_blank">{{ $group->twitter }}</a></li>
                        <li>Instagram : <a href="https://instagram.com/{{ $group->instagram }}" target="_blank">{{ $group->instagram }}</a></li>
                        <li>Email     : <a href="mailto:{{ $group->email }}">{{ $group->email }}</a></li>
                        <li>Website : <a href="http://{{ $group->website }}" target="_blank">{{ $group->website }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
    