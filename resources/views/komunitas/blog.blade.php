@extends('komunitas.home')
@section('communitycontent')
<div class="panel-heading clearfix">
  <h4 class="panel-title">Daftar Blog member</h4>
  <a href="{{ url('member/blog/create') }}" class="btn btn-primary btn-xs" style="float: right;">Buat Blog Baru</a>
</div>



<div class="panel-body">
  <div class="table-responsive">
    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
      <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Status</th>
          <th>Date</th>
          <th>Author</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Status</th>
          <th>Date</th>
          <th>Author</th>
        </tr>
      </tfoot>

      <tbody>
        
        <?php $n = 1; ?>
        @foreach($com as $kpm => $vpm)
        
        @if($vpm->nm == $c->community_name)        
        <tr>
          <td>{{ $n++ }}</td>
          <td>{{ $vpm->title }}</td>
          <td>{{ ($vpm->status == 1) ? "Published" : "Draft" }}</td>
          
          <td>
            <a href="javascript::void(0)">{{ $vpm->posted_by }}</a>
          </td>
        </tr>
        @endif
        @endforeach
      </tbody>

    </table>
  </div>
</div>

@endsection
