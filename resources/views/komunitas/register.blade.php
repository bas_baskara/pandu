@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia register member" />
<meta name="description" content="register panduasia member" />
<meta name="keywords" content="Pandu Asia register" />

<link rel="stylesheet" href="{{URL::asset('blog/css/login.css')}}">


<!-- Theme Styles -->
<link href="{{URL::asset('plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<script src="{{URL::asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>


@endsection @section('main-content')

<section class="login-block">
    <div class="container">
        <div class="row">
            <center>
                <div class="col-md-12 login-sec">
                    <h2 class="text-center">Register komunitas </h2>
                    <div class="row">
                        <a class="btn-register-komunitas" href="#">Member</a>
                <a class="btn-register-komunitas" href="{{URL::to('komunitas/register-ketua')}}">Ketua</a>
                    </div>
                    @include('includes.messages')
                <form class="login-form" method="POST" action="{{ route('register.komunitas') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase">* Nama Lengkap</label>
                                    <input type="text" class="form-control" required name="fullname">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase"  required>* Email</label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group center">
                                    <label for="exampleInputPassword1" class="text-uppercase">* Password</label>
                                    <input type="password" class="form-control" required name="password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase">* No telp</label>
                                    <input type="text" maxlength="13" class="form-control" required name="phone" maxlength="12">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group center">
                                        <label for="exampleInputEmail1" class="text-uppercase">* TTL</label>
                                        <input type="text" class="form-control" required placeholder="tempat lahir" name="place">
                                    </div>
                                </div>
                            </div>

                        <div class="input-group date" data-provide="datepicker">
                            <div class="row">
                                    <input type="text" class="form-control" name="date_of_birth">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase">* Alamat</label>
                                    <input type="text" class="form-control" required name="address">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase">* Domisili</label>
                                    <input type="text" class="form-control" required name="domicile">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group center">
                                    <label for="exampleInputEmail1" class="text-uppercase">* URL Facebook</label>
                                    <input type="text" class="form-control" required name="url_facebook">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <center>
                            <p class="small">Dengan mengklik Daftar, Anda menyetujui <a href="{{URL('/about/syaratketentuan')}}">syarat dan ketentuan</a> dan <a href="{{URL('/about/kebijakanpribadi')}}">Kebijakan pribadi kami.</a> </p>
                                <input type="submit" class="btn btn-login float-right" value="Daftar">
                                <div class="copy-text">Login
                                    <i class="fa fa-sign-in"></i>
                                    <a href="">Disini</a>
                                </div>
                            </center>
                        </div>
                    </form>
                    <br>

                </div>
            </center>
        </div>
    </div>

</section>

@endsection
