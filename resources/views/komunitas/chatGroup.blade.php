@extends('komunitas.home')
@section('communitycontent')

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Groups</h2>
        <hr>
      </div>
    </div>
    
    <ul class="list-group">
      @foreach($communities as $community)
      <li class="list-group-item chatGroup" id="{{$community->id}}" style="cursor: pointer">{{$community->community_name}}</li>
       <!-- Chat -->
<!-- Modal -->
<div class="modal fade" id="chatModal_{{$community->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">Group {{$community->community_name}}
      </div>
      <div class="modal-body" id="chatToGuide" :com_id="{{$community->id}}">
         <div class="panel-body">
        <chat-group-receive :messages="messageforGroup" :community_id="{{$community->id}}" :sender="'{{Auth::user()->first_name.' '.Auth::user()->last_name}}'"></chat-group-receive>
        </div>
        <div class="panel-footer">
          <chat-group-send v-on:messagesent="addMessageGroup" :community_id="{{$community->id}}" :sender="'{{Auth::user()->first_name.' '.Auth::user()->last_name}}'"></chat-group-send>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- endchat -->
      @endforeach
    </ul>
  </div>
</div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  $(function(){
    $('.chatGroup').each(function(){
      $(this).click(function(){
        var id = $(this).attr('id');
        // $.ajaxSetup({
        //   headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //   }
        // });
        // $.ajax({
        //   method: 'post',
        //   url: '/community/chat/comId',
        //   data:{comId:id},
        //   success: function(data){
        //   }
        // });
        $('#chatModal_'+id).modal('show');
      });
    });
  });
</script>
@endsection
