@extends('komunitas.home')
@section('communitycontent')
<div class="panel-heading clearfix">
    <h2 class="panel-title">Review & Rating Komunitas</h2>
    <a href="{{ url('member/blog') }}" class="btn btn-danger btn-xs" style="float: right;">Kembali</a>
</div>

<div class="panel-body">
  <div class="col-sm-12">
     <form action="{{ url('community/reviews/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
      {{ csrf_field() }}
        
      <div class="row">
        <div class="col-sm-5 rating-collection">
          <div class="form-group rating-button">
          <h4>Rating: </h4>
          <input type="radio" id="r1" name="rg1[]" value="1">
          <label for="r1">&#10038;</label>
          <input type="radio" id="r2" name="rg1[]" value="1">
          <label for="r2">&#10038;</label>
          <input type="radio" id="r3" name="rg1[]" value="1">
          <label for="r3">&#10038;</label>
          <input type="radio" id="r4" name="rg1[]" value="1">
          <label for="r4">&#10038;</label>
          <input type="radio" id="r5" name="rg1[]" value="1">
          <label for="r5">&#10038;</label>
        </div>
        </div>
      </div>
        
      <div class="form-group">
        <label>Review</label>
        <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
      </div>

      <div class="form-group">
        <input type="submit" value="Submit" class="btn btn-primary btn-lg" style="width: 100%;">
      </div>
    </form>
  </div>
</div>

<div class="panel-heading clearfix">
    <h2 class="panel-title">Review Members</h2>
</div>

<div class="panel-body">
  
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

    <div class="col-sm-12">
      <label>Rating: </label>
      @php $rating = $c->review; @endphp 

        @foreach(range(1,5) as $i)
            <span class="fa-stack rating" style="width:1em">
                
                @if($rating > 0)
                    @if($rating > 0.5)
                        <i class="fa fa-star" style="color: orange;"></i>
                    @else
                        <i class="fa fa-star-half" style="color: orange;"></i>
                    @endif
                @else
                    <i class="fa fa-star-o"></i>
                @endif
                @php $rating--; @endphp
            </span>
        @endforeach
    </div>
    <br>
    <p><i class="fa fa-calendar"></i> 04-10-2018 &nbsp;&nbsp; <i class="fa fa-user"></i> Member</p> 

    <hr>
</div>

<div class="panel-body">
  
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

    <div class="col-sm-12">
      <label>Rating: </label>
      @php $rating = $c->review; @endphp 

        @foreach(range(1,5) as $i)
            <span class="fa-stack rating" style="width:1em">
                
                @if($rating > 0)
                    @if($rating > 0.5)
                        <i class="fa fa-star" style="color: orange;"></i>
                    @else
                        <i class="fa fa-star-half" style="color: orange;"></i>
                    @endif
                @else
                    <i class="fa fa-star-o"></i>
                @endif
                @php $rating--; @endphp
            </span>
        @endforeach
    </div>
    <br>
    <p><i class="fa fa-calendar"></i> 04-10-2018 &nbsp;&nbsp; <i class="fa fa-user"></i> Member</p> 

    <hr>
</div>

<div class="panel-body">
  
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

    <div class="col-sm-12">
      <label>Rating: </label>
      @php $rating = $c->review; @endphp 

        @foreach(range(1,5) as $i)
            <span class="fa-stack rating" style="width:1em">
                
                @if($rating > 0)
                    @if($rating > 0.5)
                        <i class="fa fa-star" style="color: orange;"></i>
                    @else
                        <i class="fa fa-star-half" style="color: orange;"></i>
                    @endif
                @else
                    <i class="fa fa-star-o"></i>
                @endif
                @php $rating--; @endphp
            </span>
        @endforeach
    </div>
    <br>
    <p><i class="fa fa-calendar"></i> 04-10-2018 &nbsp;&nbsp; <i class="fa fa-user"></i> Member</p> 

    <hr>
</div>

@endsection
