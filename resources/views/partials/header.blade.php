    <div class="gtco-container">

      <div class="row">
        <div class="col-2">
          <div id="gtco-logo">
            <a href="{{ URL::to('/') }}">
              <img src="{{ URL::asset('www/assets/images/logo.png') }}" alt="logo panduasia">
            </a>
          </div>
        </div>

        <div class="col-10 text-right menu-1"  style="margin-top:15px;">
          <ul>
            <li class="active">
              <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="has-dropdown">
              <a href="javascript:;">Cara kerja</a>
              <ul class="dropdown">
                <li><a href="{{ URL::to('p/panduan-menjadi-member-komunitas-di-panduasia') }}">Panduan Member</a></li>
                <li><a href="{{ URL::to('p/panduan-mencari-tour-guide-di-panduasia') }}">Panduan Mencari Tour Guide</a></li>
                <li><a href="{{ URL::to('p/panduan-menjadi-tour-guide-di-panduasia') }}">Panduan Menjadi Tour Guide</a></li>
                {{-- <li><a href="{{ URL::to('p/panduan-menjadi-ketua-komunitas-di-panduasia') }}">Panduan Ketua Komunitas</a></li> --}}
              </ul>
            </li>
            <li>
              <a href="{{URL::to('/blog')}}">Blog</a>
            </li>
            
            <li>
              <a href="{{URL::to('/komunitas/blog')}}" >Komunitas</a>
            </li>
            <li>
              <a href="{{ URL::to('/member/register') }}">Be a Tourguide</a>
            </li>
            <li>
              <a href="{{URL::to('/about')}}">About</a>
            </li>
            
            <li class="has-dropdown">
              @if(Auth::guard('member')->user() && Auth::guard('member')->user()['ismember'] == 1)
              <a href="javascript:;">{{ Auth::guard('member')->user()->username }}</a>
              <ul class="dropdown">
                <li>
                  @if(Request::segment(1) == 'community')
                  <a href="{{ url('member/home') }}">My Dashboard</a>
                  @else
                  <a href="{{ url('community/home') }}">Komunitas Dashboard</a>
                  @endif
                  <a href="{{ url('member/message') }}">Inbox</a>
                  <a href="{{ url('member/profile/'.Auth::guard('member')->user()->id) }}">My Profile</a>
                  <a href="{{ url('member/profile/'.Auth::guard('member')->user()->id.'/password') }}">Change Password</a>
                  <a href="{{ url('community/reviews')}}">Review</a>
                  <br/>
                  <a href="{{ url('member/logout') }}">Sign Out</a>
                </li>
              </ul>
              @elseif(Auth::guard('member')->user() && Auth::guard('member')->user()['isguide'] == 1)
              <a href="javascript:;">{{ Auth::guard('member')->user()->username }}</a>
              <ul class="dropdown">
                <li>
                  @if(Request::segment(1) == 'community')
                  <a href="{{ url('member/home') }}">My Dashboard</a>
                  @else
                  <a href="{{ url('community/home') }}">Komunitas Dashboard</a>
                  @endif
                  <a href="{{ url('member/tour') }}">My Tour</a>
                  <a href="{{ url('member/message') }}">Message</a>
                  <a href="{{ url('member/transactions')}}">Transaksi</a>
                  <a href="{{ url('member/profile/'.Auth::guard('member')->user()->id) }}">My Profile</a>
                  <a href="{{ url('community/reviews')}}">Review</a>
                  <a href="{{ url('member/profile/'.Auth::guard('member')->user()->id.'/password') }}">Change Password</a>
                  <br/>
                  <a href="{{ url('member/logout') }}">Sign Out</a>
                </li>
              </ul>
              
              @elseif(Auth::guard('member')->user() && Auth::guard('member')->user()['isleader'] == 1)
              <a href="javascript:;">{{ Auth::guard('member')->user()->username }}</a>
              <ul class="dropdown">
                <li>
                  @if(Request::segment(1) == 'community')
                  <a href="{{ url('member/home') }}">My Dashboard</a>
                  @else
                  <a href="{{ url('community/home') }}">Komunitas Dashboard</a>
                  @endif
                  <a href="{{ url('member/profile/'.Auth::guard('member')->user()->id ) }}">My Account</a>
                  <a href="{{ url('community/members')}}">My Member</a>
                  <a href="{{ url('community/transaksi') }}">Transaksi Member</a>
                  <a href="{{ url('member/message') }}">Message</a>
                  <a href="{{ url('community/blog') }}">Blog</a>
                  <a href="{{ url('community/reviews') }}">Review</a>
                  <br/>
                  <a href="{{ url('member/logout') }}">Sign Out</a>
                </li>
              </ul>
              @elseif(Auth::guard('pelanggan')->user())
              <a href="#">{{Auth::guard('pelanggan')->user()->username}}</a>
              <ul class="dropdown">
                <li>
                  <a href="{{URL('user/profile/'.Auth::guard('pelanggan')->user()->username.'/edit')}}">Edit Profile</a>
                  <a href="#">Transaksi</a>
                  <a href="{{URL('user/signout')}}">Logout</a>
                </li>
              </ul>
              @elseif(!Auth::user())
              <a href="#">Masuk</a>
              <ul class="dropdown">
                <li>
                  <a href="{{URL('user/signin')}}">Login</a>
                  <a href="{{URL('user/register')}}">Register</a>
                </li>
              </ul>
              @endif
              
            </ul>
          </div>
        </div>
</div>

