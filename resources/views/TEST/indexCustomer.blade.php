<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Chat User to Guide</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}">
<style>
  .chat {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  .chat li {
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
  }

  .chat li .chat-body p {
    margin: 0;
    color: #777777;
  }

  .panel-body {
    overflow-y: scroll;
    height: 350px;
  }

  ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar {
    width: 12px;
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar-thumb {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
  }
</style>
	<script src="{{ asset('public/js/app.js') }}" defer></script>
</head>
<body>
	<div id="app">
		<div class="container">
			<div class="card">
				<div class="card-body">
					<p class="text-center h3 font-weight-bold">Daftar Guide</p>
					<h1>{{$user->first_name}}</h1>
					<div class="clearfix">&nbsp;</div>
					<table class="table" width="100%">
						<thead class="thead-dark">
							<tr>
								<th width="5%">No</th>
								<th width="30%">Nama Guide</th>
								<th width="30%">Asal Guide</th>
								<th width="20%">Image</th>
								<th width="15%">Aksi</th>
							</tr>							
						</thead>
						<tbody>
							@foreach($guides as $i => $guide)
							<tr>
								<td>{{$i + 1}}</td>
								<td>{{$guide->firts_name.' '.$guide->last_name}}</td>
								<td>{{$asal[$guide->id]->name}}</td>
								<td><img src="{{'/www/assets/communities/avatars/'.$guide->profile_img}}" style="max-width: 100px;"></td>
								<td><span class="btn btn-primary btn-chat" guideId="{{$guide->id}}">Chat</span></td>
							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<!--  -->


<!-- Modal -->
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Chat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="chatToGuide">
        <div class="panel-body">
                	<terima-pesan :messages="messages"></terima-pesan>
                </div>
                <div class="panel-footer">
                    <kirim-pesan v-on:messagesent="addMessage" id=
                    "kirimPesan" :user="{{ Auth::guard('pelanggan')->user()}}"></kirim-pesan>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--  -->
	</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(function(){
		$('.btn-chat').each(function(){
			$(this).click(function(){
				var customer = "{{ Auth::guard('pelanggan')->user()->id }}";
				var guide = $(this).attr('guideId');
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					method: 'post',
					url: '/chat/start',
					data:{guide:guide, customer:customer},
					success: function(data){
						console.log(data);
					}
				});
				$('#chatModal').modal('show');
			});
		});
	});



</script>	
</body>
</html>