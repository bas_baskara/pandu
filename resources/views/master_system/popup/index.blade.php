@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>List Popup</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/popup')}}">List Popup</a></li>
      </ol>
    </div>
  </div>
  
  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                 <a href="{{URL::to('rahasiadapur/popup/create')}}" class="btn btn-primary" >Tambah baru</a>
                <div class="panel-heading clearfix">
                     
                    <h4 class="panel-title">Tabel Tag</h4>
                  
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Image</th>
                              <th>Link</th>
                              <th>Mode</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Image</th>
                              <th>Link</th>
                              <th>Mode</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                        @foreach($datas as $data)
                        <tbody>
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->image }}</td>
                                <td>{{ $data->link }}</td>
                                <td>{{ $data->mode }}</td>
                                <td>{{ ($data->status == 1) ? "Active" : "Inactive" }}</td>
                                <td>
                                    <a href="{{ url('rahasiadapur/popup/edit',$data->id) }}"><span class="glyphicon glyphicon-edit"></span></a> |

                                  <form id="delete-form-{{ $data->id }}" method="tag" action="{{ url('rahasiadapur/popup/delete',$data->id) }}" style="display: none">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    </form>
                                      <a href="{{ url('rahasiadapur/popup/delete',$data->id) }}" onclick="
                                     return confirm('Are you sure, You Want to delete this?')" ><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  
@endsection