@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>Create Category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/popup/create')}}">Create Popup</a></li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/popup/update/'.$data->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" name="title" value="{{ $data->title }}">
          </div>
          <div class="form-group">
            <label>Title</label>
            <textarea class="form-control summernote" name="description">{{ $data->description }}</textarea>
          </div>
          <div class="form-group">
            <label>Image</label>
            <input type="file" name="image">
            
          </div>
          <div class="form-group">
            <label>Link</label>
            <input type="text" class="form-control" required="required" name="link"  value="{{ $data->link }}" />
          </div>
          <div class="form-group">
            <label>Mode</label>
            <input type="text" class="form-control" required="required" name="mode" value="{{ $data->mode }}" />
          </div>
          <div class="form-group">
            <label>Status</label>
            <input type="text" class="form-control" required="required" name="mode" value="{{ $data->status }}" />
          </div>
          
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan">
          </div>
        
      </div>
    </div><!-- Main Wrapper -->
    
@endsection