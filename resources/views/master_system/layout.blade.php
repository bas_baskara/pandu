<!DOCTYPE html>
<html>
<head>
  <title>Blog | Admin Panduasia</title>

  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta charset="UTF-8">
  <meta name="robots" content="noindex, nofollow"/>
  <meta name="googlebot" content="noindex"/>

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

    <link href="{{URL::asset('www/assets/vendors/datatables/css/jquery.datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/datatables/css/jquery.datatables_themeroller.css')}}" rel="stylesheet" type="text/css"/>

    {{-- {{ Html::style('www/assets/vendors/uniform/css/uniform.default.min.css') }} --}}
    <link href="{{URL::asset('www/assets/vendors/pace-master/themes/blue/pace-theme-flash.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('www/assets/vendors/uniform/css/uniform.default.min.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('www/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/fontawesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/line-icons/simple-line-icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/offcanvasmenueffects/css/menu_cornerbox.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/waves/waves.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/3d-bold-navigation/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/slidepushmenus/css/component.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/summernote-master/summernote.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/bootstrap-colorpicker/css/colorpicker.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Theme Styles -->
    <link href="{{URL::asset('www/assets/css/modern.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/css/backend/green.css')}}" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="{{URL::asset('www/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>

    <script src="{{URL::asset('www/assets/vendors/3d-bold-navigation/js/modernizr.js')}}"></script>
    <script src="{{URL::asset('www/assets/vendors/offcanvasmenueffects/js/snap.svg-min.js')}}"></script>

</head>
<body class="page-header-fixed">
  <div class="overlay"></div>

  <div class="wrapper">
    @include('master_system.partials.topnavigation')

    {{-- @if(Auth::user('level') == "Admin") --}}
    <main class="page-content content-wrap">
        <div class="navbar">
          @include('master_system.partials.header')
        </div>
        
        <!-- Page Sidebar -->
        <div class="page-sidebar sidebar">
          @include('master_system.partials.sidebar')
        </div><!-- Page Sidebar -->

        <!-- Page Content -->
        <div class="page-inner">
          @yield('content')
            
          <div class="page-footer">
            @include('master_system.partials.footer')
          </div>
        </div><!-- Page Inner -->
    </main><!-- Page Content -->
    {{-- @endif --}}
</div><!-- Main Wrapper -->

<nav class="cd-nav-container" id="cd-nav">
  <header>
      <h3>Navigation</h3>
      <a href="#0" class="cd-close-nav">Close</a>
  </header>
  <ul class="cd-nav list-unstyled">
      <li class="cd-selected" data-menu="index">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-home"></i>
              </span>
              <p>Dashboard</p>
          </a>
      </li>
      <li data-menu="profile">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-user"></i>
              </span>
              <p>Profile</p>
          </a>
      </li>
      <li data-menu="inbox">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-envelope"></i>
              </span>
              <p>Mailbox</p>
          </a>
      </li>
      <li data-menu="#">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-tasks"></i>
              </span>
              <p>Tasks</p>
          </a>
      </li>
      <li data-menu="#">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-cog"></i>
              </span>
              <p>Settings</p>
          </a>
      </li>
      <li data-menu="calendar">
          <a href="javsacript:void(0);">
              <span>
                  <i class="glyphicon glyphicon-calendar"></i>
              </span>
              <p>Calendar</p>
          </a>
      </li>
  </ul>
</nav>
<div class="cd-overlay"></div>

<!-- Javascripts -->
<script src="{{URL::asset('www/assets/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/pace-master/pace.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/jquery-blockui/jquery.blockui.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/switchery/switchery.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/uniform/jquery.uniform.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/offcanvasmenueffects/js/classie.js')}}"></script>
{{-- <script src="{{URL::asset('www/assets/vendors/offcanvasmenueffects/js/main.js')}}"></script> --}}
<script src="{{URL::asset('www/assets/vendors/waves/waves.min.js')}}"></script>
{{-- <script src="{{URL::asset('www/assets/vendors/3d-bold-navigation/js/main.js')}}"></script> --}}
<script src="{{URL::asset('www/assets/vendors/jquery-mockjax-master/jquery.mockjax.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/moment/moment.js')}}"></script>

 <script src="{{URL::asset('www/assets/vendors/datatables/js/jquery.datatables.min.js')}}"></script>
 <script src="{{URL::asset('www/assets/vendors/datatables/js/jquery.datatables.min.js')}}"></script>
 <script src="{{URL::asset('www/assets/vendors/x-editable/bootstrap3-editable/js/bootstrap-editable.js')}}"></script>
 <script src="{{URL::asset('www/assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
 <script src="{{URL::asset('www/assets/js/pages/table-data.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('www/assets/vendors/keepformdata/jquery.keepFormData.min.js') }}"></script>

<!-- Javascripts -->
<script src="{{URL::asset('www/assets/vendors/summernote-master/summernote.min.js')}}"></script>

<script src="{{URL::asset('www/assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{URL::asset('www/assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{URL::asset('www/assets/js/modern.min.js')}}"></script>
<script src="{{URL::asset('www/assets/js/pages/form-elements.js')}}"></script>

<script src="{{URL::asset('www/assets/vendors/select2/js/select2.min.js')}}"></script>
<script src="{{URL::asset('www/assets/js/pages/form-select2.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {
  $('#summernote').summernote({
    height: 150
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#summernote_2').summernote({
    height: 150
  });
});
</script>
<script type="text/javascript">
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.preview_featured_image')
                  .attr('src', e.target.result)
                  .width(250)
                  .height(200);
          };

          reader.onload = function (e){
            $('.preview_profile_image')
              .attr('src',e.target.result)
              .width(250)
              height(200);
          }

          reader.onload = function (e){
            $('.preview_banner_image')
              .attr('src',e.target.result)
              .width(250)
              height(200);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }
  


  function readProfileURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e){
            $('.preview_profile_image')
              .attr('src',e.target.result)
              .width(250)
              height(200);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  function readBannerURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e){
            $('.preview_banner_image')
              .attr('src',e.target.result)
              .width(250)
              height(200);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
</body>
</html>
