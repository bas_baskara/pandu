@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/listpost')}}">List Job Internship</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Job Internship</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Author</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Author</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <?php $n = 1; ?>
                @foreach($career as $careers)
                <tbody>
                  <tr>
                    <td>{{ $n++ }}</td>
                    <td>{{ $careers->title }}</td>
                      <td><a href="{{ URL::asset('/image_post/'.$careers->image) }}">{{ $careers->image }}</a></td>
                    <td>{{ $careers->posted_by }}</td>
                    <td>
                      <a href="{{ url('rahasiadapur/career/edit',$careers->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                      <a href="{{ url('rahasiadapur/career/delete', $careers->id) }}" onclick="return confirm('Are you sure, You Want to delete this?')
                      "><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                </tbody>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
