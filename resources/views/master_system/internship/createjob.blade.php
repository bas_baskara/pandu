@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<!-- Page Content -->
  <div class="page-title">
    <h3>Create Job</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/career/create')}}">Create Job</a></li>
      </ol>
    </div>
  </div>
  
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/career/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" />
          </div>
          <div class="form-group">
            <label>Body description</label>
            <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>Upload Image (Main Image)</label>
            <input type="file" name="file">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary">
          </div>
        </div>
        </div>
      </div><!-- Main Wrapper -->
      
  @endsection
