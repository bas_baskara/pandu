@extends('master_system.layout')
@section('content')

<!-- Page Content -->

  <div class="page-title">
    <h3>Edit Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/post/edit')}}">Edit Post</a></li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/post/update',$posts->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          
          {{ csrf_field() }}
          
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" value="{{ $posts->title }}"/>
          </div>
          <div class="form-group">
            <h4 class="no-m m-b-sm m-t-lg">Category</h4>
            <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%"  name="categories[]">
              <optgroup label="Pilih Kategori">
                <?php
                  $throw_cats = explode(",", $posts->category);
                ?>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}"
                  @foreach ($throw_cats as $postCategory)
                    @if ($postCategory == $category->id)
                      selected
                    @endif
                  @endforeach
                  >{{ $category->title }}</option>
                </optgroup>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="summernote" cols="30" rows="10">{{ $posts->description }}</textarea>
            </div>

            <div class="form-group">
                <h4 class="no-m m-b-sm m-t-lg">Tag</h4>
                <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="tags[]">
                  <optgroup label="Pilih Tag">
                    <?php
                      $throw_tags = explode(",", $posts->tag);
                    ?>
                    @foreach ($tags as $tag)
                      <option value="{{ $tag->id }}"
                        @foreach($throw_tags as $postTag)
                          @if($postTag == $tag->id)
                            selected
                          @endif
                        @endforeach
                          >{{ $tag->title }}</option>
                    @endforeach
                  </optgroup>
                </select>
            </div>

            <div class="form-group">
              <label>Upload Image (Main Image)</label>
              <input type="file" name="image_post">
              @if(empty($posts->image) || $posts->image == "null")
              {{-- <img src="{{ asset('www/assets/images/default.png') }}" alt="{{ $posts->title }}" width="300"> --}}
              <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
              @else
              <img src="{{ asset('www/assets/blog_images/'.$posts->image) }}" alt="{{ $posts->title }}" width="300"/>
              @endif
            <span class="help-block text-danger"></span>

            </div>

            <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php 
                $selected = 'selected="selected"'; 

                foreach ($status as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($posts->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

            <h2>SEO Tools</h2>
          <br>
          <div class="form-group">
            <label>Meta Title (50 - 70 karakter)</label>
          <input type="text" class="form-control" name="metatitle" value="{{$posts->metatitle}}"/>
          </div>
          <div class="form-group">
            <label>Meta Keyword (dipisah koma)</label>
          <input type="text" class="form-control" name="metakeyword" value="{{$posts->metakeyword}}"/>
          </div>
          <div class="form-group">
            <label>Meta Description (160 - 300 karakter)</label>
          <input type="text" class="form-control" name="metadescription" value="{{$posts->metadescription}}"/>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-success" value="Update post">
          </div>
          </div>
        </div><!-- Main Wrapper -->

    @endsection