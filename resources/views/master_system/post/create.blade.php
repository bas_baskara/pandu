@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/post/create')}}">Create Post</a></li>
      </ol>
    </div>
  </div>


  <form role="form" action="{{ url('rahasiadapur/post/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
    {{ csrf_field() }}

<div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" required="required" name="title" />
        </div>

        <div class="form-group">
          <h4 class="no-m m-b-sm m-t-lg">Category</h4>
          <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="categories[]">
            <optgroup label="Pilih Kategori">
              @foreach ($categories as $category)
              <option value="{{ $category->id }}">{{ $category->title }}</option>
              @endforeach
            </optgroup>
          </select>
        </div>

        <div class="form-group">
          <h4 class="no-m m-b-sm m-t-lg">Tag</h4>
          <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="tags[]">
            <optgroup label="Pilih Tag">
              @foreach ($tags as $tag)
              <option value="{{ $tag->id }}">{{ $tag->title }}</option>
              @endforeach
            </optgroup>
          </select>
        </div>
        
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
        </div>

        <div class="form-group">
          <label>Upload Image (Main Image)</label>
          <input type="file" name="image_post" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
          <span class="help-block text-danger"></span>
          <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
        </div>

        <div class="form-group">
          <label for="status">Status</label>
          <select class="form-control" name="status" id="status">
            <?php foreach ($status as $key => $value) { ?>
              <option value="<?php echo $key ?>"><?php echo $value ?></option>
           <?php } ?>
          </select>
        </div>

        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" maxlength="120"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" id="metakeyword" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" maxlength="300"/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

      </div><!-- Main Wrapper -->
    </form>
  @endsection
  
  
  