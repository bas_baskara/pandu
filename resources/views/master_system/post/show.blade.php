@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/listpost')}}">List Post</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Post</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Demo</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Demo</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <?php $n = 1; ?>
                @foreach($posts as $post)
                @if($post['pageataupost'] == 'post')
                <tbody>
                  <tr>
                    <td>{{ $n++ }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ ($post->status == 1) ? "Published" : "Draft" }}</td>
                    <td><?php echo date('D, d-m-Y g:i A', strtotime($post->created_at)) ?></td>
                    <td>{{ $post->posted_by }}</td>
                    <td><a href="{{ url('blog/'.$post->slug) }}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                    <td>
                      <a href="{{ url('rahasiadapur/post/edit',$post->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                    |
                      <a href="{{ url('rahasiadapur/post/delete', $post->id) }}" onclick="return confirm('Are you sure, You Want to delete this?')
                      "><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                </tbody>
                @endif
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
