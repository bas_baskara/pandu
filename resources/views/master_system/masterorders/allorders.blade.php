@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>Master Orders</h3>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">All Order</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No Order</th>
                    <th>Member Name</th>
                    <th>Community</th>
                    <th>Date</th>
                    <th>Rating</th>
                    <th>Detail</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No Order</th>
                    <th>Member Name</th>
                    <th>Community</th>
                    <th>Date</th>
                    <th>Rating</th>
                    <th>Detail</th>
                  </tr>
                </tfoot>

                <tbody>
                  <tr>
                    <td>No Order</td>
                    <td>Member Name</td>
                    <td>Community</td>
                    <td>Date</td>
                    <td>Rating</td>
                    <td>Detail</td>
                  </tr>
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
