@extends('master_system.layout')
@section('content')
<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/listpost')}}">List Post</a></li>
      </ol>
    </div>
  </div>
  

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Post</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Fullname</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Level</th>
                              <th>Pict</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Fullname</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Level</th>
                              <th>Pict</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                        @foreach($users as $user)
                        <tbody>
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>
                                  @foreach($roles as $k => $v)
                                  {{ ($user->level == $k) ? $v : "" }}
                                  @endforeach
                                </td>
                                <td><a href="{{ URL::asset('/avatar/'.$user->avatar) }}">{{ $user->avatar }}</a></td>
                                <td>
                                    
                                  <a href="{{ url('rahasiadapur/user/edit',$user->id) }}"><span class="glyphicon glyphicon-edit"></span></a> |
                                
                                  <form id="delete-form-{{ $user->id }}" method="post" action="{{ url('rahasiadapur/user/delete',$user->id) }}" style="display: none">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    </form>
                                      <a href="" onclick="
                                     if(confirm('Are you sure, You Want to delete this?'))
                                      {
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                      }
                                      else{
                                        event.preventDefault();
                                      }" ><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
