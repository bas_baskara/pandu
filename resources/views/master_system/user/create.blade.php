@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create User</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/user/create')}}">Create User</a></li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/user/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" required="required" name="name" />
          </div>
          <div class="form-group">
            <label>Fullname</label>
            <input type="text" class="form-control" required="required" name="fullname" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required="required" name="email" />
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" required="required" name="password" />
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="required" name="phone" />
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" required="required" name="address" />
          </div>
          <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control">
              @foreach ($roles as $k => $v)
                <option value="{{ $k }}">{{ $v }}</option>
              @endforeach
            </select> 
          </div>
          <div class="form-group">
            <label>Upload Image</label>
            <input type="file" name="file" onchange="readURL(this);" accept="image/*">
            <span class="help-block text-danger"></span>
            <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
          </div>
          
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan">
          </div>
          
        </div>
      </div><!-- Main Wrapper -->
      
  @endsection