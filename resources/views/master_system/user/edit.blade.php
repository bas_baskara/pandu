@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Edit User</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/user/edit')}}">Edit User</a></li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/user/update',$user->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" required="required" name="name" value="{{ $user->name }}"  />
          </div>
          <div class="form-group">
            <label>Fullname</label>
            <input type="text" class="form-control" name="fullname" value="{{ $user->fullname }}"/>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required="required" name="email" value="{{ $user->email }}"/>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" required="required" name="password" value=""/>
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}"/>
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" name="address" value="{{ $user->address }}"/>
          </div>
          <div class="form-group">
              <label>Upload Image (avatar)</label>
              <input type="file" name="file" onchange="readURL(this);" accept="image/*">
              <span class="help-block text-danger"></span>
              <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
          </div>
          <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control">
              <?php $selected = 'selected=selected';  ?>
              @foreach($roles as $k => $v)
                  <option value="{{ $k }}" {{ ($k == $user->level) ? $selected : "" }}>{{ $v }}</option>
              @endforeach
            </select> 
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan">
          </div>
          
        </div>
      </div><!-- Main Wrapper -->
    
  @endsection