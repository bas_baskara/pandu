@extends('master_system.layout')
@section('content')
<!-- Page Content -->
{{-- <div class="page-inner"> --}}
  <div class="page-title">
    <h3>Community</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/community')}}">Community</a></li>
      </ol>
    </div>
  </div>

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Community</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Community Name</th>
                              <th>Identity</th>
                              <th>Country</th>
                              <th>Province</th>
                              <th>City</th>
                              <th>Status</th>
                              <th>Member</th>
                              <th>Guide</th>
                              <th>Leader</th>
                              <th>Review</th>
                              <th>Built Date</th>
                              <th>View</th>
                              <th>Demo</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Community Name</th>
                              <th>Identity</th>
                              <th>Country</th>
                              <th>Province</th>
                              <th>City</th>
                              <th>Status</th>
                              <th>Member</th>
                              <th>Guide</th>
                              <th>Leader</th>
                              <th>Review</th>
                              <th>Built Date</th>
                              <th>View</th>
                              <th>Demo</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                        
                        <tbody>
                          <?php $n = 1; ?>
                            @foreach($communities as $c)
                            <tr>
                              <td>{{ $n++ }}</td>
                              <td>{{ $c->community_name }}</td>
                              <td>{{ $c->community_identity }}</td>
                              <td>{{ $c->negara }}</td>
                              <td>{{ $c->provinsi }}</td>
                              <td>{{ $c->kota }}</td>
                              <td>{{ ($c->status == 1) ? "Published" : "Draft" }}</td>
                              <td>{{ ($c->member == 0) ? "Belum Ada" : $c->member }}</td>
                              <td>{{ ($c->guide  == 0) ? "Belum Ada" : $c->guide }}</td>
                              <td>{{ ($c->leader == 0) ? "Belum Ada" : $c->leader }}</td>
                              <td>{{ ($c->review == 0) ? "Belum Ada" : $c->review }}</td>
                              <td><?php echo date('D, d-m-Y g:i A', strtotime($c->created_at)) ?></td>
                              <td><a href="{{ url('rahasiadapur/community/'.$c->community_identity) }}"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                              <td><a href="{{ url('c/'.$c->community_identity) }}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                              <td><a href="{{ url('rahasiadapur/community/edit',$c->id) }}"><i class="glyphicon glyphicon-edit"></i></a> | 
                                
                                <a href="{{ URL::to('rahasiadapur/community/delete/'.$c->id) }}"
                                  onclick="return confirm('Are you sure, You Want to delete this Community?')">
                                  <i class="glyphicon glyphicon-trash"></a></td>
                            </tr>
                            @endforeach
                        </tbody>


                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{-- </div> --}}

@endsection
