@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create Community</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/community/create')}}">Create Community</a></li>
      </ol>
    </div>
  </div>
  
  <form role="form" action="{{ url('rahasiadapur/community/store') }}" method="POST" enctype="multipart/form-data" class=" form-horizontal">
  {{ csrf_field() }}
    <div id="main-wrapper">
      <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group">
              <label>Community Name</label>
              <input type="text" class="form-control" required="required" name="title" id="title" />
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Negara</h4>
              <select class="js-example-tokenizer js-states form-control" tabindex="-1" name="country" id="country" onchange="fiterProvince()"  style="display: none; width: 100%">
                <optgroup label="Pilih Negara" required>
                  <option value="0">-- Pilih Negara --</option>
                  @foreach ($countries as $country)
                  <option value="{{ $country->id }}">{{ $country->country_name }}</option>
                  @endforeach
                </optgroup>
              </select>
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Provinsi</h4>
              <select class="js-example-tokenizer js-states form-control" tabindex="-1" name="province" id="province" onchange="filterCity()"  style="display: none; width: 100%">
                <optgroup label="Pilih Provinsi" required>
                  <option value="0">-- Pilih Provinsi --</option>
                  @foreach ($provinces as $province)
                  <option value="{{ $province->id }}">{{ $province->name }}</option>
                  @endforeach
                </optgroup>
              </select>
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Kota</h4>
              <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" style="display: none; width: 100%" name="city[]" id="city" required>
                <optgroup label="Pilih Kabupaten/Kota">

                  {{-- @foreach ($regencies as $regency)
                    <option value="{{ $regency->id }}">{{ $regency->name }}</option>
                  @endforeach --}}
                </optgroup>
              </select>
            </div>

            <div class="form-group">
              <label>Profile Image</label>
              <input type="file" name="profile_image" class="form-control output" value="" onchange="readProfileURL(this);" accept="image/*">
            <span class="help-block text-danger"></span>
            <img class="preview_profile_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
            </div>

            <div class="form-group">
              <label>Banner Image</label>
              <input type="file" name="banner_image" class="form-control output" value="" onchange="readBannerURL(this);" accept="image/*">
            <span class="help-block text-danger"></span>
            <img class="preview_banner_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
            </div>

            <div class="form-group">
              <label for="status">Status</label>
              <select class="form-control" name="status" id="status">
                <?php foreach ($status as $k => $v) { ?>
                  <option value="<?php echo $k ?>"><?php echo $v ?></option>
               <?php } ?>
              </select>
            </div>

        </div>
      </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" maxlength="120"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" id="metakeyword" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" maxlength="300"/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

    </div><!-- Main Wrapper -->

</form>

<script type="text/javascript">

  function fiterProvince()
  {
    var countryID = $('#country').val();
    if(countryID)
    {
      $.ajax({
        url: 'getprovinces/'+countryID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#province').empty();
          $.each(data, function(key, value){
            $('#province').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#province').empty();
    }

  }

  function filterCity()
  {
    var provinceID = $('#province').val();

    if(provinceID)
    {
      $.ajax({
        url: 'getcities/'+provinceID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#city').empty();
          $.each(data, function(key, value){
            $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#city').empty();
    }
    // console.log(provinceID);
  }
</script>

@endsection
