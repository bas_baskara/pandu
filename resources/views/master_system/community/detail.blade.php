@extends('master_system.layout')
@section('content')

  <!-- Page Content -->
  <div class="page-title">
    <h3>Community Detail</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/community/'. Request::segment(4)); ?>">Community Detail</a></li>
      </ol>
    </div>
  </div>

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
           <form class="form-horizontal">
         <div class="form-group">
          <label class="col-sm-2 control-label">Nama Komunitas  </label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->community_name }}</p>
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2 control-label">Ketua</label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->leader == 0 ? 'Belum ada' : '' }}</p>
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2 control-label">Member</label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->member == 0 ? 'Belum ada' : 'Orang' }}</p>
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2 control-label">Provinsi</label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->provinsi }}</p>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Kota</label>
          <div class="col-sm-10">
            @foreach($complement as $kc => $vc)
            @if($vc->idkomunitas == $community->id)
            <p class="form-control-static">{{ $vc->namakota }}</p>
            @endif
            @endforeach
          </div>
        </div>
          <div class="form-group">
          <label class="col-sm-2 control-label">Status</label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->status == 1 ? 'Aktif' : 'Tidak Aktif' }}</p>
          </div>
        </div>
          <div class="form-group">
          <label class="col-sm-2 control-label">Pemandu</label>
          <div class="col-sm-10">
            <p class="form-control-static">{{ $community->guide == 0 ? 'Belum ada' : '' }}</p>
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2 control-label">Image</label>
          <div class="col-sm-10">
            <p class="form-control-static"><img src="{{ url('www/assets/communities/'.$community->profile_image) }}"></p>
          </div>
        </div>
         <div class="form-group">
          <label class="col-sm-2 control-label">Image</label>
          <div class="col-sm-10">
            <p class="form-control-static"><img src="{{ url('www/assets/communities/'.$community->banner_image) }}"></p>
          </div>
        </div>
        </div>
       </div>
       
    </div>
</div><!-- Main Wrapper -->


@endsection