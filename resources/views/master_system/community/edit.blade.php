@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Edit Community</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/community/edit/'.Request::segment(4))}}">Edit Community</a></li>
      </ol>
    </div>
  </div>
  
  <form role="form" action="{{ url('rahasiadapur/community/update/'.Request::segment(4)) }}" method="POST" enctype="multipart/form-data" class=" form-horizontal">
  {{ csrf_field() }}
    <div id="main-wrapper">
      <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group">
              <label>Community Name</label>
            <input type="text" class="form-control" value="{{ $community->community_name }}" required="required" name="title" id="title" />
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="summernote" cols="30" rows="10">{{ $community->description }}</textarea>
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Negara</h4>
              <select class="js-example-tokenizer js-states form-control" tabindex="-1" name="country" id="country" onchange="filterCity()" style="display: none; width: 100%" required>
                <optgroup label="Pilih Negara">
                  @foreach ($countries as $country)
                    <option value="{{ $country->id }}"
                      @foreach($complement as $kc => $vc)
                        @if($country->id == $vc->idnegara)
                        selected
                        @endif
                      @endforeach
                      >{{ $country->country_name }}</option>
                  @endforeach
                </optgroup>
              </select>
            </div>


            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Province</h4>
              <select class="js-example-tokenizer js-states form-control" tabindex="-1" name="province" id="province" onchange="filterCity()" style="display: none; width: 100%" required>
                <optgroup label="Pilih Provinsi">
                  @foreach ($provinces as $province)
                    <option value="{{ $province->id }}"
                      @if($community->province == $province->id)
                      selected
                      @endif
                      >{{ $province->name }}</option>
                  @endforeach
                </optgroup>
              </select>
            </div>

            <div class="form-group">
              <h4 class="no-m m-b-sm m-t-lg">Regency</h4>
              <select class="js-example-tokenizer js-states form-control" multiple="multiple" tabindex="-1" name="city[]" id="city" style="display: none; width: 100%" required>
                <optgroup label="Pilih Kabupaten/Kota">
                  @if(empty($complement))
                    @foreach ($regencies as $regency)
                      <option value="{{ $regency->id }}">{{ $regency->name }}</option>
                    @endforeach
                  @else
                    @foreach ($regencies as $regency)
                      <option value="{{ $regency->id }}"
                        @foreach($complement as $kc => $vc)
                        @if($community->id == $vc->idkomunitas)
                          @if($regency->id == $vc->idkota)
                          selected 
                          @endif
                        @endif
                        @endforeach
                        >{{ $regency->name }}</option>
                    @endforeach
                  @endif
                </optgroup>
              </select>
            </div>

            <div class="form-group">
              <label>Profile Image</label>
              <input type="file" name="profile_image" class="form-control output" value="" onchange="readProfileURL(this);" accept="image/*">
              @if(empty($community->profile_image) || $community->profile_image == "null")
              <img class="preview_profile_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
              @else
              <img src="{{ asset('www/assets/communities/'.$community->profile_image) }}" alt="{{ $community->community_name }}" width="300"/>
              @endif
              <span class="help-block text-danger"></span>
            </div>

            <div class="form-group">
              <label>Banner Image</label>
              <input type="file" name="banner_image" class="form-control output" value="" onchange="readBannerURL(this);" accept="image/*">
              @if(empty($community->banner_image) || $community->banner_image == "null")
              <img class="preview_banner_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
              @else
              <img src="{{ asset('www/assets/communities/'.$community->banner_image) }}" alt="{{ $community->community_name }}" width="300"/>
              @endif
              <span class="help-block text-danger"></span>
            
            </div>

            <div class="form-group">
              <label for="status">Status</label>
              <select class="form-control" name="status" id="status">
                <?php 
                $selected = 'selected="selected"'; 

                foreach ($status as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($community->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
              </select>
            </div>

        </div>
      </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
            <input type="text" class="form-control" name="metatitle" value="{{$community->metatitle}}"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
            <input type="text" class="form-control" name="metakeyword" value="{{$community->metakeyword}}"/>
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
            <input type="text" class="form-control" name="metadescription" value="{{$community->metadescription}}"/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

    </div><!-- Main Wrapper -->

</form>

<script type="text/javascript">

  function filterCity()
  {
    var provinceID = $('#province').val();
    if(provinceID)
    {
      $.ajax({
        url: '../getcities/'+provinceID,
        type: "GET",
        dataType: "json",
        success: function(data){
          $('#city').empty();
          $.each(data, function(key, value){
            $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
          });
        }
      });
    }
    else
    {
      $('#city').empty();
    }
    // console.log(provinceID);
  }
</script>

@endsection
