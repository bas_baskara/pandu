@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Update Tag</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/tag/edit/'. Request::segment(4)); ?>">Edit Tag</a></li>
      </ol>
    </div>
  </div>

  <form role="form" action="{{ URL::to('rahasiadapur/tag/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
  {{ csrf_field() }}
{{--   {{ method_field('PATCH') }} --}}

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" value="{{ $tag->title }}"/>
          </div>

          <div class="form-group">
            <label>Description</label>
               <textarea name="description" id="summernote" cols="30" rows="10"> {{ $tag->description }}</textarea>
          </div>

          <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php 
                $selected = 'selected="selected"'; 

                foreach ($tags as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($tag->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

      </div>
    </div>

      <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" maxlength="70" value="{{ $tag->metatitle }}" />
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" value="{{ $tag->metakeyword }}" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" maxlength="300" value="{{ $tag->metadescription }}" />
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="Simpan" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>
    </div><!-- Main Wrapper -->

  </form>

@endsection