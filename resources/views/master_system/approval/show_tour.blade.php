@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/listpost')}}">List Post</a></li>
      </ol>
    </div>
  </div>


  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Tour</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>_Id</th>
                                <th>Title</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                                <th>Approve</th>
                                <th>Reject</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>_Id</th>
                              <th>Title</th>
                              <th>Edit</th>
                              <th>Hapus</th>
                              <th>Approve</th>
                              <th>Reject</th>
                            </tr>
                        </tfoot>
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="page-footer">
    <p class="no-s">2018 &copy; Panduasia .</p>
  </div>

@endsection
