@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>List Approval Masuk Komunitas</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="#"></a></li>
      </ol>
    </div>
  </div>


  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Member</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>_Id</th>
                                <th>Nama</th>
                                <th>Komunitas</th>
                                <th>Approve</th>
                                <th>Reject</th>
                            </tr>
                        </thead>
                        
                        
                        @foreach($jc as $kjc => $vjc)     
                        @if($vjc->status == 0 && $vjc->admin_approve != 1)
                        <tbody>
                             
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $vjc->member_id }}</td>
                                <td>
                                    @php
                                    foreach($mm as $kmm => $vmm) {
                                      if($vmm->id == $vjc->member_id)
                                      {
                                        echo $vmm->username ;
                                      }
                                    }
                                   @endphp
                                </td>
                                <td>
                                    @php
                                    foreach($mc as $kmc => $vmc) {
                                      if($vmc->id == $vjc->community_id)
                                      {
                                        echo $vmc->community_name ;
                                      }
                                    }
                                   @endphp
                                    
                                </td>
                                
                                <td>
                                    <a href="{{ url('/rahasiadapur/approval/joincommunity/update/'.$vjc->id) }}"><span class="glyphicon glyphicon-ok"></span></a>
                                </td>
                                <td>
                                    <a href="{{ url('/rahasiadapur/approval/reject/'.$vjc->id) }}"><span class="glyphicon glyphicon-remove"></span></a>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                        
                        <tfoot>
                            <tr>
                               <th>No</th>
                                <th>_Id</th>
                                <th>Nama</th>
                                <th>Komunitas</th>
                                <th>Approve</th>
                                <th>Reject</th>
                            </tr>
                        </tfoot>
                       
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="page-footer">
    <p class="no-s">2018 &copy; Panduasia .</p>
  </div>

@endsection
