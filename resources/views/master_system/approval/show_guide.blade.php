@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>List Approval Menjadi Tour Guide</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="#"></a></li>
      </ol>
    </div>
  </div>


  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Member</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>_Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                                <th>Approve</th>
                                <th>Reject</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                                <th>_Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                                <th>Approve</th>
                                <th>Reject</th>
                            </tr>
                        </tfoot>
                        @foreach($member as $mbr)
                        @if(!empty($mbr->license_id) && !empty($mbr->license_id_img) && $mbr->isguide == 0 || !empty($mbr->c_license) && !empty($c_license_img))
                        <tbody>
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $mbr->id }}</td>
                                <td>{{ $mbr->first_name }}</td>
                                <td>{{ $mbr->last_name }}</td>
                                <td>{{ $mbr->email }}</td>
                                <td>

                                    <a href="#"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                                <td>
                                   <span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                                <td>
                                    <a href="{{ url('/rahasiadapur/approval/approve_guide/'.$mbr->id) }}"><span class="glyphicon glyphicon-ok"></span></a>
                                </td>
                                <td>
                                    <a href="#"><span class="glyphicon glyphicon-remove"></span></a>
                                </td>
                            </tr>
                        </tbody>
                        @endif
                        @endforeach
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="page-footer">
    <p class="no-s">2018 &copy; Panduasia .</p>
  </div>

@endsection
