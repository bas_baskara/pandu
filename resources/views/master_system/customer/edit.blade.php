@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Update Category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/customer/edit/'. Request::segment(4)); ?>">Edit Post</a></li>
      </ol>
    </div>
  </div>
  
  <form role="form" action="{{ URL::to('rahasiadapur/customer/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
    {{ csrf_field() }}
    {{--   {{ method_field('PATCH') }} --}}
    
    <div id="main-wrapper">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>ID</label>
            <input type="text" class="form-control" required="required" name="id" value="{{ $customers->id }}"/>
          </div>
          <div class="form-group">
            <label>Dibuat</label>
            <input type="text" class="form-control" required="required" name="created_at" value="{{ $customers->created_at }}"/>
          </div>
          <div class="form-group">
            <label>Diedit</label>
            <input type="text" class="form-control" required="required" name="updated_at" value="{{ $customers->updated_at }}"/>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <input type="submit" value="Edit" class="btn btn-primary btn-lg">
          </div>
        </div>
      </div>
    </div><!-- Main Wrapper -->
    
  </form>
  
  @endsection