@extends('master_system.layout')
@section('content')
<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>List category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('/rahasiadapur/customer')}}">List Customers</a></li>
      </ol>
    </div>
  </div>
  

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Customers</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>created_at</th>
                                <th>updated_at</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>created_at</th>
                                <th>updated_at</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                            </tr>
                        </tfoot>
                        @foreach($customers as $customer)
                        <tbody>
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $customer->created_at }}</td>
                                <td>{{ $customer->updated_at }}</td>
                                <td>
                                    <a href="{{ url('rahasiadapur/customer/edit',$customer->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                                <td>
                                    
                                  <form id="delete-form-{{ $customer->id }}" action="{{ url('rahasiadapur/customer/delete',$customer->id) }}" style="display: none">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    </form>
                                      <a href="" onclick="
                                     if(confirm('Are you sure, You Want to delete this?'))
                                      {
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $customer->id }}').submit();
                                      }
                                      else{
                                        event.preventDefault();
                                      }" ><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

@endsection
