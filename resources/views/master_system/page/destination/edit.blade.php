@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-title">
  <h3>City Destination</h3>
</div>
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-heading clearfix">
          <h4 class="panel-title">Create City Destination</h4>
        </div>
        <form role="form" action="{{ URL::to('rahasiadapur/citydestination/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          
          {{ csrf_field() }}

          <div id="main-wrapper">
            <div class="panel panel-default">
              <div class="panel-body">

                <div class="form-group">
                  <label>Main City</label>
                  <input type="text" class="form-control" required="required" name="mc" value="{{ $p->main_city }}"/>
                </div>

                <div class="form-group">
                  <label>Main Description</label>
                  <textarea name="md" cols="30" rows="10" class="form-control" >{{ $p->main_description }}</textarea>
                </div>

                <div class="form-group">
                  <label>Upload Image (Main Image)</label>
                  <input type="file" name="mic" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
                  <span class="help-block text-danger"></span>
                  <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
                </div>

              </div>
              <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" maxlength="120" value="{{ $p->meta_title }}"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" id="metakeyword" value="{{ $p->meta_keyword }}"/>
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" maxlength="300" value="{{ $p->meta_description }}"/>
            </div>
          </div>
        </div>

              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="form-group">
                    <input type="submit" value="submit" class="btn btn-primary btn-lg">
                  </div>
                </div>
              </div>

          </form>
      </div>
    </div>
  </div>

  @endsection
