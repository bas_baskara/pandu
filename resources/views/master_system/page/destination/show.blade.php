@extends('master_system.layout')
@section('content')
<!-- Page Content -->
{{-- <div class="page-inner"> --}}
  <div class="page-title">
    <h3>List City Destination</h3>
  </div>

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">List City Destination</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Kota</th>
                              <th>Deskripsi</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Kota</th>
                              <th>Deskripsi</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                            <tbody>
                          <?php $n = 1; ?>
                            @foreach($data as $p)
                            <tr>
                              <td>{{ $n++ }}</td>
                              <td>{{ $p->main_city }}</td>
                              <td>{{ $p->main_description }}</td>
                              <td><a href="{{ url('rahasiadapur/citydestination/edit',$p->id) }}"><i class="glyphicon glyphicon-edit"></i></a> | 
                                
                                <a href="{{ URL::to('rahasiadapur/citydestination/delete/'.$p->id) }}"
                                  onclick="return confirm('Are you sure, You Want to delete this?')">
                                  <i class="glyphicon glyphicon-trash"></a></td>
                            </tr>
                            @endforeach
                        </tbody>
        
                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{-- </div> --}}

@endsection
