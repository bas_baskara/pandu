@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create Page</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/page/create')}}">Create Page</a></li>
      </ol>
    </div>
  </div>
  
  <form role="form" action="{{ url('rahasiadapur/page/store') }}" method="POST" enctype="multipart/form-data" class=" form-horizontal">
          {{ csrf_field() }}
    <div id="main-wrapper">
      <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control" required="required" name="title" id="title" />
            </div>

            <div class="form-group">
              <label for="status">Status</label>
              <select class="form-control" name="status" id="status">
                <?php foreach ($status as $k => $v) { ?>
                  <option value="<?php echo $k ?>"><?php echo $v ?></option>
               <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
              <label>Upload Image (Main Image)</label>
              <input type="file" name="image_post" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
            <span class="help-block text-danger"></span>
            <img class="preview_featured_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
            </div>

        </div>
      </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" maxlength="120"/>
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" id="metakeyword" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" maxlength="300"/>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

    </div><!-- Main Wrapper -->

</form>

@endsection
