@extends('master_system.layout')
@section('content')
<!-- Page Content -->
{{-- <div class="page-inner"> --}}
  <div class="page-title">
    <h3>List Page</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('admin/listpost')}}">List Page</a></li>
      </ol>
    </div>
  </div>

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">Tabel Page</h4>
                </div>
                <div class="panel-body">
                   <div class="table-responsive">
                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Title</th>
                              <th>Status</th>
                              <th>Date</th>
                              <th>Author</th>
                              <th>Demo</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Title</th>
                              <th>Status</th>
                              <th>Date</th>
                              <th>Author</th>
                              <th>Demo</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                        
                        <tbody>
                          <?php $n = 1; ?>
                            @foreach($pages as $p)
                            <tr>
                              <td>{{ $n++ }}</td>
                              <td>{{ $p->title }}</td>
                              <td>{{ ($p->status == 1) ? "Published" : "Draft" }}</td>
                              <td><?php echo date('D, d-m-Y g:i A', strtotime($p->created_at)) ?></td>
                              <td>{{ $p->posted_by }}</td>
                              <td><a href="{{ url('p/'.$p->slug) }}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
                              <td><a href="{{ url('rahasiadapur/page/edit',$p->id) }}"><i class="glyphicon glyphicon-edit"></i></a> | 
                                
                                <a href="{{ URL::to('rahasiadapur/page/delete/'.$p->id) }}"
                                  onclick="return confirm('Are you sure, You Want to delete this?')">
                                  <i class="glyphicon glyphicon-trash"></a></td>
                            </tr>
                            @endforeach
                        </tbody>

                       </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{-- </div> --}}

@endsection
