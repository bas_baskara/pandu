@extends('master_system.layout')
@section('content')

  <!-- Page Content -->
  <div class="page-title">
    <h3>Update Page</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/page/edit/'. Request::segment(4)); ?>">Edit Post</a></li>
      </ol>
    </div>
  </div>

  <form role="form" action="{{ URL::to('rahasiadapur/page/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
  {{ csrf_field() }}

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" value="{{ $p->title }}"/>
          </div>

          <div class="form-group">
            <label>Description</label>
               <textarea name="description" id="summernote" cols="30" rows="10"> {{ $p->description }}</textarea>
          </div>

          <div class="form-group">
            <label>Upload Image (Main Image)</label>
            <input type="file" name="image_post">
            @if(empty($posts->image) || $posts->image == "null")
              <img src="{{ asset('www/assets/images/default.png') }}" alt="{{ $p->title }}" width="300">
              @else
              <img src="{{ asset('www/assets/blog_images/'.$p->image) }}" alt="{{ $p->title }}" width="300"/>
              @endif
            <span class="help-block text-danger"></span>
          </div>

          <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php 
                $selected = 'selected="selected"'; 

                foreach ($status as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($p->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>SEO</h4>
        <hr/>
        <div class="form-group">
          <label>Meta Title (50 - 70 karakter)</label>
          <input type="text" class="form-control" name="metatitle" maxlength="70" value="{{ $p->metatitle }}" />
        </div>
        <div class="form-group">
          <label>Meta Keyword (dipisah koma)</label>
          <input type="text" class="form-control" name="metakeyword" value="{{ $p->metakeyword }}" />
        </div>
        <div class="form-group">
          <label>Meta Description (160 - 300 karakter)</label>
          <input type="text" class="form-control" name="metadescription" maxlength="300" value="{{ $p->metadescription }}" />
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group">
          <input type="submit" value="submit" class="btn btn-primary btn-lg">
        </div>
      </div>
    </div>
</div><!-- Main Wrapper -->

</form>

@endsection