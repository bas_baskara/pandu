@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Update Tour Category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="<?php echo url('rahasiadapur/tourcategory/edit/'. Request::segment(4)); ?>">Edit Tour Category</a></li>
      </ol>
    </div>
  </div>

    @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
  @endif

  <form role="form" action="{{ URL::to('rahasiadapur/tourcategory/update/'.Request::segment(4)) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
  {{ csrf_field() }}
{{--   {{ method_field('PATCH') }} --}}

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" value="{{ $tc->title }}"/>
          </div>

          <div class="form-group">
            <label>Description</label>
               <textarea name="description" id="summernote" cols="30" rows="10"> {{ $tc->description }}</textarea>
          </div>

          <div class="form-group">
              <label>Upload Image (Main Image)</label>
              <input type="file" name="featured_image" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
              @if(empty($tc->featured_image) || $tc->featured_image == "null")
              <img src="{{ asset('www/assets/images/default.png') }}" alt="{{ $tc->title }}" width="300">
              @else
              <img class="preview_banner_image" src="{{ asset('www/assets/blog_images/'.$tc->featured_image) }}" alt="{{ $tc->title }}" width="300"/>
              @endif
            <span class="help-block text-danger"></span>
          </div>

          <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php 
                $selected = 'selected="selected"'; 

                foreach ($categories as $key => $value) { ?>
                <option value="<?php echo $key ?>" <?php echo ($tc->status == $key) ? $selected : ""; ?>><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

      </div>
    </div>

      <div class="panel panel-default">
          <div class="panel-body">
            <h4>SEO</h4>
            <hr/>
            <div class="form-group">
              <label>Meta Title (50 - 70 karakter)</label>
              <input type="text" class="form-control" name="metatitle" maxlength="70" value="{{ $tc->metatitle }}" />
            </div>
            <div class="form-group">
              <label>Meta Keyword (dipisah koma)</label>
              <input type="text" class="form-control" name="metakeyword" value="{{ $tc->metakeyword }}" />
            </div>
            <div class="form-group">
              <label>Meta Description (160 - 300 karakter)</label>
              <input type="text" class="form-control" name="metadescription" maxlength="300" value="{{ $tc->metadescription }}" />
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="Simpan" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>
    </div><!-- Main Wrapper -->

  </form>

@endsection