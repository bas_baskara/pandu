@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>Tour Category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/tourcategory')}}">Tour Category List</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Tour Category</h4>

            <a href="{{ url('rahasiadapur/tourcategory/create') }}" class="btn btn-success btn-lg" style="float:right;margin-top:-20px;">Add New</a>
            <br/>
          </div>
          <div class="panel-body">
            @if ($message = Session::get('success'))
                      <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Slug</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Name</th>
                              <th>Slug</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                        </tfoot>
                        @foreach($tourcategory as $tc)
                        <tbody>
                            <tr>
                              <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $tc->title }}</td>
                                <td>{{ $tc->slug }}</td>
                                <td>{{ ($tc->status == 1) ? "Active" : "Inactive" }}</td>
                                <td>
                                    <a href="{{ url('rahasiadapur/tourcategory/edit',$tc->id) }}"><span class="glyphicon glyphicon-edit"></span></a> |

                                  <form id="delete-form-{{ $tc->id }}" method="category" action="{{ url('rahasiadapur/tourcategory/delete',$tc->id) }}" style="display: none">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    </form>
                                      <a href="{{ url('rahasiadapur/tourcategory/delete',$tc->id) }}" onclick="return confirm('Are you sure, You Want to delete this?')"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                       </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
