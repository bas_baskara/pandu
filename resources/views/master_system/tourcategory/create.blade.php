@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>Create Tour Category</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li>Tour Category Form</li>
      </ol>
    </div>
  </div>
  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/tourcategory/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" required="required" name="title" />
          </div>

          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="summernote" cols="30" rows="30"></textarea>
          </div>

          <div class="form-group">
              <label>Upload Image (Main Image)</label>
              <input type="file" name="featured_image" class="form-control output" value="" onchange="readURL(this);" accept="image/*">
            <span class="help-block text-danger"></span>
            <img class="preview_banner_image" src="{{ asset('www/assets/images/default.png') }}" style="width:200px">
          </div>

          <div class="form-group">
            <label for="status"></label>
            <select class="form-control" name="status" id="status">
              <?php foreach ($categories as $key => $value) { ?>
                <option value="<?php echo $key ?>"><?php echo $value ?></option>
             <?php } ?>
            </select>
          </div>

          <h2>SEO Tools</h2>
          <br>
          <div class="form-group">
            <label for="">Meta Title</label>
            <input type="text" class="form-control" name="metatitle" />
          </div>
          <div class="form-group">
            <label for="">Meta Keyword</label>
            <input type="text" class="form-control" name="metadescription" />
          </div>
          <div class="form-group">
            <label for="">Meta Description</label>
            <input type="text" class="form-control" name="metakeyword" />
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Simpan">
          </div>
        
      </div>
    </div><!-- Main Wrapper -->
    
@endsection