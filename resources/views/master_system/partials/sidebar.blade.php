<?php 
  $segment2 = Request::segment(2);
  $segment3 = Request::segment(3);
?>
@if(Auth::user()->level == 1)
<div class="page-sidebar-inner slimscroll">
    <div class="sidebar-header">
      <div class="sidebar-profile">
        <a href="{{ url('rahasiadapur/home') }}" id="profile-menu-link">
          <div class="sidebar-profile-image">
            <img src="{{ URL::asset('www/assets/profile_images/'.Auth::user()->avatar) }}" class="img-circle img-responsive" alt="">
          </div>
          <div class="sidebar-profile-details">
            <span>{{ Auth::user()->name }}<br><small>{{ Auth::user()->name }}</small></span>
          </div>
        </a>
      </div>
    </div>
    <ul class="menu accordion-menu">
      <li class="<?php echo ($segment2 == "home") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/home')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Dashboard</p></a></li>
      
      <li class="droplink <?php echo ($segment2 == "post") ? "open" : ""; ?>"><a href="{{ url('rahasiadapur/post')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-pencil"></span><p>Post</p><span class="arrow"></span></a>
          <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/post/index') }}">All Posts</a></li>
            <li class="<?php echo ($segment3 == "member") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/post/member') }}">Member Posts</a></li>
            <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/post/create')}}">Add New</a></li>
            <li class="droplink <?php echo ($segment2 == "tag") ? "open" : "";  ?>"><a href="{{ url('rahasiadapur/tag')}}" class="waves-effect waves-button"><p>Tag</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/tag/index') }}">All Tags</a></li>
                    <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/tag/create')}}">Add New</a></li>
                </ul>
            </li>
            <li class="droplink <?php echo ($segment2 == "category") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><p>Category</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/category/index') }}">All Category</a></li>
                    <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/category/create') }}">Add New</a></li>
                </ul>
            </li>
          </ul>
      </li>
      <li class="droplink <?php echo ($segment2 == "page") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-file"></span><p>Page</p><span class="arrow"></span></a>
          <ul class="sub-menu">
              <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/page/index') }}">All Pages</a></li>
              <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/page/create') }}">Add New</a></li>
               <li class="droplink"><a href="#" class="waves-effect waves-button"><p>City Destination</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li class=""><a href="{{url('rahasiadapur/citydestination/index')}}">All Destination</a></li>
                    <li class=""><a href="{{url('rahasiadapur/citydestination/create')}}">Add New</a></li>
                </ul>
            </li>
          </ul>
      </li>
      
      <li class="droplink <?php echo ($segment2 == "customer") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>Customer</p><span class="arrow"></span></a>
          <ul class="sub-menu">
              <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/customer/index')}}">All Customers</a></li>
              <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/customer/create')}}">Add New</a></li>
          </ul>
      </li>

      <li class="droplink <?php echo ($segment2 == "community") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-globe"></span><p>Community</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/community/index')}}">All Communities</a></li>
            <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/community/create')}}">Add New</a></li>
        </ul>
      </li>

      <li class="droplink <?php echo ($segment2 == "member") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>Member/Guide</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/members/index')}}">All Members</a></li>
            <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/members/create')}}">Add New</a></li>
        </ul>
      </li>

      <li class="droplink <?php echo ($segment2 == "tour") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-globe"></span><p>Tour Packages</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/tour/index')}}">All Tours</a></li>
            <li class="<?php echo ($segment2 == "tourcategory") ? "active" : "" ?>"><a href="{{ url('rahasiadapur/tourcategory/index') }}">Tour Category</a></li>
            <li class="<?php echo ($segment3 == "complement") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/tour/complement')}}">Include &amp; Exclude</a></li>
        </ul>
      </li>

      <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-ok"></span><p>Approval</p><span class="arrow"></span></a>
        <ul class="sub-menu">
           <li class="droplink"><a href="#" class="waves-effect waves-button"><p>Blog Approval</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li class=""><a href="{{ url('rahasiadapur/approval/blog') }}">Blog</a></li>
                    <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/approval/blog_member')}}">Member Blog</a></li>
                </ul>
            </li>
            <li><a href="{{ url('rahasiadapur/approval/joincommunity') }}">Join Community Approval</a></li>
            <li><a href="{{ url('rahasiadapur/approval/guide') }}">Guide Approval</a></li>
            <li><a href="{{ url('rahasiadapur/approval/leader') }}">Approval Ketua</a></li>
            <li><a href="{{ url('rahasiadapur/approval/tour') }}">Approval Tour</a></li>
        </ul>
      </li>

      <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-send"></span><p>Career</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li><a href="{{ url('rahasiadapur/career/create') }}">Create Job</a></li>
            <li><a href="{{ url('rahasiadapur/career/index') }}">List Job Internship</a></li>
            <li><a href="{{ url('rahasiadapur/list_resume/index') }}">List Resume</a></li>
        </ul>
      </li>

      <li class="droplink" <?php echo ($segment2 == "payment") ? "open" : ""; ?>><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-usd"></span><p>Payment</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "banks") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/payment/banks/1') }}">Bank Transfer</a></li>
        </ul>
      </li>

      <li class="<?php echo ($segment2 == "masterorder") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/masterorder')}}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-euro"></span><p>Master Order</p></a></li>

      <li class="droplink <?php echo ($segment2 == "report") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-stats"></span><p>Master Reports</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "community_report") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/community_report/index')}}">Community Report</a></li>
            <li class="<?php echo ($segment3 == "guide_report") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/guide_report/index')}}">Guide Report</a></li>
            <li class="<?php echo ($segment3 == "transaction_report") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/transaction_report/index')}}">Transaction Report</a></li>
        </ul>
      </li>

      <li class="droplink <?php echo ($segment2 == "system") ? "open" : ""; ?>"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-cog"></span><p>System</p><span class="arrow"></span></a>
        <ul class="sub-menu">
            <li class="<?php echo ($segment3 == "index") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/user/index')}}">All Users</a></li>
            <li class="<?php echo ($segment3 == "create") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/user/create')}}">Add New</a></li>
            <li class="<?php echo ($segment3 == "settings") ? "active" : "";  ?>"><a href="{{ url('rahasiadapur/system/settings/1')}}">Setting</a></li>
        </ul>
    </li>
    </ul>
  </div><!-- Page Sidebar Inner -->

@endif