@extends('master_system.layout')
@section('content')

  <!-- Page Content -->
  <div class="page-title">
    <h3>Update Banks</h3>
  </div>

  <form role="form" action="{{ url('rahasiadapur/payment/banks/update/1') }}" method="post" class="form-horizontal">
  {{ csrf_field() }}

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

          <div class="form-group">
            <label>BCA</label>
            <input type="text" class="form-control" name="bca_title" value="{{ $b['bca_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="bca_rek" value="{{ $b['bca_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="bca_owner" value="{{ $b['bca_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status BCA</label>
            <select class="form-control" name="bca_status" id="bca_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>Mandiri Bank</label>
            <input type="text" class="form-control" name="mandiri_title" value="{{ $b['mandiri_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="mandiri_rek" value="{{ $b['mandiri_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="mandiri_owner" value="{{ $b['mandiri_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status Mandiri</label>
            <select class="form-control" name="mandiri_status" id="mandiri_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>BRI</label>
            <input type="text" class="form-control" name="bri_title" value="{{ $b['bri_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="bri_rek" value="{{ $b['bri_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="bri_owner" value="{{ $b['bri_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status BRI</label>
            <select class="form-control" name="bri_status" id="bri_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>BNI</label>
            <input type="text" class="form-control" name="mandiri_title" value="{{ $b['bni_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="bni_rek" value="{{ $b['bni_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="bni_owner" value="{{ $b['bni_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status BNI</label>
            <select class="form-control" name="bni_status" id="bni_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>Panin Bank</label>
            <input type="text" class="form-control" name="panin_title" value="{{ $b['panin_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="panin_rek" value="{{ $b['panin_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="panin_owner" value="{{ $b['panin_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status Panin</label>
            <select class="form-control" name="panin_status" id="panin_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>Mega Bank</label>
            <input type="text" class="form-control" name="mega_title" value="{{ $b['mega_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="mega_rek" value="{{ $b['mega_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="mega_owner" value="{{ $b['mega_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status Mega</label>
            <select class="form-control" name="mega_status" id="mega_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>Permata Bank</label>
            <input type="text" class="form-control" name="permata_title" value="{{ $b['permata_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="permata_rek" value="{{ $b['permata_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="permata_owner" value="{{ $b['permata_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status Permata</label>
            <select class="form-control" name="permata_status" id="permata_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>BII</label>
            <input type="text" class="form-control" name="bii_title" value="{{ $b['bii_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="bii_rek" value="{{ $b['bii_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="bii_owner" value="{{ $b['bii_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status BII</label>
            <select class="form-control" name="bii_status" id="bii_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>

      <div class="panel-body">
          <div class="form-group">
            <label>DBS</label>
            <input type="text" class="form-control" name="dbs_title" value="{{ $b['dbs_title'] }}"/>
          </div>

          <div class="form-group">
            <label>No. Rekening</label>
            <input type="text" class="form-control" name="dbs_rek" value="{{ $b['dbs_rek'] }}"/>
          </div>

          <div class="form-group">
            <label>Pemilik Rekening</label>
            <input type="text" class="form-control" name="dbs_owner" value="{{ $b['dbs_owner'] }}"/>
          </div>

          <div class="form-group">
            <label for="status">Status DBS</label>
            <select class="form-control" name="dbs_status" id="dbs_status">
                <option value="1">Active</option>
                <option value="0">In Active</option>
            </select>
          </div>

      </div>




    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group">
          <input type="submit" value="submit" class="btn btn-primary btn-lg">
        </div>
      </div>
    </div>
</div><!-- Main Wrapper -->

</form>

@endsection