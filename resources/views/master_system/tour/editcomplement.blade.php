@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create Complements</h3>
  </div>

  <form role="form" action="{{ url('rahasiadapur/tour/complement/update/'.$tie->id) }}" method="post" class="form-horizontal">
    {{ csrf_field() }}

<div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" required="required" name="title" value="{{ $tie->title }}" />
        </div>
        
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="summernote" cols="30" rows="10">{{ $tie->description }}</textarea>
        </div>

        <div class="form-group">
          <label for="includeexclude">Tipe</label>
          <select class="form-control" name="includeexclude" id="includeexclude">
            @foreach ($inex as $key => $value)
              <option value="<?php echo $key ?>"><?php echo $value ?></option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label>Price</label>
          <input type="text" class="form-control" name="price" value="{{ $tie->price }}" />
        </div>

        <div class="form-group">
          <label for="status">Status</label>
          <select class="form-control" name="status" id="status">
            @foreach ($status as $key => $value)
            @php $selected='selected="selected"';  @endphp
              <option value="<?php echo $key ?>" {{ $key == $tie['status'] ? $selected : "" }}><?php echo $value ?></option>
           @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="tourcategory">Destinasi</label>
          <select class="form-control" name="tourcategory[]" id="tourcategory" multiple="">
            @foreach ($c as $kc => $vc)
              
              @foreach($tc as $ktc)
              @php $selected='selected="selected"';  @endphp
                @if($vc->id == $ktc)
                  <option value="{{ $vc->id }}" {{ $selected }}>{{ $vc->title }}</option>
                @endif
              @endforeach
            @endforeach
          </select>
        </div>

        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

      </div><!-- Main Wrapper -->
    </div>
    </form>
 
  @endsection
  