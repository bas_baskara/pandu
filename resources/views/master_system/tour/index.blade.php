@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>List Post</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/listpost')}}">List Post</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Post</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Demo</th>

                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Demo</th>
                  </tr>
                </tfoot>

                <tbody>
                  <tr>
                    <td>1</td>
                    <td>judul</td>
                    <td>Published</td>
                    <td><?php echo date('D, d-m-Y g:i A') ?></td>
                    <td>Author</td>
                    <td>see</td>
                  </tr>
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
