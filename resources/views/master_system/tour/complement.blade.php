  @extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>Include &amp; Exclude Tour</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/listpost')}}">All Include &amp; Exclude</a></li>
      </ol>
    </div>
  </div>
  
  
  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Include & Exclude</h4>

            <a href="{{ url('rahasiadapur/tour/complement/create') }}" class="btn btn-success btn-lg" style="float:right;margin-top:-20px;">Add New</a>
            <br/>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Tipe</th>
                    <th>Tour Category</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Tipe</th>
                    <th>Tour Category</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Author</th>
                    <th>Action</th>
                  </tr>
                </tfoot>

                <tbody>
                  @php $n = 1 @endphp
                    @foreach($tie as $ktie => $vtie)
                      <tr>
                        <td>{{ $n++ }}</td>
                        <td>{{ $vtie->title }}</td>
                        <td>{{ $vtie->tipe == 1 ? "Include" : "Exclude" }}</td>
                        <td>
                          @php

                          $show_tc = explode(',', $vtie->tour_category_id);

                          foreach ($show_tc as $kst) {
                            foreach ($c as $kc => $vc) {
                              if($kst == $vc->id)
                              {
                                echo $vc->title . ", ";
                              }
                            }
                          }

                          @endphp

                        </td>
                        <td>{{ $vtie->status == 1 ? "Active" : "InActive" }}</td>
                        <td>{{ date('D, d-m-Y g:i A', strtotime($vtie->created_at)) }}</td>
                        <td>
                          {{ $vtie->issystem == Auth::user()->id ? Auth::user()->name : "" }}
                          {{ $vtie->ismember == Auth::user()->id ? Auth::user()->name : "" }}
                        </td>
                        <td>
                          <a href="{{ url('rahasiadapur/tour/complement/edit',$vtie->id) }}"><i class="glyphicon glyphicon-edit"></i></a> |           
                          <a href="{{ URL::to('rahasiadapur/tour/complement/delete/'.$vtie->id) }}"
                                      onclick="return confirm('Are you sure, You Want to delete this?')">
                                      <i class="glyphicon glyphicon-trash">
                          </a>
                        </td>
                      </tr>
                    @endforeach
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
