@extends('master_system.layout')
@section('content')

<!-- Page Content -->
  <div class="page-title">
    <h3>Create Complements</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/tour/complement/create')}}">Create Complements</a></li>
      </ol>
    </div>
  </div>

  <form role="form" action="{{ url('rahasiadapur/tour/complement/store') }}" method="post" class="form-horizontal">
    {{ csrf_field() }}

<div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">

        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" required="required" name="title" />
        </div>
        
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
        </div>

        <div class="form-group">
          <label for="includeexclude">Tipe</label>
          <select class="form-control" name="includeexclude" id="includeexclude">
            <?php foreach ($inex as $key => $value) { ?>
              <option value="<?php echo $key ?>"><?php echo $value ?></option>
           <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Price</label>
          <input type="text" class="form-control" name="price" />
        </div>

        <div class="form-group">
          <label for="status">Status</label>
          <select class="form-control" name="status" id="status">
            <?php foreach ($status as $key => $value) { ?>
              <option value="<?php echo $key ?>"><?php echo $value ?></option>
           <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label for="status">Tour Category</label>
          <select class="form-control" name="tourcategory[]" id="tourcategory" multiple="multiple">
            @foreach ($tc as $ktc => $vtc)
              <option value="{{ $vtc->id }}">{{ $vtc->title }}</option>
           @endforeach
          </select>
        </div>


        </div>

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="form-group">
              <input type="submit" value="submit" class="btn btn-primary btn-lg">
            </div>
          </div>
        </div>

      </div><!-- Main Wrapper -->
    </div>
    </form>
  @endsection
  
  
  