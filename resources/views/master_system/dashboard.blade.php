@extends('master_system.layout')
@section('content')
<!-- Page Content -->

  <div class="page-title">
    <h3>Dashboard</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </div>
  </div>
  @if(Auth::user('level') == "Admin")
  <div id="main-wrapper">
    <p style="margin-left:2px;">Hi , {{ Auth::user()->name }}</p>
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="counter">{{$allpost}}</p>
              <span class="info-box-title">All Post Total</span>
            </div>
            <div class="info-box-icon">
              <i class="fa fa-file"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="counter">{{$pendingpost}}</p>
              <span class="info-box-title">Pending Post Total</span>
            </div>
            <div class="info-box-icon">
              <i class="fa fa-minus-circle"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="counter">{{$publishpost}}</p>
              <span class="info-box-title">Publish Post Total</span>
            </div>
            <div class="info-box-icon">
              <i class="fa fa-upload"></i>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Row -->
  </div><!-- Main Wrapper -->
  @endif

  <div id="main-wrapper">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="counter">{{$userpost}}</p>
              <span class="info-box-title">Post Total {{Auth::user()->count()}}</span>
            </div>
            <div class="info-box-icon">
              <i class="fa fa-file"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="counter">{{$userviewpost}}</p>
              <span class="info-box-title">Viewers All Post</span>
            </div>
            <div class="info-box-icon">
              <i class="fa fa-eye"></i>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Row -->
  </div><!-- Main Wrapper -->
  
@endsection
