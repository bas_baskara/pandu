@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Edit Member</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/members/create')}}">Edit Member</a></li>
      </ol>
    </div>
  </div>

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/members/update/.$data->id') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control" required="required" name="firstname" value="{{ $data->first_name }}" />
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" required="required" name="last_name" value="{{ $data->last_name }}"/>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" required="required" name="username" value="{{ $data->username }}" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" required="required" name="email" value="{{ $data->email }}"/>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" required="required" name="password" />
          </div>
          <div class="form-group">
            <label>Mobile Phone</label>
            <input type="text" class="form-control" required="required" name="mobilephone" value="{{ $data->mobilephone }}"/>
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="required" name="phone" value="{{ $data->phone }}"/>
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" required="required" name="address" value="{{ $data->address }}"/>
          </div>
          <div class="form-group">
            <label>City</label>
            <input type="text" class="form-control" required="required" name="city" value="{{ $data->city }}" />
          </div>
          <div class="form-group">
            <label>Province</label>
            <input type="text" class="form-control" required="required" name="province" value="{{ $data->province }}"/>
          </div>
          <div class="form-group">
            <label>Country</label>
            <input type="text" class="form-control" required="required" name="country" value="{{ $data->country }}"/>
          </div>
          <div class="form-group">
            <label>Postal Code</label>
            <input type="text" class="form-control" required="required" name="postalcode" value="{{ $data->postalcode }}"/>
          </div>
          <div class="form-group">
            <label>Leader/Member</label>
            <input type="text" class="form-control" required="required" name="postalcode" value="{{ $data->isleader }}" disabled/>
          </div>
          <div class="form-group">
            <label>Profile Image</label>
            <input type="file" name="profile">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="summernote" cols="30" rows="10">{{ $data->description }}</textarea>
          </div> 
          <div class="form-group">
            <label>Biography</label>
            <textarea name="biography" id="summernote_2" cols="30" rows="10">{{ $data->biography }}</textarea>
          </div>
          <div class="form-group">
            <label>License Id</label>
            <input type="text" class="form-control" required="required" name="license_id" value="{{ $data->license_id }}"/>
          </div>
          <div class="form-group">
            <label>License Id Image</label>
            <input type="file" name="license_id_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>C License</label>
            <input type="text" class="form-control" required="required" name="c_license" value="{{ $data->c_license }}"/>
          </div>
          <div class="form-group">
            <label>C License Image</label>
            <input type="file" name="c_license_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>A License</label>
            <input type="text" class="form-control" required="required" name="a_license" value="{{ $data->a_license }}"/>
          </div>
          <div class="form-group">
            <label>A License Image</label>
            <input type="file" name="a_license_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Passport</label>
            <input type="text" class="form-control" required="required" name="passport" value="{{ $data->passport }}"/>
          </div>
          <div class="form-group">
            <label>Passport Image</label>
            <input type="file" name="passport_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Facebook</label>
            <input type="text" class="form-control" required="required" name="fb" value="{{ $data->facebook }}"/>
          </div>
          <div class="form-group">
            <label>Twitter</label>
            <input type="text" class="form-control" required="required" name="twt" value="{{ $data->twitter }}"/>
          </div>
          <div class="form-group">
            <label>Google+</label>
            <input type="text" class="form-control" required="required" name="g" value="{{ $data->googleplus }}" />
          </div>
          <div class="form-group">
            <label>Instagram</label>
            <input type="text" class="form-control" required="required" name="ig" value="{{ $data->instagram }}" />
          </div>
          <div class="form-group">
            <label>Linked In</label>
            <input type="text" class="form-control" required="required" name="li" value="{{ $data->linkedin }}"/>
          </div>
          <div class="form-group">
            <label>Youtube</label>
            <input type="text" class="form-control" required="required" name="yt" value="{{ $data->youtube }}"/>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-success" value="Update">
          </div>

        </div>
      </div><!-- Main Wrapper -->
      
  @endsection
