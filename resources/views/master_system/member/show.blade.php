@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>List Member</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/members/index')}}">List Member</a></li>
      </ol>
    </div>
  </div>


  <div id="main-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading clearfix">
            <h4 class="panel-title">Tabel Members</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Edit</th>
                    <th>Hapus</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Edit</th>
                    <th>Hapus</th>
                  </tr>
                </tfoot>
                @foreach($data as $datas)
                <tbody>
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $datas->first_name }}</td>
                    <td>{{ $datas->last_name }}</td>
                    <td>{{ $datas->username }}</td>
                    <td>{{$datas->email}}</td>
                    <td>
                      <a href="{{ url('rahasiadapur/members/edit',$datas->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                    </td>
                    <td>
                      {{-- {{ route('post.destroy',$post->id) }} --}}
                      <form id="delete-form-{{ $datas->id }}" method="post" action="{{ url('rahasiadapur/members/delete/',$datas->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                      </form>
                      <a href="" onclick="
                      if(confirm('Are you sure, You Want to delete this?'))
                      {
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $datas->id }}').submit();
                      }
                      else{
                        event.preventDefault();
                      }" ><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                </tbody>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
