@extends('master_system.layout')
@section('content')

<!-- Page Content -->
<div class="page-inner">
  <div class="page-title">
    <h3>Create Member</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li><a href="{{ url('rahasiadapur/members/create')}}">Create Member</a></li>
      </ol>
    </div>
  </div>

  <div id="main-wrapper">
    <div class="panel panel-default">
      <div class="panel-body">
        <form role="form" action="{{ url('rahasiadapur/members/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control" required="required" name="firstname" />
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" required="required" name="last_name" />
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" required="required" name="username" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" required="required" name="email" />
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" required="required" name="password" />
          </div>
          <div class="form-group">
            <label>Mobile Phone</label>
            <input type="text" class="form-control" required="required" name="mobilephone" />
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" required="required" name="phone" />
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" required="required" name="address" />
          </div>
          <div class="form-group">
            <label>City</label>
            <input type="text" class="form-control" required="required" name="city" />
          </div>
          <div class="form-group">
            <label>Province</label>
            <input type="text" class="form-control" required="required" name="province" />
          </div>
          <div class="form-group">
            <label>Country</label>
            <input type="text" class="form-control" required="required" name="country" />
          </div>
          <div class="form-group">
            <label>Postal Code</label>
            <input type="text" class="form-control" required="required" name="postalcode" />
          </div>
          <div class="form-group">
          <label>Leader/Member</label>
          <select name="isleader" class="form-control">
              <option value="1">Leader</option>
              <option value="0">Member</option>
          </select>
          </div> 
          <div class="form-group">
            <label>Profile Image</label>
            <input type="file" name="profile">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="summernote" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>Biography</label>
            <textarea name="biography" id="summernote_2" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>License Id</label>
            <input type="text" class="form-control" required="required" name="license_id" />
          </div>
          <div class="form-group">
            <label>License Id Image</label>
            <input type="file" name="license_id_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>C License</label>
            <input type="text" class="form-control" required="required" name="c_license" />
          </div>
          <div class="form-group">
            <label>C License Image</label>
            <input type="file" name="c_license_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>A License</label>
            <input type="text" class="form-control" required="required" name="a_license" />
          </div>
          <div class="form-group">
            <label>A License Image</label>
            <input type="file" name="a_license_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Passport</label>
            <input type="text" class="form-control" required="required" name="passport" />
          </div>
          <div class="form-group">
            <label>Passport Image</label>
            <input type="file" name="passport_img">
            <span class="help-block text-danger"></span>
          </div>
          <div class="form-group">
            <label>Facebook</label>
            <input type="text" class="form-control" required="required" name="fb" />
          </div>
          <div class="form-group">
            <label>Twitter</label>
            <input type="text" class="form-control" required="required" name="twt" />
          </div>
          <div class="form-group">
            <label>Google+</label>
            <input type="text" class="form-control" required="required" name="g" />
          </div>
          <div class="form-group">
            <label>Instagram</label>
            <input type="text" class="form-control" required="required" name="ig" />
          </div>
          <div class="form-group">
            <label>Linked In</label>
            <input type="text" class="form-control" required="required" name="li" />
          </div>
          <div class="form-group">
            <label>Youtube</label>
            <input type="text" class="form-control" required="required" name="yt" />
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-success" value="Add">
          </div>

        </div>
      </div><!-- Main Wrapper -->
      
      @endsection
