@extends('master_system.layout')
@section('content')

  <div class="page-title">
    <h3>Settings</h3>
    <div class="page-breadcrumb">
      <ol class="breadcrumb">
        <li>Settings form</li>
      </ol>
    </div>
  </div>
  

  <div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">General Settings</h4>
                </div>
                
                <div class="panel-body">
            
                   <form role="form" action="{{ url('rahasiadapur/system/settings/update',$setting->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        
                      {{ csrf_field() }}
                  {{ method_field('PATCH') }}

                            @if (session()->has('success'))
                            <div class="alert alert-success text-center animated fadeIn">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>

                                </button>

                                <strong>
                                    {!! session()->get('success') !!}
                                </strong>
                            </div>
                            @endif
                
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Articles</label>
                    <div class="col-sm-10">
                        <label class="control-label">{{$post}}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Facebook</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="facebook" name="facebook" value="{{$setting->facebook }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Instagram</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="instagram" name="instagram" value="{{$setting->instagram }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Google +</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Google +" name="googleplus"  value="{{$setting->googleplus }}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Twitter</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Twitter" name="twitter" value="{{$setting->twitter }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Youtube</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Youtube" name="youtube" value="{{$setting->youtube }}">
                    </div>
                </div>
                <hr>
                <h4 class="panel-title">Webmaster Verification</h4>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Google Webmaster</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="webmaster" name="webmaster" value="{{$setting->webmaster }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Google Analitik</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="analytics" name="analytics" value="{{$setting->analytics }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Facebook Pixel</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="facebook pixel" name="facebook_pixel" value="{{$setting->facebook_pixel }}" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Copyright</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="copyright" name="copyright" 
                        value="{{$setting->copyright }}">
                    </div>
                </div>
                <hr>
                <h4 class="panel-title">Seo Tools Homepage</h4>
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta title" name="metatitle"   value="{{$setting->metatitle }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Description " name="metadescription" value="{{$setting->metadescription }}"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Keyword</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Keyword " name="metakeyword"  value="{{$setting->metakeyword }}"> 
                    </div>
                </div>
                <hr>
                <h4 class="panel-title">Seo Tools Page Blog</h4>
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta title" name="meta_title_blog"  value="{{$setting->meta_title_blog }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" >Meta Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Description " name="meta_desc_blog" value="{{$setting->meta_desc_blog }}" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Keyword</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Keyword " name="meta_keyword_blog" value="{{$setting->meta_keyword_blog }}">
                    </div>
                </div>
                <hr>
                <h4 class="panel-title">Seo Tools Page About</h4>
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta title" name="meta_title_about"  value="{{$setting->meta_title_about }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" >Meta Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Description " name="meta_desc_about" value="{{$setting->meta_desc_about }}" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Meta Keyword</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Meta Keyword " name="meta_keyword_about" value="{{$setting->meta_keyword_about }}">
                    </div>
                </div>
                <hr>
                <h4 class="panel-title">File</h4>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Logo</label>
                    <div class="col-sm-10">
                        <input type="file" placeholder="Meta Keyword " name="logo" accept="image/*" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Favicon</label>
                    <div class="col-sm-10">
                        <input type="file" placeholder="Meta Keyword " name="favicon" accept="image/*" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
                
                   </form>
                </div>
            </div>
        </div>
    </div>
  </div>

@endsection
