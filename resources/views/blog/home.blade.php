@extends('blog/app')
@section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Website Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia" />
<meta name="keywords" content="Pandu Asia,panduasia,travel indonesia ,travel bandung, travel bali,jasa tour guide murah,tour guide indonesia, cari jadi tour guide, tour guide job, pemandu wisata, pemandu wisata online, tour guide online, website tour guide, cari tour guide,  wisata bandung, tempat wisata bandung, tempat wisata bogor, tempat wisata bali, tempat wisata lombok, tempat wisata jakarta, wisata indonesia,tourism indonesia, all about tourism,tourism indonesian " />
@endsection

@section('main-content')

  <header id="gtco-header" role="banner">
			<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
        <!-- Overlay -->
        <div class="overlay"></div>
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#bs-carousel" data-slide-to="1"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item slides active">
                <div class="slide-1">
                    <img src="{{URL::asset('public/assets/blog/images/bali.jpg')}}" alt="bali" class="gtco-cover">
                </div>
                <div class="hero">
                    <hgroup>
                        <div class="main-bg-text">
                            <h1>
                                <strong class="heading-primary-main">Guide others to happiness</strong>
                            </h1>
                        </div>
                    </hgroup>
                </div>
            </div>
            <div class="item slides">
                <div class="slide-2">
                    <img src="{{URL::asset('public/assets/blog/images/coming.jpg')}}" alt="bali" class="gtco-cover">
                </div>
                {{--
                <div class="hero">
                    <hgroup>

                    </hgroup>
                    <button class="btn btn-hero btn-lg" role="button">See all features</button>
                </div> --}}
            </div>
        </div>
    </div>
	</header>
		<!-- END #gtco-header -->

		<div class="gtco-client" style="background-color:white">
			<div class="row row-pb-sm">
				<div class="col-md-4 col-md-offset-4">
					<form action="#" method="POST">
						<div class="form-group text-center">
							<label for="name">Where do you want to go ?</label>
							<input type="text" class="form-control " id="search" name="search" placeholder="City, place to go">
							<br>
							<a href="#" class="btn btn-info btn-lg">
								<i class="fa fa-search" aria-hidden="true"></i>Search
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="gtco-section gtco-products" style="background-color: white;">
			<div class="gtco-container">
				<div class="row row-pb-sm">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>Destinasi Kota</h2>
						<hr>
					</div>
				</div>
				    <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel1">
            <!-- Overlay -->
            <div class="overlay"></div>
            <!-- Indicators -->
            <ol class="carousel-indicators carouseldestinasi">
                <li data-target="#bs-carousel1" data-slide-to="0" class="active"></li>
                <li data-target="#bs-carousel1" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item slides active">
                    <div class="slide-1">
                        <div class="row row-pb-md">
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Bali') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/balisnork.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Bali</h3>
                                            <p>Sebuah pulau yang terdapat banyak sekali destinasi wisata internasional</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/balisnork.jpg')}}" class="hidden" alt="wisata bali">
                                </a>
                                <a href="{{ URL::to('wisata/Bandung') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/bandung.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Bandung</h3>
                                            <p>Bandung dikenal dengan paris van java nya Indonesia.</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/bandung.jpg')}}" class="hidden" alt="wisata bandung">
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Jakarta') }}" class="gtco-item one-row bg-img animate-box" style="background-image: url(public/assets/blog/images/monas.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Jakarta</h3>
                                            <p>Sebuah ikon Ibukota yang mempunyai banyak sejarah</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/monas.jpg')}}" class="hidden" alt="wisata jakarta">
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Manado') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/bunakan.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Bunaken</h3>
                                            <p>Keindahan</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/bunakan.jpg')}}" class="hidden" alt="wisata bunaken">
                                </a>
                                <a href="{{ url('wisata/Yogyakarta') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/yogyakarta.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Yogyakarta</h3>
                                            <p></p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/yogyakarta.jpg')}}" class="hidden" alt="wisata jogjakarta">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item slides">
                    <div class="slide-2">
                        <div class="row row-pb-md">
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Jakarta') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/pseribu.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Pulau Seribu</h3>
                                            <p>Keindahan di banyak pulau</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/pseribu.jpg')}}" class="hidden" alt="wisata pulau seribu">
                                </a>
                                <a href="{{ url('wisata/Jepara') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/jepara.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Jepara</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/jepara.jpg')}}" class="hidden" alt="wisata jepara">
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Samosir') }}" class="gtco-item one-row bg-img animate-box" style="background-image: url(public/assets/blog/images/toba.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Danau Toba</h3>
                                            <p></p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/toba.jpg')}}" class="hidden" alt="wisata toba">
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <a href="{{ url('wisata/Bogor') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/cikuluwung.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Bogor</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/cikuluwung.jpg')}}" class="hidden" alt="wisata di bogor">
                                </a>
                                <a href="{{ url('wisata/Pangandaran') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/greencanyon.jpg)">
                                    <div class="overlay">
                                        <i class="ti-arrow-top-right"></i>
                                        <div class="copy">
                                            <h3>Pangandaran</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <img src="{{URL::asset('public/assets/blog/images/greencanyon.jpg')}}" class="hidden" alt="Wisata di pangandaran">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- end courousel --}}
    </div>
</div>

<!-- END .gtco-products -->

		<div class="gtco-section gtco-products" style="background-color: white;">
			<div class="gtco-container">
				<div class="row row-pb-sm">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>Popular Destinations</h2>
						<p>Top 5 Destionations from domestic.</p>
						<hr>
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-4 col-sm-4">
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/tnh_lot.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									<h3>Bali</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							<img src="{{URL::asset('public/assets/blog/images/tnh_lot.jpg')}}" class="hidden" alt="wisata di bali">
						</a>
						<a href="{{ url('wisata/Bali') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/pseribu.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									<h3>Pulau Pramuka</h3>
									<p></p>
								</div>
							</div>
							<img src="{{URL::asset('public/assets/blog/images/pseribu.jpg')}}" class="hidden" alt="wisata pulau seribu">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="{{ url('wisata/Samosir') }}" class="gtco-item one-row bg-img animate-box" style="background-image: url(public/assets/blog/images/toba.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									<h3>Danau toba</h3>
									<p></p>
								</div>
							</div>
							<img src="{{URL::asset('public/assets/blog/images/toba.jpg')}}" class="hidden" alt="wisata danau toba">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="{{ url('wisata/Jepara') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/jepara.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									<h3>Karimun Jawa</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							<img src="{{URL::asset('public/assets/blog/images/jepara.jpg')}}" class="hidden" alt="wisata karimun jawa">
						</a>
						<a href="{{ url('wisata/Pangandaran') }}" class="gtco-item two-row bg-img animate-box" style="background-image: url(public/assets/blog/images/greencanyon.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									<h3>Green Canyon</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							<img src="{{URL::asset('public/assets/blog/images/greencanyon.jpg')}}" class="hidden" alt="wisata greencanyon">
						</a>
					</div>
					<div class="col-md-12 text-center">
						<p>
							<a href="" class="btn btn-special">Load More</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- END .gtco-products -->

		    <div class="gtco-section gtco-products" style="background-color: white;">
        <div class="gtco-container">
            <div class="row row-pb-sm">
                <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                    <h2>Artikel terbaru</h2>
                    <hr>
                </div>
                
                <div class="container">
                    <div class="row">
                        @foreach($posts as $post)
                            @if($post->pageataupost == "page")
                                <?php echo "" ?>
                            @else
                        <div class="col-md-4">
                            <div class="column">
                                <!-- Post-->
                                <div class="post-module">
                                    <!-- Thumbnail-->
                                    <div class="thumbnail">
                                        <div class="date">
                                            <div class="day">{{$post->created_at->format('d')}}</div>
                                            <div class="month">{{$post->created_at->format('M')}}</div>
                                        </div>
                                        <img src="{{ URL::asset('public/assets/images/'.$post->image)}}" alt="{{$post->title}}">
                                    </div>
                                    <!-- Post Content-->
                                    <div class="post-content">
                                        @foreach ($post->categories as $category)
                                        <div class="category">{{$category->name}}</div>
                                        @endforeach
                                        <h1 class="title">
                                            <a href="{{ url('blog', $post->slug) }}">{{$post->title}}</a>
                                        </h1>
                                        
                                        {{-- <p class="description">
                                            {!! str_limit($post->description,150) !!}
                                        </p> --}}
                                        <div class="post-meta">
                                            <span class="timestamp">
                                                <i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
                                                
                                                
                                                <span class="views">
                                                    <i class="fa fa-user"></i>
                                                    <a href="#"> {{$post->visitor}} Views</a>
                                                </span>
                                                <a href="{{ url('blog',$post->slug) }}" style="color:red;">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
							<div class="col-md-12 text-center">
							<p>
								<a href="{{ url('blog') }}" class="btn btn-special">Load More</a>
							</p>
						</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
