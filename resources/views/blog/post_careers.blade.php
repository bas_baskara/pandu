@extends('blog/app')
@section('head')
<meta name="title" content="{{$careers->title}}" />
<meta name="description" content="{{$careers->description}}" />
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($careers as $careers)		 --}}
	<div class="cardPostView">
		<img src="{{ URL::asset('public/assets/image_post/'.$careers->image)}}" alt="{{$careers->image}}" class="bannerpost">
		<div class="containerPostView">
			<div class="texturl">
				<a href="{{ url('home/')}}">Home /</a>
				<a href="{{ url('home/careers/')}}">Careers /</a>
				<a href="{{ url('home/careers/'.$careers->title)}}">{{$careers->title}}</a>

			</div>
			<br>
			<div class="post-content-view">
				<h1 class="title">{{$careers->title}}</h1>
				<h5 class="timetext">
					<ion-icon name="time" size="small"></ion-icon>{{$careers->created_at}}</h5>
					<p class="bodycontent">
						{!! htmlspecialchars_decode($careers->description) !!}
					</p>
				</div>
				<hr>
				<div class="foot">
					@foreach ($users as $user)
					@if ($careers->posted_by == $user['name'])
					<img src="{{ URL::asset('/avatar/'.$user->avatar)}}" alt="author" class="authorimg">
					@endif
					@endforeach
					<h5 style="padding-top:30px ; padding-left: 15px;">by
						<a href="">{{$careers->posted_by}}</a>
					</h5>

					<div class="row">
						<h5>Share :</h5>
						<ul>
							<a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class="">Facebook</a>
						</ul>
					</div>
				</div>
				<div>
					<center>
						<a href="{{route('usr_careers',$careers->slug)}}" class="btn btn-success">Apply</a>
					</center>
				</div>
			</div>
			<br>
		</div>
		{{-- @endforeach --}}
	</div>
</div>
</div>

@endsection
