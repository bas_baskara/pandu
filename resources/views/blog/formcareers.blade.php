@extends('blog/app')
@section('head')

<meta name="title" content="Panduasia Career | Informasi tentang program internship" />
<meta name="description" content="Temukan program magang untuk studi anda dan dapatkan pengalaman kerja professional bagi persiapan karir kamu. Mulai internship kamu bersama Panduasia." />
<meta name="keywords" content="Panduasia | Career " />

@endsection

@section('main-content')
    <br>
    <div class="container" style="background-color:white;">
        <h3>Personal Information</h3>
        <form role="form" action="{{ url('careers/store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
          {{ csrf_field() }}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name *</label>
                    <input class="form-control" name="name" type="text" placeholder="Nama panggilan." required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Full Name *</label>
                    <input class="form-control" name="fullname" type="text" placeholder="Nama lengkap." required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address *</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" placeholder="Enter email" required>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Phone *</label>
                    <input class="form-control" name="phone" type="text" placeholder="no telp." required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Graduated</label>
                    <input class="form-control" name="graduated" type="text" placeholder="Lulusan terakhir.">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Summary *</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleFormControlFile1"> file resume</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
                    <small  class="form-text text-muted">Accepted file formats: pdf, doc, docx (3 MB max)</small>
                </div>
            </div>
            <input type="submit" class="btn btn-outline-success" value="kirim">
        </form>
        <br>
    </div>

@endsection
