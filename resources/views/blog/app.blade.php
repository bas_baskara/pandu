<!DOCTYPE html>
<html lang="en">
<head>
  @include('blog/_layouts/head')
</head>
<body>

  @include('blog/_layouts/header')
        @section('main-content')
            @show

   @include('blog/_layouts/footer')

</body>
</html>
