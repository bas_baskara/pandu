@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Login" />
<meta name="description" content="Login panduasia" />
<meta name="keywords" content="Pandu Asia login, login panduasia" />
<link rel="stylesheet" href="{{URL::asset('blog/css/loginuser.css')}}">

@endsection

@section('main-content')

<div class="bg">

<div class="container">
  <div class="row">
    <div class="col-md-6" style="margin-bottom:50px;">
      <div class="account-box"  style="border-radius : 15px;">
        <div class="logo">
          <img src="http://localhost:8000/blog/images/logo.png" alt="panduasia"/>
        </div>
        <div class="login-text">
          <h2>Register</h2>
        </div>

        <form class="login-form" method="POST" action="{{ route('users.register') }}">
          @include('includes.messages')

          @csrf

          <div class="form-group">
            <input type="text" class="form-control" placeholder="Email"  name="email"/>
          </div>

              <div class="col-sm-6">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="Nama Depan"  name="namadepan"/>
                  </div>
              </div>

              <div class="col-sm-6">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="Nama Belakang"  name="namabelakang"/>
                  </div>

              </div>

          <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="password" required />
          </div>

          <div class="form-group">
              <input type="text" class="form-control" placeholder="Address" name="address" required />
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Phone" name="phone" required />
              </div>

              <div class="form-group">
                <label for="dataofbirth" style="font-size:16px;" >Date of birth</label>
                  <input type="date" class="form-control" name="date_of_birth" required/>
              </div>


          <label class="clickcheckbox">
              <center>
                  <p class="small">Dengan mengklik Daftar, Anda menyetujui <a href="{{URL('/home/about/syaratketentuan')}}">syarat dan ketentuan</a> dan <a href="{{URL('/home/about/kebijakanpribadi')}}">Kebijakan pribadi kami.</a> </p>
              </center>
          </label>

          <button class="btn btn-lg btn-block purple-bg" type="submit">
            Create Account</button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>


  @endsection
