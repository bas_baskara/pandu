@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia"
/>
<meta name="keywords" content="Pandu Asia" /> @endsection @section('main-content')

<header role="banner" style="display:block;background-color:white;color:white;">
    <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="false" id="bs-carousel">
        <!-- Overlay -->
        <div class="overlay"></div>
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#bs-carousel" data-slide-to="1"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item slides active">
                <div class="slide-1">
                    <img src="{{URL::asset('blog/images/bali.jpg')}}" alt="bali" class="bannerpost" role="banner">
                </div>
                
                
                <div class="main-bg-text">
                    <h1>
                        <strong class="heading-primary-main">Guide others to happiness</strong>
                    </h1>
                </div>
                
                
            </div>
            <div class="item slides">
                <div class="slide-2">
                    <img src="{{URL::asset('blog/images/coming.jpg')}}" alt="bali" class="bannerpost" role="banner">
                </div>
                {{--
                    <div class="hero">
                        <hgroup>
                            
                        </hgroup>
                        <button class="btn btn-hero btn-lg" role="button">See all features</button>
                    </div> --}}
                </div>
            </div>
        </div>
    </header>
    <!-- END #gtco-header -->
    
    <div class="gtco-client" style="background-color:white">
        <div class="row row-pb-sm">
            <div class="col-md-4 col-md-offset-4">
                <form action="#">
                    <div class="form-group text-center">
                        <label for="name">Where do you want to go ?</label>
                        <input type="text" class="form-control " id="search" name="search" placeholder="City, place to go">
                        <br>
                        <a href="#" class="btn btn-info btn-lg">
                            <i class="fa fa-search" aria-hidden="true"></i>Search
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="gtco-section gtco-products" style="background-color: white;">
        <div class="gtco-container">
            <div class="row row-pb-sm">
                <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                    <h2>Destinasi Kota</h2>
                    <hr>
                </div>
            </div>
            
            <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel1">
                <!-- Overlay -->
                <div class="overlay"></div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#bs-carousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#bs-carousel1" data-slide-to="1"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item slides active">
                        <div class="slide-1">
                            <div class="row row-pb-md">
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/balisnork.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Bali</h3>
                                                <p>Sebuah pulau yang terdapat banyak sekali destinasi wisata internasional</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/balisnork.jpg')}}" class="hidden" alt="wisata bali">
                                    </a>
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/bandung.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Bandung</h3>
                                                <p>Bandung dikenal dengan paris van java nya Indonesia.</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/bandung.jpg')}}" class="hidden" alt="wisata bandung">
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item one-row bg-img animate-box" style="background-image: url(blog/images/monas.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Jakarta</h3>
                                                <p>Sebuah ikon Ibukota yang mempunyai banyak sejarah</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/monas.jpg')}}" class="hidden" alt="wisata jakarta">
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/bunakan.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Bunaken</h3>
                                                <p>Keindahan</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/bunakan.jpg')}}" class="hidden" alt="wisata bunaken">
                                    </a>
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/yogyakarta.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Yogyakarta</h3>
                                                <p></p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/yogyakarta.jpg')}}" class="hidden" alt="wisata jogjakarta">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item slides">
                        <div class="slide-2">
                            <div class="row row-pb-md">
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/pseribu.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Pulau Seribu</h3>
                                                <p>Keindahan di banyak pulau</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/pseribu.jpg')}}" class="hidden" alt="wisata pulau seribu">
                                    </a>
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/jepara.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Jepara</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            </div>
                                        </div>
                                        <img src="{{('blog/images/jepara.jpg')}}" class="hidden" alt="wisata jepara">
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item one-row bg-img animate-box" style="background-image: url(blog/images/toba.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Danau Toba</h3>
                                                <p></p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/toba.jpg')}}" class="hidden" alt="wisata toba">
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/cikuluwung.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Bogor</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/cikuluwung.jpg')}}" class="hidden" alt="wisata di bogor">
                                    </a>
                                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/greencanyon.jpg)">
                                        <div class="overlay">
                                            <i class="ti-arrow-top-right"></i>
                                            <div class="copy">
                                                <h3>Pangandaran</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            </div>
                                        </div>
                                        <img src="{{URL::asset('blog/images/greencanyon.jpg')}}" class="hidden" alt="Wisata di pangandaran">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- end courousel --}}
        </div>
    </div>
    <!-- END .gtco-products -->
    
    <div class="gtco-section gtco-products" style="background-color: white;">
        <div class="gtco-container">
            <div class="row row-pb-sm">
                <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                    <h2>Popular Destinations</h2>
                    <p>Top 5 Destionations from domestic.</p>
                    <hr>
                </div>
            </div>
            <div class="row row-pb-md">
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/tnh_lot.jpg)">
                        <div class="overlay">
                            <i class="ti-arrow-top-right"></i>
                            <div class="copy">
                                <h3>Bali</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <img src="{{URL::asset('blog/images/tnh_lot.jpg')}}" class="hidden" alt="wisata di bali">
                    </a>
                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/pseribu.jpg)">
                        <div class="overlay">
                            <i class="ti-arrow-top-right"></i>
                            <div class="copy">
                                <h3>Pulau Pramuka</h3>
                                <p></p>
                            </div>
                        </div>
                        <img src="{{URL::asset('blog/images/pseribu.jpg')}}" class="hidden" alt="wisata pulau seribu">
                    </a>
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="gtco-item one-row bg-img animate-box" style="background-image: url(blog/images/toba.jpg)">
                        <div class="overlay">
                            <i class="ti-arrow-top-right"></i>
                            <div class="copy">
                                <h3>Danau toba</h3>
                                <p></p>
                            </div>
                        </div>
                        <img src="{{URL::asset('blog/images/toba.jpg')}}" class="hidden" alt="wisata danau toba">
                    </a>
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/jepara.jpg)">
                        <div class="overlay">
                            <i class="ti-arrow-top-right"></i>
                            <div class="copy">
                                <h3>Karimun Jawa</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <img src="{{URL::asset('blog/images/jepara.jpg')}}" class="hidden" alt="wisata karimun jawa">
                    </a>
                    <a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(blog/images/greencanyon.jpg)">
                        <div class="overlay">
                            <i class="ti-arrow-top-right"></i>
                            <div class="copy">
                                <h3>Green Canyon</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <img src="{{URL::asset('blog/images/greencanyon.jpg')}}" class="hidden" alt="wisata greencanyon">
                    </a>
                </div>
                <div class="col-md-12 text-center">
                    <p>
                        <a href="" class="btn btn-special">Load More</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- END .gtco-products -->
    
    <div class="gtco-section gtco-products" style="background-color: white;">
        <div class="gtco-container">
            <div class="row row-pb-sm">
                <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                    <h2>Artikel terbaru</h2>
                    <hr>
                </div>
                
                <div class="container">
                    <div class="row">
                        @foreach($posts as $post) @if($post->status == "pending")
                        <?php echo " " ?> @else
                        <div class="col-md-4">
                            <div class="column">
                                <!-- Post-->
                                <div class="post-module">
                                    <!-- Thumbnail-->
                                    <div class="thumbnail">
                                        <div class="date">
                                            <div class="day">{{$post->created_at->format('d')}}</div>
                                            <div class="month">{{$post->created_at->format('F')}}</div>
                                        </div>
                                        <img src="{{ URL::asset('image_post/'.$post->image)}}" alt="{{$post->title}}">
                                    </div>
                                    <!-- Post Content-->
                                    <div class="post-content">
                                        @foreach ($post->categories as $category)
                                        <div class="category">{{$category->name}}</div>
                                        @endforeach
                                        <h1 class="title">
                                            <a href="{{route('posts',$post->slug)}}">{{$post->title}}
                                            </a>
                                        </h1>
                                        <p class="description">
                                            {!! htmlspecialchars_decode(str_limit($post->description,85)) !!}
                                        </p>
                                        <div class="post-meta">
                                            <span class="timestamp">
                                                <i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
                                                
                                                
                                                <span class="views">
                                                    <i class="fa fa-user"></i>
                                                    <a href="#"> {{$post->visitor}} Views</a>
                                                </span>
                                                <a href="{{route('posts',$post->slug)}}" style="color:red;">Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <p>
                            <a href="{{URL::to('home/blog')}}" class="btn btn-special">Load More</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="gtco-section gtco-products" style="background-color: white;">
            <div class="gtco-container">
                <div class="row row-pb-sm">
                    <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                        <h2>Artikel komunitas terbaru</h2>
                        <hr>
                    </div>
                    
                    <div class="container">
                        <div class="row">
                            @foreach($data as $datas) @if($datas->status == 0)
                            <?php echo " " ?> @else
                            <aside class="col-sm-4" style="padding-top:30px;">
                                <div class="card-banner-komunitas align-items-end">
                                    <img src="{{ URL::asset('komunitas_artikel/'.$datas->image)}}" alt="{{$datas->title}}">
                                    
                                    <article class="overlay-komunitas overlay-cover d-flex align-items-center justify-content-center">
                                        <h5 class="card-title">{{$datas->title}}</h5>
                                        <a href="{{route('postskomunitas',$datas->slug)}}" class="btn btn-info btn-sm"> Selengkapnya </a>
                                    </article>
                                </div>
                            </aside>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <br> 
        @endsection
        