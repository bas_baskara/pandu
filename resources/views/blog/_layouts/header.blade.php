<div class="gtco-loader"></div>

<div id="page">

  <nav class="gtco-nav" role="navigation">
    <div class="gtco-container">

      <div class="row">
        <div class="col-sm-2 col-xs-12">
          <div id="gtco-logo">
            <a href="{{URL::to('/')}}">
              <img src="{{URL::asset('public/assets/blog/images/logo.png')}}" alt="logo panduasia">
            </a>
          </div>
        </div>
        <div class="col-xs-10 text-right menu-1"  style="margin-top:15px;">
          <ul>
            <li class="active">
              <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="has-dropdown">
              <a href="javascript:;">Cara kerja</a>
              <ul class="dropdown">
                <li><a href="{{ URL::to('p/panduan-menjadi-member-komunitas-di-panduasia') }}">Panduan Member</a></li>
                <li><a href="{{ URL::to('p/panduan-mencari-tour-guide-di-panduasia') }}">Panduan Mencari Tour Guide</a></li>
                <li><a href="{{ URL::to('p/panduan-menjadi-tour-guide-di-panduasia') }}">Panduan Menjadi Tour Guide</a></li>
                <?php /*<li><a href="{{ URL::to('p/panduan-menjadi-ketua-komunitas-di-panduasia') }}">Panduan Ketua Komunitas</a></li>*/?>
              </ul>
            </li>
            <li>
              <a href="{{URL::to('/blog')}}">Blog</a>
            </li>
            <li>
              <a href="#" data-toggle="modal" data-target="#komunitas">Komunitas</a>
            </li>
            <li>
              <a href="#" data-toggle="modal" data-target="#tourguide">Be a Tourguide</a>
            </li>
            <li>
              <a href="{{URL::to('/about')}}">About</a>
            </li>
            <li class="has-dropdown">
              <a href="javascript:;">Masuk</a>
              <ul class="dropdown">
                <li>
                  <a href="javascript:;">Login</a>
                  <a href="javascript:;">Register</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- Modal -->
<div class="modal fade" id="komunitas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Fitur Komunitas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: relative;top: -20px;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <h1>Fitur Komunitas Masih Dalam Tahap Pengembangan</h1>
        <br/>
        <h1>Terimakasih</h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tourguide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Fitur Tour Guide</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: relative;top: -20px;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <h1>Fitur Guide Masih Dalam Tahap Pengembangan</h1>
        <br/>
        <h1>Terimakasih</h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@section('header')
@show
