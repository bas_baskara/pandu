@extends('blog/app')
@section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik" />
<meta name="keywords" content="Pandu Asia Blog" />
@endsection

@section('main-content')

<header id="gtco-header" role="banner">
			<img src="{{URL::asset('public/assets/blog/images/blog.jpg')}}" alt="panduasia blog" class="gtco-cover">
</header>
		<!-- END #gtco-header -->

<div class="container">
	<div class="row">
		<div class="gtco-services gtco-section">
		  <div class="gtco-heading text-center">
			<h2>Pages List</h2>
			<br>
		  </div>
		</div>
	</div>

    <div class="row">
      @foreach($pages as $post)
      @if($post->pageataupost == "post")
      <?php echo " " ?>
      @else
      <div class="col-md-4">
        <div class="column">
          <!-- Post-->
          <div class="post-module">
            <!-- Thumbnail-->
            <div class="thumbnail">
              <div class="date">
                <div class="day">{{$post->created_at->format('d')}}</div>
                <div class="month">{{$post->created_at->format('M')}}</div>
              </div>
              @if(empty($post->image) || $post->image == "null")
              <img src="{{ URL::asset('public/assets/avatar/default.png')}}" alt="{{$post->title}}">
              @else
              <img src="{{ URL::asset('public/assets/images/'.$post->image)}}" alt="{{$post->title}}">
              @endif
            </div>
            <!-- Post Content-->
            <div class="post-content">
              @foreach ($post->categories as $category)
              <div class="category">{{$category->name}}</div>
              @endforeach
              <h1 class="title">
                <a href="{{ url('p/'.$post->slug) }}">{{$post->title}}</a>
              </h1>
                
               {{--  <p class="description">
                  {!! htmlspecialchars_decode(str_limit($post->description,150)) !!}
                </p> --}}
                <div class="post-meta">
                  <span class="timestamp">
                    <i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
                    
                    
                    <span class="views">
                      <i class="fa fa-user"></i>
                      <a href="#"> {{$post->visitor}} Views</a>
                    </span>
                    <a href="{{ url('p/'.$post->slug)}}" style="color:red;">Selengkapnya</a>
                  </div>
                </div>
              </div>
            </div>
		</div>
		@endif
		@endforeach
    </div>
    
	<!-- Pager -->
	<div style="text-align: center;">
		<!--<div class="next">-->
		{{-- {{$page->links()}}	             --}}
		<!--</div>-->
    </div>

    </div>

  </div>

</div>

@endsection
