@extends('blog.app')
@section('head')
<!-- cardourteam -->
    <link rel="stylesheet" href="{{URL::asset('public/assets/blog/css/card-ourteam.css')}}">

@endsection
@section('main-content')

<header id="gtco-header" class="gtco-cover gtco-cover-xs gtco-inner" role="banner">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-12 col-md-offset-0 text-left">
        <div class="display-t">
          <div class="display-tc">
            <div class="row">
              <div class="col-md-8">
                <h1 class="no-margin">Panduasia</h1>
                <p>Panduasia membantu para traveller dalam mencari tempat destinasi wisata yang menarik di berbagai wilayah di Indonesia
                  dengan cara mempertemukan traveller dengan para pemandu / tourguide yang berkualitas dari berbagai wilayah dan
                  komunitas.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- END #gtco-header -->

<!-- END #gtco-header -->
<div class="gtco-services gtco-section" style="background-color: white;">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <img src="{{URL::asset('public/assets/blog/images/about.jpg')}}" alt="image_about" width="250px" height="250px">
        <h2>Tentang Panduasia</h2>
        <p>Platform marketplace di bidang wisata terdepan di Indonesia, panduasia, membantu para pemandu dari berbagai wilayah,
          komunitas dan traveller untuk berpartisipasi dalam pertumbuhan pariwisata di indonesia.
          <b>Visi Panduasia adalah meningkatkan dunia pariwisata di indonesia dengan menciptakan koneksi antara para traveller
            dengan para pemandu wisata dan komunitasnya.</b>
          Panduasia percaya dengan adanya marketplace di bidang wisata ini dapat menumbuhkan perekonomian para pemandu dari berbagai
          wilayah dan menciptakan komunitas-komunitas yang aktif berperan di dalam meningkatkan industri pariwisata di Indonesia
        </p>
      </div>
    </div>
    <div class="row row-pb-md">
      <div class="row">
        <div class="service ">
          <img src="{{URL::asset('public/assets/blog/images/panduasia.png')}}" alt="logo_panduasia" width="390px" height="150px">
          <p class="gtco-heading text-center">Panduasia yang berdiri pada tanggal <b> 30 April 2018</b> memiliki komitmen yang kuat untuk membangun dunia pariwisata yang attraktif, menakjubkan, menarik, indah, dan menjadikan moment yang tak terlupakan bagi para traveller dengan dibantu oleh para pemandu wisata yang ramah, berpengetahuan dan berpengalaman di berbagai destinasi wisata.
            </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="gtco-services gtco-section" style="background-color: white;">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Misi Panduasia</h2>
        <p>Panduasia berusaha mewujudkan misi pemberdayaan para pemandu beserta komunitasnya untuk berpartisipasi meningkatkan
          industri pariwisata di Indonesia. Dalam rangka menumbuhkan misi besar ini, panduasia mengajak para pemandu/tour guide,
          para komunitas dan para talenta-talenta terbaik yang ingin bergabung bersama kami, siapapun dan dimanapun untuk menggapai
          mimpi dan cita-cita.
        </p>
      </div>
      <div class="row row-pb-md" >
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3 >
              <i class="ti-pie-chart"></i>Join Tour Guide</h3>
            <p style="color: #2f2a2a;"> Join sebagai tour guide bersama panduasia.</p>
            <p style="color: #2f2a2a;">klik register
              <a href="">Tour Guide</a>
            </p>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3>
              <i class="ti-ruler-pencil"></i>Komunitas</h3>
            <p style="color: #2f2a2a;"> Mari ciptakan komunitas-komunitas yang dapat meningkatkan dunia industri pariwisata di Indonesia
            klik register komunitas<a href=""> disini</a></p>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 service-wrap" >
          <div class="service" style="background-color: #d7edef;">
            <h3>
              <i class="ti-settings"></i>Program Internship</h3>
            <p style="color: #2f2a2a;">Ingin belajar banyak tentang Dunia Teknologi? gabung bersama Panduasia.</p>
            <p style="color: #2f2a2a;">
              klik program
               <a href="{{URL::to('/careers')}}">Internship</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="gtco-services gtco-section">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2 class="description">Our Team</h2>

      </div>
    </div>

    <section class="team">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="col-lg-12">

              <div class="row pt-md">
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto1.jpg')}}" class="img-responsive">
                    <ul class="text-center">
                    </ul>
                    <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                    <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                  </div>
                  <h1>Abu Tholib</h1>
                  <h2>C E O - Founder </h2>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto2.jpg')}}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h1>Ilham Nur Hakim</h1>
                  <h2>Fullstack Developer</h2>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto3.jpg')}}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h1>Danar Winanda</h1>
                  <h2>Back-end Developer</h2>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                </div>
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto4.jpeg')}}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h1>Dina Nisa</h1>
                  <h2>Digital Marketing</h2>
                </div>
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto6.jpeg')}}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h1>Saugi</h1>
                  <h2>Vlog</h2>
                </div>
                <div class="col-6 col-md-4 col-xs-12 profile">
                  <div class="img-box">
                    <img src="{{URL::asset('public/assets/blog/images/foto5.jpeg')}}" class="img-responsive">
                    <ul class="text-center">
                      <!-- <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                      <a href="#"><li><i class="fa fa-linkedin"></i></li></a> -->
                    </ul>
                  </div>
                  <h1>Faisal</h1>
                  <h2>Finance & Accounting</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



  </div>
</div>
<!-- END .gtco-services -->

<div id="map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d991.4575726173897!2d106.8498052291678!3d-6.286026299715641!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f26fad5aaba1%3A0xb912dda63b14fff0!2sJl.+Kayu+Manis+No.17%2C+Balekambang%2C+Kramatjati%2C+Kota+Jakarta+Timur%2C+Daerah+Khusus+Ibukota+Jakarta+13530!5e0!3m2!1sen!2sid!4v1526653689537"
    width="1349px" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
@endsection
