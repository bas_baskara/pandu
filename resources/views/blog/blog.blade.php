@extends('blog/app')
@section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik" />
<meta name="keywords" content="Pandu Asia Blog" />
@endsection

@section('main-content')

<header id="gtco-header" role="banner">
	<img src="{{URL::asset('public/assets/blog/images/blog.jpg')}}" alt="panduasia blog" class="gtco-cover">
</header>
<!-- END #gtco-header -->

<div class="container">
	<div class="row">
		<div class="gtco-services gtco-section">
			<div class="gtco-heading text-center">
				<h2>New Articles</h2>
				<br>
				<form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
					<div class="search-wrapper">
						<div class="input-holder">
							<input type="text" class="search-input" name="keyword" id="keyword" placeholder="Cari artikel menarik disini ..." />
							<div id="articlelist"></div>
						</div>
						{{ csrf_field() }}
					</div>
				</form>
				<h4>Search By kategori : </h4>

				<div class="category-button col-md-12">
					@foreach($categories as $category)		
					<a href="{{ url('blog/category/'.$category->slug)}}"  class="flex-item btn btn-info" >{{$category->name}}</a>
					@endforeach

			<!-- {{-- <a class="btn btn-info" href="#">
			  <ion-icon name="help-buoy" size="small"></ion-icon>
			  <br> Pantai</a>
			<a class="btn btn-primary" href="#">
			  <ion-icon name="reverse-camera" size="small"></ion-icon>
			  <br>Rekreasi</a>
			<a class="btn btn-success" href="#">
			  <ion-icon name="photos" size="small"></ion-icon>
			  <br> Pegunungan</a>
			<a class="btn btn-warning" href="#">
			  <ion-icon name="body" size="small"></ion-icon>
			  <br> Budaya</a>
			<a class="btn btn-danger" href="#">
			  <ion-icon name="bulb" size="small"></ion-icon>
			  <br> Tips</a> --}} -->
			  <a class="btn btn-dark flex-item" href="">All</a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	@foreach($posts as $post)
	@if($post->pageataupost == "page")
	<?php echo " " ?>
	@else
	<div class="col-md-4">
		<div class="column">
			<!-- Post-->
			<div class="post-module">
				<!-- Thumbnail-->
				<div class="thumbnail">
					<div class="date">
						<div class="day">{{$post->created_at->format('d')}}</div>
						<div class="month">{{$post->created_at->format('M')}}</div>
					</div>

					@if(empty($post->image) || $post->image == "null")
					<img src="{{ URL::asset('public/assets/avatar/default.png')}}" alt="{{$post->title}}">
					@else
					<img src="{{ URL::asset('public/assets/images/'.$post->image)}}" alt="{{$post->title}}">
					@endif

				</div>
				<!-- Post Content-->
				<div class="post-content">
					@foreach ($post->categories as $category)
					<div class="category">{{$category->name}}</div>
					@endforeach

					<h1 class="title">
						<a href="{{ url('blog/'.$post->slug) }}">{{$post->title}}</a>
					</h1>
					
					{{-- <p class="description">
						{!! htmlspecialchars_decode(str_limit($post->description,150)) !!}
					</p> --}}
					<div class="post-meta">
						<span class="timestamp">
							<i class="fa fa-clock-"></i>{{ $post->created_at->diffForHumans() }}</span>
							
							<span class="views">
								<i class="fa fa-user"></i>
								<a href="#"> {{$post->visitor}} Views</a>
							</span>
							<a href="{{ url('blog/'.$post->slug) }}" style="color:red;">Selengkapnya</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		@endforeach
	</div>
	
	<!-- Pager -->
	<div style="text-align: center;">
		<!--<div class="next">-->
			{{ $posts->links() }}	            
			<!--</div>-->
		</div>

	</div>

</div>

</div>
<script>
	$(document).ready(function(){
		$('#keyword').keyup(function(){
			var query = $(this).val();
			if(query != ''){
				var _token = $('input[name="_token"]').val();
				$.ajax({
					url:"{{ route('autosuggest.fetch') }}",
					method:"POST",
					data:{query:query, _token:_token},
					success:function(data){
						$('#articlelist').fadeIn();
						$('#articlelist').html(data);
					}
				})
			}
		});
		$(document).on('click','li',function(){
			$('#keyword').val($(this).text());
			$('#articlelist').fadeOut();
		});
	});
</script>

@endsection
