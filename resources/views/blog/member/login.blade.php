@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia Login" />
<meta name="description" content="Login panduasia" />
<meta name="keywords" content="Pandu Asia login, login panduasia" />
<link rel="stylesheet" href="{{URL::asset('www/assets/css/loginuser.css')}}">

@endsection

@section('main-content')

<div class="bg">

<div class="container">
  <div class="row">
    <div class="col-md-4" style="margin-bottom:50px;">
      <div class="account-box"  style="border-radius : 15px;">
        <div class="logo">
         <img src="{{URL::asset('www/assets/images/logo.png')}}" alt="panduasia"/>
        </div>
        <div class="login-text">
          <h2>Login Members</h2>
        </div>

        <form class="login-form" method="POST" action="{{ route('members.login') }}">
          @include('includes.messages')

          @csrf
          @if(session()->has('login_error'))
          <div class="alert alert-success">
              {{ session()->get('login_error') }}
          </div>
          @endif

          <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }}">
            <input type="text" class="form-control" placeholder="Email"  name="email"/>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }}">
            <input type="password" class="form-control" placeholder="Password" name="password" required />
            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>

          <button class="btn btn-lg btn-block purple-bg" type="submit">
            Masuk</button>

        </form>
        <br>
            <a href="">lupa password</a>
        <div class="or-box row-block">
            <div class="row">
                <div class="col-md-12 row-block">
                    <a href="{{URL::to('/register-page')}}" class="btn btn-primary btn-block">Buat akun baru</a>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>


  @endsection
