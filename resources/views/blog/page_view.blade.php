@extends('blog/app')
@section('head')

	<meta name="title" content="{{$result->metatitle}}" />
	<meta name="description" content="{{ $result->metadescription }}" />
	<meta name="keywords" content="{{$result->metakeyword}}" />
	
	
	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="{{$result->title}}" />
	<meta property="og:image" content="{{ URL::asset('public/assets/images/'.$result->image)}}" />
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="panduasia.com" />
	<meta property="og:description" content="{{$result->description}}" />
	<meta name="twitter:title" content="{{$result->title}}" />
	<meta name="twitter:image" content="{{ URL::asset('public/assets/images/'.$result->image)}}" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	
@endsection
@section('main-content')

<div class="containerPostView">
	{{-- @foreach ($page as $post) --}}
	<div class="cardPostView">
		@if(empty($result->image) || $result->image == "null")
		<img src="{{ URL::asset('public/assets/avatar/default.png')}}" alt="{{$result->title}}" style="width: 100%">
        @else
		<img src="{{ URL::asset('public/assets/images/'.$result->image)}}" alt="{{$result->title}}" class="bannerpost">
		@endif

		<div class="containerPostView">

			<br>
			<div class="post-content-view">
				<h1 class="title">{{$result->title}}</h1>

					{{-- <p class="bodycontent"> --}}
						{!! htmlspecialchars_decode($result->description) !!}
						{{-- </p> --}}
				</div>
				<hr>
				<div class="foot">
					<div class="col-sm-8">
						@foreach ($users as $user)
						@if ($result->posted_by == $user['name'])
						<img src="{{ URL::asset('public/assets/avatar/'.$user->avatar)}}" alt="author" class="authorimg">
						@endif
						@endforeach
						
						<p>by {{$result->posted_by}}</p>
					</div>
					
					<div class="col-sm-4">
						<p>Share :</p>
						<ul class="social-share">
							<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-facebook"></ion-icon></a></li>
							<li><a href="https://plus.google.com/share?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-googleplus"></ion-icon></a></li>
							<li><a href="https://twitter.com/intent/tweet?url={{ rawurlencode(Request::fullUrl()) }}" class=""><ion-icon name="logo-twitter"></ion-icon></a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		{{-- @endforeach --}}

		<div class="commentBox">
			<div class="fb-comments" data-href="{{ Request::url() }}" data-numpage="10" data-width="100%"></div>
			</div>
			
		<br>
{{-- 		<div class="container">
        <div class="row"> --}}
        <div class="col-sm-6">
		@if(isset($previous))
		<a href="{{ url('/blog/'.$previous->slug) }}" class="btn btn-success pull-left post-nav">Previous</a>
		@endif
		</div>
		<div class="col-sm-6">
		@if(isset($next))
		<a href="{{ url('/blog/'.$next->slug) }}" class="btn btn-success pull-right post-nav">Next</a>
		@endif
		{{-- </div>
		</div> --}}
		</div>
	</div>
</div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=214610849349025&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection
