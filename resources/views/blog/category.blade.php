@extends('blog/app')
@section('head')

<meta name="title" content="Pandu Asia Blog | Tips & Informasi tentang wisata" />
<meta name="description" content="Informasi cerita terbaru yang menarik dan tips para traveller terkait destinasi wisata terbaik" />
<meta name="keywords" content="Pandu Asia Blog" />
@endsection

@section('main-content')

<header id="gtco-header" class="gtco-cover gtco-cover-xs gtco-inner" role="banner">
  <div class="gtco-container">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
  </div>
</header>
<!-- END #gtco-header -->

<div class="gtco-services gtco-section">
  <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
    <h2>New Articles</h2>
    <br>
    <form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
      <div class="search-wrapper">
        <div class="input-holder">
            <input type="text" class="search-input" name="keyword" placeholder="Cari artikel menarik disini ..." />
            <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
              <span></span>
            </button>
        </div>
        <span class="close" onclick="searchToggle(this, event);"></span>
        <div class="result-container">
        </div>
      </div>
    </form>
    <h4>Search By kategori : </h4>

    {{-- @foreach($categories as $category)		
      <a href="{{route('category',$category->slug)}}"  class="btn btn-info" >{{$category->name}}</a>
		@endforeach --}}

    {{-- <a class="btn btn-info" href="#">
      <ion-icon name="help-buoy" size="small"></ion-icon>
      <br> Pantai</a>
    <a class="btn btn-primary" href="#">
      <ion-icon name="reverse-camera" size="small"></ion-icon>
      <br>Rekreasi</a>
    <a class="btn btn-success" href="#">
      <ion-icon name="photos" size="small"></ion-icon>
      <br> Pegunungan</a>
    <a class="btn btn-warning" href="#">
      <ion-icon name="body" size="small"></ion-icon>
      <br> Budaya</a>
    <a class="btn btn-danger" href="#">
      <ion-icon name="bulb" size="small"></ion-icon>
      <br> Tips</a> --}}
    <a class="btn btn-dark" href="{{URL::to('/home/blog')}}">
      <ion-icon name="apps" size="small"></ion-icon>
      <br> All Post</a>
  </div>

  <div class="container">

    <div class="row">

      <div class="col-md-3">
          @foreach($posts as $post)
          @if($post->status == "pending")
            <?php echo " " ?>
          @else
        <div class="column">
          <!-- Post-->
          <div class="post-module">
            <!-- Thumbnail-->
            <div class="thumbnail">
              <div class="date">
                <div class="day">27</div>
                <div class="month">Mar</div>
              </div>
              <img src="{{ URL::asset('public/assets/image_post/'.$post->image)}}" alt="">
            </div>
            <!-- Post Content-->
            <div class="post-content">
                @foreach ($post->categories as $category)
              <div class="category">{{$category->name}}</div>
              @endforeach
              <h1 class="title">
                <a href="{{route('posts',$post->slug)}}">{{$post->title,50}} </a> 
              </h1>
              <h2 class="sub_title">{{$post->subtitle}}</h2>
              <p class="description">
                {!! htmlspecialchars_decode(str_limit($post->description,10)) !!}
              </p>
              <div class="post-meta">
                <span class="timestamp">
                   <i class="fa fa-clock-"></i> {{$post->created_at}} | {{ $post->created_at->diffForHumans() }}
                </span>
                <br>
                <span class="views">
                  <i class="fa fa-user"></i>
                  <a href="#"> {{$post->visitor}} Views</a>
                </span>
                <a href="{{route('posts',$post->slug)}}" style="color:red;">Selengkapnya</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      @endforeach
      <!-- Pager -->
		<ul class="pager">
			<li class="next">
				{{$posts->links()}}	            
			</li>
    </ul>
     
    </div>

  </div>

</div>

@endsection
