
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// window.Fire = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('kirim-pesan', require('./components/SendMessage.vue').default);
Vue.component('terima-pesan', require('./components/ReceiveMessage.vue').default);
Vue.component('chat-dari-customer', require('./components/GuideReceiveChat.vue').default);
Vue.component('chat-ke-customer', require('./components/GuideSendChat.vue').default);
Vue.component('chat-group-receive', require('./components/ChatGroupReceive.vue').default);
Vue.component('chat-group-send', require('./components/ChatGroupSend.vue').default);
// Vue.component('Chat', require('./components/Chat.vue'));
// Vue.component('PrivateChat', require('./components/PrivateChat.vue'));

const app = new Vue({
    el: '#app',
     data: {
        messagesforCustomer: [],    
        messagesforGuide: [],
        messageforGroup: []
    },

    created() {
        if(typeof memberId !== 'undefined'){
            this.fetchMessagesForCustomer();
            Echo.channel('toguide')
            .listen('SendMessage', (e) => {
                    this.messagesforCustomer.push({
                        message: e.message.message,
                        sender: e.message.sender,
                        member_id: e.message.member_id,
                        customer_id: e.message.customer_id
                    });
            });
        }else{
            this.fetchMessagesForGuide();
            Echo.channel('toguide')
            .listen('SendMessage', (e) => {
                    this.messagesforGuide.push({
                        message: e.message.message,
                        sender: e.message.sender,
                        member_id: e.message.member_id,
                        customer_id: e.message.customer_id
                    });
            });
        }
            if(window.location.pathname == '/community/chat'){
            this.getMessageGroup();
            Echo.channel('chatgroup')
                .listen('GroupChatSend', (e) => {
                   this.messageforGroup.push({
                    message: e.chatGroup.message,
                    community_id: e.chatGroup.community_id,
                    sender: e.chatGroup.sender
                    });
               });
            }
       
    },

    methods: {
        fetchMessagesForCustomer() {
            axios.get('/chat/messages/customer/'+memberId).then(response => {
                this.messagesforCustomer = response.data;
            });
        },
        fetchMessagesForGuide() {
            axios.get('/chat/messages/guide').then(response => {
                this.messagesforGuide = response.data;
            });
        },

        addMessage(message) {
            if(typeof memberId !== 'undefined'){
                this.messagesforCustomer.push(message);                
            }else{
                this.messagesforGuide.push(message);
            }
            axios.post('/chat/messages', message).then(response => {
            });
        },

        addMessageGroup(message){
            this.messageforGroup.push(message);
            axios.post('/community/chat/message', message).then(response => {
            });

        },

        getMessageGroup()
        {
                axios.get('/community/chat/message').then(response => {
                this.messageforGroup = response.data;
                console.log(response.data);
                });
        }

    }
});
