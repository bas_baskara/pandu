-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2018 at 02:44 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lchat`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `receiver_id`, `created_at`, `updated_at`) VALUES
(27, 1, 'hello', NULL, '2018-10-26 23:06:55', '2018-10-26 23:06:55'),
(28, 1, 'hello bro', NULL, '2018-10-26 23:12:46', '2018-10-26 23:12:46'),
(29, 1, 'heii man', NULL, '2018-10-26 23:14:16', '2018-10-26 23:14:16'),
(30, 2, 'hello juga bro', NULL, '2018-10-26 23:18:14', '2018-10-26 23:18:14'),
(31, 1, 'asd', NULL, '2018-10-26 23:20:54', '2018-10-26 23:20:54'),
(32, 2, 'aku ilham', NULL, '2018-10-26 23:24:41', '2018-10-26 23:24:41'),
(33, 1, 'aku siapa', NULL, '2018-10-26 23:24:50', '2018-10-26 23:24:50'),
(34, 1, 'hasd', NULL, '2018-10-26 23:31:19', '2018-10-26 23:31:19'),
(35, 2, 'siapa yak', NULL, '2018-10-26 23:31:31', '2018-10-26 23:31:31'),
(36, 2, 'haha', NULL, '2018-10-26 23:31:34', '2018-10-26 23:31:34'),
(37, 1, 'ok', NULL, '2018-10-26 23:31:52', '2018-10-26 23:31:52'),
(38, 2, 'hai', NULL, '2018-10-26 23:32:43', '2018-10-26 23:32:43'),
(39, 1, 'asda', NULL, '2018-10-26 23:32:52', '2018-10-26 23:32:52'),
(40, 1, 'hello', NULL, '2018-10-26 23:32:58', '2018-10-26 23:32:58'),
(41, 2, 'sdas', NULL, '2018-10-26 23:33:05', '2018-10-26 23:33:05'),
(42, 1, 'gagag', NULL, '2018-10-26 23:34:47', '2018-10-26 23:34:47'),
(43, 2, 'ca', NULL, '2018-10-26 23:35:07', '2018-10-26 23:35:07'),
(44, 1, 'tes', 3, '2018-10-27 22:32:19', '2018-10-27 22:32:19'),
(45, 1, 'gg', 3, '2018-10-27 22:35:08', '2018-10-27 22:35:08'),
(46, 3, 'hello bro', 1, '2018-10-27 22:36:25', '2018-10-27 22:36:25'),
(47, 1, 'iyaa', 3, '2018-10-27 22:36:47', '2018-10-27 22:36:47'),
(48, 1, 'hai', 3, '2018-10-27 22:56:52', '2018-10-27 22:56:52'),
(49, 3, 'haii juga', 1, '2018-10-27 22:57:14', '2018-10-27 22:57:14'),
(50, 1, 'oc', 3, '2018-10-27 22:57:45', '2018-10-27 22:57:45'),
(51, 1, 'halo bang', 3, '2018-10-27 22:58:57', '2018-10-27 22:58:57'),
(52, 1, 'kaka', 3, '2018-10-27 23:03:05', '2018-10-27 23:03:05'),
(53, 3, 'selamat sore', NULL, '2018-10-28 01:32:41', '2018-10-28 01:32:41'),
(54, 1, 'sore juga', NULL, '2018-10-28 01:33:05', '2018-10-28 01:33:05'),
(55, 3, 'test sore', NULL, '2018-10-28 01:33:18', '2018-10-28 01:33:18'),
(56, 3, 'heyy bro', 1, '2018-10-28 01:34:11', '2018-10-28 01:34:11'),
(57, 1, 'hai', 2, '2018-10-28 09:17:42', '2018-10-28 09:17:42'),
(58, 2, 'iya', 1, '2018-10-28 09:18:16', '2018-10-28 09:18:16'),
(59, 1, 'lagi apa ?', 2, '2018-10-28 09:19:01', '2018-10-28 09:19:01'),
(60, 2, 'mkn', 1, '2018-10-28 09:19:10', '2018-10-28 09:19:10'),
(61, 2, 'tesss', NULL, '2018-10-29 03:00:53', '2018-10-29 03:00:53'),
(62, 2, 'tesss', NULL, '2018-10-29 03:00:57', '2018-10-29 03:00:57'),
(63, 2, 'ngopii', NULL, '2018-10-31 08:19:33', '2018-10-31 08:19:33'),
(64, 2, 'masuk lah', NULL, '2018-10-31 08:19:54', '2018-10-31 08:19:54'),
(65, 1, 'haiiiii', NULL, '2018-10-31 22:01:28', '2018-10-31 22:01:28'),
(66, 1, 'haiiiiiiiiiiiiiiii', 2, '2018-11-03 21:14:19', '2018-11-03 21:14:19'),
(67, 1, 'oiiiiiiiiiiiiiiiiii', 2, '2018-11-03 21:14:50', '2018-11-03 21:14:50'),
(68, 1, 'hai juga', 3, '2018-11-03 22:44:02', '2018-11-03 22:44:02'),
(69, 1, 'tesss', 2, '2018-11-05 06:40:26', '2018-11-05 06:40:26'),
(70, 1, 'woooiii', NULL, '2018-11-05 06:40:50', '2018-11-05 06:40:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_24_143903_create_messages_table', 1),
(4, '2018_10_27_073927_add_receiver_id_field_to_messages_table', 2),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(8, '2016_06_01_000004_create_oauth_clients_table', 3),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `receiver_id`) VALUES
(1, 'ilham', 'ilhamnurhakim378@gmail.com', NULL, '$2y$10$153c1YUkTlshROK9uU/5Ne0HkuCKfuXt5ZWPxORbhYRCFJCQazary', 'e1ssDH4HlWQdRYnRSLkVLQg94AuTNB7GmeN1PMQaq5xeSoORKWRwuR6dSPYX', '2018-10-25 04:57:29', '2018-10-25 04:57:29', NULL),
(2, 'tayo', 'ilham@mail.com', NULL, '$2y$10$yEsIRU21V2NOgBSAzAsjxOpqFAoTNOBCxnJa9dOUodK7zs6IIHLqK', NULL, '2018-10-26 23:18:05', '2018-10-26 23:18:05', NULL),
(3, 'danar', 'danar@mail.com', NULL, '$2y$10$D3xQ7ZWqQxJJgxFR4kCnbe810W9.GVRqNtC3jki9XpC2jAK5D7hwO', 'w5D7an4hTvecPRNE6zzdbeWGiTjCHw6JOz9sMK7BCWb3zqrEcNDQyDDyjSmy', '2018-10-27 20:28:59', '2018-10-27 20:28:59', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
