<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Message extends Model
{
    protected $guarded=[];
    use HasApiTokens;
    public function user(){
        return $this->belongsTo(User::class);
    }
}
