@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia" />
<meta name="keywords" content="Pandu Asia" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection
@section('main-content')



<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Kota</h2>
        <hr>
        <form autocomplete="off" onsubmit="submitFn(this, event);" action="{{ url()->current() }}">
            <div class="search-wrapper">
                <div class="input-holder">
                    <input type="text" class="search-input" name="keyword" />
                    <button class="search-icon" type="submit" onclick="searchToggle(this, event);">
                        <span></span>
                    </button>
                </div>
                <span class="close" onclick="searchToggle(this, event);"></span>
                <div class="result-container">
                </div>
            </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <a href="{{URL::to('/bali')}}" class="gtco-item two-row bg-img animate-box" >
          <img src="{{URL::asset('blog/images/monas.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota jakarta">
          <h1 class="text-kota">Jakarta</h1>
          <div class="overlay">
            <i class="ti-arrow-top-right"></i>
            <div class="copy">
              <h3>Jakarta</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-4">
        <a href="#" class="gtco-item two-row bg-img animate-box">
          <img src="{{URL::asset('blog/images/bandung.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota bandung">
          <h1 class="text-kota">Bandung</h1>
          <div class="overlay">
            <i class="ti-arrow-top-right"></i>
            <div class="copy">
              <h3>Bandung</h3>
              <p></p>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-4">
        <a href="#" class="gtco-item two-row bg-img animate-box">
          <img src="{{URL::asset('blog/images/yogyakarta.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota yogyakarta">
          <h1 class="text-kota">Yogyakarta</h1>
          <div class="overlay">
            <i class="ti-arrow-top-right"></i>
            <div class="copy">
              <h3>Yogyakarta</h3>
              <p></p>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-4 col-sm-4">
          <a href="#" class="gtco-item two-row bg-img animate-box">
            <img src="{{URL::asset('blog/images/beach.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota Bali">
            <h1 class="text-kota">Bali</h1>
            <div class="overlay">
              <i class="ti-arrow-top-right"></i>
              <div class="copy">
                <h3>Bali</h3>
                <p></p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-4">
            <a href="#" class="gtco-item two-row bg-img animate-box">
              <img src="{{URL::asset('blog/images/toba.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota Medan">
              <h1 class="text-kota">Medan</h1>
              <div class="overlay">
                <i class="ti-arrow-top-right"></i>
                <div class="copy">
                  <h3>Medan</h3>
                  <p></p>
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-4 col-sm-4">
              <a href="#" class="gtco-item two-row bg-img animate-box">
                <img src="{{URL::asset('blog/images/toraja.jpg')}}" class="gtco-item two-row bg-img animate-box" alt="wisata di kota Tana Toraja">
                <h1 class="text-kota">Tana Toraja</h1>
                <div class="overlay">
                  <i class="ti-arrow-top-right"></i>
                  <div class="copy">
                    <h3>Tana Toraja</h3>
                    <p></p>
                  </div>
                </div>
              </a>
            </div>

    </div>
  </div>
</div>

@endsection