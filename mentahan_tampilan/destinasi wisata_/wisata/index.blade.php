@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia" />
<meta name="keywords" content="Pandu Asia" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection
@section('main-content')



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
                <h2>Destinasi Kota</h2>
            </div>
        </div>
    </div>
    
    <div class="row head-info">
        <div class="col-6 col-md-4 head-info"><img src="https://d27k8xmh3cuzik.cloudfront.net/wp-content/uploads/2015/08/Viceroy-Bali-Hotel-Pool-Cover-image.jpg"
            alt="bali"></div>
            <div class="col-12 col-md-8" style="padding:15px;">
                <h2 class="card-title">Bali</h2>
                <small>Bali adalah sebuah provinsi di Indonesia. ... Mayoritas penduduk Bali adalah pemeluk agama
                    Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil
                    seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan
                    julukan Pulau Dewata dan Pulau Seribu Pura.</small>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-3" style="margin-top:15px;float:left;">
                    <div class="card-filter">
                        <div class="card-body">
                            <h5 class="card-title">Urutkan berdasarkan</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Harga Terendah</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Review</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Populer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Jemput di Hotel</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Makan Siang / Malam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Foto Dokumentasi</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Tiket gratis umur dibawah 6 tahun</label>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Kategori</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pantai</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Rekreasi Keluarga</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Romantis</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Pegunungan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Alam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Budaya</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Perkotaan</label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <br>
                
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-top:15px;">
                    <div class="list-group">
                       
                        <a href="{{URL::to('/bali/tripdewata')}}" class="list-group-item">
                            <div class="fav-box"><i class="fa fa-heart-o" aria-hidden="true"></i>
                            </div>     
                            <div class="media col-lg-3">
                                <figure class="pull-left">
                                    <img class="media-list-img" src="{{URL::asset('blog/images/greencanyon.jpg')}}" alt="placehold.it/350x250">
                                </figure>
                            </div>
                            
                            <div class="col-md-6">
                                <h3 class="list-group-item-heading"> Open Trip Dewata </h3>
                                <p class="list-group-item-text"> 
                                    <i class="glyphicon glyphicon-map-marker"></i> Jl Seminyak, Bali
                                    <ul class="list-group-item-include">Include :
                                        <li> Makan</li>
                                        <li> Tiket Masuk</li>
                                        <li> Homestay</li>
                                    </ul>
                                    <small>Pemandu : Ilham Nur Hakim </small>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h4> Rp. 150.000  <small> / orang </small></h4>
                                <button class="btn btn-book" style="
                                "> Book Now! </button>
                                <div class="stars">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </div>
                                <p class="rating-wisata"> Average 4.5 <small> / </small> 5 </p>    
                            </div>
                            
                            <hr>
                            
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        @endsection
        