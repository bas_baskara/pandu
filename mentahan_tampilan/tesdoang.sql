-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2018 at 08:14 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tesdoang`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `receiver_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'selamat malam !', NULL, '2018-11-30 21:14:18', '2018-12-01 17:00:00'),
(2, 2, 'halo', NULL, '2018-12-01 10:45:44', '2018-12-01 10:45:44'),
(3, 2, 'malming nya galau gan', NULL, '2018-12-01 11:05:24', '2018-12-01 11:05:24'),
(4, 2, 'testtttt', 1, '2018-12-01 11:41:55', '2018-12-01 11:41:55'),
(5, 2, 'apaa', 1, '2018-12-01 11:44:47', '2018-12-01 11:44:47'),
(6, 1, 'iyaa ?', 2, '2018-12-01 11:45:01', '2018-12-01 11:45:01'),
(7, 1, 'lagi gabut', 2, '2018-12-01 11:52:00', '2018-12-01 11:52:00'),
(8, 2, 'asdasdasda', 1, '2018-12-01 11:59:10', '2018-12-01 11:59:10'),
(9, 1, 'tess remote nya', 3, '2018-12-01 12:14:25', '2018-12-01 12:14:25'),
(10, 1, 'woii', NULL, '2018-12-01 21:24:04', '2018-12-01 21:24:04'),
(11, 3, 'iyaa remote nya bagus ga ?', 1, '2018-12-01 21:34:43', '2018-12-01 21:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_01_170156_create_messages_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ilhamnurhakim378@gmail.com', '$2y$10$aWVD8M0U7qSWsFH.Ql2YfOb.KVrSTW4dPPQ9wtZCE6jw7u2YpPDke', '2018-11-24 00:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ilham', 'ilhamnurhakim378@gmail.com', NULL, '$2y$10$HmjO5s4WNJxH88vhoYxs1.L.9nEzat73GkMp/r3sR/LMPtrgOP2SG', '8qebRegKbd6k1mHxGz1TlBHsBGZ57uyOXcdHEevTzjmYIlRBE8FBYvN8bjGy', '2018-11-24 00:46:39', '2018-11-24 00:46:39'),
(2, 'tayo', 'tayo@mail.com', NULL, '$2y$10$EOkfxXFXhp/YtHI6AXaQie6VsXgyR5WDcn/LtmA2ZIk5kt3v4cgrG', 'NpqFDUBxGasr68gWlREmvQd5tthPqFCNvCsRFQscxMmFzvA9C9WtvIpLL3H5', '2018-12-01 10:41:40', '2018-12-01 10:41:40'),
(3, 'remot', 'remote@mail.com', NULL, '$2y$10$t.XkwjxBGVpqxxeWi3rk4e4VWLsmSDxShJzSgVCU2F5CrWGFIanse', 'xsiYfzkO9nuxhyFEyJPsvlJ2HZxONtgmP3eo4yXqq8ETr9IvW2aIVA0qJO6V', '2018-12-01 12:01:16', '2018-12-01 12:01:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
