@extends('blog/app') @section('head')

<meta name="title" content="Pandu Asia | Guide others to happiness" />
<meta name="description" content="Marketplace terbesar dan terpercaya di Indonesia di bidang wisata. jasa tour termurah dan terlengkap dari pemandu wisata yang berasal dari komunitas-komunitas wisata terkenal di berbagai tempat menarik di Indonesia" />
<meta name="keywords" content="Pandu Asia" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection
@section('main-content')



<div class="gtco-section gtco-products">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-3 col-sm-3" style="margin-top : 50px;">
              <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active">
                  Edit Profile
                </a>
                <a href="#" class="list-group-item list-group-item-action">Edit Profil</a>
                <a href="#" class="list-group-item list-group-item-action">Ubah Foto</a>
                <a href="#" class="list-group-item list-group-item-action">Lihat Profile</a>
              </div>
            </div>
            <div class="col-md-9 col-sm-9" style="margin-top : 50px;">
              <div class="panel panel-default">
                <div class="panel-heading">Wajib</div>
                <div class="panel-body">
                  <form>
                      <div class="form-group ">
                          <label for="exampleFormControlInput1">Email address</label>
                          <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email">
                      </div>
                      <div class="form-group">
                          <label >Password</label>
                          <input type="password" class="form-control" placeholder="" name="password">
                      </div>
                      <div class="form-group">
                          <label >Nama Depan</label>
                          <input type="text" class="form-control" placeholder="" name="namadepan">
                      </div>
                      <div class="form-group">
                          <label >Nama Belakang</label>
                          <input type="text" class="form-control" placeholder="" name="namabelakang">
                      </div>
                      <div class="form-group">
                          <label >Saya</label>
                          <select class="form-control">
                              <option>Pilih</option>
                              <option value="laki-laki">Laki-laki</option>
                              <option value="perempuan">Perempuan</option>
                            </select>
                       </div>
                       <div class="form-group">
                          <label>No Telp</label>
                          <input type="text" class="form-control" placeholder="+62" name="phone">
                      </div>
                      <div class="form-group">
                          <label>Asal Kota</label>
                          <input type="text" class="form-control" placeholder="" name="mycity" >
                      </div>
                      <div class="form-group">
                          <label>Lahir</label>
                          <input type="date" name="date_of_birth" required style="margin-left:25px;" name="dateofbirth"/>
                      </div>
                      <div class="form-group">
                          <label>Alamat</label>
                          <input type="text" class="form-control" placeholder="" name="alamat">
                      </div>
                      <a href="" class="btn btn-success">Update</a>
                      <a href="" class="btn btn-warning">Batal</a>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
