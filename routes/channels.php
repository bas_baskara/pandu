<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
use App\PrivateChat;
use App\Customer;
use App\MasterMember;
use Illuminate\Support\Facades\Auth;



Broadcast::channel('toguide', function(){
	return true;
});
Broadcast::channel('chatgroup', function(){
	return true;
});




// Broadcast::channel('toguide.{messageId}.{customerId}.{memberId}', function($messageId, $customerId, $memberId){
// 	return (PrivateChat::where('customer_id', $customerId)->where('member_id', $memberId)->where('id', $messageId)->first()->id === $messageId && (Auth::guard('pelanggan')->user()->id === $customerId || Auth::guard('member')->user()->id === $memberId));
// });
