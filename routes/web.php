<?php

use App\MasterTour;
use App\Category;
use App\Country;
use App\Province;
use App\Regency;
use App\MasterMember;
use App\Library\General;
use App\Library\Common;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){

	// Route::get('/', function(){
	// 	return view('frontends.layout');
	// });

	Route::get('/', 'Frontends\GeneralController@index');
 	Route::get('/home', 'Frontends\GeneralController@index');
	Route::get('/blog','Frontends\GeneralController@blog')->name('post');
	//autosuggest
	Route::post('autosuggest/fetch','Frontends\GeneralController@fetch')->name('autosuggest.fetch');
	//autosuggest
	Route::post('autosuggest/fetchcity','Frontends\GeneralController@fetch_city')->name('autosuggest.city');
    Route::get('careers/{careers}','Blog\PostController@careers')->name('careers');
    Route::get('careers/{careers}/apply','Blog\UsercareerController@create')->name('usr_careers');
    Route::post('carrers/store', 'Blog\UsercareerController@store');
    Route::get('careers','Blog\HomeController@career')->name('career');
	Route::get('/about','Frontends\GeneralController@about')->name('home');
	Route::get('/blog/{posts}','Frontends\GeneralController@detailpost');//->name('posts');
	Route::get('/category/{category}','Frontends\GeneralController@category')->name('category');

	Route::get('/p','Frontends\GeneralController@page');
	Route::get('/p/{page}','Frontends\GeneralController@detailpage');
	Route::get('/wisata/{w}', 'Frontends\CityController@wisata');

	Route::get('/wisata/', function(){
	    return view('frontends.city');
	});

	// Route::get('/wisata/bandung/booking', function (){
	//     return view('frontends.form_booking');
	// });
	
	Route::get('/payment/invoices/2019', function (){
	    return view('frontends.payment');
	});
	
	Route::get('/ratingsystem', function (){
	    return view('frontends.rating');
	});

	Route::get('rating_wisata','Frontends\GeneralController@view_rating_wisata');
	Route::get('rating_member','Frontends\GeneralController@view_rating_member');
	Route::post('rateswisata','Frontends\GeneralController@rating_wisata');
	Route::post('rateswisata','Frontends\GeneralController@rating_member');

	Route::post('rating','Member\MembersController@rating')->name('rates');
	
	Route::post('/wisata/booking/process','BookingController@store');

	Route::any('search', function(){
		// var_dump($_GET); die();
		$keysearch = $_GET;
		$city = Regency::all();
		$province = Province::all();
		$country = Country::all();
		$category = Category::all();
		$g = MasterMember::all();

		$s = new General;

        $durasi = $s->durasitour();

        $tipe = $s->tipetrip();

		$tour = DB::table('master_tours')
				->join('categories', 'master_tours.category','=','categories.id')
				->join('countries', 'master_tours.country_id','=','countries.id')
				->join('provinces','master_tours.province_id','=','provinces.id')
				->join('regencies','master_tours.city_id','=','regencies.id')
				->select('master_tours.*','categories.title as c','countries.country_name as n','provinces.name as p','regencies.name as k')
				->where('master_tours.title', 'LIKE', '%' . $keysearch['cari'] .'%' )
				// ->orWhere('master_tours.category','==',$keysearch['cat'])
                ->orWhere('master_tours.category','==', empty($keysearch['cat']) ? NULL : $keysearch['cat']) 
				->orWhere('regencies.name', 'LIKE', '%' . $keysearch['cari'] .'%')
				->orWhere('provinces.name', 'LIKE', '%' . $keysearch['cari'] .'%')
				->orWhere('countries.country_name', 'LIKE', '%' . $keysearch['cari'] .'%')
				->get();

		if(count($tour) > 0)
		{
			return view('frontends.search', compact('tour','city','province','country','category','durasi','g'));
		}
		else
		{
			return view('frontends.search', compact('city','province','country','category','durasi','g'))->with(['message' => 'No Details found. Try to search again !']);
		}

	});

	// Social Auth
	Route::get('auth/social', 'Pelanggan\Auth\SocialAuthController@show')->name('social.login');
	Route::get('oauth/{driver}', 'Pelanggan\Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
	Route::get('oauth/{driver}/callback', 'Pelanggan\Auth\SocialAuthController@handleProviderCallback')->name('social.callback');

	/***********************************************************************************************************
	 * 	Tour
	 ***********************************************************************************************************/

	Route::group(['prefix' => 'tour'], function(){
		
		Route::get('/','Frontends\TourController@alltours');

		//tour by city / regency
		Route::get('city/{slug}','Frontends\TourController@citytour');
		Route::get('city','Frontends\TourController@tourincities');
		//filter
		Route::get('city/filterbycategory/{category}','Frontends\TourController@filterbycategory');

		Route::get('regency/{slug}','Frontends\TourController@citytour');
		Route::get('regency','Frontends\TourController@alltours');
		//filter
		Route::get('regency/filterbycategory/{category}','Frontends\TourController@filterbycategory');

		//tour by province
		Route::get('province/{slug}','Frontends\TourController@provincetour');
		Route::get('province','Frontends\TourController@tourinprovinces');
		//filter
		Route::get('province/filterbycategory/{category}','Frontends\TourController@filterbycategory');
		
		//tour by country
		Route::get('country/{slug}','Frontends\TourController@countrytour');
		Route::get('country','Frontends\TourController@tourincountries');
		//filter
		Route::get('country/filterbycategory/{category}','Frontends\TourController@filterbycategory');

		//tour by category
		Route::get('category/{slug}','Frontends\TourController@tourcategory');

		//tour detail
		Route::get('/{slug}', 'Frontends\TourController@tourdetail');

		//booking
		Route::post('booking/{slug}','Frontends\TourController@bookingtour');



	});


	/***********************************************************************************************************
	 * 	CUSTOMERS GROUP
	 ***********************************************************************************************************/
	Route::group(['prefix' => 'user'], function(){
		Route::get('/', 'Pelanggan\Auth\LoginController@showLoginForm');
	    Route::get('/signin', 'Pelanggan\Auth\LoginController@showLoginForm');
		Route::post('/signin', 'Pelanggan\Auth\LoginController@login')->name('users.login');
		Route::get('/register', 'Pelanggan\Auth\RegisterController@showRegistrationForm');
	    Route::post('/register', 'Pelanggan\Auth\RegisterController@register')->name('users.register');
	    //$this->get('/verify-user/{code}', 'Pelanggan\Auth\RegisterController@activateUser')->name('activate.user');
	    Route::get('signout','Pelanggan\Auth\LoginController@signout');
	    // Route::post('profile/{id}/update','Pelanggan\UsersController@update');

	    Route::get('profile/{username}','Pelanggan\UsersController@show');
	    
	    Route::get('profile/{username}/edit','Pelanggan\UsersController@edit');

	    Route::post('profile/{username}/update','Pelanggan\UsersController@update');
	    
	    Route::get('logout','Pelanggan\Auth\LoginController@signout');
	    Route::get('forgot','Pelanggan\Auth\ForgotPasswordController@showform');
	 	$this->post('password/email', 'Pelanggan\Auth\ForgotPasswordController@sendResetLinkEmail')->name('passwords.email');
	    $this->get('/password/reset/{token}', 'Pelanggan\Auth\ResetPasswordController@showResetForm')->name('passwords.reset');
	    $this->post('/password/reset', 'Pelanggan\Auth\ResetPasswordController@reset');

	    Route::get('home','Pelanggan\UsersController@index');
		});


		Route::group(['prefix' => 'komunitas'], function(){
	        Route::get('/blog','Frontends\KomunitasController@blog');
	    	Route::get('/blog/{pm}','Frontends\KomunitasController@detailpost');
	    });


	 /***********************************************************************************************************
	 * 	KOMUNITAS GROUP
	 ***********************************************************************************************************/
	Route::group(['prefix' => 'community'], function(){
		Route::get('/','Komunitas\KomunitasController@index');
		Route::get('id/{identity}', 'Komunitas\KomunitasController@community');
		Route::post('id/{identity}/join','Komunitas\KomunitasController@joincommunity');
		Route::post('id/{identity}/update','Komunitas\KomunitasController@joinupdate');
		Route::get('home','Komunitas\KomunitasController@index');
		Route::get('profile/{id}', 'Komunitas\KomunitasController@profile');
		Route::post('profile/{id}/update','Komunitas\KomunitasController@updateprofile');
		Route::get('message', 'Komunitas\KomunitasController@message');
		Route::get('members', 'Komunitas\KomunitasController@members');
		Route::get('reviews', 'Komunitas\KomunitasController@reviews');
		Route::post('reviews/store','Komunitas\KomunitasController@storereviews');
		Route::get('blog', 'Komunitas\KomunitasController@blog');
		Route::get('blog/create', 'Komunitas\KomunitasController@createblog');
		Route::get('blog/edit/{id}','Komunitas\KomunitasController@editblog');
		Route::post('blog/update/{id}','Komunitas\KomunitasController@updateblog');
		Route::get('blog/delete/{id}','Komunitas\KomunitasController@deleteblog');
		Route::get('transaksi','Komunitas\KomunitasController@transaksi');
		Route::get('chat','Komunitas\ChatController@chat');
		Route::get('chat/message','Komunitas\ChatController@getchat');
		Route::post('chat/message','Komunitas\ChatController@sendchat');
		Route::post('chat/comId','Komunitas\ChatController@comId');
	});

	/***********************************************************************************************************
	 * 	GUIDE GROUP
	 ***********************************************************************************************************/
	Route::group(['prefix' => 'guide'], function(){

	});

	/***********************************************************************************************************
	 * 	MEMBER GROUP
	 ***********************************************************************************************************/
	Route::group(['prefix' => 'member'], function(){
	    Route::get('/', 'Member\Auth\LoginController@showLoginForm');
		Route::get('login', 'Member\Auth\LoginController@showLoginForm');
		Route::post('login','Member\Auth\LoginController@login')->name('members.login');
		Route::get('joincommunity', 'Member\MembersController@joincommunity');
		Route::get('home', 'Member\MembersController@index');
		Route::get('profile/{id}','Member\MembersController@profile');
		Route::post('profile/{id}/update','Member\MembersController@updateprofile');
		Route::get('profile/{id}/password','Member\MembersController@changepassword');
		Route::post('profile/{id}/updatepassword','Member\MembersController@updatepassword');
		Route::get('message','Member\MembersController@message');
		// 
		Route::get('chatfromcustomer','Member\MembersController@chatfromcustomer');
		// 
		Route::get('upgrademember/{id}','Member\MembersController@upgrademember');
		Route::post('upgrademember/{id}/update','Member\MembersController@submitupgrademember');
		Route::get('transactions','Member\MembersController@transaksi');
		Route::get('tour','Member\MembersController@tour');
		Route::get('tour/create','Member\MembersController@createtour');
		Route::post('tour/store','Member\MembersController@storetour');
		Route::get('tour/detail/{slug}','Member\MembersController@seetour');
		Route::get('tour/edit/{id}','Member\MembersController@edittour');
		Route::post('tour/update/{id}','Member\MembersController@updatetour');
		Route::get('tour/delete/{id}','Member\MembersController@deletetour');
		Route::get('tour/getcategorytour/{id}','Member\MembersController@getcategorytour');
		// Route::get('tour/getprovinces/{country}','Member\MembersController@getprovinces');
		// Route::get('tour/getcities/{province}','Member\MembersController@getcities');
		Route::get('tour/getprovinces/{country}','Frontends\GeneralController@getprovinces');
		Route::get('tour/getcities/{province}','Frontends\GeneralController@getcities');
		Route::get('customerbooking','Member\MembersController@customerbooking');
		Route::get('blog', 'Member\MembersController@blog');
		Route::get('blog/create', 'Member\MembersController@createblog');
		Route::post('blog/store','Member\MembersController@storeblog');
		Route::get('blog/edit/{id}','Member\MembersController@editblog');
		Route::post('blog/update/{id}','Member\MembersController@updateblog');
		Route::get('blog/delete/{id}','Member\MembersController@deleteblog');
		Route::get('bankaccount/{id}','Member\MembersController@bankaccount');
		Route::post('bankaccount/{id}/update','Member\MembersController@updatebankaccount');
		Route::get('logout','Member\Auth\LoginController@logout');
		Route::get('register', 'Member\Auth\RegisterController@showRegistrationForm');
		Route::post('register', 'Member\Auth\RegisterController@register')->name('members.register');
	});
	


	/***********************************************************************************************************
	 * 	RAHASIADAPUR GROUP
	 ***********************************************************************************************************/
	Route::group(['prefix' => 'rahasiadapur'], function(){
		Route::get('/',function(){
			return view('master_system.auth.login');
		});
		Route::get('/login',function(){
			return view('master_system.auth.login');
		});
        Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
		Route::get('home','Master_System\HomeController@index');

		Route::get('post','Master_System\PostController@index');
		Route::get('post/index','Master_System\PostController@index');
		Route::get('post/create','Master_System\PostController@create');
		Route::post('post/store','Master_System\PostController@store');
		Route::get('post/edit/{id}','Master_System\PostController@edit');
		Route::post('post/update/{id}','Master_System\PostController@update');
		Route::get('post/delete/{id}','Master_System\PostController@destroy');

		Route::get('post/member','Master_System\PostController@postmember');

		Route::get('page', 'Master_System\PageController@index');
		Route::get('page/index', 'Master_System\PageController@index');
		Route::get('page/create', 'Master_System\PageController@create');
		Route::post('page/store','Master_System\PageController@store');
		Route::get('page/edit/{id}','Master_System\PageController@edit');
		Route::post('page/update/{id}', 'Master_System\PageController@update');
		Route::get('page/delete/{id}','Master_System\PageController@destroy');

		Route::get('category', 'Master_System\CategoryController@index');
		Route::get('category/index', 'Master_System\CategoryController@index');
		Route::get('category/create', 'Master_System\CategoryController@create');
		Route::post('category/store', 'Master_System\CategoryController@store');
		Route::get('category/edit/{id}', 'Master_System\CategoryController@edit');
		Route::post('category/update/{id}', 'Master_System\CategoryController@update');
		Route::get('category/delete/{id}', 'Master_System\CategoryController@destroy');

		Route::get('tag', 'Master_System\TagController@index');
		Route::get('tag/index', 'Master_System\TagController@index');
		Route::get('tag/create', 'Master_System\TagController@create');
		Route::post('tag/store', 'Master_System\TagController@store');
		Route::get('tag/edit/{id}', 'Master_System\TagController@edit');
		Route::post('tag/update/{id}', 'Master_System\TagController@update');
		Route::get('tag/delete/{id}', 'Master_System\TagController@destroy');

		Route::get('user','Master_System\UserController@index');
		Route::get('user/index','Master_System\UserController@index');
		Route::get('user/create', 'Master_System\UserController@create');
		Route::post('user/store', 'Master_System\UserController@store');
		Route::get('user/edit/{id}', 'Master_System\UserController@edit');
		Route::post('user/update/{id}', 'Master_System\UserController@update');
		Route::get('user/delete/{id}', 'Master_System\UserController@destroy');

		Route::get('popup','Master_System\PopupController@index');
		Route::get('popup/index','Master_System\PopupController@index');
		Route::get('popup/create','Master_System\PopupController@create');
		Route::post('popup/store','Master_System\PopupController@store');

		Route::get('community','Master_System\MasterCommunityController@index');
		Route::get('community/getcities/{province}','Master_System\MasterCommunityController@getcities');
		Route::get('community/getprovinces/{country}','Master_System\MasterCommunityController@getprovinces');
		Route::get('community/index','Master_System\MasterCommunityController@index');
		Route::get('community/create','Master_System\MasterCommunityController@create');
		Route::get('community/{identity}','Master_System\MasterCommunityController@show')->name('community');
		Route::post('community/store','Master_System\MasterCommunityController@store');
		Route::get('community/edit/{id}','Master_System\MasterCommunityController@edit');
		Route::post('community/update/{id}','Master_System\MasterCommunityController@update');
		Route::get('community/delete/{id}','Master_System\MasterCommunityController@destroy');
		
		Route::get('career', 'Master_System\CareerController@index');
		Route::get('career/index', 'Master_System\CareerController@index');
		Route::get('career/create', 'Master_System\CareerController@create');
		Route::post('career/store', 'Master_System\CareerController@store');
		Route::get('career/edit/{id}', 'Master_System\CareerController@edit');
		Route::post('career/update/{id}', 'Master_System\CareerController@update');
		Route::get('career/delete/{id}', 'Master_System\CareerController@destroy');
	    Route::get('list_resume/index', 'Blog\UsercareerController@index');
		
		Route::get('customer', 'Master_System\CustomerController@index');
		Route::get('customer/index', 'Master_System\CustomerController@index');
		Route::get('customer/edit/{id}', 'Master_System\CustomerController@edit');
		Route::post('customer/update/{id}', 'Master_System\CustomerController@update');
		Route::get('customer/delete/{id}', 'Master_System\CustomerController@destroy');
		
		Route::get('members','Master_System\MasterMemberController@index');
		Route::get('members/index','Master_System\MasterMemberController@index');
		Route::get('members/create','Master_System\MasterMemberController@create');
		Route::post('members/store','Master_System\MasterMemberController@store');
		Route::get('members/edit/{id}','Master_System\MasterMemberController@edit');
		Route::post('members/update/{id}','Master_System\MasterMemberController@update');
		Route::get('members/delete/{id}','Master_System\MasterMemberController@destroy');

		Route::get('tour','Master_System\MasterTourController@index');
		Route::get('tour/index','Master_System\MasterTourController@index');
		Route::get('tour/complement','Master_System\TourIncludeExcludeController@index');
		Route::get('tour/complement/index','Master_System\TourIncludeExcludeController@index');
		Route::get('tour/complement/create','Master_System\TourIncludeExcludeController@create');
		Route::post('tour/complement/store','Master_System\TourIncludeExcludeController@store');
		Route::get('tour/complement/edit/{id}','Master_System\TourIncludeExcludeController@edit');
		Route::post('tour/complement/update/{id}','Master_System\TourIncludeExcludeController@update');
		Route::get('tour/complement/delete/{id}','Master_System\TourIncludeExcludeController@destroy');

        Route::get('tourcategory','Master_System\TourCategoryController@index');
		Route::get('tourcategory/index','Master_System\TourCategoryController@index');
		Route::get('tourcategory/create','Master_System\TourCategoryController@create');
		Route::post('tourcategory/store','Master_System\TourCategoryController@store');
		Route::get('tourcategory/edit/{id}','Master_System\TourCategoryController@edit');
		Route::post('tourcategory/update/{id}','Master_System\TourCategoryController@update');
		Route::get('tourcategory/delete/{id}','Master_System\TourCategoryController@destroy');
        
        Route::get('citydestination/index','Master_System\TourDestinationController@index');
        Route::get('citydestination/create','Master_System\TourDestinationController@create');
    	Route::post('citydestination/store','Master_System\TourDestinationController@store');
    	Route::get('citydestination/edit/{id}','Master_System\TourDestinationController@edit');
    	Route::post('citydestination/update/{id}','Master_System\TourDestinationController@update');
		Route::get('citydestination/delete/{id}','Master_System\TourDestinationController@destroy');

		Route::get('payment/banks/{id}','Master_System\MasterBankController@index');
		// Route::get('payment/banks/edit/{id}','Master_System\MasterBankController@edit');
		Route::post('payment/banks/update/{id}','Master_System\MasterBankController@update');
		Route::get('payment/paypal/{id}','Master_System\MasterBankController@paypal');
		Route::post('payment/paypal/update/{id}','Master_System\MasterBankController@updatepaypal');

		Route::get('masterorder','Master_System\HomeController@masterorder');

		Route::get('approval/blog','Master_System\ApprovalController@index');
		Route::get('approval/approve/{id}','Master_System\ApprovalController@approve');
		Route::get('approval/blog_member','Master_System\ApprovalController@member');
		Route::get('approval/approve_member_blog/{id}','Master_System\ApprovalController@approve_blog_member');
		
        Route::get('approval/joincommunity','Master_System\ApprovalController@community');
        
        Route::get('approval/joincommunity/update/{id}','Master_System\ApprovalController@joincommunity');
        
        Route::get('approval/leader/update/{id}','Master_System\ApprovalController@leadercommunity');
    
        Route::get('approval/leader','Master_System\ApprovalController@leader');
        
        Route::get('approval/guide','Master_System\ApprovalController@guide');
        Route::get('approval/approve_guide/{id}','Master_System\ApprovalController@approve_guide');
        
		Route::get('system/settings/{id}','Master_System\SettingController@show')->name('settings.show');
		Route::patch('system/settings/update/{id}','Master_System\SettingController@update');

		Auth::routes();
	});

});

// Baskara ->
Route::get('/chat', 'ChatController@indexCustomer');
Route::get('/chat/messages/customer/{id}', 'ChatController@fetchMessages');
Route::get('/chat/messages/guide', 'ChatController@fetchMessages');
Route::post('/chat/start', 'ChatController@start');
Route::post('/chat/messages', 'ChatController@sendMessage');
Route::post('/chat/guide', 'ChatController@guide');
Route::post('/chat/status', 'ChatController@status');

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', 'HomeController@index')->name('home');
