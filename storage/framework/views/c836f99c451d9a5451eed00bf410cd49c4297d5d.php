
<?php $__env->startSection('content'); ?>

<div class="col-sm-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(url('/')); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('/tour')); ?>"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(1))))); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('/tour/province')); ?>"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(2))))); ?></a></li>
          <li class="breadcrumb-item "><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(3))))); ?></li>
      </ol>
  </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="gtco-heading text-center">
                <h2>Destinasi Provinsi</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
           <?php /*  <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;">  */ ?>
        </div>
        <div class=" col-md-8" style="padding:15px;">
        <?php /* <h2 class="card-title">{{ $result->main_city }}</h2>
        
        <small>{{ str_limit($result->main_description, 700) }}</small> */ ?>
        </div>
    </div>
            
    <div class="row">
        <div class="col-4" style="margin-top:15px;float:left; ">
            <div class="card-filter" style="position:sticky;">
                <div class="card-body">
                    <h5 class="card-title">Urutkan berdasarkan</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="radio">
                                <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="optradio">Harga Terendah</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="radio">
                                <label><input type="radio" name="optradio" checked>Review</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="optradio">Populer</label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <h5 class="card-title">Destinasi</h5>
                    <?php $__currentLoopData = $filtercity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kfc => $vfc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="checkbox">
                            <label><input type="checkbox" name="tourcity" value="<?php echo e($vfc->id); ?>"><?php echo e($vfc->name); ?></label>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <hr>
                <div class="card-body">
                    <h5 class="card-title">Kategori</h5>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kcat => $vcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="checkbox">
                            <label><input type="checkbox" id="tourcategory" name="tourcategory" value="<?php echo e($vcat->id); ?>" onclick="catTour()"><?php echo e($vcat->title); ?></label>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>

        <div class="col-8" style="margin-top:15px;">

            <?php $__currentLoopData = $tour_by_province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kprovince => $vprovince): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card mb-3" style="max-width: 100%;">
                  <div class="row no-gutters">
                    <div class="col-md-4">
                      <?php 
                        $pics = json_decode($vprovince->images); 
                    ?>
                    <div class="tourslide">
                    <?php $__currentLoopData = $pics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <img class="card-img" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt="">                              
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    </div>
                    <div class="col-md-8">
                      <div class="card-body">
                        <a href="<?php echo e(url('tour/'.$vprovince->tourslug)); ?>"><h5 class="card-title"><?php echo e($vprovince->title); ?></h5></a>
                        <a href="<?php echo e(url('tour/'.$vprovince->tourslug)); ?>" class="btn btn-book" style="
                        ">Book Now!</a>
                        <h3>Rp. <?php echo e(empty($vprovince->promo_price) ? number_format($vprovince->actual_price,0,",",".") : number_format($vprovince->promo_price,0,",",".")); ?>  <small> / orang </small></h3>
                        <p class="card-text"><?php echo e(str_limit($vprovince->description, 129, '...')); ?></p>
                        <p class="card-text">
                            <small><i class="glyphicon glyphicon-map-marker"></i> 
                            <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kc => $vc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($vprovince->city_id == $vc->id): ?>
                                    <?php echo e($vc->name); ?>

                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                     , 
                            <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kp => $vp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($vprovince->province_id == $vp->id): ?>
                                    <?php echo e($vp->name); ?>

                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> , 
                            <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kco => $vco): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($vprovince->country_id == $vco->id): ?>
                                    <?php echo e($vco->country_name); ?>

                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </small>
                            <br>
                            <div class="stars">
                        <?php $rating = $vprovince->rating; ?> 

                        <?php $__currentLoopData = range(1,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <span class="fa-stack rating" style="width:1em">
                                
                                <?php if($rating > 0): ?>
                                    <?php if($rating > 0.5): ?>
                                        <i class="fa fa-star" style="color: orange;"></i>
                                    <?php else: ?>
                                        <i class="fa fa-star-half" style="color: orange;"></i>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <i class="fa fa-star-o"></i>
                                <?php endif; ?>
                                <?php $rating-- ?>
                            </span>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <p class="rating-wisata"> Rate <?php echo e($vprovince->rating / count($vprovince->rating)); ?> <small> / </small> 5 </p> 
                            <hr>
                            <small class="pull-right">
                                <button type="button" class="btn btn-outline-warning btn-sm">
                                <?php $__currentLoopData = $durasi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kd => $vd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($vprovince->category == $kd): ?>
                                        <?php echo e($vd); ?>

                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </button>
                            </small>
                            <small>Pemandu : 
                                <?php $__currentLoopData = $g; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kg => $vg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($vprovince->member_id == $vg->id): ?>
                                        <?php echo e($vg->first_name); ?> <?php echo e($vg->last_name); ?> 
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </small>
                            <br>
                            <small class="text-muted">Last updated 3 mins ago</small>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
</div>

<script type="text/javascript">
    function catTour(){
        var cat = document.getElementByTagName('tourcategory');

       if(cat.checked == true){
            alert(cat.value);
       }
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>