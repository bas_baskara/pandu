
<?php $__env->startSection('content'); ?>

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
        <h2>Semua Tour</h2>
      </div>
    </div>
    <div class="row"> 
      <?php $__currentLoopData = $alltour; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kat => $vat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kc => $vc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php if($vat->city_id == $vc->id): ?>
            <div class="col-md-3">
              <div class="card  ">
                <div class="social-card-header align-middle text-center ">

                  <?php 
                    $pics = json_decode($vat->images); 
                    $rpic = array_slice($pics,0,1);
                  ?>

                  <div class="tourslide">
                    <?php $__currentLoopData = $rpic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div><img class="image" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt=""></div>                                    
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>

                </div>
                <hr>
                <a href="<?php echo e(url('tour/city/'. $vc->slug )); ?>">
                    <div class="card-body text-center">
                      <div class="row">
                        <div class="col border-right">
                          <span class="text-muted"><?php echo e($vat->city_id == $vc->id ? $vc->name : ""); ?></span>
                        </div>
                        <div class="col">
                          <span class="text-muted">mulai dari</span>
                          <div class="text-harga">Rp. <?php echo e(number_format($vat->actual_price,0,",",".")); ?></div>
                        </div>
                      </div>
                    </div>
                </a>
              </div>
            </div>
          <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>