
<?php $__env->startSection('content'); ?>

<div class="col-sm-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(url('/')); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('/tour')); ?>"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(1))))); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('/tour/city')); ?>"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(2))))); ?></a></li>
          <li class="breadcrumb-item "><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(3))))); ?></li>
      </ol>
  </nav>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="gtco-heading text-center">
                <h2>Destinasi Kota</h2>
            </div>
        </div>
    </div>

    <div class="row head-info">
        <div class="col-lg-4">
           <?php /*  <img src="{{URL::asset('www/assets/destinations/'.$result->main_image_tours)}}"
            alt="{{ $result->main_city }}" style="width:300px; height: 210px;">  */ ?>
        </div>
            <div class=" col-md-8" style="padding:15px;">
                <?php /* <h2 class="card-title">{{ $result->main_city }}</h2>
                
                <small>{{ str_limit($result->main_description, 700) }}</small> */ ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4" style="margin-top:15px;float:left; ">
                    <div class="card-filter" style="position:sticky;">
                        <div class="card-body">
                            <h5 class="card-title">Urutkan berdasarkan</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Harga Tertinggi</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Harga Terendah</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" checked>Review</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Populer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas</h5>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Kendaraan</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Jemput di Hotel</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Makan Siang / Malam</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Foto Dokumentasi</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Tiket gratis umur dibawah 6 tahun</label>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <h5 class="card-title">Kategori</h5>
                            <form id="tourcategory" method="POST">  
                                <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kcat => $vcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="categorytour" onclick="getcategorytour(this);" name="categorytour[]" value="<?php echo e($vcat->id); ?>"><?php echo e($vcat->title); ?></label>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-top:15px;" id="filtercontents">
                    <?php $__currentLoopData = $tour_by_city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kcity => $vcity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="list-group-item wisata">
                       <div  class="list-group-item-wisata">
                                                    
                            <div class="media col-lg-3">
                                <figure class="pull-left">
                                    <?php 
                                        $pics = json_decode($vcity->images); 
                                        $rpic = array_slice($pics,0,1);
                                    ?>
                                    <div class="tourslide">
                                    <?php $__currentLoopData = $rpic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <div><img class="media-list-img" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt="" style="width: 160px;"></div>                                    
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </figure>
                            </div>
                            
                            <div class="col-md-6">
                                <a href="<?php echo e(url('tour/'.$vcity->tourslug)); ?>"><h4 class="list-group-item-heading text-center"><?php echo e($vcity->title); ?></h4></a>
                                <p class="list-group-item-text"> 
                                 <small><i class="glyphicon glyphicon-map-marker"></i> 
                                    <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kc => $vc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($vcity->city_id == $vc->id): ?>
                                            <?php echo e($vc->name); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             , 
                                    <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kp => $vp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($vcity->province_id == $vp->id): ?>
                                            <?php echo e($vp->name); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> , 
                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kco => $vco): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($vcity->country_id == $vco->id): ?>
                                            <?php echo e($vco->country_name); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </small>
                                         
                                <hr>
                                <small class="pull-right">
                                    <button type="button" class="btn btn-outline-warning btn-sm">
                                    <?php $__currentLoopData = $durasi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kd => $vd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($vcity->category == $kd): ?>
                                            <?php echo e($vd); ?>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </button>
                                </small>

                                <div class="entry">
                                    <a href="<?php echo e(url('tour/'.$vcity->tourslug)); ?>" style="color:#000;"><?php echo e(str_limit($vcity->description, 129, '...')); ?></a>
                                </div>
  
                                
                                <small>Pemandu : 
                                <?php $__currentLoopData = $g; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kg => $vg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($vcity->member_id == $vg->id): ?>
                                        <?php echo e($vg->first_name . " " . $vg->last_name); ?>

                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </small>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h4> Rp. <?php echo e(empty($vcity->promo_price) ? number_format($vcity->actual_price,0,",",".") : number_format($vcity->promo_price,0,",",".")); ?>  <small> / orang </small></h4>
                                <a href="<?php echo e(url('tour/'.$vcity->tourslug)); ?>" class="btn btn-book" style="
                                ">Book Now!</a>
                                <div class="stars">
                                    <?php $rating = $vcity->rating; ?> 

                                <?php $__currentLoopData = range(1,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <span class="fa-stack rating" style="width:1em">
                                        
                                        <?php if($rating > 0): ?>
                                            <?php if($rating > 0.5): ?>
                                                <i class="fa fa-star" style="color: orange;"></i>
                                            <?php else: ?>
                                                <i class="fa fa-star-half" style="color: orange;"></i>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <i class="fa fa-star-o"></i>
                                        <?php endif; ?>
                                        <?php $rating-- ?>
                                    </span>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <p class="rating-wisata"> Rate 
                                    
                                     <small> / </small> 5 </p>    
                            </div>
                            
                            <hr>

                       </div>
                    </div>
                    <br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                </div>
            </div>
        </div>

        <script type="text/javascript">
           
            function getcategorytour(e)
            {
                var v = e.value;

                console.log(v);

                $.ajax({
                    url: 'filterbycategory/'+v,
                    type:'GET',
                    dataType: 'json',
                    success: function(data){
                        $.each(data, function(key,value){
                            console.log(value.name);
                            $('#filtercontents').append(
                                '<div class="list-group-item wisata"><div class="list-group-item-wisata"><div class="media col-lg-3">'+

                                '<figure class="pull-left">' +
                                    
                                    '<div class="tourslide">' +

                                    '</div>'+
                                '</figure>'+

                                + '</div>'+
                                '<div class="col-md-6">' +
                                '<a href="'+ value.slug +'"><h4 class="list-group-item-heading text-center">'+ value.title +'</h4></a>'+
                  
                                +'</div></div>'
                                );
                        });
                    }
                });

                
               // var cat = document.getElementById('categorytour');

               // if(cat.checked)
               //      console.log(cat.value);
            }
        </script>
        <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>