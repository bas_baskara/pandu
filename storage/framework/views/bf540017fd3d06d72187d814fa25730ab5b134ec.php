
<?php $__env->startSection('head'); ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
var memberId = '<?php echo e($member->id); ?>';
</script>  
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="col-12">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(url('/')); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('/tour')); ?>"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(1))))); ?></a></li>
          <li class="breadcrumb-item"><?php echo e(str_replace('-', ' ', ucwords(trans(Request::segment(2))))); ?></li>
      </ol>
  </nav>
</div>

<div class="container">
  <div class="row">

        <div class="col-8">
          <h2 class="card-title-wisata"><?php echo e($detailtour->title); ?></h2>
          <p><i class="glyphicon glyphicon-map-marker"></i><?php echo e($detailtour->city_id == $city->id ? $city->name : ""); ?>, <?php echo e($detailtour->province_id == $province->id ? $province->name : ""); ?>, <?php echo e($detailtour->country_id == $country->id ? $country->country_name : ""); ?></p>  

            <?php 
                $pics = json_decode($detailtour->images); 
            ?>

            <div class="main-image-wisata">
                <?php $__currentLoopData = $pics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div><img class="img-responsive" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt="" style="width: 100px;"></div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <hr>
          </div>

          <div class="col-4">
            <div class="card">
              
                <div class="card-body">
                  <div class="text-center">  

                    <h2> Rp. <?php echo e(empty($detailtour->promo_price) ? number_format($detailtour->actual_price,0,",",".") : number_format($detailtour->promo_price,0,",",".")); ?> <small> / orang </small></h2>

                    <form action="<?php echo e(url('tour/booking/'.Request::segment(2))); ?>" method="post">
                      <?php echo e(csrf_field()); ?>

                      <div class="form-group">
                        <input type="number" class="form-control" name="jumlahpeserta" id="jumlahpeserta" min="<?php echo e($detailtour->min_kuota); ?>" max="<?php echo e($detailtour->max_kuota); ?>" onkeyup="tourprice()" autofocus placeholder="Jumlah Peserta" />
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" name="tanggalberangkat" id="tanggalberangkat" placeholder="Tanggal Berangkat" />
                      </div>
                      <input type="hidden" name="customer_id" id="customer_id" value="<?php echo e(isset(Auth::user()->id) ? Auth::user()->id : NULL); ?>" />
                      <input type="hidden" name="bookingstatus" id="bookingstatus" value="2" />
                      <input type="hidden" name="slug" id="slug" value="<?php echo e(Request::segment(2)); ?>" />
<!--  -->

<!--  -->





                      

                      <table>
                        <thead>
                          <tr>
                            <th><h2>Total : </h2></th>
                            <th><h2><div class="showfinalprice"></div></h2></th>
                          </tr>
                        </thead>
                      </table>

                      <input type="submit" value="Book Now!"  class="btn btn-book btn-lg" style="width: 100%;">
                    </form>
                                       
                  </div>
                </div>
              </div>

              <div class="card bg-default">
                <div class="card-body text-center">
                  <?php if(empty($member->profile_img)): ?>
                  <img src="<?php echo e(asset('www/assets/images/default.png')); ?>" alt="" class="img-responsive" style="width: 100px;height:100px;" />
                  <?php else: ?>
                  <img src="<?php echo e(asset('www/assets/communities/avatars/'.$member->profile_img)); ?>" alt="" class="img-responsive" style="width: 100px;height:100px;" />
                  <?php endif; ?>
                  <br>
                  <small>Guide : </small><label for=""><?php echo e($detailtour['member_id'] == $member['id'] ? $member['first_name'] ." ". $member['last_name'] : ""); ?></label>
                  <small>from </small><label for=""><?php echo e($detailtour['community_id'] == $community['id'] ? $community['community_name'] : ""); ?> </label> Community
                  <br>
                  <small>Bahasa : </small><label for="">Indonesia, English</label>
                  <div class="text-center">
                    <small>Rating</small>
                    <div class="stars">
                      <?php $rating = $detailtour->rating; ?> 

                      <?php $__currentLoopData = range(1,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <span class="fa-stack rating" style="width:1em">
                            
                            <?php if($rating > 0): ?>
                                <?php if($rating > 0.5): ?>
                                    <i class="fa fa-star" style="color: orange;"></i>
                                <?php else: ?>
                                    <i class="fa fa-star-half" style="color: orange;"></i>
                                <?php endif; ?>
                            <?php else: ?>
                                <i class="fa fa-star-o"></i>
                            <?php endif; ?>
                            <?php $rating-- ?>
                        </span>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <p class="rating-wisata"> Rate <?php echo e($detailtour->rating / 1); ?> <small> / </small> 5 </p>
                    
                  </div>
                </div>
              </div>
            </div>
                      
            <div class="col-md-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Deskripsi
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <?php echo $detailtour->description; ?>

                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#priceDetail" aria-expanded="false" aria-controls="priceDetail">
                          Rincian Harga
                        </a>
                      </h4>
                    </div>
                    <div id="priceDetail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        <?php echo $detailtour->pricedetail; ?>

                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#tripPlans" aria-expanded="false" aria-controls="tripPlans">
                          Rencana Perjalanan
                        </a>
                      </h4>
                    </div>
                    <div id="tripPlans" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        <?php echo $detailtour->tripplans; ?>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="" id="chat" aria-expanded="false" aria-controls="tripPlans">
                          Chat
                        </a>
                      </h4>
                    </div>
                  </div>

                </div>

            </div>
              
            <div class="col-sm-12">
              <h2 class="page-header">Comments</h2>
              <section class="comment-list">
                <!-- First Comment -->
                <article class="row">
                  <div class="col-md-2 col-sm-2">
                    <figure class="thumbnail">
                      <img class="img-responsive" src="<?php echo e(asset('www/assets/profile_images/rotating_card_profile.png')); ?>" />
                      <figcaption class="text-center">username</figcaption>
                    </figure>
                  </div>
                  <div class="col-md-10 col-sm-10">
                    <div class="card bg-default arrow left">
                      <div class="card-body">
                        <header class="text-left">
                          <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                          <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                            Dec 16, 2014</time>
                          </header>
                          <div class="comment-post">
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                              eiusmod tempor
                              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              quis
                              nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat.
                            </p>
                          </div>
                          <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                          </div>
                        </div>
                      </div>
                    </article>
                    <!-- Second Comment Reply -->
                    <article class="row">
                      <div class="col-md-2 col-sm-2">
                        <figure class="thumbnail">
                          <img class="img-responsive" src="<?php echo e(asset('www/assets/profile_images/rotating_card_profile.png')); ?>" />
                          <figcaption class="text-center">username</figcaption>
                        </figure>
                      </div>
                      <div class="col-md-10 col-sm-10">
                        <div class="card bg-default arrow left">
                          <div class="card-heading right">Reply</div>
                          <div class="card-body">
                            <header class="text-left">
                              <div class="comment-user"><i class="fa fa-user"></i> That Guy</div>
                              <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i>
                                Dec 16, 2014</time>
                              </header>
                              <div class="comment-post">
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                  eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                  quis
                                  nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                  consequat.
                                </p>
                              </div>
                              <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                reply</a></p>
                              </div>
                            </div>
                          </div>
                        </article>
                      </section>
                    </div>


                    <div class="col-sm-12">
                      <br><br><br>
                      <div class="text-center">
                        <h2>Trip Lainya dari Guide <strong><?php echo e($member['first_name'] . " " . $member['last_name']); ?></strong></h2>
                      </div>
                      <hr>
                      
                      <?php $__currentLoopData = $relatedtour_by_member; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kbm => $vbm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($vbm->slug != Request::segment(2)): ?>
                          <div class="col-sm-4 col-md-3">
                            <div class="thumbnail">
                              <?php 
                                  $pics = json_decode($vbm->images); 
                              ?>
                              <div class="tourslide">
                              <?php $__currentLoopData = $pics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div><img class="media-list-img" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt=""></div>                                    
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                              <div class="caption">
                                <h3><?php echo e($vbm->title); ?></h3>
                                <p>Rp. <?php echo e(empty($vbm->promo_price) ? number_format($vbm->actual_price,0,",",".") : number_format($vbm->promo_price,0,",",".")); ?></p>
                                <p><a href="<?php echo e(url('tour/'.$vbm->slug)); ?>" class="btn btn-primary" role="button" style="width: 100%;">Detail</a></p>
                              </div>
                            </div>
                          </div>
                        <?php else: ?>
                          <div class="text-center">
                            <br>
                              <p>Belum ada trip lain</p>
                            <br>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                </div>
              </div>

  <!-- Chat -->
<?php if(Auth::guard('pelanggan')->check()): ?>
<!-- Modal -->
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Pesan dari <?php echo e($member->first_name.' '.$member->last_name); ?> </h5>
      </div>
      <div class="modal-body" id="chatToGuide">
        <div class="panel-body">
          <terima-pesan :messages="messagesforCustomer" :customer_id="<?php echo e(Auth::guard('pelanggan')->user()->id); ?>" :member_id="<?php echo e($member->id); ?>"></terima-pesan>
        </div>
        <div class="panel-footer">
          <kirim-pesan v-on:messagesent="addMessage" :sender="'<?php echo e(Auth::guard('pelanggan')->user()->first_name.' '.Auth::guard('pelanggan')->user()->last_name); ?>'" :customer_id="<?php echo e(Auth::guard('pelanggan')->user()->id); ?>" :member_id="<?php echo e($member->id); ?>"></kirim-pesan>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- endchat -->
<?php endif; ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>    
<?php if(Auth::guard('pelanggan')->check()): ?>

<script>
  $(function(){
    $('#chat').click(function(){

        var customer = "<?php echo e(Auth::guard('pelanggan')->user()->id); ?>";
        var guide = '<?php echo e($member->id); ?>';
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          method: 'post',
          url: '/chat/start',
          data:{guide:guide, customer:customer},
          success: function(data){
            console.log(data);
            var privateChatId = data;
          }
        });
        $('#chatModal').modal('show');
      
    });
  });
  
</script>
<?php endif; ?>
<script>

  function tourprice()
  {
    var peserta = $('#jumlahpeserta').val();
    var harga = "<?php echo e(empty($detailtour->promo_price) ? $detailtour->actual_price : $detailtour->promo_price); ?>";

    var total = harga * peserta;

    $('.showfinalprice').text("Rp. "+total);
  }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>