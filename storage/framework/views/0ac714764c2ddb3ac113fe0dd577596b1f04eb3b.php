
<?php $__env->startSection('content'); ?>

<div class="gtco-section gtco-products">
  <div class="gtco-container">
    <div class="row row-pb-sm">
      <div class="col-12 gtco-heading text-center">
        <h2>Tour berdasarkan Provinsi</h2>
      </div>
    </div>
    <div class="row"> 
      <?php $__currentLoopData = $tourinprovinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ktip => $vtip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kp => $vp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php if($vtip->province_id == $vp->id): ?>
            <div class="col-md-3">
              <div class="card">

                  <?php 
                    $pics = json_decode($vtip->images);
                    $rpic = array_slice($pics,0,1); 
                  ?>

                  
                    <?php $__currentLoopData = $rpic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div><img class="img-responsive" src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" alt="" style="width: 100%;"></div>                                    
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                <a href="<?php echo e(url('tour/province/'. $vp->slug )); ?>">
                    <div class="card-body text-center">
                      <div class="row">
                        <div class="col-12">
                          <span class="text-muted"><?php echo e($vtip->province_id == $vp->id ? $vp->name : ""); ?></span>
                        <br>
                          <span class="text-muted">Start from</span>
                          <div class="text-harga">Rp. <?php echo e(number_format($vtip->actual_price,0,",",".")); ?></div>
                        </div>
                      </div>
                    </div>
                </a>
              </div>
            </div>
          <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>