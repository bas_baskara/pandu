<?php $__env->startSection('content'); ?>

<header role="banner">
  

  <div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?php echo e(URL::asset('www/assets/blog_images/bali.jpg')); ?>" class="d-block w-100" alt="bali">
        <div class="carousel-caption d-none d-md-block">
          <h1 class="main-bg-text">
            Guide others to happiness
          </h1>
        </div>
      </div>
      <div class="carousel-item">
        <img src="<?php echo e(URL::asset('www/assets/blog_images/coming.jpg')); ?>" class="d-block w-100" alt="bali">
        
      </div>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</header>

<!-- END #gtco-header -->

<?php if(!Auth::guard('pelanggan','member')->user()): ?> 

<div class="gtco-client">
  <div class="row">
    <div class="col-6" style="margin: 0 auto;">
      <form action="<?php echo e(url('search')); ?>" method="GET" autocomplete="off">
        <div class="form-group text-center">
            <label for="name"><b>Mau liburan kemana?</b></label>
            <input type="text" class="form-control " id="search_city" name="cari" placeholder=" misal : Bandung ">
            
            <br>
            <input type="submit" value="Search" class="btn btn-info btn-lg">  
          </div>
          <?php echo e(csrf_field()); ?>

        </form>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      
      <div class="col-md-12 gtco-heading text-center">
        <h2 style="font-weight:800;">Wisata Terpopuler</h2>
        <hr>
      </div>
      
      <?php $__currentLoopData = $tour; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ktour => $vtour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="col-md-3">
        <div class="card ">
          <div class="social-card-header align-middle text-center">

            <?php 
              $pics = json_decode($vtour->images); 
              $rpic = array_slice($pics,0,1);
            ?>

            <?php $__currentLoopData = $rpic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <img src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" class="image" alt="<?php echo e($vtour->title); ?>">
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <hr>
          <a href="<?php echo e(url('tour/province/')); ?>">
          <div class="card-body text-center">
            <div class="row">
              <div class="col-12">
                <span class="text-muted">
                <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kp => $vp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($vtour->province_id == $vp->id): ?>
                    <?php echo e($vp->name); ?>

                  <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </span>
                <!-- <div class="text-harga"><?php echo e($vtour->province_id); ?> Wisata</div> -->
              <br>
                <span class="text-muted">mulai dari</span>
                <div class="text-harga">
                  <?php if($vtour->actual_price >= 100000): ?>
                    <?php echo e("100k an"); ?>

                  <?php elseif($vtour->actual_price >= 200000): ?>
                    <?php echo e("200k an"); ?>

                  <?php elseif($vtour->actual_price >= 300000): ?>
                    <?php echo e("300k an"); ?>

                  <?php elseif($vtour->actual_price >= 400000): ?>
                    <?php echo e("400k an"); ?>

                  <?php elseif($vtour->actual_price >= 500000): ?>
                    <?php echo e("500k an"); ?>

                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    
    <center>
      <a href="<?php echo e(URL::to('/tour')); ?>" class="btn btn-primary">Lebih banyak lagi</a>
    </center>
    
  </div>

  <br>          
  <div class="gtco-section gtco-products">
    <div class="gtco-container">
      <div class="row row-pb-sm">
        <div class="col-12 gtco-heading text-center">
          <h2 style="font-weight:800;">Artikel terbaru</h2>
          <hr>
        </div>
        
        <div class="container">
          <div class="row">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
              <div class="column">
                <!-- Post-->
                <div class="post-module">
                  <!-- Thumbnail-->
                  <div class="thumbnail">
                    <div class="date">
                      <div class="day"><?php echo e($post->created_at->format('d')); ?></div>
                      <div class="month"><?php echo e($post->created_at->format('M')); ?></div>
                    </div>
                    <img src="<?php echo e(URL::asset('www/assets/blog_images/'.$post->image)); ?>" alt="<?php echo e($post->title); ?>">
                  </div>
                  <!-- Post Content-->
                  <div class="post-content">
                    
                    <h1 class="title">
                      <a href="<?php echo e(url('blog', $post->slug)); ?>"><?php echo e($post->title); ?></a>
                    </h1>
                    
                    <div class="post-meta">
                      <span class="timestamp">
                        <i class="fa fa-clock-"></i><?php echo e($post->created_at->diffForHumans()); ?></span>
                                    
                        <span class="views">
                          <i class="fa fa-user"></i>
                          <a href="#"> <?php echo e($post->visitor); ?> Views</a>
                        </span>
                        <a href="<?php echo e(url('blog',$post->slug)); ?>" style="color:red;">Selengkapnya</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <div class="col-md-12 text-center">
                <p>
                  <a href="<?php echo e(url('blog')); ?>" class="btn btn-special">Load More</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="gtco-section gtco-products" >
      <div class="gtco-container">
        <div class="row row-pb-sm">
          <div class="col-12 gtco-heading text-center">
            <h2 style="font-weight:800;">Artikel Komunitas terbaru</h2>
            <hr>
          </div>
          
          <div class="container">
            <div class="row">
              <?php $__currentLoopData = $pm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="col-sm-6 col-lg-4">
                <div class="komunitas_card home__head">
                  <img src="<?php echo e(URL::asset('www/assets/communities/blog_images/'.$po->image)); ?>" alt="<?php echo e($po->title); ?>" class="home__img">
                  <h5 class="home__name" ><?php echo e($po->title); ?></h5>
                  
                  <a class="btn home__btn" href="<?php echo e(url('komunitas/blog/'.$po->slug)); ?>">Selengkapnya</a>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
    <?php endif; ?>
    
    <?php if( Auth::guard('pelanggan','member')->user()): ?> 
    
    
    <div class="container">
      
      <div class="row">
        
        <div class="cardPostView" style="padding:15px; border-radius:15px;">
          <div class="gtco-heading ">
            <a href="#" ><span class="label label-danger pull-right">logout</span></a>
            <h3 style="font-weight:800;">Hello <?php echo e(Auth::guard('pelanggan')->user()->username); ?>.</h3>
            <p> Nikmati liburan-mu bersama Panduasia.</p>
            <hr>
          </div>
          <ol class="breadcrumb text-center">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Wishlist</a></li>
            <li><a href="#">Transaksi </a></li>
            <li><a href="#">Chat</a></li>
            <li><a href="#">Menunggu Pembayaran</a></li>
            <li><a href="#">Ulasan</a></li>
          </ol>
          <br>
          <br>
          <br>
        </div>
        
        <div class="col-md-8 search-wisata">
          <div class="cardPostView"  >
            <div class="bs-callout bs-callout-primary">
              <h4>Pencarian</h4>
              <form class="form-inline">
                <div class="form-group ">
                  <input type="text " class="form-control col-lg-6" placeholder=' misal: "Bandung"' name="cari">
                </div>
                <div class="form-group" style="margin-left:55px;">
                  <label for="pwd">Kategori</label>
                  <select class="form-control" id="sel1">
                    <option>Pantai</option>
                    <option>Pegunungan</option>
                    <option>Keluarga</option>
                    <option>Romantis</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-danger "><i class="fa fa-search" aria-hidden="true"></i></button>
              </form>
            </div>
          </div>
        </div>
        
        
        
        <div class="col-12 gtco-heading text-center">
          <h2 style="font-weight:800;">Wisata Terpopuler</h2>
          <hr>
        </div>
        
        <?php $__currentLoopData = $tour; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ktour => $vtour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-3">
          <div class="card ">
            <div class="social-card-header align-middle text-center">

            <?php 
              $pics = json_decode($vtour->images); 
              $rpic = array_slice($pics,0,1);
            ?>

            <?php $__currentLoopData = $rpic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <img src="<?php echo e(asset('www/assets/tour_images/'.$pic)); ?>" class="image" alt="<?php echo e($vtour->title); ?>">
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <hr>
          <a href="<?php echo e(url('tour/province/')); ?>">
          <div class="card-body text-center">
            <div class="row">
              <div class="col-12">
                <span class="text-muted">
                <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kp => $vp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($vtour->province_id == $vp->id): ?>
                    <?php echo e($vp->name); ?>

                  <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </span>
                <div class="text-harga"><?php echo e($vtour->province_id); ?> Wisata</div>
              <br>
                <span class="text-muted">mulai dari</span>
                <div class="text-harga">
                  <?php if($vtour->actual_price >= 100000): ?>
                    <?php echo e("100k an"); ?>

                  <?php elseif($vtour->actual_price >= 200000): ?>
                    <?php echo e("200k an"); ?>

                  <?php elseif($vtour->actual_price >= 300000): ?>
                    <?php echo e("300k an"); ?>

                  <?php elseif($vtour->actual_price >= 400000): ?>
                    <?php echo e("400k an"); ?>

                  <?php elseif($vtour->actual_price >= 500000): ?>
                    <?php echo e("500k an"); ?>

                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
      </div>
      
      <center>
        <a href="<?php echo e(URL::to('/wisata')); ?>" class="btn btn-primary">Lebih banyak lagi</a>
      </center>
      
    </div>
    
    <br>
    
    
    <div class="col-12 gtco-heading text-center">
      <h2 style="font-weight:800;">Rekomendasi Destinasi Wisata</h2>
      
    </div>
    
    <div class="container">
      <div class="row">
        <div class="responsive " style="margin-top :50px;">
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Surabaya</span>
                    <div class="font-weight-bold">24 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5 ">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Pangandaran</span>
                    <div class="font-weight-bold">9 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">150K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Banten</span>
                    <div class="font-weight-bold">5 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Bandung</span>
                    <div class="font-weight-bold">12 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">130K</div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Makassar</span>
                    <div class="font-weight-bold">8 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">138K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
                alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Lombok</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">420K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
        </div>
        
      </div>
    </div>
    
    
    
    <div class="gtco-section gtco-products" >
      <div class="gtco-container">
        <div class="row row-pb-sm">
          <div class="col-md-8 col-md-offset-2 gtco-heading text-center">
            <h2 style="font-weight:800;">Artikel Komunitas terbaru</h2>
            <hr>
          </div>
          
          <div class="container">
            <div class="row">
              <?php $__currentLoopData = $pm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="col-sm-6 col-lg-4">
                <div class="komunitas_card home__head">
                  <img src="<?php echo e(URL::asset('www/assets/communities/blog_images/'.$po->image)); ?>" alt="<?php echo e($po->title); ?>" class="home__img">
                  <h5 class="home__name" ><?php echo e($po->title); ?></h5>
                  
                  <a class="btn home__btn" href="<?php echo e(url('komunitas/blog/'.$po->slug)); ?>">Selengkapnya</a>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
    
    <div class=" gtco-heading" style="margin-left:73px;">
      <h2 style="font-weight:800;">Tempat Wisata Baru </h2>
    </div>
    
    <div class="container">
      <div class="row">
        <div class="responsive slider" style="margin-top :50px;">
          <div class="col-md-3 mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Surabaya</span>
                    <div class="font-weight-bold">24 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5 ">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Pangandaran</span>
                    <div class="font-weight-bold">9 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">150K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Banten</span>
                    <div class="font-weight-bold">5 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Bandung</span>
                    <div class="font-weight-bold">12 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">130K</div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Makassar</span>
                    <div class="font-weight-bold">8 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">138K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
                alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Lombok</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">420K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
        </div>
        
      </div>
    </div>
    
    
    <div class=" gtco-heading " style="margin-left:73px;">
      <h2 style="font-weight:800;">Liburan ke Pantai</h2>
    </div>
    
    <div class="container">
      <div class="row">
        <div class="responsive slider " style="margin-top :50px;">
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Surabaya</span>
                    <div class="font-weight-bold">24 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5 ">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Pangandaran</span>
                    <div class="font-weight-bold">9 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">150K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Banten</span>
                    <div class="font-weight-bold">5 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Bandung</span>
                    <div class="font-weight-bold">12 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">130K</div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Makassar</span>
                    <div class="font-weight-bold">8 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">138K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
                alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Lombok</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">420K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
        </div>
        
      </div>
    </div>
    
    <div class="gtco-heading"  style="margin-left:73px;">
      <h2 style="font-weight:800;">Liburan Keluarga</h2>
    </div>
    
    <div class="container">
      <div class="row">
        <div class="responsive slider " style="margin-top :50px;">
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://corporate.larizhotels.com/wp-content/uploads/2018/01/Berwisata-Di-Klenteng-Sanggar-Agung-Surabaya3-fix.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Surabaya</span>
                    <div class="font-weight-bold">24 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5 ">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://3.bp.blogspot.com/-iFw2LH6cNe0/Vd1xRgCwi4I/AAAAAAAAAYE/ywtkGdsYirg/s1600/gua_greencanyon.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Pangandaran</span>
                    <div class="font-weight-bold">9 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">150K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="http://pesonawisataindonesia.com/wp-content/uploads/2015/08/Pesona-Wisata-Banten-Pulau-Burung1.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Banten</span>
                    <div class="font-weight-bold">5 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="https://www.arififandi.com/wp-content/uploads/2018/08/up-2-3.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Bandung</span>
                    <div class="font-weight-bold">12 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">130K</div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="https://pojoksatu.id/wp-content/uploads/2017/05/makassar.jpg" class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Makassar</span>
                    <div class="font-weight-bold">8 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">138K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center">
                <img src="  https://media-cdn.tripadvisor.com/media/photo-s/10/92/97/16/borobudur-statue.jpg" class="image"
                alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    <span class="text-muted">Yogyakarta</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">220K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-3  mt-5">
            <div class="card">
              <div class="social-card-header align-middle text-center ">
                <img src="http://www.netralnews.com/foto/2018/02/04/446-pulau_komodo_salah_satu_destinasi_wisata_andalan_indonesia.jpg"
                class="image" alt="">
              </div>
              <hr>
              <div class="card-body text-center">
                <div class="row">
                  <div class="col border-right">
                    
                    <span class="text-muted">Lombok</span>
                    <div class="font-weight-bold">16 Wisata</div>
                  </div>
                  <div class="col">
                    
                    <span class="text-muted">Start from</span>
                    <div class="font-weight-bold">420K</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
    
    <?php endif; ?>        
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>