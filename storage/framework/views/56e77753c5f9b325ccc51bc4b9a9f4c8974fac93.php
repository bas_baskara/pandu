    <div class="gtco-container">

      <div class="row">
        <div class="col-2">
          <div id="gtco-logo">
            <a href="<?php echo e(URL::to('/')); ?>">
              <img src="<?php echo e(URL::asset('www/assets/images/logo.png')); ?>" alt="logo panduasia">
            </a>
          </div>
        </div>

        <div class="col-10 text-right menu-1"  style="margin-top:15px;">
          <ul>
            <li class="active">
              <a href="<?php echo e(URL::to('/')); ?>">Home</a>
            </li>
            <li class="has-dropdown">
              <a href="javascript:;">Cara kerja</a>
              <ul class="dropdown">
                <li><a href="<?php echo e(URL::to('p/panduan-menjadi-member-komunitas-di-panduasia')); ?>">Panduan Member</a></li>
                <li><a href="<?php echo e(URL::to('p/panduan-mencari-tour-guide-di-panduasia')); ?>">Panduan Mencari Tour Guide</a></li>
                <li><a href="<?php echo e(URL::to('p/panduan-menjadi-tour-guide-di-panduasia')); ?>">Panduan Menjadi Tour Guide</a></li>
                
              </ul>
            </li>
            <li>
              <a href="<?php echo e(URL::to('/blog')); ?>">Blog</a>
            </li>
            
            <li>
              <a href="<?php echo e(URL::to('/komunitas/blog')); ?>" >Komunitas</a>
            </li>
            <li>
              <a href="<?php echo e(URL::to('/member/register')); ?>">Be a Tourguide</a>
            </li>
            <li>
              <a href="<?php echo e(URL::to('/about')); ?>">About</a>
            </li>
            
            <li class="has-dropdown">
              <?php if(Auth::guard('member')->user() && Auth::guard('member')->user()['ismember'] == 1): ?>
              <a href="javascript:;"><?php echo e(Auth::guard('member')->user()->username); ?></a>
              <ul class="dropdown">
                <li>
                  <?php if(Request::segment(1) == 'community'): ?>
                  <a href="<?php echo e(url('member/home')); ?>">My Dashboard</a>
                  <?php else: ?>
                  <a href="<?php echo e(url('community/home')); ?>">Komunitas Dashboard</a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('member/message')); ?>">Inbox</a>
                  <a href="<?php echo e(url('member/profile/'.Auth::guard('member')->user()->id)); ?>">My Profile</a>
                  <a href="<?php echo e(url('member/profile/'.Auth::guard('member')->user()->id.'/password')); ?>">Change Password</a>
                  <a href="<?php echo e(url('community/reviews')); ?>">Review</a>
                  <br/>
                  <a href="<?php echo e(url('member/logout')); ?>">Sign Out</a>
                </li>
              </ul>
              <?php elseif(Auth::guard('member')->user() && Auth::guard('member')->user()['isguide'] == 1): ?>
              <a href="javascript:;"><?php echo e(Auth::guard('member')->user()->username); ?></a>
              <ul class="dropdown">
                <li>
                  <?php if(Request::segment(1) == 'community'): ?>
                  <a href="<?php echo e(url('member/home')); ?>">My Dashboard</a>
                  <?php else: ?>
                  <a href="<?php echo e(url('community/home')); ?>">Komunitas Dashboard</a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('member/tour')); ?>">My Tour</a>
                  <a href="<?php echo e(url('member/message')); ?>">Message</a>
                  <a href="<?php echo e(url('member/transactions')); ?>">Transaksi</a>
                  <a href="<?php echo e(url('member/profile/'.Auth::guard('member')->user()->id)); ?>">My Profile</a>
                  <a href="<?php echo e(url('community/reviews')); ?>">Review</a>
                  <a href="<?php echo e(url('member/profile/'.Auth::guard('member')->user()->id.'/password')); ?>">Change Password</a>
                  <br/>
                  <a href="<?php echo e(url('member/logout')); ?>">Sign Out</a>
                </li>
              </ul>
              
              <?php elseif(Auth::guard('member')->user() && Auth::guard('member')->user()['isleader'] == 1): ?>
              <a href="javascript:;"><?php echo e(Auth::guard('member')->user()->username); ?></a>
              <ul class="dropdown">
                <li>
                  <?php if(Request::segment(1) == 'community'): ?>
                  <a href="<?php echo e(url('member/home')); ?>">My Dashboard</a>
                  <?php else: ?>
                  <a href="<?php echo e(url('community/home')); ?>">Komunitas Dashboard</a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('member/profile/'.Auth::guard('member')->user()->id )); ?>">My Account</a>
                  <a href="<?php echo e(url('community/members')); ?>">My Member</a>
                  <a href="<?php echo e(url('community/transaksi')); ?>">Transaksi Member</a>
                  <a href="<?php echo e(url('member/message')); ?>">Message</a>
                  <a href="<?php echo e(url('community/blog')); ?>">Blog</a>
                  <a href="<?php echo e(url('community/reviews')); ?>">Review</a>
                  <br/>
                  <a href="<?php echo e(url('member/logout')); ?>">Sign Out</a>
                </li>
              </ul>
              <?php elseif(Auth::guard('pelanggan')->user()): ?>
              <a href="#"><?php echo e(Auth::guard('pelanggan')->user()->username); ?></a>
              <ul class="dropdown">
                <li>
                  <a href="<?php echo e(URL('user/profile/'.Auth::guard('pelanggan')->user()->username.'/edit')); ?>">Edit Profile</a>
                  <a href="#">Transaksi</a>
                  <a href="<?php echo e(URL('user/signout')); ?>">Logout</a>
                </li>
              </ul>
              <?php elseif(!Auth::user()): ?>
              <a href="#">Masuk</a>
              <ul class="dropdown">
                <li>
                  <a href="<?php echo e(URL('user/signin')); ?>">Login</a>
                  <a href="<?php echo e(URL('user/register')); ?>">Register</a>
                </li>
              </ul>
              <?php endif; ?>
              
            </ul>
          </div>
        </div>
</div>

