<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number_post')->default(10);
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->text('webmaster')->nullable();
            $table->text('analytics')->nullable();
            $table->text('facebook_pixel')->nullable();
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->string('copyright')->nullable();
            $table->string('metatitle')->nullable();
            $table->text('metakeyword')->nullable();
            $table->string('metadescription')->nullable();
            $table->string('meta_title_blog')->nullable();
            $table->text('meta_keyword_blog')->nullable();
            $table->string('meta_desc_blog')->nullable();
            $table->string('meta_title_about')->nullable();
            $table->text('meta_keyword_about')->nullable();
            $table->string('meta_desc_about')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
