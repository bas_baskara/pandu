<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('category');
            $table->text('description')->nullable();
            $table->string('tag')->nullable();
            $table->string('pageataupost')->nullable();
            $table->boolean('status')->default(0);
            $table->string('posted_by')->nullable();
            $table->string('image')->nullable();
            $table->integer('visitor')->default(0);
            $table->string('metatitle')->nullable();
            $table->string('metakeyword')->nullable();
            $table->string('metadescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
