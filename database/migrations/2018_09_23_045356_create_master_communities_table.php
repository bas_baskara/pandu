<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_communities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('community_name');
            $table->string('community_identity');
            $table->text('description')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('banner_image')->nullable();
            $table->string('join_cities',50)->nullable();
            $table->boolean('status')->default(0);
            $table->integer('member')->default(0);
            $table->integer('guide')->default(0);
            $table->integer('leader')->default(0);
            $table->integer('review')->default(0);
            $table->integer('transaksi_sukses')->default(0);
            $table->integer('transaksi_gagal')->default(0);
            $table->string('hp',14)->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->string('metatitle')->nullable();
            $table->text('metakeyword')->nullable();
            $table->string('metadescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_communities');
    }
}
