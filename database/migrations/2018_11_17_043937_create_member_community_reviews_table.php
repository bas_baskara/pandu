<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberCommunityReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_community_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->integer('community_id');
            $table->text('description')->nullable();
            $table->decimal('rating',5,1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_community_reviews');
    }
}
