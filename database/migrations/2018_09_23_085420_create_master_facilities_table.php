<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facility_name');
            $table->text('description')->nullable();
            $table->integer('estimate_price')->default(0);
            $table->integer('discount')->default(0);
            $table->integer('final_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_facilities');
    }
}
