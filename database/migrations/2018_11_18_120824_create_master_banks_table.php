<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bca_title')->nullable();
            $table->string('bca_rek')->nullable();
            $table->string('bca_owner')->nullable();
            $table->boolean('bca_status')->default(0);
            $table->string('mandiri_title')->nullable();
            $table->string('mandiri_rek')->nullable();
            $table->string('mandiri_owner')->nullable();
            $table->boolean('mandiri_status')->default(0);
            $table->string('bri_title')->nullable();
            $table->string('bri_rek')->nullable();
            $table->string('bri_owner')->nullable();
            $table->boolean('bri_status')->default(0);
            $table->string('bni_title')->nullable();
            $table->string('bni_rek')->nullable();
            $table->string('bni_owner')->nullable();
            $table->boolean('bni_status')->default(0);
            $table->string('panin_title')->nullable();
            $table->string('panin_rek')->nullable();
            $table->string('panin_owner')->nullable();
            $table->boolean('panin_status')->default(0);
            $table->string('mega_title')->nullable();
            $table->string('mega_rek')->nullable();
            $table->string('mega_owner')->nullable();
            $table->boolean('mega_status')->default(0);
            $table->string('permata_title')->nullable();
            $table->string('permata_rek')->nullable();
            $table->string('permata_owner')->nullable();
            $table->boolean('permata_status')->default(0);
            $table->string('bii_title')->nullable();
            $table->string('bii_rek')->nullable();
            $table->string('bii_owner')->nullable();
            $table->boolean('bii_status')->default(0);
            $table->string('dbs_title')->nullable();
            $table->string('dbs_rek')->nullable();
            $table->string('dbs_owner')->nullable();
            $table->boolean('dbs_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_banks');
    }
}
