<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('community_name');
            $table->string('community_identity');
            $table->text('description')->nullable();
            $table->string('province');
            $table->string('city');
            $table->string('profile_image')->nullable();
            $table->string('banner_image')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('member')->default(0);
            $table->integer('guide')->default(0);
            $table->integer('leader')->default(0);
            $table->integer('review');
            $table->string('hp',14);
            $table->string('email');
            $table->string('website');
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->string('copyright')->nullable();
            $table->string('metatitle')->nullable();
            $table->text('metakeyword')->nullable();
            $table->string('metadescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities');
    }
}
