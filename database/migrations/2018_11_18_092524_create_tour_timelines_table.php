<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTimelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_timelines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id');
            $table->integer('destination_id');
            $table->integer('inc_exc_id')->nullable();
            $table->integer('status')->nullable();
            $table->decimal('progress_percentage')->nullable();
            $table->text('progress_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_timelines');
    }
}
