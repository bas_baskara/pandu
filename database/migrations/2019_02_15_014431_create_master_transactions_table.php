<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('tour_id');
            $table->integer('member_id');
            $table->integer('community_id');
            $table->integer('booking_id');
            $table->integer('status')->default(0);
            $table->datetime('paid_date')->nullable();
            $table->datetime('complete_date')->nullable();
            $table->datetime('canceled_date')->nullable();
            $table->integer('rating')->default(0);
            $table->text('review')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_transactions');
    }
}
