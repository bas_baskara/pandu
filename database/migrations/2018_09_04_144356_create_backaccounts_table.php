<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backaccounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guide_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('city')->nullable();
            $table->string('kantor_cabang')->nullable();
            $table->string('nomor_rekening')->nullable();
            $table->text('ktp_img')->nullable();
            $table->string('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backaccounts');
    }
}
