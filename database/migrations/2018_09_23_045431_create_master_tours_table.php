<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->integer('category')->nullable();
            $table->text('description')->nullable();
            $table->text('pricedetail')->nullable();
            $table->text('tripplans')->nullable();
            $table->integer('destination')->nullable();
            $table->string('includes')->nullable();
            $table->string('excludes')->nullable();
            $table->integer('actual_price')->nullable();
            $table->integer('promo_price')->nullable();
            $table->integer('fake_price')->nullable();
            $table->text('images')->nullable();
            $table->boolean('trippackage')->default(0);
            $table->boolean('tipe')->default(0);
            $table->integer('min_kuota')->default(0);
            $table->integer('max_kuota')->default(0);
            $table->boolean('status')->default(0);
            $table->boolean('ispromo')->default(0);
            $table->integer('rating')->default(0);
            $table->integer('country_id')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('member_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_tours');
    }
}
