<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username',150)->unique();
            $table->string('email',150)->unique();
            $table->string('password');
            $table->string('mobilephone',12)->unique();
            $table->string('phone',12)->unique();
            $table->text('address')->nullable();
            $table->integer('city');
            $table->integer('province');
            $table->integer('country')->default(99);
            $table->string('postalcode',5)->nullable();
            $table->text('profile_img')->nullable();
            $table->text('description')->nullable();
            $table->text('biography')->nullable();
            $table->string('license_id',16)->unique()->nullable();
            $table->text('license_id_img')->nullable();
            $table->string('c_license', 12)->unique()->nullable();
            $table->text('c_license_img')->nullable();
            $table->string('a_license', 12)->unique()->nullable();
            $table->text('a_license_img')->nullable();
            $table->string('passport',100)->unique()->nullable();
            $table->text('passport_img')->nullable();
            $table->string('bank_name',100);
            $table->string('account_name',100);
            $table->string('bank_acc_number',25);
            $table->string('bank_area')->nullable();
            $table->string('bank_additional')->nullable();
            $table->integer('status')->default(0);
            $table->boolean('ismember')->default(1);
            $table->boolean('isguide')->default(0);
            $table->boolean('isleader')->default(0);
            $table->string('activation_code')->nullable();
            $table->integer('rating')->default(0);
            $table->integer('review')->default(0);
            $table->integer('transaski_sukses')->nullable();
            $table->integer('transaski_gagal')->nullable();
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->string('linkedin')->default('https://wwww.linkedin.com/');
            // $table->string('avatar')->nullable();
            // $table->string('provider', 20)->nullable();
            // $table->string('provider_id')->nullable();
            // $table->string('access_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_members');
    }
}
