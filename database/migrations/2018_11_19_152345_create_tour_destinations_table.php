<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('province_id');
            $table->integer('city_id');
            $table->string('main_city')->nullable();
            $table->text('main_description')->nullable();
            $table->integer('tour_category');
            $table->string('place')->nullable();
            $table->text('location')->nullable();
            $table->text('info')->nullable();
            $table->string('qr_code')->nullable();
            $table->text('main_image_tours')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_destinations');
    }
}
