<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->default(0);
            $table->integer('customer_id')->default(0);
            $table->integer('community_id')->default(0);
            $table->integer('post_id')->default(0);
            $table->integer('tour_id')->default(0);
            $table->integer('comment_id')->default(0);
            $table->integer('review_id')->default(0);
            $table->decimal('review',5,1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_ratings');
    }
}
