<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterGuidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_guides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username',150)->unique();
            $table->string('email',150)->unique();
            $table->string('password');
            $table->string('mobilephone',12)->unique();
            $table->string('phone',12)->unique();
            $table->text('address')->nullable();
            $table->integer('city');
            $table->integer('province');
            $table->integer('country');
            $table->string('postalcode',5);
            $table->text('profile_img')->nullable();
            $table->text('description')->nullable();
            $table->text('biography')->nullable();
            $table->string('license_id',16)->unique();
            $table->text('license_id_img');
            $table->string('c_license', 12)->unique();
            $table->text('c_license_img');
            $table->string('a_license', 12)->unique();
            $table->text('a_license_img');
            $table->string('passport',100)->unique();
            $table->text('passport_img')->nullable();
            $table->integer('status')->default(0);
            $table->boolean('ismember')->default(1);
            $table->boolean('isguide')->default(0);
            $table->boolean('isleader')->default(0);
            $table->string('activation_code')->nullable();
            $table->string('rating')->default(0);
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->string('linkedin')->default('https://wwww.linkedin.com/');
            $table->integer('comment_id')->nullable();
            $table->integer('withdraw_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_guides');
    }
}
