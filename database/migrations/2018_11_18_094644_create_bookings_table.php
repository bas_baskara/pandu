<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable();
            $table->integer('tour_id');
            $table->integer('tour_team_id')->nullable();
            $table->integer('number_of_team')->default(1);
            $table->integer('status')->nullable();
            $table->datetime('booking_date')->nullable();
            $table->datetime('departure_date');
            $table->datetime('paid_date')->nullable();
            $table->datetime('complete_date')->nullable();
            $table->datetime('canceled_date')->nullable();
            $table->string('whoscanceled')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
