<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityProvinceCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_province_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id');
            $table->integer('city_id');
            $table->integer('province_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('join_cities',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_province_cities');
    }
}
