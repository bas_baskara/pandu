<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username',150)->unique();
            $table->string('email',150)->unique();
            $table->string('password');
            $table->string('mobilephone',12)->unique();
            $table->text('address')->nullable();
            $table->integer('city');
            $table->integer('province');
            $table->integer('country');
            $table->string('postalcode',5);
            $table->text('profile_img')->nullable();
            $table->text('description')->nullable();
            $table->integer('status')->default(0);
            $table->string('activation_code')->nullable();
            $table->string('rating')->nullable();
            $table->string('facebook')->default('https://wwww.facebook.com/');
            $table->string('twitter')->default('https://wwww.twitter.com/');
            $table->string('instagram')->default('https://wwww.instagram.com/');
            $table->string('googleplus')->default('https://plus.google.com/');
            $table->string('youtube')->default('https://wwww.youtube.com/user/');
            $table->integer('comment_id')->nullable();
            $table->integer('transaction_id')->nullable();
            // $table->string('avatar')->nullable();
            // $table->string('provider', 20)->nullable();
            // $table->string('provider_id')->nullable();
            // $table->string('access_token')->nullable();
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
