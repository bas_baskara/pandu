<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('category')->nullable();
            $table->text('description')->nullable();
            $table->string('tag')->nullable();
            $table->char('pageataupost',5)->nullable();
            $table->boolean('status')->nullable();
            $table->string('posted_by')->nullable();
            $table->string('image');
            $table->integer('visitor')->default(0);
            $table->string('metatitle')->nullable();
            $table->string('metakeyword')->nullable();
            $table->string('metadescription')->nullable();
            $table->boolean('ismember')->default(0);
            $table->boolean('isapprove')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
