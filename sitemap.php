

<?php
// PDO connect *********
function connect() {
return new PDO('mysql:host=localhost;dbname=panduasia', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}

$pdo = connect();

// posts *******************************
$sql = 'SELECT * FROM posts ORDER BY id DESC';
$query = $pdo->prepare($sql);
$query->execute();
$rs_post = $query->fetchAll();

// The XML structure
$data .= '<?xml version="1.0" encoding="UTF-8"?>';
$data .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
foreach ($rs_post as $row) {
$data .= '<url>';
$data .= '<loc>http://panduasia.com/'.$row['url'].'</loc>';
$data .= '<changefreq>weekly</changefreq>';
$data .= '</url>';
}
$data .= '</urlset>';

header('Content-Type: application/xml');
echo $data;
?>
